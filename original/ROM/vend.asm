;0006	slot number

;Rows are numbered 1-9, skipping 5.  There is no row 5.
;2800	home sensors
;	7  row 9
;	6  row 8
;	5  row 7
;	4  row 6
;	3  row 4
;	2  row 3
;	1  row 2
;	0  row 1
;Motor driver data is sent cols 7, 6, 5, 4, 3, 2, 1, 0 rows 9, 8, 7, 6, 4, 3, 2
;1.  Cols 8 and 9 are separate outputs (they ran out of driver chip outputs).
;Motor driver column output enable is PA6
;Motor driver serial clock is PA5
;It looks like motor overcurrent sense is on PE1 (PE1 will be low if a motor
;is drawing too much current)
;3000	misc outputs
;	7  motor driver serial data
;	6  col 9 motor driver
;	5  col 8 motor driver
;	4  display connector
;	3  display connector
;	2  not used
;	1  note acceptor connector
;	0  note acceptor connector
;3800	changer outputs
;	7  /ACCEPT
;	6  /SEND
;	5  RESET
;	4  /$.05
;	3  /$.10
;	2  /$.25
;	1  /$1.00
;	0  motor driver row output enable
;PA5 selects between RS232 and current loop serial interface?

; Current status:
;  JSR mapped out
;  JMP mapped out

; TODO:



; 0014 - ????
; 001a - ????
; 1000
; 1009
; 1026
; 1028
; 1030
; 103c

8000 43              C     coma 
8001 20 50            P    bra 50
8003 4F              O     clra 
8004 57              W     asrb 
8005 45              E     illegal 
8006 52              R     illegal 
8007 20 43            C    bra 43
8009 4F              O     clra 
800A 4D              M     tsta 
800B 50              P     negb 
800C 41              A     illegal 
800D 4E              N     illegal 
800E 59              Y     rolb 
800F 00              .     test 
8010 43              C     coma 
8011 4F              O     clra 
8012 50              P     negb 
8013 59              Y     rolb 
8014 52              R     illegal 
8015 49              I     rola 
8016 47              G     asra 
8017 48              H     asla 
8018 54              T     lsrb 
8019 20 31            1    bra 31
801B 39              9     rts 
801C 39              9     rts 
801D 33              3     pulb 
801E 20 00            .    bra 00
8020 20 20                 bra 20
8022 20 20                 bra 20
8024 20 20                 bra 20
8026 20 20                 bra 20
8028 20 20                 bra 20
802A 20 20                 bra 20
802C 20 20                 bra 20
802E 20 00            .    bra 00
8030 56              V     rorb 
8031 45              E     illegal 
8032 4E              N     illegal 
8033 44              D     lsra 
8034 49              I     rola 
8035 4E              N     illegal 
8036 47              G     asra 
8037 20 4D            M    bra 4d
8039 41              A     illegal 
803A 43              C     coma 
803B 48              H     asla 
803C 49              I     rola 
803D 4E              N     illegal 
803E 45              E     illegal 
803F 00              .     test 
8040 53              S     comb 
8041 4F              O     clra 
8042 46              F     rora 
8043 54              T     lsrb 
8044 57              W     asrb 
8045 41              A     illegal 
8046 52              R     illegal 
8047 45              E     illegal 
8048 20 57            W    bra 57
804A 52              R     illegal 
804B 4F              O     clra 
804C 54              T     lsrb 
804D 45              E     illegal 
804E 20 00            .    bra 00
8050 42              B     illegal 
8051 59              Y     rolb 
8052 20 42            B    bra 42
8054 52              R     illegal 
8055 45              E     illegal 
8056 54              T     lsrb 
8057 20 42            B    bra 42
8059 4F              O     clra 
805A 52              R     illegal 
805B 45              E     illegal 
805C 4E              N     illegal 
805D 2E 20           .     bgt 20
805F 00              .     test 
8060 20 20                 bra 20
8062 20 20                 bra 20
8064 20 20                 bra 20
8066 20 20                 bra 20
8068 20 20                 bra 20
806A 20 20                 bra 20
806C 20 20                 bra 20
806E 20 00            .    bra 00
8070 50              P     negb 
8071 52              R     illegal 
8072 4F              O     clra 
8073 47              G     asra 
8074 52              R     illegal 
8075 41              A     illegal 
8076 4D              M     tsta 
8077 20 46            F    bra 46
8079 4F              O     clra 
807A 52              R     illegal 
807B 20 54            T    bra 54
807D 48              H     asla 
807E 45              E     illegal 
807F 00              .     test 
8080 35              5     txs 
8081 30              0     tsx 
8082 35              5     txs 
8083 20 41            A    bra 41
8085 4E              N     illegal 
8086 44              D     lsra 
8087 20 35            5    bra 35
8089 39              9     rts 
808A 30              0     tsx 
808B 30              0     tsx 
808C 2E 20           .     bgt 20
808E 20 00            .    bra 00
8090 20 20                 bra 20
8092 20 20                 bra 20
8094 20 20                 bra 20
8096 20 20                 bra 20
8098 20 20                 bra 20
809A 20 20                 bra 20
809C 20 20                 bra 20
809E 20 00            .    bra 00
80A0 50              P     negb 
80A1 41              A     illegal 
80A2 52              R     illegal 
80A3 54              T     lsrb 
80A4 20 4E            N    bra 4e
80A6 55              U     illegal 
80A7 4D              M     tsta 
80A8 42              B     illegal 
80A9 45              E     illegal 
80AA 52              R     illegal 
80AB 3A              :     abx 
80AC 20 20                 bra 20
80AE 20 00            .    bra 00
80B0 39              9     rts 
80B1 33              3     pulb 
80B2 37              7     pshb 
80B3 2D 33           -3    blt 33
80B5 30              0     tsx 
80B6 30              0     tsx 
80B7 30              0     tsx 
80B8 31              1     ins 
80B9 20 56            V    bra 56
80BB 45              E     illegal 
80BC 52              R     illegal 
80BD 20 35            5    bra 35
80BF 00              .     test 
80C0 43              C     coma 
80C1 48              H     asla 
80C2 45              E     illegal 
80C3 43              C     coma 
80C4 4B              K     illegal 
80C5 53              S     comb 
80C6 55              U     illegal 
80C7 4D              M     tsta 
80C8 20 45            E    bra 45
80CA 4E              N     illegal 
80CB 44              D     lsra 
80CC 3A              :     abx 
80CD 30              0     tsx 
80CE 30              0     tsx 
80CF 00              .     test 
80D0 20 20                 bra 20
80D2 20 20                 bra 20
80D4 20 20                 bra 20
80D6 20 20                 bra 20
80D8 20 20                 bra 20
80DA 20 20                 bra 20
80DC 20 20                 bra 20
80DE 20 00            .    bra 00
80E0 20 20                 bra 20
80E2 20 20                 bra 20
80E4 20 20                 bra 20
80E6 20 20                 bra 20
80E8 20 20                 bra 20
80EA 20 20                 bra 20
80EC 20 20                 bra 20
80EE 20 00            .    bra 00
80F0 20 20                 bra 20
80F2 20 20                 bra 20
80F4 20 20                 bra 20
80F6 20 20                 bra 20
80F8 00              .     test 
80F9 20 0C            .    bra 0c
80FB 93 73 00        .s.   subd 73
80FE 01              .     nop 
80FF 05              .     asld 

jump72:
8100 F6 00 1D        ...   ldab 001d
8103 C4 10           ..    andb #10
8105 27 3D           '=    beq 3d
8107 BD D6 52        ..R   jsr d652		;jump5
810A CE 00 1D        ...   ldx #001d
810D 1D 00           ..    bclr add,x 00,x
810F 10              .     sba 
8110 CE 00 1D        ...   ldx #001d
8113 1C 00           ..    bset add,x 00,x
8115 80 7F           ..    suba #7f
8117 00              .     test 
8118 42              B     illegal 
8119 7F 00 60        ..`   clr 0060
811C 7F 00 48        ..H   clr 0048
811F 7F 00 38        ..8   clr 0038
8122 F6 0B A8        ...   ldab 0ba8
8125 27 08           '.    beq 08
8127 CE 0B A9        ...   ldx #0ba9
812A 1C 00           ..    bset add,x 00,x
812C 02              .     idiv 
812D 20 06            .    bra 06
812F CE 0B A9        ...   ldx #0ba9
8132 1D 00           ..    bclr add,x 00,x
8134 02              .     idiv 
8135 7F 0E 00        ...   clr 0e00
8138 7F 0E 01        ...   clr 0e01
813B 7F 0E 02        ...   clr 0e02
813E 7F 0E 03        ...   clr 0e03
8141 7F 0E 04        ...   clr 0e04
8144 F6 00 42        ..B   ldab 0042
8147 C1 0B           ..    cmpb #0b
8149 26 0F           &.    bne 0f
814B BD AB 89        ...   jsr ab89		;jump7
814E 7F 00 19        ...   clr 0019
8151 7F 00 60        ..`   clr 0060
8154 7F 00 42        ..B   clr 0042
8157 BD E6 1B        ...   jsr e61b		;jump4
815A F6 0B A8        ...   ldab 0ba8
815D 26 03           &.    bne 03
815F 7E 81 EF        ~..   jmp 81ef		;goto1
8162 F6 00 60        ..`   ldab 0060
8165 C1 04           ..    cmpb #04
8167 24 28           $(    bcc 28
8169 F6 00 42        ..B   ldab 0042
816C 27 21           '!    beq 21
816E F6 00 42        ..B   ldab 0042
8171 C1 0A           ..    cmpb #0a
8173 26 03           &.    bne 03
8175 7F 00 42        ..B   clr 0042
8178 F6 00 42        ..B   ldab 0042
817B CB 30           .0    addb #30
817D 37              7     pshb 
817E F6 00 60        ..`   ldab 0060
8181 4F              O     clra 
8182 C3 00 62        ..b   addd #0062
8185 8F              .     xgdx 
8186 33              3     pulb 
8187 E7 00           ..    stb 00,x
8189 7F 00 42        ..B   clr 0042
818C 7C 00 60        |.`   inc 0060
818F 20 5C            \    bra 5c
8191 F6 00 62        ..b   ldab 0062
8194 F1 0B A4        ...   cmpb 0ba4
8197 26 3D           &=    bne 3d
8199 F6 00 63        ..c   ldab 0063
819C F1 0B A5        ...   cmpb 0ba5
819F 26 35           &5    bne 35
81A1 F6 00 64        ..d   ldab 0064
81A4 F1 0B A6        ...   cmpb 0ba6
81A7 26 2D           &-    bne 2d
81A9 F6 00 65        ..e   ldab 0065
81AC F1 0B A7        ...   cmpb 0ba7
81AF 26 25           &%    bne 25
81B1 CE 0B A9        ...   ldx #0ba9
81B4 1D 00           ..    bclr add,x 00,x
81B6 02              .     idiv 
81B7 CE 00 1F        ...   ldx #001f
81BA 1C 00           ..    bset add,x 00,x
81BC 04              .     lsrd 
81BD CE 00 1F        ...   ldx #001f
81C0 1C 00           ..    bset add,x 00,x
81C2 80 CE           ..    suba #ce
81C4 00              .     test 
81C5 1D 1C           ..    bclr add,x 1c,x
81C7 00              .     test 
81C8 10              .     sba 
81C9 CC 02 33        ..3   ldd #0233
81CC FD 00 3B        ..;   stad 003b
81CF C6 05           ..    ldab #05
81D1 F7 00 38        ..8   stb 0038
81D4 20 17            .    bra 17
81D6 7F 00 42        ..B   clr 0042
81D9 7F 00 60        ..`   clr 0060
81DC BD BE 69        ..i   jsr be69		;jump3
81DF BD BE 4B        ..K   jsr be4b		;jump1
81E2 CC B0 81        ...   ldd #b081		;"NO  ACCESS"
81E5 BD C0 BA        ...   jsr c0ba		;display:
81E8 C6 0A           ..    ldab #0a
81EA F7 00 38        ..8   stb 0038
81ED 20 03            .    bra 03
goto1:
81EF 7F 00 42        ..B   clr 0042
81F2 F6 00 38        ..8   ldab 0038
81F5 27 03           '.    beq 03
81F7 7E 84 9F        ~..   jmp 849f		;goto2
81FA BD BE 4B        ..K   jsr be4b		;jump1
81FD F6 00 48        ..H   ldab 0048
8200 26 03           &.    bne 03
8202 BD AB A2        ...   jsr aba2		;jump8
8205 F6 00 48        ..H   ldab 0048
8208 4F              O     clra 
8209 BD FE 16        ...   jsr fe16		;jump2
820C 00              .     test 
820D 00              .     test 
820E 00              .     test 
820F 0D              .     sec 
8210 84 9F           ..    anda #9f
8212 82 2E           ..    sbca #2e
8214 82 50           .P    sbca #50
8216 82 B7           ..    sbca #b7
8218 82 D9           ..    sbca #d9
821A 83 40 83        .@.   subd #4083
821D 62              b     illegal 
821E 83 A5 83        ...   subd #a583
8221 C2 83           ..    sbcb #83
8223 DF 83 FC        ...   stx 83
8226 84 19           ..    anda #19
8228 84 35           .5    anda #35
822A 84 51           .Q    anda #51
822C 84 78           .x    anda #78
822E F6 00 19        ...   ldab 0019
8231 C4 01           ..    andb #01
8233 27 13           '.    beq 13
8235 CC AF F2        ...   ldd #aff2	;"OVER CRNT="
8238 BD C0 BA        ...   jsr c0ba		;display:
823B C6 0A           ..    ldab #0a
823D F7 00 38        ..8   stb 0038
8240 7F 00 54        ..T   clr 0054
8243 7C 00 48        |.H   inc 0048
8246 20 05            .    bra 05
8248 C6 02           ..    ldab #02
824A F7 00 48        ..H   stb 0048
824D 7E 84 9F        ~..   jmp 849f		;goto2
8250 F6 00 54        ..T   ldab 0054
8253 4F              O     clra 
8254 CE 00 05        ...   ldx #0005
8257 02              .     idiv 
8258 5D              ]     tstb 
8259 27 4C           'L    beq 4c
825B F6 00 54        ..T   ldab 0054
825E F7 00 06        ...   stb 0006
8261 F6 00 06        ...   ldab 0006
8264 BD D7 A0        ...   jsr d7a0		;jump10
8267 83 00 00        ...   subd #0000
826A 27 15           '.    beq 15
826C CC 7F FF        ...   ldd #7fff
826F 37              7     pshb 
8270 36              6     psha 
8271 F6 00 06        ...   ldab 0006
8274 4F              O     clra 
8275 05              .     asld 
8276 C3 08 00        ...   addd #0800
8279 38              8     pulx 
827A 8F              .     xgdx 
827B A4 00           ..    anda 00,x
827D E4 01           ..    andb 01,x
827F ED 00           ..    stad 00,x
8281 F6 00 54        ..T   ldab 0054
8284 4F              O     clra 
8285 05              .     asld 
8286 C3 08 00        ...   addd #0800
8289 8F              .     xgdx 
828A EC 00           ..    ldd 00,x
828C 84 80           ..    anda #80
828E 27 17           '.    beq 17
8290 F6 00 54        ..T   ldab 0054
8293 4F              O     clra 
8294 05              .     asld 
8295 C3 08 C8        ...   addd #08c8
8298 8F              .     xgdx 
8299 EC 00           ..    ldd 00,x
829B 84 02           ..    anda #02
829D 27 08           '.    beq 08
829F BD 98 A5        ...   jsr 98a5		;jump11
82A2 C6 0A           ..    ldab #0a
82A4 F7 00 38        ..8   stb 0038
82A7 7C 00 54        |.T   inc 0054
82AA F6 00 54        ..T   ldab 0054
82AD C1 64           .d    cmpb #64
82AF 26 03           &.    bne 03
82B1 7C 00 48        |.H   inc 0048
82B4 7E 84 9F        ~..   jmp 849f		;goto2
82B7 F6 00 19        ...   ldab 0019
82BA C4 02           ..    andb #02
82BC 27 13           '.    beq 13
82BE CC AF FD        ...   ldd #affd	"HOME FAIL="
82C1 BD C0 BA        ...   jsr c0ba		;display:
82C4 C6 0A           ..    ldab #0a
82C6 F7 00 38        ..8   stb 0038
82C9 7F 00 54        ..T   clr 0054
82CC 7C 00 48        |.H   inc 0048
82CF 20 05            .    bra 05
82D1 C6 04           ..    ldab #04
82D3 F7 00 48        ..H   stb 0048
82D6 7E 84 9F        ~..   jmp 849f		;goto2
82D9 F6 00 54        ..T   ldab 0054
82DC 4F              O     clra 
82DD CE 00 05        ...   ldx #0005
82E0 02              .     idiv 
82E1 5D              ]     tstb 
82E2 27 4C           'L    beq 4c
82E4 F6 00 54        ..T   ldab 0054
82E7 F7 00 06        ...   stb 0006
82EA F6 00 06        ...   ldab 0006
82ED BD D7 A0        ...   jsr d7a0		;jump10
82F0 83 00 00        ...   subd #0000
82F3 27 15           '.    beq 15
82F5 CC 7F FF        ...   ldd #7fff
82F8 37              7     pshb 
82F9 36              6     psha 
82FA F6 00 06        ...   ldab 0006
82FD 4F              O     clra 
82FE 05              .     asld 
82FF C3 08 00        ...   addd #0800
8302 38              8     pulx 
8303 8F              .     xgdx 
8304 A4 00           ..    anda 00,x
8306 E4 01           ..    andb 01,x
8308 ED 00           ..    stad 00,x
830A F6 00 54        ..T   ldab 0054
830D 4F              O     clra 
830E 05              .     asld 
830F C3 08 00        ...   addd #0800
8312 8F              .     xgdx 
8313 EC 00           ..    ldd 00,x
8315 84 80           ..    anda #80
8317 27 17           '.    beq 17
8319 F6 00 54        ..T   ldab 0054
831C 4F              O     clra 
831D 05              .     asld 
831E C3 08 C8        ...   addd #08c8
8321 8F              .     xgdx 
8322 EC 00           ..    ldd 00,x
8324 84 04           ..    anda #04
8326 27 08           '.    beq 08
8328 BD 98 A5        ...   jsr 98a5		;jump11
832B C6 0A           ..    ldab #0a
832D F7 00 38        ..8   stb 0038
8330 7C 00 54        |.T   inc 0054
8333 F6 00 54        ..T   ldab 0054
8336 C1 64           .d    cmpb #64
8338 26 03           &.    bne 03
833A 7C 00 48        |.H   inc 0048
833D 7E 84 9F        ~..   jmp 849f		;goto2
8340 F6 00 19        ...   ldab 0019
8343 C4 04           ..    andb #04
8345 27 13           '.    beq 13
8347 CC B0 08        ...   ldd #b008
834A BD C0 BA        ...   jsr c0ba		;display:
834D C6 0A           ..    ldab #0a
834F F7 00 38        ..8   stb 0038
8352 7F 00 54        ..T   clr 0054
8355 7C 00 48        |.H   inc 0048
8358 20 05            .    bra 05
835A C6 06           ..    ldab #06
835C F7 00 48        ..H   stb 0048
835F 7E 84 9F        ~..   jmp 849f		;goto2
8362 F6 00 54        ..T   ldab 0054
8365 4F              O     clra 
8366 CE 00 05        ...   ldx #0005
8369 02              .     idiv 
836A 5D              ]     tstb 
836B 27 28           '(    beq 28
836D F6 00 54        ..T   ldab 0054
8370 F7 00 06        ...   stb 0006
8373 F6 00 06        ...   ldab 0006
8376 BD D7 A0        ...   jsr d7a0		;jump10
8379 83 00 00        ...   subd #0000
837C 26 17           &.    bne 17
837E F6 00 54        ..T   ldab 0054
8381 4F              O     clra 
8382 05              .     asld 
8383 C3 08 C8        ...   addd #08c8
8386 8F              .     xgdx 
8387 EC 00           ..    ldd 00,x
8389 84 01           ..    anda #01
838B 27 08           '.    beq 08
838D BD 98 A5        ...   jsr 98a5		;jump11
8390 C6 0A           ..    ldab #0a
8392 F7 00 38        ..8   stb 0038
8395 7C 00 54        |.T   inc 0054
8398 F6 00 54        ..T   ldab 0054
839B C1 64           .d    cmpb #64
839D 26 03           &.    bne 03
839F 7C 00 48        |.H   inc 0048
83A2 7E 84 9F        ~..   jmp 849f		;goto2
83A5 F6 0B C1        ...   ldab 0bc1
83A8 C4 01           ..    andb #01
83AA 27 10           '.    beq 10
83AC CC B0 34        ..4   ldd #b034	"CHGR PWRUP"
83AF BD C0 BA        ...   jsr c0ba		;display:
83B2 7C 00 48        |.H   inc 0048
83B5 C6 0A           ..    ldab #0a
83B7 F7 00 38        ..8   stb 0038
83BA 20 03            .    bra 03
83BC 7C 00 48        |.H   inc 0048
83BF 7E 84 9F        ~..   jmp 849f		;goto2
83C2 F6 0B C1        ...   ldab 0bc1
83C5 C4 02           ..    andb #02
83C7 27 10           '.    beq 10
83C9 CC B0 4A        ..J   ldd #b04a	"CARD PWRUP"
83CC BD C0 BA        ...   jsr c0ba		;display:
83CF 7C 00 48        |.H   inc 0048
83D2 C6 0A           ..    ldab #0a
83D4 F7 00 38        ..8   stb 0038
83D7 20 03            .    bra 03
83D9 7C 00 48        |.H   inc 0048
83DC 7E 84 9F        ~..   jmp 849f		;goto2
83DF F6 0B C1        ...   ldab 0bc1
83E2 C4 04           ..    andb #04
83E4 27 10           '.    beq 10
83E6 CC B0 3F        ..?   ldd #b03f	"LINK PWRUP"
83E9 BD C0 BA        ...   jsr c0ba		;display:
83EC 7C 00 48        |.H   inc 0048
83EF C6 0A           ..    ldab #0a
83F1 F7 00 38        ..8   stb 0038
83F4 20 03            .    bra 03
83F6 7C 00 48        |.H   inc 0048
83F9 7E 84 9F        ~..   jmp 849f		;goto2
83FC F6 0B C1        ...   ldab 0bc1
83FF C4 10           ..    andb #10
8401 27 10           '.    beq 10
8403 CC B0 1E        ...   ldd #b01e	"COIN   JAM"
8406 BD C0 BA        ...   jsr c0ba		;display:
8409 7C 00 48        |.H   inc 0048
840C C6 0A           ..    ldab #0a
840E F7 00 38        ..8   stb 0038
8411 20 03            .    bra 03
8413 7C 00 48        |.H   inc 0048
8416 7E 84 9F        ~..   jmp 849f		;goto2
8419 F6 0B C1        ...   ldab 0bc1
841C C4 20           .     andb #20
841E 27 10           '.    beq 10
8420 CC B0 29        ..)   ldd #b029	"BAD SENSOR"
8423 BD C0 BA        ...   jsr c0ba		;display:
8426 7C 00 48        |.H   inc 0048
8429 C6 0A           ..    ldab #0a
842B F7 00 38        ..8   stb 0038
842E 20 03            .    bra 03
8430 7C 00 48        |.H   inc 0048
8433 20 6A            j    bra 6a
8435 F6 0B C1        ...   ldab 0bc1
8438 C4 08           ..    andb #08
843A 27 10           '.    beq 10
843C CC B0 55        ..U   ldd #b055	"BILL ERROR"
843F BD C0 BA        ...   jsr c0ba		;display:
8442 7C 00 48        |.H   inc 0048
8445 C6 0A           ..    ldab #0a
8447 F7 00 38        ..8   stb 0038
844A 20 03            .    bra 03
844C 7C 00 48        |.H   inc 0048
844F 20 4E            N    bra 4e
8451 F6 0B C1        ...   ldab 0bc1
8454 C4 40           .@    andb #40
8456 27 10           '.    beq 10
8458 CC B0 60        ..`   ldd #b060	"TUBE ERROR"
845B BD C0 BA        ...   jsr c0ba		;display:
845E 7F 00 48        ..H   clr 0048
8461 C6 0A           ..    ldab #0a
8463 F7 00 38        ..8   stb 0038
8466 20 0E            .    bra 0e
8468 7F 00 48        ..H   clr 0048
846B CC B0 13        ...   ldd #b013	"HIT RESET "
846E BD C0 BA        ...   jsr c0ba		 ;display:
8471 C6 0A           ..    ldab #0a
8473 F7 00 38        ..8   stb 0038
8476 20 27            '    bra 27
8478 F6 00 1D        ...   ldab 001d
847B 2C 0E           ,.    bge 0e
847D CC AE A8        ...   ldd #aea8	"  SYSTEM  "
8480 BD C0 BA        ...   jsr c0ba		;display:
8483 CE 00 1D        ...   ldx #001d
8486 1D 00           ..    bclr add,x 00,x
8488 80 20           .     suba #20
848A 0C              .     clc 
848B CC AE B3        ...   ldd #aeb3	"    OK    "
848E BD C0 BA        ...   jsr c0ba		;display:
8491 CE 00 1D        ...   ldx #001d
8494 1C 00           ..    bset add,x 00,x
8496 80 C6           ..    suba #c6
8498 0F              .     sei 
8499 F7 00 38        ..8   stb 0038
849C 7F 00 48        ..H   clr 0048
goto2:
849F 39              9     rts 

jump73:
84A0 FC 00 3B        ..;   ldd 003b
84A3 26 03           &.    bne 03
84A5 7E 85 96        ~..   jmp 8596		;goto3
84A8 F6 00 1F        ...   ldab 001f
84AB 2C 40           ,@    bge 40
84AD BD D6 52        ..R   jsr d652		;jump5
84B0 7F 00 42        ..B   clr 0042
84B3 7F 00 09        ...   clr 0009
84B6 7F 00 0A        ...   clr 000a
84B9 7F 00 06        ...   clr 0006
84BC 5F              _     clrb 
84BD 4F              O     clra 
84BE FD 00 40        ..@   stad 0040
84C1 5F              _     clrb 
84C2 4F              O     clra 
84C3 FD 00 3E        ..>   stad 003e
84C6 BD E5 AA        ...   jsr e5aa		;jump12
84C9 CE 00 1F        ...   ldx #001f
84CC 1D 00           ..    bclr add,x 00,x
84CE 80 CE           ..    suba #ce
84D0 00              .     test 
84D1 1F 1C 00 02     ....  brclr 1c,x 00 02
84D5 CE 00 1D        ...   ldx #001d
84D8 1C 00           ..    bset add,x 00,x
84DA 10              .     sba 
84DB CE 00 17        ...   ldx #0017
84DE 1D 00           ..    bclr add,x 00,x
84E0 20 CE            .    bra ce
84E2 00              .     test 
84E3 17              .     tba 
84E4 1D 00           ..    bclr add,x 00,x
84E6 40              @     nega 
84E7 CE 00 17        ...   ldx #0017
84EA 1D 00           ..    bclr add,x 00,x
84EC 80 F6           ..    suba #f6
84EE 0B              .     sev 
84EF A9 C4           ..    adca c4,x
84F1 02              .     idiv 
84F2 27 20           '     beq 20
84F4 F6 00 42        ..B   ldab 0042
84F7 4F              O     clra 
84F8 C3 0B E4        ...   addd #0be4
84FB 8F              .     xgdx 
84FC E6 00           ..    ldab 00,x
84FE 27 14           '.    beq 14
8500 BD BE 4B        ..K   jsr be4b		;jump1
8503 CC B0 81        ...   ldd #b081		;"NO  ACCESS"
8506 BD C0 BA        ...   jsr c0ba		;display:
8509 CC 02 58        ..X   ldd #0258
850C BD E5 71        ..q   jsr e571		;jump13
850F BD 85 AF        ...   jsr 85af		;jump14
8512 20 6C            l    bra 6c
8514 F6 00 42        ..B   ldab 0042
8517 4F              O     clra 
8518 BD FE 16        ...   jsr fe16		;jump2
851B 00              .     test 
851C 00              .     test 
851D 00              .     test 
851E 0B              .     sev 
851F 85 80           ..    bita #80
8521 85 39           .9    bita #39
8523 85 4A           .J    bita #4a
8525 85 4F           .O    bita #4f
8527 85 54           .T    bita #54
8529 85 59           .Y    bita #59
852B 85 5E           .^    bita #5e
852D 85 63           .c    bita #63
852F 85 68           .h    bita #68
8531 85 6D           .m    bita #6d
8533 85 72           .r    bita #72
8535 85 77           .w    bita #77
8537 85 7C           .|    bita #7c
8539 CC AE 92        ...   ldd #ae92	"-         "
853C BD C0 BA        ...   jsr c0ba		;display:
853F 7F 00 42        ..B   clr 0042
8542 CE 00 1F        ...   ldx #001f
8545 1C 00           ..    bset add,x 00,x
8547 02              .     idiv 
8548 20 36            6    bra 36
854A BD 86 84        ...   jsr 8684		;jump15
854D 20 31            1    bra 31
854F BD 88 2E        ...   jsr 882e		;jump16
8552 20 2C            ,    bra 2c
8554 BD 88 B8        ...   jsr 88b8		;jump17
8557 20 27            '    bra 27
8559 BD 8C B4        ...   jsr 8cb4		;jump18
855C 20 22            "    bra 22
855E BD 8E 35        ..5   jsr 8e35		;jump19
8561 20 1D            .    bra 1d
8563 BD 8F 0D        ...   jsr 8f0d		;jump20
8566 20 18            .    bra 18
8568 BD 90 D1        ...   jsr 90d1		;jump21
856B 20 13            .    bra 13
856D BD 92 EE        ...   jsr 92ee		;jump22
8570 20 0E            .    bra 0e
8572 BD 93 A5        ...   jsr 93a5		;jump23
8575 20 09            .    bra 09
8577 BD 94 DD        ...   jsr 94dd		;jump24
857A 20 04            .    bra 04
857C 8D 31           .1    bsr dest 31
857E 20 00            .    bra 00
8580 7F 00 0A        ...   clr 000a
8583 F6 00 42        ..B   ldab 0042
8586 C1 0A           ..    cmpb #0a
8588 27 0A           '.    beq 0a
858A F6 00 42        ..B   ldab 0042
858D C1 07           ..    cmpb #07
858F 27 03           '.    beq 03
8591 7F 00 09        ...   clr 0009
8594 20 18            .    bra 18
goto3:
8596 CE 00 1F        ...   ldx #001f
8599 1D 00           ..    bclr add,x 00,x
859B 04              .     lsrd 
859C CE 00 1F        ...   ldx #001f
859F 1C 00           ..    bset add,x 00,x
85A1 80 CE           ..    suba #ce
85A3 00              .     test 
85A4 1D 1C           ..    bclr add,x 1c,x
85A6 00              .     test 
85A7 10              .     sba 
85A8 BD BE 69        ..i   jsr be69		;jump3
85AB BD E5 AA        ...   jsr e5aa		;jump12
85AE 39              9     rts 

jump14:
85AF 7F 00 42        ..B   clr 0042
85B2 7F 00 06        ...   clr 0006
85B5 CE 00 1F        ...   ldx #001f
85B8 1C 00           ..    bset add,x 00,x
85BA 02              .     idiv 
85BB BD BE 69        ..i   jsr be69		;jump3
85BE 39              9     rts 

jump28:
85BF F6 00 12        ...   ldab 0012
85C2 C4 1E           ..    andb #1e
85C4 4F              O     clra 
85C5 BD FE 32        ..2   jsr fe32		;jump25
85C8 85 DE           ..    bita #de
85CA 00              .     test 
85CB 0E              .     cli 
85CC 86 08           ..    ldaa #08
85CE 00              .     test 
85CF 08              .     inx 
85D0 86 32           .2    ldaa #32
85D2 00              .     test 
85D3 04              .     lsrd 
85D4 86 5C           .\    ldaa #5c
85D6 00              .     test 
85D7 02              .     idiv 
85D8 86 83           ..    ldaa #83
85DA FF E4 00        ...   stx e400
85DD 00              .     test 
85DE F6 0B DC        ...   ldab 0bdc
85E1 F7 00 01        ...   stb 0001
85E4 BD C4 30        ..0   jsr c430		;jump26
85E7 C6 01           ..    ldab #01
85E9 F7 00 48        ..H   stb 0048
85EC 7F 00 2C        ..,   clr 002c
85EF F6 0C D9        ...   ldab 0cd9
85F2 27 03           '.    beq 03
85F4 7A 0C D9        z..   dec 0cd9
85F7 BD 97 F6        ...   jsr 97f6		;jump27
85FA CC 01 5E        ..^   ldd #015e
85FD BD E5 71        ..q   jsr e571		;jump13
8600 CC 02 33        ..3   ldd #0233
8603 FD 00 3B        ..;   stad 003b
8606 20 7B            {    bra 7b
8608 F6 0B DB        ...   ldab 0bdb
860B F7 00 01        ...   stb 0001
860E BD C4 30        ..0   jsr c430		;jump26
8611 C6 02           ..    ldab #02
8613 F7 00 48        ..H   stb 0048
8616 7F 00 2C        ..,   clr 002c
8619 F6 0C DA        ...   ldab 0cda
861C 27 03           '.    beq 03
861E 7A 0C DA        z..   dec 0cda
8621 BD 97 F6        ...   jsr 97f6		;jump27
8624 CC 01 5E        ..^   ldd #015e
8627 BD E5 71        ..q   jsr e571		;jump13
862A CC 02 33        ..3   ldd #0233
862D FD 00 3B        ..;   stad 003b
8630 20 51            Q    bra 51
8632 F6 0B DA        ...   ldab 0bda
8635 F7 00 01        ...   stb 0001
8638 BD C4 30        ..0   jsr c430		;jump26
863B C6 03           ..    ldab #03
863D F7 00 48        ..H   stb 0048
8640 7F 00 2C        ..,   clr 002c
8643 F6 0C DB        ...   ldab 0cdb
8646 27 03           '.    beq 03
8648 7A 0C DB        z..   dec 0cdb
864B BD 97 F6        ...   jsr 97f6		;jump27
864E CC 01 5E        ..^   ldd #015e
8651 BD E5 71        ..q   jsr e571		;jump13
8654 CC 02 33        ..3   ldd #0233
8657 FD 00 3B        ..;   stad 003b
865A 20 27            '    bra 27
865C C6 40           .@    ldab #40
865E F7 00 01        ...   stb 0001
8661 BD C4 30        ..0   jsr c430		;jump26
8664 C6 04           ..    ldab #04
8666 F7 00 48        ..H   stb 0048
8669 7F 00 2C        ..,   clr 002c
866C F6 0C DC        ...   ldab 0cdc
866F 27 03           '.    beq 03
8671 7A 0C DC        z..   dec 0cdc
8674 BD 97 F6        ...   jsr 97f6		;jump27
8677 CC 01 5E        ..^   ldd #015e
867A BD E5 71        ..q   jsr e571		;jump13
867D CC 02 33        ..3   ldd #0233
8680 FD 00 3B        ..;   stad 003b
8683 39              9     rts 

jump15:
8684 F6 00 1F        ...   ldab 001f
8687 C4 02           ..    andb #02
8689 27 2B           '+    beq 2b
868B CE 00 1F        ...   ldx #001f
868E 1D 00           ..    bclr add,x 00,x
8690 02              .     idiv 
8691 C6 01           ..    ldab #01
8693 F7 00 48        ..H   stb 0048
8696 F6 0B C0        ...   ldab 0bc0
8699 C4 01           ..    andb #01
869B 27 16           '.    beq 16
869D C6 27           .'    ldab #27
869F F7 10 2D        ..-   stb 102d
86A2 C6 07           ..    ldab #07
86A4 F7 10 2B        ..+   stb 102b
86A7 CE 00 1A        ...   ldx #001a
86AA 1D 00           ..    bclr add,x 00,x
86AC 01              .     nop 
86AD F6 00 1A        ...   ldab 001a
86B0 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
86B3 7E 87 E8        ~..   jmp 87e8		;goto4
86B6 F6 00 09        ...   ldab 0009
86B9 4F              O     clra 
86BA BD FE 32        ..2   jsr fe32		;jump25
86BD 86 D7           ..    ldaa #d7
86BF 00              .     test 
86C0 00              .     test 
86C1 87              .     illegal 
86C2 39              9     rts 

86C3 00              .     test 
86C4 01              .     nop 
86C5 87              .     illegal 
86C6 75              u     illegal 
86C7 00              .     test 
86C8 01              .     nop 
86C9 87              .     illegal 
86CA C5 00           ..    bitb #00
86CC 08              .     inx 
86CD 87              .     illegal 
86CE D9 00 01        ...   adcb 00
86D1 87              .     illegal 
86D2 E8 FF           ..    eorb ff,x
86D4 F5 00 00        ...   bitb 0000
86D7 C6 FF           ..    ldab #ff
86D9 F7 0B BC        ...   stb 0bbc
86DC F6 18 00        ...   ldab 1800
86DF 2D 29           -)    blt 29
86E1 F6 00 07        ...   ldab 0007
86E4 26 24           &$    bne 24
86E6 CE 00 1A        ...   ldx #001a
86E9 1D 00           ..    bclr add,x 00,x
86EB 02              .     idiv 
86EC F6 00 1A        ...   ldab 001a
86EF F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
86F2 CC 00 32        ..2   ldd #0032
86F5 BD E5 71        ..q   jsr e571		;jump13
86F8 CE 00 1A        ...   ldx #001a
86FB 1C 00           ..    bset add,x 00,x
86FD 02              .     idiv 
86FE F6 00 1A        ...   ldab 001a
8701 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
8704 CC 02 33        ..3   ldd #0233
8707 FD 00 3B        ..;   stad 003b
870A 5F              _     clrb 
870B 4F              O     clra 
870C FD 00 40        ..@   stad 0040
870F 5F              _     clrb 
8710 4F              O     clra 
8711 FD 00 3E        ..>   stad 003e
8714 F6 0B C0        ...   ldab 0bc0
8717 C4 01           ..    andb #01
8719 27 03           '.    beq 03
871B BD 85 BF        ...   jsr 85bf		;jump28
871E BD DC A3        ...   jsr dca3		;jump29
8721 BD 97 F6        ...   jsr 97f6		;jump27
8724 CE 00 17        ...   ldx #0017
8727 1D 00           ..    bclr add,x 00,x
8729 20 CE            .    bra ce
872B 00              .     test 
872C 17              .     tba 
872D 1D 00           ..    bclr add,x 00,x
872F 40              @     nega 
8730 CE 00 17        ...   ldx #0017
8733 1D 00           ..    bclr add,x 00,x
8735 80 7E           .~    suba #7e
8737 87              .     illegal 
8738 E8 F6           ..    eorb f6,x
873A 00              .     test 
873B 36              6     psha 
873C 26 2F           &/    bne 2f
873E F6 00 48        ..H   ldab 0048
8741 4F              O     clra 
8742 BD FE 16        ...   jsr fe16		;jump2
8745 00              .     test 
8746 01              .     nop 
8747 00              .     test 
8748 03              .     fdiv 
8749 87              .     illegal 
874A 65              e     illegal 
874B 87              .     illegal 
874C 53              S     comb 
874D 87              .     illegal 
874E 58              X     aslb 
874F 87              .     illegal 
8750 5D              ]     tstb 
8751 87              .     illegal 
8752 62              b     illegal 
8753 7C 0C D9        |..   inc 0cd9
8756 20 0D            .    bra 0d
8758 7C 0C DA        |..   inc 0cda
875B 20 08            .    bra 08
875D 7C 0C DB        |..   inc 0cdb
8760 20 03            .    bra 03
8762 7C 0C DC        |..   inc 0cdc
8765 BD 97 F6        ...   jsr 97f6		;jump27
8768 C6 02           ..    ldab #02
876A F7 00 36        ..6   stb 0036
876D CE 00 1B        ...   ldx #001b
8770 1C 00           ..    bset add,x 00,x
8772 10              .     sba 
8773 20 73            s    bra 73
8775 F6 00 36        ..6   ldab 0036
8778 26 43           &C    bne 43
877A F6 00 48        ..H   ldab 0048
877D 4F              O     clra 
877E BD FE 16        ...   jsr fe16		;jump2
8781 00              .     test 
8782 01              .     nop 
8783 00              .     test 
8784 03              .     fdiv 
8785 87              .     illegal 
8786 B5 87 8F        ...   bita 878f
8789 87              .     illegal 
878A 99 87 A3        ...   adca 87
878D 87              .     illegal 
878E AD F6           ..    jsr f6,x
8790 0C              .     clc 
8791 D9 27 03        .'.   adcb 27
8794 7A 0C D9        z..   dec 0cd9
8797 20 1C            .    bra 1c
8799 F6 0C DA        ...   ldab 0cda
879C 27 03           '.    beq 03
879E 7A 0C DA        z..   dec 0cda
87A1 20 12            .    bra 12
87A3 F6 0C DB        ...   ldab 0cdb
87A6 27 03           '.    beq 03
87A8 7A 0C DB        z..   dec 0cdb
87AB 20 08            .    bra 08
87AD F6 0C DC        ...   ldab 0cdc
87B0 27 03           '.    beq 03
87B2 7A 0C DC        z..   dec 0cdc
87B5 BD 97 F6        ...   jsr 97f6		;jump27
87B8 C6 02           ..    ldab #02
87BA F7 00 36        ..6   stb 0036
87BD CE 00 1B        ...   ldx #001b
87C0 1C 00           ..    bset add,x 00,x
87C2 10              .     sba 
87C3 20 23            #    bra 23
87C5 7C 00 48        |.H   inc 0048
87C8 F6 00 48        ..H   ldab 0048
87CB C1 04           ..    cmpb #04
87CD 23 05           #.    bls 05
87CF C6 01           ..    ldab #01
87D1 F7 00 48        ..H   stb 0048
87D4 BD 97 F6        ...   jsr 97f6		;jump27
87D7 20 0F            .    bra 0f
87D9 CE 00 1A        ...   ldx #001a
87DC 1C 00           ..    bset add,x 00,x
87DE 01              .     nop 
87DF F6 00 1A        ...   ldab 001a
87E2 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
87E5 BD 85 AF        ...   jsr 85af		;jump14
goto4:
87E8 39              9     rts 

jump74:
87E9 7F 00 05        ...   clr 0005
87EC 7F 00 54        ..T   clr 0054
87EF F6 00 54        ..T   ldab 0054
87F2 C1 50           .P    cmpb #50
87F4 24 2E           $.    bcc 2e
87F6 F6 00 54        ..T   ldab 0054
87F9 4F              O     clra 
87FA C3 B0 D3        ...   addd #b0d3
87FD 8F              .     xgdx 
87FE E6 00           ..    ldab 00,x
8800 F7 00 06        ...   stb 0006
8803 F6 00 06        ...   ldab 0006
8806 BD D7 A0        ...   jsr d7a0		;jump10
8809 83 00 00        ...   subd #0000
880C 26 11           &.    bne 11
880E BD D3 11        ...   jsr d311		;jump30
8811 83 00 00        ...   subd #0000
8814 26 03           &.    bne 03
8816 7C 00 05        |..   inc 0005
8819 CC 00 05        ...   ldd #0005
881C BD E5 71        ..q   jsr e571		;jump13
881F 7C 00 54        |.T   inc 0054
8822 20 CB            .    bra cb
8824 BD 99 81        ...   jsr 9981		;jump144
8827 CC 07 D0        ...   ldd #07d0
882A BD E5 71        ..q   jsr e571		;jump13
882D 39              9     rts 

jump16:
882E F6 00 1F        ...   ldab 001f
8831 C4 02           ..    andb #02
8833 27 12           '.    beq 12
8835 FC 0B B5        ...   ldd 0bb5
8838 C4 FF           ..    andb #ff
883A 84 01           ..    anda #01
883C FD 00 52        ..R   stad 0052
883F CE 00 1F        ...   ldx #001f
8842 1D 00           ..    bclr add,x 00,x
8844 02              .     idiv 
8845 20 70            p    bra 70
8847 F6 00 09        ...   ldab 0009
884A 4F              O     clra 
884B BD FE 32        ..2   jsr fe32		;jump25
884E 88 68           .h    eora #68
8850 00              .     test 
8851 00              .     test 
8852 88 6D           .m    eora #6d
8854 00              .     test 
8855 01              .     nop 
8856 88 83           ..    eora #83
8858 00              .     test 
8859 01              .     nop 
885A 88 A7           ..    eora #a7
885C 00              .     test 
885D 08              .     inx 
885E 88 B4           ..    eora #b4
8860 00              .     test 
8861 01              .     nop 
8862 88 B7           ..    eora #b7
8864 FF F5 00        ...   stx f500
8867 00              .     test 
8868 BD 9A 05        ...   jsr 9a05		;jump102
886B 20 4A            J    bra 4a
886D FC 00 52        ..R   ldd 0052
8870 83 01 F4        ...   subd #01f4
8873 24 09           $.    bcc 09
8875 FC 00 52        ..R   ldd 0052
8878 C3 00 32        ..2   addd #0032
887B FD 00 52        ..R   stad 0052
887E BD 9A 05        ...   jsr 9a05		;jump102
8881 20 34            4    bra 34
8883 FC 00 52        ..R   ldd 0052
8886 83 00 32        ..2   subd #0032
8889 23 17           #.    bls 17
888B FC 00 52        ..R   ldd 0052
888E C3 FF CE        ...   addd #ffce
8891 FD 00 52        ..R   stad 0052
8894 FC 00 52        ..R   ldd 0052
8897 83 00 32        ..2   subd #0032
889A 24 06           $.    bcc 06
889C CC 00 32        ..2   ldd #0032
889F FD 00 52        ..R   stad 0052
88A2 BD 9A 05        ...   jsr 9a05		;jump102
88A5 20 10            .    bra 10
88A7 BD A3 7C        ..|   jsr a37c		;jump141
88AA 5F              _     clrb 
88AB 4F              O     clra 
88AC FD 0B B9        ...   stad 0bb9
88AF BD 85 AF        ...   jsr 85af		;jump14
88B2 20 03            .    bra 03
88B4 BD 85 AF        ...   jsr 85af		;jump14
88B7 39              9     rts 

jump17:
88B8 F6 00 1F        ...   ldab 001f
88BB C4 02           ..    andb #02
88BD 26 0A           &.    bne 0a
88BF F6 00 3A        ..:   ldab 003a
88C2 26 28           &(    bne 28
88C4 F6 00 1D        ...   ldab 001d
88C7 2D 23           -#    blt 23
88C9 7F 00 2C        ..,   clr 002c
88CC CC AE 9D        ...   ldd #ae9d	"SET PRICE?"
88CF BD C0 BA        ...   jsr c0ba		;display:
88D2 CE 00 1F        ...   ldx #001f
88D5 1D 00           ..    bclr add,x 00,x
88D7 02              .     idiv 
88D8 CE 00 1F        ...   ldx #001f
88DB 1D 00           ..    bclr add,x 00,x
88DD 40              @     nega 
88DE CE 00 1D        ...   ldx #001d
88E1 1D 00           ..    bclr add,x 00,x
88E3 80 7F           ..    suba #7f
88E5 00              .     test 
88E6 54              T     lsrb 
88E7 C6 32           .2    ldab #32
88E9 F7 00 3A        ..:   stb 003a
88EC F6 00 09        ...   ldab 0009
88EF 26 03           &.    bne 03
88F1 7E 89 E1        ~..   jmp 89e1		;goto5
88F4 F6 00 1D        ...   ldab 001d
88F7 2C 03           ,.    bge 03
88F9 7E 89 E1        ~..   jmp 89e1		;goto5
88FC F6 00 09        ...   ldab 0009
88FF C1 0B           ..    cmpb #0b
8901 26 06           &.    bne 06
8903 BD 85 AF        ...   jsr 85af		;jump14
8906 7E 89 DE        ~..   jmp 89de		;goto6
8909 F6 00 54        ..T   ldab 0054
890C 26 26           &&    bne 26
890E BD BE 4B        ..K   jsr be4b		;jump1
8911 F6 00 09        ...   ldab 0009
8914 C1 0A           ..    cmpb #0a
8916 26 03           &.    bne 03
8918 7F 00 09        ...   clr 0009
891B F6 00 09        ...   ldab 0009
891E CB 30           .0    addb #30
8920 37              7     pshb 
8921 CC 0B 95        ...   ldd #0b95
8924 F0 00 54        ..T   subb 0054
8927 82 00           ..    sbca #00
8929 8F              .     xgdx 
892A 33              3     pulb 
892B E7 00           ..    stb 00,x
892D C6 32           .2    ldab #32
892F F7 00 3A        ..:   stb 003a
8932 20 37            7    bra 37
8934 F6 00 54        ..T   ldab 0054
8937 C1 01           ..    cmpb #01
8939 26 27           &'    bne 27
893B F6 00 09        ...   ldab 0009
893E C1 05           ..    cmpb #05
8940 27 20           '     beq 20
8942 F6 00 09        ...   ldab 0009
8945 C1 0A           ..    cmpb #0a
8947 27 19           '.    beq 19
8949 F6 00 09        ...   ldab 0009
894C CB 30           .0    addb #30
894E 37              7     pshb 
894F CC 0B 95        ...   ldd #0b95
8952 F0 00 54        ..T   subb 0054
8955 82 00           ..    sbca #00
8957 8F              .     xgdx 
8958 33              3     pulb 
8959 E7 00           ..    stb 00,x
895B C6 32           .2    ldab #32
895D F7 00 3A        ..:   stb 003a
8960 20 09            .    bra 09
8962 CE 00 1F        ...   ldx #001f
8965 1C 00           ..    bset add,x 00,x
8967 02              .     idiv 
8968 7F 00 3A        ..:   clr 003a
896B BD BE 01        ...   jsr be01		;jump70
896E 7C 00 54        |.T   inc 0054
8971 F6 00 54        ..T   ldab 0054
8974 C1 02           ..    cmpb #02
8976 26 60           &`    bne 60
8978 CE 00 1D        ...   ldx #001d
897B 1C 00           ..    bset add,x 00,x
897D 80 F6           ..    suba #f6
897F 0B              .     sev 
8980 94 CB D0        ...   anda cb
8983 37              7     pshb 
8984 F6 0B 95        ...   ldab 0b95
8987 CB D0           ..    addb #d0
8989 86 0A           ..    ldaa #0a
898B 3D              =     mul 
898C 30              0     tsx 
898D EB 00           ..    addb 00,x
898F 31              1     ins 
8990 F7 00 06        ...   stb 0006
8993 C6 32           .2    ldab #32
8995 F7 00 3A        ..:   stb 003a
8998 F6 00 06        ...   ldab 0006
899B BD D7 A0        ...   jsr d7a0		;jump10
899E 83 00 00        ...   subd #0000
89A1 26 27           &'    bne 27
89A3 BD D3 11        ...   jsr d311		;jump30
89A6 83 00 00        ...   subd #0000
89A9 26 0B           &.    bne 0b
89AB CE 00 1F        ...   ldx #001f
89AE 1C 00           ..    bset add,x 00,x
89B0 40              @     nega 
89B1 BD 8C 73        ..s   jsr 8c73		;jump75
89B4 20 0F            .    bra 0f
89B6 CE 00 1F        ...   ldx #001f
89B9 1D 00           ..    bclr add,x 00,x
89BB 40              @     nega 
89BC CC 2A FF        .*.   ldd #2aff
89BF FD 00 27        ..'   stad 0027
89C2 BD A3 85        ...   jsr a385		;jump40
89C5 BD 9A 47        ..G   jsr 9a47		;jump39
89C8 20 0C            .    bra 0c
89CA CE 00 1D        ...   ldx #001d
89CD 1D 00           ..    bclr add,x 00,x
89CF 80 CE           ..    suba #ce
89D1 00              .     test 
89D2 1F 1C 00 02     ....  brclr 1c,x 00 02
89D6 20 06            .    bra 06
89D8 CE 00 1D        ...   ldx #001d
89DB 1D 00           ..    bclr add,x 00,x
89DD 80 7E           .~    suba #7e
;goto6
;89DE????
89DF 8C 72 F6        .r.   cpx #72f6
;goto5
; 89e1?????
;
89E2 00              .     test 
89E3 1D 2D           .-    bclr add,x 2d,x
89E5 03              .     fdiv 
89E6 7E 8C 72        ~.r   jmp 8c72		;goto7
89E9 F6 00 09        ...   ldab 0009
89EC 4F              O     clra 
89ED BD FE 16        ...   jsr fe16		;jump2
89F0 00              .     test 
89F1 00              .     test 
89F2 00              .     test 
89F3 0B              .     sev 
89F4 8C 72 8A        .r.   cpx #728a
89F7 0E              .     cli 
89F8 8A 14           ..    ora #14
89FA 8A AA           ..    ora #aa
89FC 8B 5E           .^    adda #5e
89FE 8B 9E           ..    adda #9e
8A00 8B DB           ..    adda #db
8A02 8C 72 8C        .r.   cpx #728c
8A05 72              r     illegal 
8A06 8C 72 8B        .r.   cpx #728b
8A09 FC 8C 67        ..g   ldd 8c67
8A0C 8C 6F BD        .o.   cpx #6fbd
8A0F 9A 47 7E        .G~   ora 47
8A12 8C 72 F6        .r.   cpx #72f6
8A15 00              .     test 
8A16 36              6     psha 
8A17 27 03           '.    beq 03
8A19 7E 8A 9B        ~..   jmp 8a9b		;goto8
8A1C F6 00 1F        ...   ldab 001f
8A1F C4 40           .@    andb #40
8A21 27 78           'x    beq 78
8A23 F6 0B C0        ...   ldab 0bc0
8A26 C4 02           ..    andb #02
8A28 27 52           'R    beq 52
8A2A F6 0B AA        ...   ldab 0baa
8A2D C1 32           .2    cmpb #32
8A2F 24 22           $"    bcc 22
8A31 F6 0B AA        ...   ldab 0baa
8A34 4F              O     clra 
8A35 F3 00 27        ..'   addd 0027
8A38 FD 00 27        ..'   stad 0027
8A3B F6 0B AA        ...   ldab 0baa
8A3E 4F              O     clra 
8A3F 37              7     pshb 
8A40 36              6     psha 
8A41 CC 00 FA        ...   ldd #00fa
8A44 BD FB BA        ...   jsr fbba		;jump38
8A47 B3 00 27        ..'   subd 0027
8A4A 24 05           $.    bcc 05
8A4C 5F              _     clrb 
8A4D 4F              O     clra 
8A4E FD 00 27        ..'   stad 0027
8A51 20 27            '    bra 27
8A53 F6 0B AA        ...   ldab 0baa
8A56 C1 32           .2    cmpb #32
8A58 26 20           &     bne 20
8A5A F6 0B AA        ...   ldab 0baa
8A5D 4F              O     clra 
8A5E F3 00 27        ..'   addd 0027
8A61 FD 00 27        ..'   stad 0027
8A64 F6 0B AA        ...   ldab 0baa
8A67 4F              O     clra 
8A68 37              7     pshb 
8A69 36              6     psha 
8A6A CC 00 C7        ...   ldd #00c7
8A6D BD FB BA        ...   jsr fbba		;jump38
8A70 B3 00 27        ..'   subd 0027
8A73 24 05           $.    bcc 05
8A75 5F              _     clrb 
8A76 4F              O     clra 
8A77 FD 00 27        ..'   stad 0027
8A7A 20 17            .    bra 17
8A7C F6 0B AA        ...   ldab 0baa
8A7F 4F              O     clra 
8A80 F3 00 27        ..'   addd 0027
8A83 FD 00 27        ..'   stad 0027
8A86 FC 00 27        ..'   ldd 0027
8A89 83 27 0F        .'.   subd #270f
8A8C 23 05           #.    bls 05
8A8E 5F              _     clrb 
8A8F 4F              O     clra 
8A90 FD 00 27        ..'   stad 0027
8A93 BD 9A 47        ..G   jsr 9a47		;jump39
8A96 C6 02           ..    ldab #02
8A98 F7 00 36        ..6   stb 0036

goto8:
8A9B CE 00 1B        ...   ldx #001b
8A9E 1C 00           ..    bset add,x 00,x
8AA0 10              .     sba 
8AA1 CC 02 33        ..3   ldd #0233
8AA4 FD 00 3B        ..;   stad 003b
8AA7 7E 8C 72        ~.r   jmp 8c72		;goto7
8AAA F6 00 36        ..6   ldab 0036
8AAD 27 03           '.    beq 03
8AAF 7E 8B 4F        ~.O   jmp 8b4f		;goto9
8AB2 F6 00 1F        ...   ldab 001f
8AB5 C4 40           .@    andb #40
8AB7 26 03           &.    bne 03
8AB9 7E 8B 4F        ~.O   jmp 8b4f		;goto9
8ABC F6 0B C0        ...   ldab 0bc0
8ABF C4 02           ..    andb #02
8AC1 27 57           'W    beq 57
8AC3 FC 00 27        ..'   ldd 0027
8AC6 26 18           &.    bne 18
8AC8 F6 0B AA        ...   ldab 0baa
8ACB C1 32           .2    cmpb #32
8ACD 24 11           $.    bcc 11
8ACF F6 0B AA        ...   ldab 0baa
8AD2 4F              O     clra 
8AD3 37              7     pshb 
8AD4 36              6     psha 
8AD5 CC 00 FA        ...   ldd #00fa
8AD8 BD FB BA        ...   jsr fbba		;jump38
8ADB FD 00 27        ..'   stad 0027
8ADE 20 38            8    bra 38
8AE0 FC 00 27        ..'   ldd 0027
8AE3 26 18           &.    bne 18
8AE5 F6 0B AA        ...   ldab 0baa
8AE8 C1 32           .2    cmpb #32
8AEA 26 11           &.    bne 11
8AEC F6 0B AA        ...   ldab 0baa
8AEF 4F              O     clra 
8AF0 37              7     pshb 
8AF1 36              6     psha 
8AF2 CC 00 C7        ...   ldd #00c7
8AF5 BD FB BA        ...   jsr fbba		;jump38
8AF8 FD 00 27        ..'   stad 0027
8AFB 20 1B            .    bra 1b
8AFD F6 0B AA        ...   ldab 0baa
8B00 4F              O     clra 
8B01 B3 00 27        ..'   subd 0027
8B04 22 0D           ".    bhi 0d
8B06 FC 00 27        ..'   ldd 0027
8B09 F0 0B AA        ...   subb 0baa
8B0C 82 00           ..    sbca #00
8B0E FD 00 27        ..'   stad 0027
8B11 20 05            .    bra 05
8B13 5F              _     clrb 
8B14 4F              O     clra 
8B15 FD 00 27        ..'   stad 0027
8B18 20 2D            -    bra 2d
8B1A FC 00 27        ..'   ldd 0027
8B1D 26 0D           &.    bne 0d
8B1F CC 27 10        .'.   ldd #2710
8B22 F0 0B AA        ...   subb 0baa
8B25 82 00           ..    sbca #00
8B27 FD 00 27        ..'   stad 0027
8B2A 20 1B            .    bra 1b
8B2C F6 0B AA        ...   ldab 0baa
8B2F 4F              O     clra 
8B30 B3 00 27        ..'   subd 0027
8B33 22 0D           ".    bhi 0d
8B35 FC 00 27        ..'   ldd 0027
8B38 F0 0B AA        ...   subb 0baa
8B3B 82 00           ..    sbca #00
8B3D FD 00 27        ..'   stad 0027
8B40 20 05            .    bra 05
8B42 5F              _     clrb 
8B43 4F              O     clra 
8B44 FD 00 27        ..'   stad 0027
8B47 BD 9A 47        ..G   jsr 9a47		;jump39
8B4A C6 02           ..    ldab #02
8B4C F7 00 36        ..6   stb 0036
goto9:
8B4F CE 00 1B        ...   ldx #001b
8B52 1C 00           ..    bset add,x 00,x
8B54 10              .     sba 
8B55 CC 02 33        ..3   ldd #0233
8B58 FD 00 3B        ..;   stad 003b
8B5B 7E 8C 72        ~.r   jmp 8c72		;goto7
8B5E F6 00 36        ..6   ldab 0036
8B61 26 2C           &,    bne 2c
8B63 F6 00 1F        ...   ldab 001f
8B66 C4 40           .@    andb #40
8B68 27 25           '%    beq 25
8B6A FC 00 46        ..F   ldd 0046
8B6D 83 00 64        ..d   subd #0064
8B70 24 15           $.    bcc 15
8B72 FE 00 46        ..F   ldx 0046
8B75 08              .     inx 
8B76 FF 00 46        ..F   stx 0046
8B79 FC 00 46        ..F   ldd 0046
8B7C 83 00 64        ..d   subd #0064
8B7F 25 06           %.    bcs 06
8B81 CC 00 01        ...   ldd #0001
8B84 FD 00 46        ..F   stad 0046
8B87 BD 9A 47        ..G   jsr 9a47		;jump39
8B8A C6 02           ..    ldab #02
8B8C F7 00 36        ..6   stb 0036
8B8F CE 00 1B        ...   ldx #001b
8B92 1C 00           ..    bset add,x 00,x
8B94 10              .     sba 
8B95 CC 02 33        ..3   ldd #0233
8B98 FD 00 3B        ..;   stad 003b
8B9B 7E 8C 72        ~.r   jmp 8c72		;goto7
8B9E F6 00 36        ..6   ldab 0036
8BA1 26 29           &)    bne 29
8BA3 F6 00 1F        ...   ldab 001f
8BA6 C4 40           .@    andb #40
8BA8 27 22           '"    beq 22
8BAA FC 00 46        ..F   ldd 0046
8BAD 83 00 00        ...   subd #0000
8BB0 23 12           #.    bls 12
8BB2 FE 00 46        ..F   ldx 0046
8BB5 09              .     dex 
8BB6 FF 00 46        ..F   stx 0046
8BB9 FC 00 46        ..F   ldd 0046
8BBC 26 06           &.    bne 06
8BBE CC 00 63        ..c   ldd #0063
8BC1 FD 00 46        ..F   stad 0046
8BC4 BD 9A 47        ..G   jsr 9a47		;jump39
8BC7 C6 02           ..    ldab #02
8BC9 F7 00 36        ..6   stb 0036
8BCC CE 00 1B        ...   ldx #001b
8BCF 1C 00           ..    bset add,x 00,x
8BD1 10              .     sba 
8BD2 CC 02 33        ..3   ldd #0233
8BD5 FD 00 3B        ..;   stad 003b
8BD8 7E 8C 72        ~.r   jmp 8c72		;goto7
8BDB F6 00 1F        ...   ldab 001f
8BDE C4 40           .@    andb #40
8BE0 27 15           '.    beq 15
8BE2 F6 00 19        ...   ldab 0019
8BE5 C4 10           ..    andb #10
8BE7 27 08           '.    beq 08
8BE9 CE 00 19        ...   ldx #0019
8BEC 1D 00           ..    bclr add,x 00,x
8BEE 10              .     sba 
8BEF 20 06            .    bra 06
8BF1 CE 00 19        ...   ldx #0019
8BF4 1C 00           ..    bset add,x 00,x
8BF6 10              .     sba 
8BF7 BD 9A 47        ..G   jsr 9a47		;jump39
8BFA 20 76            v    bra 76
8BFC BD A3 85        ...   jsr a385		;jump40
8BFF F6 00 06        ...   ldab 0006
8C02 C1 64           .d    cmpb #64
8C04 24 24           $$    bcc 24
8C06 BD 97 6F        ..o   jsr 976f		;jump41
8C09 F6 00 06        ...   ldab 0006
8C0C BD D7 A0        ...   jsr d7a0		;jump10
8C0F 83 00 00        ...   subd #0000
8C12 27 16           '.    beq 16
8C14 F6 00 06        ...   ldab 0006
8C17 C1 09           ..    cmpb #09
8C19 26 03           &.    bne 03
8C1B BD 97 6F        ..o   jsr 976f		;jump41
8C1E F6 00 06        ...   ldab 0006
8C21 C1 45           .E    cmpb #45
8C23 26 05           &.    bne 05
8C25 C6 01           ..    ldab #01
8C27 F7 00 06        ...   stb 0006
8C2A F6 0E 6D        ..m   ldab 0e6d
8C2D C1 01           ..    cmpb #01
8C2F 26 11           &.    bne 11
8C31 F6 00 06        ...   ldab 0006
8C34 4F              O     clra 
8C35 05              .     asld 
8C36 C3 08 C8        ...   addd #08c8
8C39 8F              .     xgdx 
8C3A EC 00           ..    ldd 00,x
8C3C C4 7F           ..    andb #7f
8C3E 4F              O     clra 
8C3F FD 00 46        ..F   stad 0046
8C42 BD D3 11        ...   jsr d311		;jump30
8C45 83 00 00        ...   subd #0000
8C48 26 12           &.    bne 12
8C4A CE 00 1F        ...   ldx #001f
8C4D 1C 00           ..    bset add,x 00,x
8C4F 40              @     nega 
8C50 FC 00 27        ..'   ldd 0027
8C53 83 27 0F        .'.   subd #270f
8C56 23 02           #.    bls 02
8C58 8D 19           ..    bsr dest 19
8C5A 20 06            .    bra 06
8C5C CE 00 1F        ...   ldx #001f
8C5F 1D 00           ..    bclr add,x 00,x
8C61 40              @     nega 
8C62 BD 9A 47        ..G   jsr 9a47		;jump39
8C65 20 0B            .    bra 0b
8C67 BD A3 85        ...   jsr a385		;jump40
8C6A BD 85 AF        ...   jsr 85af		;jump14
8C6D 20 03            .    bra 03
8C6F BD 85 AF        ...   jsr 85af		;jump14
goto7:
8C72 39              9     rts 

jump75:
8C73 F6 00 06        ...   ldab 0006
8C76 4F              O     clra 
8C77 05              .     asld 
8C78 C3 08 00        ...   addd #0800
8C7B 8F              .     xgdx 
8C7C EC 00           ..    ldd 00,x
8C7E C4 FF           ..    andb #ff
8C80 84 3F           .?    anda #3f
8C82 FD 00 27        ..'   stad 0027
8C85 F6 00 06        ...   ldab 0006
8C88 4F              O     clra 
8C89 05              .     asld 
8C8A C3 08 C8        ...   addd #08c8
8C8D 8F              .     xgdx 
8C8E EC 00           ..    ldd 00,x
8C90 C4 7F           ..    andb #7f
8C92 4F              O     clra 
8C93 FD 00 46        ..F   stad 0046
8C96 F6 00 06        ...   ldab 0006
8C99 4F              O     clra 
8C9A 05              .     asld 
8C9B C3 08 C8        ...   addd #08c8
8C9E 8F              .     xgdx 
8C9F EC 00           ..    ldd 00,x
8CA1 84 08           ..    anda #08
8CA3 27 08           '.    beq 08
8CA5 CE 00 19        ...   ldx #0019
8CA8 1C 00           ..    bset add,x 00,x
8CAA 10              .     sba 
8CAB 20 06            .    bra 06
8CAD CE 00 19        ...   ldx #0019
8CB0 1D 00           ..    bclr add,x 00,x
8CB2 10              .     sba 
8CB3 39              9     rts 

jump18:
8CB4 F6 00 1F        ...   ldab 001f
8CB7 C4 02           ..    andb #02
8CB9 27 12           '.    beq 12
8CBB FC 0B B7        ...   ldd 0bb7
8CBE FD 00 0D        ...   stad 000d
8CC1 CE 00 1F        ...   ldx #001f
8CC4 1D 00           ..    bclr add,x 00,x
8CC6 02              .     idiv 
8CC7 7F 00 36        ..6   clr 0036
8CCA 7E 8E 34        ~.4   jmp 8e34		;goto10
8CCD F6 00 09        ...   ldab 0009
8CD0 4F              O     clra 
8CD1 BD FE 32        ..2   jsr fe32		;jump25
8CD4 8C EE 00        ...   cpx #ee00
8CD7 00              .     test 
8CD8 8C F4 00        ...   cpx #f400
8CDB 01              .     nop 
8CDC 8D 80           ..    bsr dest 80
8CDE 00              .     test 
8CDF 01              .     nop 
8CE0 8E 29 00        .).   lds #2900
8CE3 08              .     inx 
8CE4 8E 31 00        .1.   lds #3100
8CE7 01              .     nop 
8CE8 8E 34 FF        .4.   lds #34ff
8CEB F5 00 00        ...   bitb 0000
8CEE BD 9B 63        ..c   jsr 9b63		;jump101
8CF1 7E 8E 34        ~.4   jmp 8e34		;goto10
8CF4 F6 00 36        ..6   ldab 0036
8CF7 26 78           &x    bne 78
8CF9 F6 0B C0        ...   ldab 0bc0
8CFC C4 02           ..    andb #02
8CFE 27 52           'R    beq 52
8D00 F6 0B AA        ...   ldab 0baa
8D03 C1 32           .2    cmpb #32
8D05 24 22           $"    bcc 22
8D07 F6 0B AA        ...   ldab 0baa
8D0A 4F              O     clra 
8D0B F3 00 0D        ...   addd 000d
8D0E FD 00 0D        ...   stad 000d
8D11 F6 0B AA        ...   ldab 0baa
8D14 4F              O     clra 
8D15 37              7     pshb 
8D16 36              6     psha 
8D17 CC 00 FA        ...   ldd #00fa
8D1A BD FB BA        ...   jsr fbba		;jump38
8D1D B3 00 0D        ...   subd 000d
8D20 24 05           $.    bcc 05
8D22 5F              _     clrb 
8D23 4F              O     clra 
8D24 FD 00 0D        ...   stad 000d
8D27 20 27            '    bra 27
8D29 F6 0B AA        ...   ldab 0baa
8D2C C1 32           .2    cmpb #32
8D2E 26 20           &     bne 20
8D30 F6 0B AA        ...   ldab 0baa
8D33 4F              O     clra 
8D34 F3 00 0D        ...   addd 000d
8D37 FD 00 0D        ...   stad 000d
8D3A F6 0B AA        ...   ldab 0baa
8D3D 4F              O     clra 
8D3E 37              7     pshb 
8D3F 36              6     psha 
8D40 CC 00 C7        ...   ldd #00c7
8D43 BD FB BA        ...   jsr fbba		;jump38
8D46 B3 00 0D        ...   subd 000d
8D49 24 05           $.    bcc 05
8D4B 5F              _     clrb 
8D4C 4F              O     clra 
8D4D FD 00 0D        ...   stad 000d
8D50 20 17            .    bra 17
8D52 F6 0B AA        ...   ldab 0baa
8D55 4F              O     clra 
8D56 F3 00 0D        ...   addd 000d
8D59 FD 00 0D        ...   stad 000d
8D5C FC 00 0D        ...   ldd 000d
8D5F 83 27 0F        .'.   subd #270f
8D62 23 05           #.    bls 05
8D64 5F              _     clrb 
8D65 4F              O     clra 
8D66 FD 00 0D        ...   stad 000d
8D69 BD 9B 63        ..c   jsr 9b63		;jump101
8D6C C6 02           ..    ldab #02
8D6E F7 00 36        ..6   stb 0036
8D71 CE 00 1B        ...   ldx #001b
8D74 1C 00           ..    bset add,x 00,x
8D76 10              .     sba 
8D77 CC 02 33        ..3   ldd #0233
8D7A FD 00 3B        ..;   stad 003b
8D7D 7E 8E 34        ~.4   jmp 8e34		;goto10
8D80 F6 00 36        ..6   ldab 0036
8D83 27 03           '.    beq 03
8D85 7E 8E 1B        ~..   jmp 8e1b		;goto11
8D88 F6 0B C0        ...   ldab 0bc0
8D8B C4 02           ..    andb #02
8D8D 27 57           'W    beq 57
8D8F FC 00 0D        ...   ldd 000d
8D92 26 18           &.    bne 18
8D94 F6 0B AA        ...   ldab 0baa
8D97 C1 32           .2    cmpb #32
8D99 24 11           $.    bcc 11
8D9B F6 0B AA        ...   ldab 0baa
8D9E 4F              O     clra 
8D9F 37              7     pshb 
8DA0 36              6     psha 
8DA1 CC 00 FA        ...   ldd #00fa
8DA4 BD FB BA        ...   jsr fbba		;jump38
8DA7 FD 00 0D        ...   stad 000d
8DAA 20 38            8    bra 38
8DAC FC 00 0D        ...   ldd 000d
8DAF 26 18           &.    bne 18
8DB1 F6 0B AA        ...   ldab 0baa
8DB4 C1 32           .2    cmpb #32
8DB6 26 11           &.    bne 11
8DB8 F6 0B AA        ...   ldab 0baa
8DBB 4F              O     clra 
8DBC 37              7     pshb 
8DBD 36              6     psha 
8DBE CC 00 C7        ...   ldd #00c7
8DC1 BD FB BA        ...   jsr fbba		;jump38
8DC4 FD 00 0D        ...   stad 000d
8DC7 20 1B            .    bra 1b
8DC9 F6 0B AA        ...   ldab 0baa
8DCC 4F              O     clra 
8DCD B3 00 0D        ...   subd 000d
8DD0 22 0D           ".    bhi 0d
8DD2 FC 00 0D        ...   ldd 000d
8DD5 F0 0B AA        ...   subb 0baa
8DD8 82 00           ..    sbca #00
8DDA FD 00 0D        ...   stad 000d
8DDD 20 05            .    bra 05
8DDF 5F              _     clrb 
8DE0 4F              O     clra 
8DE1 FD 00 0D        ...   stad 000d
8DE4 20 2D            -    bra 2d
8DE6 FC 00 0D        ...   ldd 000d
8DE9 26 0D           &.    bne 0d
8DEB CC 27 10        .'.   ldd #2710
8DEE F0 0B AA        ...   subb 0baa
8DF1 82 00           ..    sbca #00
8DF3 FD 00 0D        ...   stad 000d
8DF6 20 1B            .    bra 1b
8DF8 F6 0B AA        ...   ldab 0baa
8DFB 4F              O     clra 
8DFC B3 00 0D        ...   subd 000d
8DFF 22 0D           ".    bhi 0d
8E01 FC 00 0D        ...   ldd 000d
8E04 F0 0B AA        ...   subb 0baa
8E07 82 00           ..    sbca #00
8E09 FD 00 0D        ...   stad 000d
8E0C 20 05            .    bra 05
8E0E 5F              _     clrb 
8E0F 4F              O     clra 
8E10 FD 00 0D        ...   stad 000d
8E13 BD 9B 63        ..c   jsr 9b63		;jump101
8E16 C6 02           ..    ldab #02
8E18 F7 00 36        ..6   stb 0036
goto11:
8E1B CE 00 1B        ...   ldx #001b
8E1E 1C 00           ..    bset add,x 00,x
8E20 10              .     sba 
8E21 CC 02 33        ..3   ldd #0233
8E24 FD 00 3B        ..;   stad 003b
8E27 20 0B            .    bra 0b
8E29 BD A4 4F        ..O   jsr a44f		;jump140
8E2C BD 85 AF        ...   jsr 85af		;jump14
8E2F 20 03            .    bra 03
8E31 BD 85 AF        ...   jsr 85af		;jump14
goto10:
8E34 39              9     rts 

jump19:
8E35 F6 00 1F        ...   ldab 001f
8E38 C4 02           ..    andb #02
8E3A 27 1E           '.    beq 1e
8E3C 7F 00 48        ..H   clr 0048
8E3F F6 0B C0        ...   ldab 0bc0
8E42 F7 00 67        ..g   stb 0067
8E45 F6 0E 6D        ..m   ldab 0e6d
8E48 F7 0E 6F        ..o   stb 0e6f
8E4B F6 0E 6E        ..n   ldab 0e6e
8E4E F7 0E 70        ..p   stb 0e70
8E51 CE 00 1F        ...   ldx #001f
8E54 1D 00           ..    bclr add,x 00,x
8E56 02              .     idiv 
8E57 7E 8F 0C        ~..   jmp 8f0c		;goto12
8E5A F6 00 09        ...   ldab 0009
8E5D 4F              O     clra 
8E5E BD FE 32        ..2   jsr fe32		;jump25
8E61 8E 7B 00        .{.   lds #7b00
8E64 00              .     test 
8E65 8E 81 00        ...   lds #8100
8E68 01              .     nop 
8E69 8E 87 00        ...   lds #8700
8E6C 04              .     lsrd 
8E6D 8F              .     xgdx 
8E6E 01              .     nop 
8E6F 00              .     test 
8E70 05              .     asld 
8E71 8F              .     xgdx 
8E72 09              .     dex 
8E73 00              .     test 
8E74 01              .     nop 
8E75 8F              .     xgdx 
8E76 0C              .     clc 
8E77 FF F5 00        ...   stx f500
8E7A 00              .     test 
8E7B BD 9C 15        ...   jsr 9c15		;jump111
8E7E 7E 8F 0C        ~..   jmp 8f0c		;goto12
8E81 BD A7 5F        .._   jsr a75f		;jump133
8E84 7E 8F 0C        ~..   jmp 8f0c		;goto12
8E87 F6 00 48        ..H   ldab 0048
8E8A C1 08           ..    cmpb #08
8E8C 25 05           %.    bcs 05
8E8E F6 00 67        ..g   ldab 0067
8E91 2D 03           -.    blt 03
8E93 BD A4 59        ..Y   jsr a459		;jump110
8E96 F6 00 48        ..H   ldab 0048
8E99 C1 0A           ..    cmpb #0a
8E9B 24 5F           $_    bcc 5f
8E9D 7C 00 48        |.H   inc 0048
8EA0 F6 00 48        ..H   ldab 0048
8EA3 C1 01           ..    cmpb #01
8EA5 26 0F           &.    bne 0f
8EA7 F6 00 67        ..g   ldab 0067
8EAA C4 02           ..    andb #02
8EAC 26 08           &.    bne 08
8EAE F6 00 48        ..H   ldab 0048
8EB1 CB 02           ..    addb #02
8EB3 F7 00 48        ..H   stb 0048
8EB6 F6 00 48        ..H   ldab 0048
8EB9 C1 02           ..    cmpb #02
8EBB 26 0A           &.    bne 0a
8EBD F6 00 67        ..g   ldab 0067
8EC0 C4 10           ..    andb #10
8EC2 26 03           &.    bne 03
8EC4 7C 00 48        |.H   inc 0048
8EC7 F6 00 48        ..H   ldab 0048
8ECA C1 06           ..    cmpb #06
8ECC 26 0F           &.    bne 0f
8ECE F6 00 67        ..g   ldab 0067
8ED1 C4 08           ..    andb #08
8ED3 26 08           &.    bne 08
8ED5 C6 02           ..    ldab #02
8ED7 FB 00 48        ..H   addb 0048
8EDA F7 00 48        ..H   stb 0048
8EDD F6 00 48        ..H   ldab 0048
8EE0 C1 0A           ..    cmpb #0a
8EE2 26 08           &.    bne 08
8EE4 F6 00 67        ..g   ldab 0067
8EE7 2D 03           -.    blt 03
8EE9 7C 00 48        |.H   inc 0048
8EEC F6 00 48        ..H   ldab 0048
8EEF C1 0A           ..    cmpb #0a
8EF1 23 09           #.    bls 09
8EF3 7F 00 48        ..H   clr 0048
8EF6 F6 0B C0        ...   ldab 0bc0
8EF9 F7 00 67        ..g   stb 0067
8EFC BD 9C 15        ...   jsr 9c15		;jump111
8EFF 20 0B            .    bra 0b
8F01 BD A4 59        ..Y   jsr a459		;jump110
8F04 BD 85 AF        ...   jsr 85af		;jump14
8F07 20 03            .    bra 03
8F09 BD 85 AF        ...   jsr 85af		;jump14
goto12:
8F0C 39              9     rts 

jump20:
8F0D F6 00 1F        ...   ldab 001f
8F10 C4 02           ..    andb #02
8F12 27 1D           '.    beq 1d
8F14 C6 01           ..    ldab #01
8F16 F7 00 22        .."   stb 0022
8F19 F6 0B AF        ...   ldab 0baf
8F1C F7 00 24        ..$   stb 0024
8F1F F6 0B B4        ...   ldab 0bb4
8F22 F7 00 2A        ..*   stb 002a
8F25 CE 00 1F        ...   ldx #001f
8F28 1D 00           ..    bclr add,x 00,x
8F2A 02              .     idiv 
8F2B 7F 00 36        ..6   clr 0036
8F2E 7E 90 D0        ~..   jmp 90d0		;goto13
8F31 F6 00 09        ...   ldab 0009
8F34 4F              O     clra 
8F35 BD FE 16        ...   jsr fe16		;jump2
8F38 00              .     test 
8F39 00              .     test 
8F3A 00              .     test 
8F3B 0B              .     sev 
8F3C 90 D0 8F        ...   suba d0
8F3F 56              V     rorb 
8F40 8F              .     xgdx 
8F41 5C              \     incb 
8F42 8F              .     xgdx 
8F43 B3 90 0C        ...   subd 900c
8F46 90 62 90        .b.   suba 62
8F49 BA 90 D0        ...   ora 90d0
8F4C 90 D0 90        ...   suba d0
8F4F D0 90 D0        ...   subb 90
8F52 90 C5 90        ...   suba c5
8F55 CD              .     illegal 
8F56 BD 9D 2A        ..*   jsr 9d2a		;jump89
8F59 7E 90 D0        ~..   jmp 90d0		;goto13
8F5C F6 00 36        ..6   ldab 0036
8F5F 26 43           &C    bne 43
8F61 F6 00 24        ..$   ldab 0024
8F64 C1 64           .d    cmpb #64
8F66 24 34           $4    bcc 34
8F68 7C 00 24        |.$   inc 0024
8F6B F6 00 24        ..$   ldab 0024
8F6E F7 00 06        ...   stb 0006
8F71 F6 00 06        ...   ldab 0006
8F74 BD D7 A0        ...   jsr d7a0		;jump10
8F77 83 00 00        ...   subd #0000
8F7A 27 03           '.    beq 03
8F7C 7C 00 24        |.$   inc 0024
8F7F F6 00 24        ..$   ldab 0024
8F82 C1 64           .d    cmpb #64
8F84 26 03           &.    bne 03
8F86 7F 00 24        ..$   clr 0024
8F89 F6 00 24        ..$   ldab 0024
8F8C 4F              O     clra 
8F8D CE 00 05        ...   ldx #0005
8F90 02              .     idiv 
8F91 5D              ]     tstb 
8F92 26 08           &.    bne 08
8F94 F6 00 24        ..$   ldab 0024
8F97 27 03           '.    beq 03
8F99 7C 00 24        |.$   inc 0024
8F9C BD 9D 2A        ..*   jsr 8d2a		;jump89
8F9F C6 02           ..    ldab #02
8FA1 F7 00 36        ..6   stb 0036
8FA4 CE 00 1B        ...   ldx #001b
8FA7 1C 00           ..    bset add,x 00,x
8FA9 10              .     sba 
8FAA CC 02 33        ..3   ldd #0233
8FAD FD 00 3B        ..;   stad 003b
8FB0 7E 90 D0        ~..   jmp 90d0		;goto13
8FB3 F6 00 36        ..6   ldab 0036
8FB6 26 45           &E    bne 45
8FB8 F6 00 24        ..$   ldab 0024
8FBB C1 00           ..    cmpb #00
8FBD 23 2C           #,    bls 2c
8FBF 7A 00 24        z.$   dec 0024
8FC2 F6 00 24        ..$   ldab 0024
8FC5 4F              O     clra 
8FC6 CE 00 05        ...   ldx #0005
8FC9 02              .     idiv 
8FCA 5D              ]     tstb 
8FCB 26 08           &.    bne 08
8FCD F6 00 24        ..$   ldab 0024
8FD0 27 03           '.    beq 03
8FD2 7A 00 24        z.$   dec 0024
8FD5 F6 00 24        ..$   ldab 0024
8FD8 F7 00 06        ...   stb 0006
8FDB F6 00 06        ...   ldab 0006
8FDE BD D7 A0        ...   jsr d7a0		;jump10
8FE1 83 00 00        ...   subd #0000
8FE4 27 03           '.    beq 03
8FE6 7A 00 24        z.$   dec 0024
8FE9 20 0A            .    bra 0a
8FEB F6 00 24        ..$   ldab 0024
8FEE 26 05           &.    bne 05
8FF0 C6 62           .b    ldab #62
8FF2 F7 00 24        ..$   stb 0024
8FF5 BD 9D 2A        ..*   jsr 9d2a		;jump89
8FF8 C6 02           ..    ldab #02
8FFA F7 00 36        ..6   stb 0036
8FFD CE 00 1B        ...   ldx #001b
9000 1C 00           ..    bset add,x 00,x
9002 10              .     sba 
9003 CC 02 33        ..3   ldd #0233
9006 FD 00 3B        ..;   stad 003b
9009 7E 90 D0        ~..   jmp 90d0		;goto13
900C F6 00 36        ..6   ldab 0036
900F 26 43           &C    bne 43
9011 F6 00 2A        ..*   ldab 002a
9014 C1 64           .d    cmpb #64
9016 24 34           $4    bcc 34
9018 7C 00 2A        |.*   inc 002a
901B F6 00 2A        ..*   ldab 002a
901E F7 00 06        ...   stb 0006
9021 F6 00 06        ...   ldab 0006
9024 BD D7 A0        ...   jsr d7a0		;jump10
9027 83 00 00        ...   subd #0000
902A 27 03           '.    beq 03
902C 7C 00 2A        |.*   inc 002a
902F F6 00 2A        ..*   ldab 002a
9032 C1 64           .d    cmpb #64
9034 26 03           &.    bne 03
9036 7F 00 2A        ..*   clr 002a
9039 F6 00 2A        ..*   ldab 002a
903C 4F              O     clra 
903D CE 00 05        ...   ldx #0005
9040 02              .     idiv 
9041 5D              ]     tstb 
9042 26 08           &.    bne 08
9044 F6 00 2A        ..*   ldab 002a
9047 27 03           '.    beq 03
9049 7C 00 2A        |.*   inc 002a
904C BD 9D 2A        ..*   jsr 9d2a		;jump89
904F C6 02           ..    ldab #02
9051 F7 00 36        ..6   stb 0036
9054 CE 00 1B        ...   ldx #001b
9057 1C 00           ..    bset add,x 00,x
9059 10              .     sba 
905A CC 02 33        ..3   ldd #0233
905D FD 00 3B        ..;   stad 003b
9060 20 6E            n    bra 6e
9062 F6 00 36        ..6   ldab 0036
9065 26 45           &E    bne 45
9067 F6 00 2A        ..*   ldab 002a
906A C1 00           ..    cmpb #00
906C 23 2C           #,    bls 2c
906E 7A 00 2A        z.*   dec 002a
9071 F6 00 2A        ..*   ldab 002a
9074 4F              O     clra 
9075 CE 00 05        ...   ldx #0005
9078 02              .     idiv 
9079 5D              ]     tstb 
907A 26 08           &.    bne 08
907C F6 00 2A        ..*   ldab 002a
907F 27 03           '.    beq 03
9081 7A 00 2A        z.*   dec 002a
9084 F6 00 2A        ..*   ldab 002a
9087 F7 00 06        ...   stb 0006
908A F6 00 06        ...   ldab 0006
908D BD D7 A0        ...   jsr d7a0		;jump10
9090 83 00 00        ...   subd #0000
9093 27 03           '.    beq 03
9095 7A 00 2A        z.*   dec 002a
9098 20 0A            .    bra 0a
909A F6 00 2A        ..*   ldab 002a
909D 26 05           &.    bne 05
909F C6 62           .b    ldab #62
90A1 F7 00 2A        ..*   stb 002a
90A4 BD 9D 2A        ..*   jsr 9d2a		;jump89
90A7 C6 02           ..    ldab #02
90A9 F7 00 36        ..6   stb 0036
90AC CE 00 1B        ...   ldx #001b
90AF 1C 00           ..    bset add,x 00,x
90B1 10              .     sba 
90B2 CC 02 33        ..3   ldd #0233
90B5 FD 00 3B        ..;   stad 003b
90B8 20 16            .    bra 16
90BA BD A5 1E        ...   jsr a51e		;jump109
90BD BD AA 9A        ...   jsr aa9a		;jump127
90C0 BD 9D 2A        ..*   jsr 9d2a		;jump89
90C3 20 0B            .    bra 0b
90C5 BD A5 1E        ...   jsr a51e		;jump109
90C8 BD 85 AF        ...   jsr 85af		;jump14
90CB 20 03            .    bra 03
90CD BD 85 AF        ...   jsr 85af		;jump14
goto13:
90D0 39              9     rts 

jump21:
90D1 F6 00 1F        ...   ldab 001f
90D4 C4 02           ..    andb #02
90D6 27 14           '.    beq 14
90D8 CE 00 1F        ...   ldx #001f
90DB 1D 00           ..    bclr add,x 00,x
90DD 02              .     idiv 
90DE CE 00 19        ...   ldx #0019
90E1 1C 00           ..    bset add,x 00,x
90E3 80 7F           ..    suba #7f
90E5 00              .     test 
90E6 09              .     dex 
90E7 7F 00 0A        ...   clr 000a
90EA 20 40            @    bra 40
90EC F6 00 09        ...   ldab 0009
90EF 4F              O     clra 
90F0 BD FE 32        ..2   jsr fe32		;jump25
90F3 91 0D 00        ...   cmpa 0d
90F6 00              .     test 
90F7 91 15 00        ...   cmpa 15
90FA 01              .     nop 
90FB 91 19 00        ...   cmpa 19
90FE 01              .     nop 
90FF 91 1E 00        ...   cmpa 1e
9102 01              .     nop 
9103 91 23 00        .#.   cmpa 23
9106 08              .     inx 
9107 91 26 FF        .&.   cmpa 26
910A F5 00 00        ...   bitb 0000
910D CC B0 AD        ...   ldd #b0ad
9110 BD C0 BA        ...   jsr c0ba		;display:
9113 20 17            .    bra 17
9115 8D 16           ..    bsr dest 16
9117 20 13            .    bra 13
9119 BD 91 C6        ...   jsr 91c6		;jump76
911C 20 0E            .    bra 0e
911E BD 92 5A        ..Z   jsr 925a		;jump149
9121 20 09            .    bra 09
9123 BD 85 AF        ...   jsr 85af		;jump14
9126 7F 00 09        ...   clr 0009
9129 7F 00 0A        ...   clr 000a
912C 39              9     rts 

912D F6 00 19        ...   ldab 0019
9130 2C 28           ,(    bge 28
9132 BD A9 CF        ...   jsr a9cf		;jump131
9135 C6 50           .P    ldab #50
9137 F7 0A 90        ...   stb 0a90
913A C6 20           .     ldab #20
913C F7 0B 8D        ...   stb 0b8d
913F C6 20           .     ldab #20
9141 F7 0B 8E        ...   stb 0b8e
9144 C6 28           .(    ldab #28
9146 F7 0B 8F        ...   stb 0b8f
9149 7F 00 54        ..T   clr 0054
914C 7F 00 60        ..`   clr 0060
914F CE 00 19        ...   ldx #0019
9152 1D 00           ..    bclr add,x 00,x
9154 80 7F           ..    suba #7f
9156 00              .     test 
9157 36              6     psha 
9158 20 6B            k    bra 6b
915A F6 00 0A        ...   ldab 000a
915D 4F              O     clra 
915E BD FE 16        ...   jsr fe16		;jump2
9161 00              .     test 
9162 00              .     test 
9163 00              .     test 
9164 0B              .     sev 
9165 91 C5 91        ...   cmpa c5
9168 7F 91 84        ...   clr 9184
916B 91 89 91        ...   cmpa 89
916E 8E 91 9F        ...   lds #919f
9171 91 B0 91        ...   cmpa b0
9174 C5 91           ..    bitb #91
9176 C5 91           ..    bitb #91
9178 C5 91           ..    bitb #91
917A B5 91 BA        ...   bita 91ba
917D 91 C2 BD        ...   cmpa c2
9180 9D 7E 20        .~    jsr 7e
9183 41              A     illegal 
9184 BD A8 64        ..d   jsr a864		;jump99
9187 20 3C            <    bra 3c
9189 BD A8 D5        ...   jsr a8d5		;jump97
918C 20 37            7    bra 37
918E BD A8 96        ...   jsr a896		;jump98
9191 CE 00 1B        ...   ldx #001b
9194 1C 00           ..    bset add,x 00,x
9196 10              .     sba 
9197 CC 02 33        ..3   ldd #0233
919A FD 00 3B        ..;   stad 003b
919D 20 26            &    bra 26
919F BD A8 E5        ...   jsr a8e5		;jump96
91A2 CE 00 1B        ...   ldx #001b
91A5 1C 00           ..    bset add,x 00,x
91A7 10              .     sba 
91A8 CC 02 33        ..3   ldd #0233
91AB FD 00 3B        ..;   stad 003b
91AE 20 15            .    bra 15
91B0 BD A9 21        ..!   jsr a921		;jump95
91B3 20 10            .    bra 10
91B5 BD A9 7A        ..z   jsr a97a		;jump132
91B8 20 0B            .    bra 0b
91BA BD A5 7D        ..}   jsr a57d		;jump139
91BD BD 85 AF        ...   jsr 85af		;jump14
91C0 20 03            .    bra 03
91C2 BD 85 AF        ...   jsr 85af		;jump14
91C5 39              9     rts 

jump76:
91C6 F6 00 19        ...   ldab 0019
91C9 2C 28           ,(    bge 28
91CB BD AA 0A        ...   jsr aa0a		;jump130
91CE C6 53           .S    ldab #53
91D0 F7 0A 90        ...   stb 0a90
91D3 C6 20           .     ldab #20
91D5 F7 0A 9D        ...   stb 0a9d
91D8 C6 20           .     ldab #20
91DA F7 0A 9E        ...   stb 0a9e
91DD C6 28           .(    ldab #28
91DF F7 0A 9F        ...   stb 0a9f
91E2 7F 00 54        ..T   clr 0054
91E5 7F 00 60        ..`   clr 0060
91E8 CE 00 19        ...   ldx #0019
91EB 1D 00           ..    bclr add,x 00,x
91ED 80 7F           ..    suba #7f
91EF 00              .     test 
91F0 36              6     psha 
91F1 20 66            f    bra 66
91F3 F6 00 0A        ...   ldab 000a
91F6 4F              O     clra 
91F7 BD FE 16        ...   jsr fe16		;jump2
91FA 00              .     test 
91FB 00              .     test 
91FC 00              .     test 
91FD 0B              .     sev 
91FE 92 59 92        .Y.   sbca 59
9201 18              .     illegal 
9202 92 1D 92        ...   sbca 1d
9205 22 92           ".    bhi 92
9207 27 92           '.    beq 92
9209 38              8     pulx 
920A 92 49 92        .I.   sbca 49
920D 59              Y     rolb 
920E 92 59 92        .Y.   sbca 59
9211 59              Y     rolb 
9212 92 59 92        .Y.   sbca 59
9215 4E              N     illegal 
9216 92 56 BD        .V.   sbca 56
9219 9D 7E 20        .~    jsr 7e
921C 3C              <     pshx 
921D BD A8 64        ..d   jsr a864		;jump99
9220 20 37            7    bra 37
9222 BD A8 D5        ...   jsr a8d5		;jump97
9225 20 32            2    bra 32
9227 BD A8 96        ...   jsr a896		;jump98
922A CE 00 1B        ...   ldx #001b
922D 1C 00           ..    bset add,x 00,x
922F 10              .     sba 
9230 CC 02 33        ..3   ldd #0233
9233 FD 00 3B        ..;   stad 003b
9236 20 21            !    bra 21
9238 BD A8 E5        ...   jsr a8e5		;jump96
923B CE 00 1B        ...   ldx #001b
923E 1C 00           ..    bset add,x 00,x
9240 10              .     sba 
9241 CC 02 33        ..3   ldd #0233
9244 FD 00 3B        ..;   stad 003b
9247 20 10            .    bra 10
9249 BD A9 21        ..!   jsr a921		;jump95
924C 20 0B            .    bra 0b
924E BD A5 AC        ...   jsr a5ac		;jump138
9251 BD 85 AF        ...   jsr 85af		;jump14
9254 20 03            .    bra 03
9256 BD 85 AF        ...   jsr 85af		;jump14
9259 39              9     rts 

jump149:
925A F6 00 19        ...   ldab 0019
925D 2C 28           ,(    bge 28
925F BD AA 3F        ..?   jsr aa3f		;jump129
9262 C6 49           .I    ldab #49
9264 F7 0A 90        ...   stb 0a90
9267 C6 20           .     ldab #20
9269 F7 0A 9D        ...   stb 0a9d
926C C6 20           .     ldab #20
926E F7 0A 9E        ...   stb 0a9e
9271 C6 28           .(    ldab #28
9273 F7 0A 9F        ...   stb 0a9f
9276 7F 00 54        ..T   clr 0054
9279 7F 00 60        ..`   clr 0060
927C CE 00 19        ...   ldx #0019
927F 1D 00           ..    bclr add,x 00,x
9281 80 7F           ..    suba #7f
9283 00              .     test 
9284 36              6     psha 
9285 20 66            f    bra 66
9287 F6 00 0A        ...   ldab 000a
928A 4F              O     clra 
928B BD FE 16        ...   jsr fe16		;jump2
928E 00              .     test 
928F 00              .     test 
9290 00              .     test 
9291 0B              .     sev 
9292 92 ED 92        ...   sbca ed
9295 AC 92           ..    cpx 92,x
9297 B1 92 B6        ...   cmpa 92b6
929A 92 BB 92        ...   sbca bb
929D CC 92 DD        ...   ldd #92dd
92A0 92 ED 92        ...   sbca ed
92A3 ED 92           ..    stad 92,x
92A5 ED 92           ..    stad 92,x
92A7 ED 92           ..    stad 92,x
92A9 E2 92           ..    sbcb 92,x
92AB EA BD           ..    orb bd,x
92AD 9D 7E 20        .~    jsr 7e
92B0 3C              <     pshx 
92B1 BD A8 64        ..d   jsr a864		;jump99
92B4 20 37            7    bra 37
92B6 BD A8 D5        ...   jsr a8d5		;jump97
92B9 20 32            2    bra 32
92BB BD A8 96        ...   jsr a896		;jump98
92BE CE 00 1B        ...   ldx #001b
92C1 1C 00           ..    bset add,x 00,x
92C3 10              .     sba 
92C4 CC 02 33        ..3   ldd #0233
92C7 FD 00 3B        ..;   stad 003b
92CA 20 21            !    bra 21
92CC BD A8 E5        ...   jsr a8e5		;jump96
92CF CE 00 1B        ...   ldx #001b
92D2 1C 00           ..    bset add,x 00,x
92D4 10              .     sba 
92D5 CC 02 33        ..3   ldd #0233
92D8 FD 00 3B        ..;   stad 003b
92DB 20 10            .    bra 10
92DD BD A9 21        ..!   jsr a921		;jump95
92E0 20 0B            .    bra 0b
92E2 BD A5 D5        ...   jsr a5d5		;jump137
92E5 BD 85 AF        ...   jsr 85af		;jump14
92E8 20 03            .    bra 03
92EA BD 85 AF        ...   jsr 85af		;jump14
92ED 39              9     rts 

jump22:
92EE F6 0C DC        ...   ldab 0cdc
92F1 4F              O     clra 
92F2 37              7     pshb 
92F3 36              6     psha 
92F4 CC 00 64        ..d   ldd #0064
92F7 BD FB BA        ...   jsr fbba		;jump38
92FA FD 0C E9        ...   stad 0ce9
92FD BD D6 F6        ...   jsr d6f6		;jump106
9300 FC 0C E9        ...   ldd 0ce9
9303 F3 0B EF        ...   addd 0bef
9306 FD 0C F1        ...   stad 0cf1
9309 FE 0C F5        ...   ldx 0cf5
930C 3C              <     pshx 
930D FC 0C F3        ...   ldd 0cf3
9310 37              7     pshb 
9311 36              6     psha 
9312 FE 0C F9        ...   ldx 0cf9
9315 3C              <     pshx 
9316 FC 0C F7        ...   ldd 0cf7
9319 BD FC 25        ..%   jsr fc25		;jump85
931C 37              7     pshb 
931D 36              6     psha 
931E FE 0C FD        ...   ldx 0cfd
9321 3C              <     pshx 
9322 FC 0C FB        ...   ldd 0cfb
9325 BD FC 25        ..%   jsr fc25		;jump85
9328 37              7     pshb 
9329 36              6     psha 
932A FE 0D 01        ...   ldx 0d01
932D 3C              <     pshx 
932E FC 0C FF        ...   ldd 0cff
9331 BD FC 25        ..%   jsr fc25		;jump85
9334 37              7     pshb 
9335 36              6     psha 
9336 FE 0D 05        ...   ldx 0d05
9339 3C              <     pshx 
933A FC 0D 03        ...   ldd 0d03
933D BD FC 25        ..%   jsr fc25		;jump85
9340 FD 0D F3        ...   stad 0df3
9343 32              2     pula 
9344 33              3     pulb 
9345 FD 0D F5        ...   stad 0df5
9348 FE 0D F9        ...   ldx 0df9
934B 3C              <     pshx 
934C FC 0D F7        ...   ldd 0df7
934F 37              7     pshb 
9350 36              6     psha 
9351 FE 0D F5        ...   ldx 0df5
9354 3C              <     pshx 
9355 FC 0D F3        ...   ldd 0df3
9358 BD FC 25        ..%   jsr fc25		;jump85
935B FD 0D EF        ...   stad 0def
935E 32              2     pula 
935F 33              3     pulb 
9360 FD 0D F1        ...   stad 0df1
9363 F6 20 00        . .   ldab 2000
9366 C4 40           .@    andb #40
9368 27 37           '7    beq 37
936A F6 00 1F        ...   ldab 001f
936D C4 02           ..    andb #02
936F 27 1F           '.    beq 1f
9371 BD 97 E9        ...   jsr 97e9		;jump147
9374 CE 00 1F        ...   ldx #001f
9377 1D 00           ..    bclr add,x 00,x
9379 02              .     idiv 
937A CE 00 1F        ...   ldx #001f
937D 1D 00           ..    bclr add,x 00,x
937F 10              .     sba 
9380 CC 02 33        ..3   ldd #0233
9383 FD 00 3B        ..;   stad 003b
9386 7F 00 09        ...   clr 0009
9389 C6 92           ..    ldab #92
938B F7 00 48        ..H   stb 0048
938E 20 0F            .    bra 0f
9390 F6 00 19        ...   ldab 0019
9393 C4 40           .@    andb #40
9395 26 05           &.    bne 05
9397 BD 98 BF        ...   jsr 98bf		;jump146
939A 20 03            .    bra 03
939C BD 85 AF        ...   jsr 85af		;jump14
939F 20 03            .    bra 03
93A1 BD 98 FF        ...   jsr 98ff		;jump145
93A4 39              9     rts 

jump23:
93A5 F6 00 1F        ...   ldab 001f
93A8 C4 02           ..    andb #02
93AA 26 0A           &.    bne 0a
93AC F6 00 3A        ..:   ldab 003a
93AF 26 2B           &+    bne 2b
93B1 F6 00 1D        ...   ldab 001d
93B4 2D 26           -&    blt 26
93B6 CC AF BB        ...   ldd #afbb
93B9 BD C0 BA        ...   jsr c0ba		;display:
93BC CE 00 1F        ...   ldx #001f
93BF 1D 00           ..    bclr add,x 00,x
93C1 02              .     idiv 
93C2 CE 00 1D        ...   ldx #001d
93C5 1D 00           ..    bclr add,x 00,x
93C7 80 C6           ..    suba #c6
93C9 32              2     pula 
93CA F7 00 3A        ..:   stb 003a
93CD F6 0B A8        ...   ldab 0ba8
93D0 F7 00 66        ..f   stb 0066
93D3 BD AA 74        ..t   jsr aa74		;jump128
93D6 7F 00 48        ..H   clr 0048
93D9 7F 00 54        ..T   clr 0054
93DC F6 00 09        ...   ldab 0009
93DF 26 03           &.    bne 03
93E1 7E 94 92        ~..   jmp 9492		;goto14
93E4 F6 00 1D        ...   ldab 001d
93E7 2C 03           ,.    bge 03
93E9 7E 94 92        ~..   jmp 9492		;goto14
93EC F6 00 09        ...   ldab 0009
93EF C1 0B           ..    cmpb #0b
93F1 26 06           &.    bne 06
93F3 BD 85 AF        ...   jsr 85af		;jump14
93F6 7E 94 90        ~..   jmp 9490		;goto15
93F9 F6 00 09        ...   ldab 0009
93FC C1 0A           ..    cmpb #0a
93FE 26 03           &.    bne 03
9400 7F 00 09        ...   clr 0009
9403 F6 00 09        ...   ldab 0009
9406 CB 30           .0    addb #30
9408 37              7     pshb 
9409 F6 00 54        ..T   ldab 0054
940C 4F              O     clra 
940D C3 00 62        ..b   addd #0062
9410 8F              .     xgdx 
9411 33              3     pulb 
9412 E7 00           ..    stb 00,x
9414 BD BE 4B        ..K   jsr be4b		;jump1
9417 F6 00 54        ..T   ldab 0054
941A 26 06           &.    bne 06
941C F6 00 62        ..b   ldab 0062
941F F7 0B 96        ...   stb 0b96
9422 F6 00 54        ..T   ldab 0054
9425 C1 01           ..    cmpb #01
9427 26 0C           &.    bne 0c
9429 F6 00 62        ..b   ldab 0062
942C F7 0B 96        ...   stb 0b96
942F F6 00 63        ..c   ldab 0063
9432 F7 0B 95        ...   stb 0b95
9435 F6 00 54        ..T   ldab 0054
9438 C1 02           ..    cmpb #02
943A 26 12           &.    bne 12
943C F6 00 62        ..b   ldab 0062
943F F7 0B 96        ...   stb 0b96
9442 F6 00 63        ..c   ldab 0063
9445 F7 0B 95        ...   stb 0b95
9448 F6 00 64        ..d   ldab 0064
944B F7 0B 94        ...   stb 0b94
944E F6 00 54        ..T   ldab 0054
9451 C1 03           ..    cmpb #03
9453 26 18           &.    bne 18
9455 F6 00 62        ..b   ldab 0062
9458 F7 0B 96        ...   stb 0b96
945B F6 00 63        ..c   ldab 0063
945E F7 0B 95        ...   stb 0b95
9461 F6 00 64        ..d   ldab 0064
9464 F7 0B 94        ...   stb 0b94
9467 F6 00 65        ..e   ldab 0065
946A F7 0B 93        ...   stb 0b93
946D BD BE 01        ...   jsr be01		;jump70
9470 7C 00 54        |.T   inc 0054
9473 F6 00 54        ..T   ldab 0054
9476 C1 04           ..    cmpb #04
9478 26 0B           &.    bne 0b
947A CE 00 1D        ...   ldx #001d
947D 1C 00           ..    bset add,x 00,x
947F 80 BD           ..    suba #bd
9481 A8 0F           ..    eora 0f,x
9483 20 06            .    bra 06
9485 CE 00 1D        ...   ldx #001d
9488 1D 00           ..    bclr add,x 00,x
948A 80 C6           ..    suba #c6
948C 32              2     pula 
948D F7 00 3A        ..:   stb 003a
goto15:
9490 20 4A            J    bra 4a
goto14:
9492 F6 00 1D        ...   ldab 001d
9495 2C 45           ,E    bge 45
9497 F6 00 09        ...   ldab 0009
949A 4F              O     clra 
949B BD FE 32        ..2   jsr fe32		;jump25
949E 94 B8 00        ...   anda b8
94A1 00              .     test 
94A2 94 BD 00        ...   anda bd
94A5 01              .     nop 
94A6 94 C2 00        ...   anda c2
94A9 04              .     lsrd 
94AA 94 D1 00        ...   anda d1
94AD 05              .     asld 
94AE 94 D9 00        ...   anda d9
94B1 01              .     nop 
94B2 94 DC FF        ...   anda dc
94B5 F5 00 00        ...   bitb 0000
94B8 BD 9E 0F        ...   jsr 9e0f		;jump142
94BB 20 1F            .    bra 1f
94BD BD A7 1F        ...   jsr a71f		;jump134
94C0 20 1A            .    bra 1a
94C2 7C 00 48        |.H   inc 0048
94C5 F6 00 48        ..H   ldab 0048
94C8 C1 0A           ..    cmpb #0a
94CA 23 03           #.    bls 03
94CC 7F 00 48        ..H   clr 0048
94CF 20 0B            .    bra 0b
94D1 BD A5 FE        ...   jsr a5fe		;jump136
94D4 BD 85 AF        ...   jsr 85af		;jump14
94D7 20 03            .    bra 03
94D9 BD 85 AF        ...   jsr 85af		;jump14
94DC 39              9     rts 

jump24:
94DD F6 00 1F        ...   ldab 001f
94E0 C4 02           ..    andb #02
94E2 27 14           '.    beq 14
94E4 CE 00 1F        ...   ldx #001f
94E7 1D 00           ..    bclr add,x 00,x
94E9 02              .     idiv 
94EA CE 00 19        ...   ldx #0019
94ED 1C 00           ..    bset add,x 00,x
94EF 80 7F           ..    suba #7f
94F1 00              .     test 
94F2 09              .     dex 
94F3 7F 00 0A        ...   clr 000a
94F6 20 46            F    bra 46
94F8 F6 00 09        ...   ldab 0009
94FB 4F              O     clra 
94FC BD FE 32        ..2   jsr fe32		;jump25
94FF 95 19 00        ...   bita 19
9502 00              .     test 
9503 95 21 00        .!.   bita 21
9506 01              .     nop 
9507 95 2C 00        .,.   bita 2c
950A 01              .     nop 
950B 95 30 00        .0.   bita 30
950E 01              .     nop 
950F 95 35 00        .5.   bita 35
9512 08              .     inx 
9513 95 38 FF        .8.   bita 38
9516 F5 00 00        ...   bitb 0000
9519 CC B0 A2        ...   ldd #b0a2
951C BD C0 BA        ...   jsr c0ba		;display:
951F 20 1D            .    bra 1d
9521 BD 87 E9        ...   jsr 87e9		;jump74
9524 CE 00 1F        ...   ldx #001f
9527 1C 00           ..    bset add,x 00,x
9529 02              .     idiv 
952A 20 12            .    bra 12
952C 8D 11           ..    bsr dest 11
952E 20 0E            .    bra 0e
9530 BD 96 88        ...   jsr 9688		;jump148
9533 20 09            .    bra 09
9535 BD 85 AF        ...   jsr 85af		;jump14
9538 7F 00 09        ...   clr 0009
953B 7F 00 0A        ...   clr 000a
953E 39              9     rts 

953F F6 00 19        ...   ldab 0019
9542 2D 0A           -.    blt 0a
9544 F6 00 3A        ..:   ldab 003a
9547 26 22           &"    bne 22
9549 F6 00 1D        ...   ldab 001d
954C 2D 1D           -.    blt 1d
954E 7F 00 2C        ..,   clr 002c
9551 CC B0 B8        ...   ldd #b0b8
9554 BD C0 BA        ...   jsr c0ba		;display:
9557 CE 00 19        ...   ldx #0019
955A 1D 00           ..    bclr add,x 00,x
955C 80 CE           ..    suba #ce
955E 00              .     test 
955F 1D 1D           ..    bclr add,x 1d,x
9561 00              .     test 
9562 80 7F           ..    suba #7f
9564 00              .     test 
9565 54              T     lsrb 
9566 C6 32           .2    ldab #32
9568 F7 00 3A        ..:   stb 003a
956B F6 00 0A        ...   ldab 000a
956E 26 03           &.    bne 03
9570 7E 96 5C        ~.\   jmp 965c		;goto16
9573 F6 00 1D        ...   ldab 001d
9576 2C 03           ,.    bge 03
9578 7E 96 5C        ~.\   jmp 965c		;goto16
957B F6 00 0A        ...   ldab 000a
957E C1 0B           ..    cmpb #0b
9580 26 09           &.    bne 09
9582 CE 00 1F        ...   ldx #001f
9585 1C 00           ..    bset add,x 00,x
9587 02              .     idiv 
9588 7E 96 5A        ~.Z   jmp 965a		;goto93
958B F6 00 54        ..T   ldab 0054
958E 26 26           &&    bne 26
9590 BD BE 4B        ..K   jsr be4b		;jump1
9593 F6 00 0A        ...   ldab 000a
9596 C1 0A           ..    cmpb #0a
9598 26 03           &.    bne 03
959A 7F 00 0A        ...   clr 000a
959D F6 00 0A        ...   ldab 000a
95A0 CB 30           .0    addb #30
95A2 37              7     pshb 
95A3 CC 0B 95        ...   ldd #0b95
95A6 F0 00 54        ..T   subb 0054
95A9 82 00           ..    sbca #00
95AB 8F              .     xgdx 
95AC 33              3     pulb 
95AD E7 00           ..    stb 00,x
95AF C6 32           .2    ldab #32
95B1 F7 00 3A        ..:   stb 003a
95B4 20 37            7    bra 37
95B6 F6 00 54        ..T   ldab 0054
95B9 C1 01           ..    cmpb #01
95BB 26 27           &'    bne 27
95BD F6 00 0A        ...   ldab 000a
95C0 C1 05           ..    cmpb #05
95C2 27 20           '     beq 20
95C4 F6 00 0A        ...   ldab 000a
95C7 C1 0A           ..    cmpb #0a
95C9 27 19           '.    beq 19
95CB F6 00 0A        ...   ldab 000a
95CE CB 30           .0    addb #30
95D0 37              7     pshb 
95D1 CC 0B 95        ...   ldd #0b95
95D4 F0 00 54        ..T   subb 0054
95D7 82 00           ..    sbca #00
95D9 8F              .     xgdx 
95DA 33              3     pulb 
95DB E7 00           ..    stb 00,x
95DD C6 32           .2    ldab #32
95DF F7 00 3A        ..:   stb 003a
95E2 20 09            .    bra 09
95E4 CE 00 19        ...   ldx #0019
95E7 1C 00           ..    bset add,x 00,x
95E9 80 7F           ..    suba #7f
95EB 00              .     test 
95EC 3A              :     abx 
95ED BD BE 01        ...   jsr be01		;jump70
95F0 7C 00 54        |.T   inc 0054
95F3 F6 00 54        ..T   ldab 0054
95F6 C1 02           ..    cmpb #02
95F8 26 5A           &Z    bne 5a
95FA CE 00 1D        ...   ldx #001d
95FD 1C 00           ..    bset add,x 00,x
95FF 80 F6           ..    suba #f6
9601 0B              .     sev 
9602 94 CB D0        ...   anda cb
9605 37              7     pshb 
9606 F6 0B 95        ...   ldab 0b95
9609 CB D0           ..    addb #d0
960B 86 0A           ..    ldaa #0a
960D 3D              =     mul 
960E 30              0     tsx 
960F EB 00           ..    addb 00,x
9611 31              1     ins 
9612 F7 00 06        ...   stb 0006
9615 C6 32           .2    ldab #32
9617 F7 00 3A        ..:   stb 003a
961A F6 00 06        ...   ldab 0006
961D BD D7 A0        ...   jsr d7a0		;jump10
9620 83 00 00        ...   subd #0000
9623 26 21           &!    bne 21
9625 BD D3 11        ...   jsr d311		;jump30
9628 83 00 00        ...   subd #0000
962B 26 0B           &.    bne 0b
962D CE 00 1F        ...   ldx #001f
9630 1C 00           ..    bset add,x 00,x
9632 40              @     nega 
9633 BD 8C 73        ..s   jsr 8c73		;jump75
9636 20 06            .    bra 06
9638 CE 00 1F        ...   ldx #001f
963B 1D 00           ..    bclr add,x 00,x
963D 40              @     nega 
963E 7F 00 2C        ..,   clr 002c
9641 BD 9A 47        ..G   jsr 9a47		;jump39
9644 20 0C            .    bra 0c
9646 CE 00 1D        ...   ldx #001d
9649 1D 00           ..    bclr add,x 00,x
964B 80 CE           ..    suba #ce
964D 00              .     test 
964E 19              .     daa 
964F 1C 00           ..    bset add,x 00,x
9651 80 20           .     suba #20
9653 06              .     tap 
9654 CE 00 1D        ...   ldx #001d
9657 1D 00           ..    bclr add,x 00,x
9659 80 20           .     suba #20
; 965a   goto93
965B 2B F6           +.    bmi f6
; 965C
;goto16
965D 00              .     test 
965E 1D 2C           .,    bclr add,x 2c,x
9660 26 BD           &.    bne bd
9662 D2 13 83        ...   sbcb 13
9665 00              .     test 
9666 00              .     test 
9667 26 0F           &.    bne 0f
9669 CC 00 64        ..d   ldd #0064
966C BD E5 71        ..q   jsr e571		;jump13
966F BD E5 D1        ...   jsr e5d1		;jump52
9672 CC 03 84        ...   ldd #0384
9675 BD E5 71        ..q   jsr e571		;jump13
9678 7F 00 2E        ...   clr 002e
967B CE 00 1D        ...   ldx #001d
967E 1D 00           ..    bclr add,x 00,x
9680 80 CE           ..    suba #ce
9682 00              .     test 
9683 1F 1C 00 02     ....  brclr 1c,x 00 02
9687 39              9     rts 

jump148:
9688 34              4     des 
9689 F6 00 19        ...   ldab 0019
968C 2C 19           ,.    bge 19
968E CE 00 19        ...   ldx #0019
9691 1D 00           ..    bclr add,x 00,x
9693 80 CC           ..    suba #cc
9695 B0 97 BD        ...   suba 97bd
9698 C0 BA           ..    subb #ba
969A C6 0A           ..    ldab #0a
969C F7 00 38        ..8   stb 0038
969F 7F 00 05        ...   clr 0005
96A2 C6 01           ..    ldab #01
96A4 F7 00 06        ...   stb 0006
96A7 F6 00 0A        ...   ldab 000a
96AA C1 0B           ..    cmpb #0b
96AC 26 0C           &.    bne 0c
96AE CC 02 33        ..3   ldd #0233
96B1 FD 00 3B        ..;   stad 003b
96B4 CE 00 1F        ...   ldx #001f
96B7 1C 00           ..    bset add,x 00,x
96B9 02              .     idiv 
96BA F6 00 38        ..8   ldab 0038
96BD 27 03           '.    beq 03
96BF 7E 97 6D        ~.m   jmp 976d		;goto17
96C2 F6 00 06        ...   ldab 0006
96C5 C1 63           .c    cmpb #63
96C7 23 03           #.    bls 03
96C9 7E 97 6D        ~.m   jmp 976d		;goto17
96CC F6 00 06        ...   ldab 0006
96CF C1 63           .c    cmpb #63
96D1 26 0F           &.    bne 0f
96D3 BD 99 C3        ...   jsr 99c3		;jump143
96D6 CC 07 D0        ...   ldd #07d0
96D9 BD E5 71        ..q   jsr e571		;jump13
96DC CE 00 1F        ...   ldx #001f
96DF 1C 00           ..    bset add,x 00,x
96E1 02              .     idiv 
96E2 BD D3 11        ...   jsr d311		;jump30
96E5 83 00 00        ...   subd #0000
96E8 26 0B           &.    bne 0b
96EA CE 00 1F        ...   ldx #001f
96ED 1C 00           ..    bset add,x 00,x
96EF 40              @     nega 
96F0 BD 8C 73        ..s   jsr 8c73		;jump75
96F3 20 06            .    bra 06
96F5 CE 00 1F        ...   ldx #001f
96F8 1D 00           ..    bclr add,x 00,x
96FA 40              @     nega 
96FB BD 9A 47        ..G   jsr 9a47		;jump39
96FE F6 00 06        ...   ldab 0006
9701 C1 63           .c    cmpb #63
9703 26 03           &.    bne 03
9705 7F 00 2C        ..,   clr 002c
9708 BD D2 13        ...   jsr d213		;jump45
970B 83 00 00        ...   subd #0000
970E 27 03           '.    beq 03
9710 7C 00 05        |..   inc 0005
9713 7F 00 2E        ...   clr 002e
9716 C6 05           ..    ldab #05
9718 F7 00 38        ..8   stb 0038
971B CC 02 33        ..3   ldd #0233
971E FD 00 3B        ..;   stad 003b
9721 30              0     tsx 
9722 6F 00           o.    clr 00,x
9724 F6 0E 6D        ..m   ldab 0e6d
9727 27 0C           '.    beq 0c
9729 F6 00 06        ...   ldab 0006
972C C1 3E           .>    cmpb #3e
972E 26 05           &.    bne 05
9730 C6 01           ..    ldab #01
9732 30              0     tsx 
9733 E7 00           ..    stb 00,x
9735 8D 38           .8    bsr dest 38
9737 F6 0E 6D        ..m   ldab 0e6d
973A 27 0C           '.    beq 0c
973C 30              0     tsx 
973D E6 00           ..    ldab 00,x
973F C1 01           ..    cmpb #01
9741 26 05           &.    bne 05
9743 C6 45           .E    ldab #45
9745 F7 00 06        ...   stb 0006
9748 F6 00 06        ...   ldab 0006
974B BD D7 A0        ...   jsr d7a0		;jump10
974E 83 00 00        ...   subd #0000
9751 27 15           '.    beq 15
9753 F6 00 06        ...   ldab 0006
9756 C1 09           ..    cmpb #09
9758 26 02           &.    bne 02
975A 8D 13           ..    bsr dest 13
975C F6 00 06        ...   ldab 0006
975F C1 45           .E    cmpb #45
9761 26 05           &.    bne 05
9763 C6 63           .c    ldab #63
9765 F7 00 06        ...   stb 0006
9768 5F              _     clrb 
9769 4F              O     clra 
976A FD 00 40        ..@   stad 0040
goto17:
976D 31              1     ins 
976E 39              9     rts 

jump41:
976F F6 0E 6D        ..m   ldab 0e6d
9772 C1 01           ..    cmpb #01
9774 26 41           &A    bne 41
9776 F6 00 06        ...   ldab 0006
9779 4F              O     clra 
977A CE 00 0A        ...   ldx #000a
977D 02              .     idiv 
977E C1 01           ..    cmpb #01
9780 26 05           &.    bne 05
9782 7C 00 06        |..   inc 0006
9785 20 1B            .    bra 1b
9787 F6 00 06        ...   ldab 0006
978A 4F              O     clra 
978B CE 00 0A        ...   ldx #000a
978E 02              .     idiv 
978F C1 02           ..    cmpb #02
9791 26 0A           &.    bne 0a
9793 C6 09           ..    ldab #09
9795 FB 00 06        ...   addb 0006
9798 F7 00 06        ...   stb 0006
979B 20 05            .    bra 05
979D C6 0B           ..    ldab #0b
979F F7 00 06        ...   stb 0006
97A2 F6 00 06        ...   ldab 0006
97A5 C1 3E           .>    cmpb #3e
97A7 22 07           ".    bhi 07
97A9 F6 00 06        ...   ldab 0006
97AC C1 0B           ..    cmpb #0b
97AE 24 05           $.    bcc 05
97B0 C6 0B           ..    ldab #0b
97B2 F7 00 06        ...   stb 0006
97B5 20 31            1    bra 31
97B7 F6 00 06        ...   ldab 0006
97BA CB 0A           ..    addb #0a
97BC F7 00 06        ...   stb 0006
97BF F6 00 06        ...   ldab 0006
97C2 C1 64           .d    cmpb #64
97C4 23 22           #"    bls 22
97C6 F6 00 06        ...   ldab 0006
97C9 CB 9C           ..    addb #9c
97CB CB 01           ..    addb #01
97CD F7 00 06        ...   stb 0006
97D0 F6 00 06        ...   ldab 0006
97D3 C1 05           ..    cmpb #05
97D5 26 05           &.    bne 05
97D7 C6 06           ..    ldab #06
97D9 F7 00 06        ...   stb 0006
97DC F6 00 06        ...   ldab 0006
97DF C1 0A           ..    cmpb #0a
97E1 26 05           &.    bne 05
97E3 C6 01           ..    ldab #01
97E5 F7 00 06        ...   stb 0006
97E8 39              9     rts 

jump147:
97E9 BD BE 4B        ..K   jsr be4b		;jump1
97EC CC AF E7        ...   ldd #afe7
97EF BD C0 BA        ...   jsr c0ba		;display:
97F2 BD F4 15        ...   jsr f415		;jump112
97F5 39              9     rts 

jump27:
97F6 BD BE 4B        ..K   jsr be4b		;jump1
97F9 F6 00 48        ..H   ldab 0048
97FC 4F              O     clra 
97FD BD FE 16        ...   jsr fe16		;jump2
9800 00              .     test 
9801 01              .     nop 
9802 00              .     test 
9803 03              .     fdiv 
9804 98 80 98        ...   eora 80
9807 0E              .     cli 
9808 98 2B 98        .+.   eora 2b
980B 48              H     asla 
980C 98 65 F6        .e.   eora 65
980F 0C              .     clc 
9810 D9 4F BD        .O.   adcb 4f
9813 E7 95           ..    stb 95,x
9815 C6 4E           .N    ldab #4e
9817 F7 0B 99        ...   stb 0b99
981A C6 49           .I    ldab #49
981C F7 0B 98        ...   stb 0b98
981F C6 4B           .K    ldab #4b
9821 F7 0B 97        ...   stb 0b97
9824 C6 4C           .L    ldab #4c
9826 F7 0B 96        ...   stb 0b96
9829 20 55            U    bra 55
982B F6 0C DA        ...   ldab 0cda
982E 4F              O     clra 
982F BD E7 95        ...   jsr e795		;jump54
9832 C6 44           .D    ldab #44
9834 F7 0B 99        ...   stb 0b99
9837 C6 49           .I    ldab #49
9839 F7 0B 98        ...   stb 0b98
983C C6 4D           .M    ldab #4d
983E F7 0B 97        ...   stb 0b97
9841 C6 45           .E    ldab #45
9843 F7 0B 96        ...   stb 0b96
9846 20 38            8    bra 38
9848 F6 0C DB        ...   ldab 0cdb
984B 4F              O     clra 
984C BD E7 95        ...   jsr e795		;jump54
984F C6 51           .Q    ldab #51
9851 F7 0B 99        ...   stb 0b99
9854 C6 55           .U    ldab #55
9856 F7 0B 98        ...   stb 0b98
9859 C6 54           .T    ldab #54
985B F7 0B 97        ...   stb 0b97
985E C6 52           .R    ldab #52
9860 F7 0B 96        ...   stb 0b96
9863 20 1B            .    bra 1b
9865 F6 0C DC        ...   ldab 0cdc
9868 4F              O     clra 
9869 BD E7 95        ...   jsr e795		;jump54
986C C6 44           .D    ldab #44
986E F7 0B 99        ...   stb 0b99
9871 C6 4F           .O    ldab #4f
9873 F7 0B 98        ...   stb 0b98
9876 C6 4C           .L    ldab #4c
9878 F7 0B 97        ...   stb 0b97
987B C6 52           .R    ldab #52
987D F7 0B 96        ...   stb 0b96
9880 C6 53           .S    ldab #53
9882 F7 0B 95        ...   stb 0b95
9885 C6 20           .     ldab #20
9887 F7 0B 94        ...   stb 0b94
988A C6 20           .     ldab #20
988C F7 0B 93        ...   stb 0b93
988F F6 00 5D        ..]   ldab 005d
9892 F7 0B 92        ...   stb 0b92
9895 F6 00 5E        ..^   ldab 005e
9898 F7 0B 91        ...   stb 0b91
989B F6 00 5F        .._   ldab 005f
989E F7 0B 90        ...   stb 0b90
98A1 BD BE 01        ...   jsr be01		;jump70
98A4 39              9     rts 

jump11:
98A5 BD BE 4B        ..K   jsr be4b		;jump1
98A8 F6 00 54        ..T   ldab 0054
98AB 4F              O     clra 
98AC BD E7 95        ...   jsr e795		;jump54
98AF F6 00 5E        ..^   ldab 005e
98B2 F7 0B 95        ...   stb 0b95
98B5 F6 00 5F        .._   ldab 005f
98B8 F7 0B 94        ...   stb 0b94
98BB BD BE 01        ...   jsr be01		;jump70
98BE 39              9     rts 

jump146:
98BF F6 00 09        ...   ldab 0009
98C2 C1 00           ..    cmpb #00
98C4 26 05           &.    bne 05
98C6 BD 9E 89        ...   jsr 9e89		;jump100
98C9 20 33            3    bra 33
98CB C1 01           ..    cmpb #01
98CD 26 1E           &.    bne 1e
98CF F6 00 48        ..H   ldab 0048
98D2 C1 92           ..    cmpb #92
98D4 26 15           &.    bne 15
98D6 F6 00 1F        ...   ldab 001f
98D9 C4 10           ..    andb #10
98DB 26 08           &.    bne 08
98DD CE 00 1F        ...   ldx #001f
98E0 1C 00           ..    bset add,x 00,x
98E2 10              .     sba 
98E3 20 06            .    bra 06
98E5 CE 00 1F        ...   ldx #001f
98E8 1D 00           ..    bclr add,x 00,x
98EA 10              .     sba 
98EB 20 11            .    bra 11
98ED C1 0B           ..    cmpb #0b
98EF 26 0D           &.    bne 0d
98F1 F6 00 1F        ...   ldab 001f
98F4 C4 10           ..    andb #10
98F6 27 03           '.    beq 03
98F8 BD E9 2B        ..+   jsr e92b		;jump103
98FB BD 85 AF        ...   jsr 85af		;jump14
98FE 39              9     rts 

jump145:
98FF F6 00 1F        ...   ldab 001f
9902 C4 02           ..    andb #02
9904 27 13           '.    beq 13
9906 C6 01           ..    ldab #01
9908 F7 00 48        ..H   stb 0048
990B CE 00 1F        ...   ldx #001f
990E 1D 00           ..    bclr add,x 00,x
9910 10              .     sba 
9911 CE 00 1F        ...   ldx #001f
9914 1D 00           ..    bclr add,x 00,x
9916 02              .     idiv 
9917 20 67            g    bra 67
9919 F6 00 09        ...   ldab 0009
991C 4F              O     clra 
991D BD FE 32        ..2   jsr fe32		;jump25
9920 99 36 00        .6.   adca 36
9923 00              .     test 
9924 99 3B 00        .;.   adca 3b
9927 01              .     nop 
9928 99 59 00        .Y.   adca 59
992B 09              .     dex 
992C 99 73 00        .s.   adca 73
992F 01              .     nop 
9930 99 80 FF        ...   adca 80
9933 F5 00 00        ...   bitb 0000
9936 BD 9E 89        ...   jsr 9e89		;jump100
9939 20 45            E    bra 45
993B F6 00 48        ..H   ldab 0048
993E C1 92           ..    cmpb #92
9940 26 15           &.    bne 15
9942 F6 00 1F        ...   ldab 001f
9945 C4 10           ..    andb #10
9947 26 08           &.    bne 08
9949 CE 00 1F        ...   ldx #001f
994C 1C 00           ..    bset add,x 00,x
994E 10              .     sba 
994F 20 06            .    bra 06
9951 CE 00 1F        ...   ldx #001f
9954 1D 00           ..    bclr add,x 00,x
9956 10              .     sba 
9957 20 27            '    bra 27
9959 7C 00 48        |.H   inc 0048
995C F6 00 48        ..H   ldab 0048
995F C1 92           ..    cmpb #92
9961 23 0B           #.    bls 0b
9963 C6 01           ..    ldab #01
9965 F7 00 48        ..H   stb 0048
9968 CE 00 1F        ...   ldx #001f
996B 1D 00           ..    bclr add,x 00,x
996D 10              .     sba 
996E BD 9E 89        ...   jsr 9e89		;jump100
9971 20 0D            .    bra 0d
9973 F6 00 1F        ...   ldab 001f
9976 C4 10           ..    andb #10
9978 27 03           '.    beq 03
997A BD E9 2B        ..+   jsr e92b		;jump103
997D BD 85 AF        ...   jsr 85af		;jump14
9980 39              9     rts 

jump144:
9981 BD BE 4B        ..K   jsr be4b		;jump1
9984 F6 00 05        ...   ldab 0005
9987 4F              O     clra 
9988 BD E7 95        ...   jsr e795		;jump54
998B C6 4D           .M    ldab #4d
998D F7 0B 99        ...   stb 0b99
9990 C6 4F           .O    ldab #4f
9992 F7 0B 98        ...   stb 0b98
9995 C6 54           .T    ldab #54
9997 F7 0B 97        ...   stb 0b97
999A C6 4F           .O    ldab #4f
999C F7 0B 96        ...   stb 0b96
999F C6 52           .R    ldab #52
99A1 F7 0B 95        ...   stb 0b95
99A4 C6 53           .S    ldab #53
99A6 F7 0B 94        ...   stb 0b94
99A9 C6 20           .     ldab #20
99AB F7 0B 93        ...   stb 0b93
99AE C6 20           .     ldab #20
99B0 F7 0B 92        ...   stb 0b92
99B3 F6 00 5E        ..^   ldab 005e
99B6 F7 0B 91        ...   stb 0b91
99B9 F6 00 5F        .._   ldab 005f
99BC F7 0B 90        ...   stb 0b90
99BF BD BE 01        ...   jsr be01		;jump70
99C2 39              9     rts 

jump143:
99C3 BD BE 4B        ..K   jsr be4b		;jump1
99C6 F6 00 05        ...   ldab 0005
99C9 4F              O     clra 
99CA BD E7 95        ...   jsr e795		;jump54
99CD C6 56           .V    ldab #56
99CF F7 0B 99        ...   stb 0b99
99D2 C6 45           .E    ldab #45
99D4 F7 0B 98        ...   stb 0b98
99D7 C6 4E           .N    ldab #4e
99D9 F7 0B 97        ...   stb 0b97
99DC C6 44           .D    ldab #44
99DE F7 0B 96        ...   stb 0b96
99E1 C6 53           .S    ldab #53
99E3 F7 0B 95        ...   stb 0b95
99E6 C6 20           .     ldab #20
99E8 F7 0B 94        ...   stb 0b94
99EB C6 20           .     ldab #20
99ED F7 0B 93        ...   stb 0b93
99F0 C6 20           .     ldab #20
99F2 F7 0B 92        ...   stb 0b92
99F5 F6 00 5E        ..^   ldab 005e
99F8 F7 0B 91        ...   stb 0b91
99FB F6 00 5F        .._   ldab 005f
99FE F7 0B 90        ...   stb 0b90
9A01 BD BE 01        ...   jsr be01		;jump70
9A04 39              9     rts 

jump102:
9A05 BD BE 4B        ..K   jsr be4b		;jump1
9A08 FC 00 52        ..R   ldd 0052
9A0B BD E7 45        ..E   jsr e745		;jump55
9A0E C6 4F           .O    ldab #4f
9A10 F7 0B 99        ...   stb 0b99
9A13 C6 44           .D    ldab #44
9A15 F7 0B 98        ...   stb 0b98
9A18 C6 44           .D    ldab #44
9A1A F7 0B 97        ...   stb 0b97
9A1D C6 53           .S    ldab #53
9A1F F7 0B 96        ...   stb 0b96
9A22 C6 20           .     ldab #20
9A24 F7 0B 95        ...   stb 0b95
9A27 C6 31           .1    ldab #31
9A29 F7 0B 94        ...   stb 0b94
9A2C C6 2F           ./    ldab #2f
9A2E F7 0B 93        ...   stb 0b93
9A31 F6 00 5D        ..]   ldab 005d
9A34 F7 0B 92        ...   stb 0b92
9A37 F6 00 5E        ..^   ldab 005e
9A3A F7 0B 91        ...   stb 0b91
9A3D F6 00 5F        .._   ldab 005f
9A40 F7 0B 90        ...   stb 0b90
9A43 BD BE 01        ...   jsr be01		;jump70
9A46 39              9     rts 

jump39:
9A47 BD BE 4B        ..K   jsr be4b		;jump1
9A4A F6 00 06        ...   ldab 0006
9A4D 4F              O     clra 
9A4E BD E7 95        ...   jsr e795		;jump54
9A51 F6 00 5E        ..^   ldab 005e
9A54 F7 0B 99        ...   stb 0b99
9A57 F6 00 5F        .._   ldab 005f
9A5A F7 0B 98        ...   stb 0b98
9A5D C6 20           .     ldab #20
9A5F F7 0B 97        ...   stb 0b97
9A62 F6 00 1F        ...   ldab 001f
9A65 C4 40           .@    andb #40
9A67 27 45           'E    beq 45
9A69 FC 00 27        ..'   ldd 0027
9A6C BD E7 45        ..E   jsr e745		;jump55
9A6F F6 00 5A        ..Z   ldab 005a
9A72 F7 0B 96        ...   stb 0b96
9A75 F6 00 5D        ..]   ldab 005d
9A78 F7 0B 95        ...   stb 0b95
9A7B F6 00 5E        ..^   ldab 005e
9A7E F7 0B 94        ...   stb 0b94
9A81 F6 00 5F        .._   ldab 005f
9A84 F7 0B 93        ...   stb 0b93
9A87 F6 00 19        ...   ldab 0019
9A8A C4 10           ..    andb #10
9A8C 27 07           '.    beq 07
9A8E C6 2A           .*    ldab #2a
9A90 F7 0B 92        ...   stb 0b92
9A93 20 05            .    bra 05
9A95 C6 20           .     ldab #20
9A97 F7 0B 92        ...   stb 0b92
9A9A FC 00 46        ..F   ldd 0046
9A9D BD E7 45        ..E   jsr e745		;jump55
9AA0 F6 00 5E        ..^   ldab 005e
9AA3 F7 0B 91        ...   stb 0b91
9AA6 F6 00 5F        .._   ldab 005f
9AA9 F7 0B 90        ...   stb 0b90
9AAC 20 1E            .    bra 1e
9AAE C6 58           .X    ldab #58
9AB0 F7 0B 96        ...   stb 0b96
9AB3 C6 58           .X    ldab #58
9AB5 F7 0B 95        ...   stb 0b95
9AB8 C6 58           .X    ldab #58
9ABA F7 0B 94        ...   stb 0b94
9ABD C6 58           .X    ldab #58
9ABF F7 0B 93        ...   stb 0b93
9AC2 C6 58           .X    ldab #58
9AC4 F7 0B 91        ...   stb 0b91
9AC7 C6 58           .X    ldab #58
9AC9 F7 0B 90        ...   stb 0b90
9ACC F6 00 13        ...   ldab 0013
9ACF C1 01           ..    cmpb #01
9AD1 23 4C           #L    bls 4c
9AD3 F6 0B 91        ...   ldab 0b91
9AD6 F7 0B 90        ...   stb 0b90
9AD9 F6 0B 92        ...   ldab 0b92
9ADC F7 0B 91        ...   stb 0b91
9ADF F6 0B 93        ...   ldab 0b93
9AE2 F7 0B 92        ...   stb 0b92
9AE5 F6 0B 94        ...   ldab 0b94
9AE8 F7 0B 93        ...   stb 0b93
9AEB F6 00 13        ...   ldab 0013
9AEE C1 02           ..    cmpb #02
9AF0 26 07           &.    bne 07
9AF2 C6 2E           ..    ldab #2e
9AF4 F7 0B 94        ...   stb 0b94
9AF7 20 26            &    bra 26
9AF9 C1 04           ..    cmpb #04
9AFB 26 0D           &.    bne 0d
9AFD F6 0B 95        ...   ldab 0b95
9B00 F7 0B 94        ...   stb 0b94
9B03 C6 2E           ..    ldab #2e
9B05 F7 0B 95        ...   stb 0b95
9B08 20 15            .    bra 15
9B0A C1 08           ..    cmpb #08
9B0C 26 11           &.    bne 11
9B0E F6 0B 95        ...   ldab 0b95
9B11 F7 0B 94        ...   stb 0b94
9B14 F6 0B 96        ...   ldab 0b96
9B17 F7 0B 95        ...   stb 0b95
9B1A C6 2E           ..    ldab #2e
9B1C F7 0B 96        ...   stb 0b96
9B1F F6 00 2C        ..,   ldab 002c
9B22 26 3E           &>    bne 3e
9B24 BD BE 69        ..i   jsr be69		;jump3
9B27 F6 00 13        ...   ldab 0013
9B2A C1 01           ..    cmpb #01
9B2C 23 2C           #,    bls 2c
9B2E CE 10 28        ..(   ldx #1028
9B31 1C 00           ..    bset add,x 00,x
9B33 40              @     nega 
9B34 F6 00 1F        ...   ldab 001f
9B37 C4 40           .@    andb #40
9B39 27 0E           '.    beq 0e
9B3B CE 00 01        ...   ldx #0001
9B3E 3C              <     pshx 
9B3F F6 00 5F        .._   ldab 005f
9B42 4F              O     clra 
9B43 BD BD B5        ...   jsr bdb5		;jump78	
9B46 38              8     pulx 
9B47 20 0B            .    bra 0b
9B49 CE 00 01        ...   ldx #0001
9B4C 3C              <     pshx 
9B4D CC 00 58        ..X   ldd #0058
9B50 BD BD B5        ...   jsr bdb5		;jump78	
9B53 38              8     pulx 
9B54 CE 10 28        ..(   ldx #1028
9B57 1D 00           ..    bclr add,x 00,x
9B59 40              @     nega 
9B5A BD BE 10        ...   jsr be10		;jump88
9B5D C6 03           ..    ldab #03
9B5F F7 00 2C        ..,   stb 002c
9B62 39              9     rts 

jump101:
9B63 BD BE 4B        ..K   jsr be4b		;jump1
9B66 FC 00 0D        ...   ldd 000d
9B69 BD E7 45        ..E   jsr e745		;jump55
9B6C C6 44           .D    ldab #44
9B6E F7 0B 99        ...   stb 0b99
9B71 C6 53           .S    ldab #53
9B73 F7 0B 98        ...   stb 0b98
9B76 C6 43           .C    ldab #43
9B78 F7 0B 97        ...   stb 0b97
9B7B C6 4E           .N    ldab #4e
9B7D F7 0B 96        ...   stb 0b96
9B80 C6 54           .T    ldab #54
9B82 F7 0B 95        ...   stb 0b95
9B85 C6 20           .     ldab #20
9B87 F7 0B 94        ...   stb 0b94
9B8A F6 00 5A        ..Z   ldab 005a
9B8D F7 0B 93        ...   stb 0b93
9B90 F6 00 5D        ..]   ldab 005d
9B93 F7 0B 92        ...   stb 0b92
9B96 F6 00 5E        ..^   ldab 005e
9B99 F7 0B 91        ...   stb 0b91
9B9C F6 00 5F        .._   ldab 005f
9B9F F7 0B 90        ...   stb 0b90
9BA2 F6 00 13        ...   ldab 0013
9BA5 C1 01           ..    cmpb #01
9BA7 23 3C           #<    bls 3c
9BA9 F6 0B 91        ...   ldab 0b91
9BAC F7 0B 90        ...   stb 0b90
9BAF F6 00 13        ...   ldab 0013
9BB2 C1 02           ..    cmpb #02
9BB4 26 07           &.    bne 07
9BB6 C6 2E           ..    ldab #2e
9BB8 F7 0B 91        ...   stb 0b91
9BBB 20 28            (    bra 28
9BBD C1 04           ..    cmpb #04
9BBF 26 0D           &.    bne 0d
9BC1 F6 0B 92        ...   ldab 0b92
9BC4 F7 0B 91        ...   stb 0b91
9BC7 C6 2E           ..    ldab #2e
9BC9 F7 0B 92        ...   stb 0b92
9BCC 20 17            .    bra 17
9BCE C1 08           ..    cmpb #08
9BD0 26 13           &.    bne 13
9BD2 F6 0B 92        ...   ldab 0b92
9BD5 F7 0B 91        ...   stb 0b91
9BD8 F6 0B 93        ...   ldab 0b93
9BDB F7 0B 92        ...   stb 0b92
9BDE C6 2E           ..    ldab #2e
9BE0 F7 0B 93        ...   stb 0b93
9BE3 20 00            .    bra 00
9BE5 F6 00 2C        ..,   ldab 002c
9BE8 26 2A           &*    bne 2a
9BEA BD BE 69        ..i   jsr be69		;jump3
9BED F6 00 13        ...   ldab 0013
9BF0 C1 01           ..    cmpb #01
9BF2 23 18           #.    bls 18
9BF4 CE 10 28        ..(   ldx #1028
9BF7 1C 00           ..    bset add,x 00,x
9BF9 40              @     nega 
9BFA CE 00 01        ...   ldx #0001
9BFD 3C              <     pshx 
9BFE F6 00 5F        .._   ldab 005f
9C01 4F              O     clra 
9C02 BD BD B5        ...   jsr bdb5		;jump78	
9C05 38              8     pulx 
9C06 CE 10 28        ..(   ldx #1028
9C09 1D 00           ..    bclr add,x 00,x
9C0B 40              @     nega 
9C0C BD BE 10        ...   jsr be10		;jump88
9C0F C6 03           ..    ldab #03
9C11 F7 00 2C        ..,   stb 002c
9C14 39              9     rts 

jump111:
9C15 BD BE 4B        ..K   jsr be4b		;jump1
9C18 F6 00 48        ..H   ldab 0048
9C1B 4F              O     clra 
9C1C BD FE 16        ...   jsr fe16		;jump2
9C1F 00              .     test 
9C20 00              .     test 
9C21 00              .     test 
9C22 0A              .     clv 
9C23 9D 29 9C        .).   jsr 29
9C26 3B              ;     rti 
9C27 9C 53 9C        .S.   cmpx 53
9C2A 6B              k     illegal 
9C2B 9C 83 9C        ...   cmpx 83
9C2E 9B 9C B2        ...   adda 9c
9C31 9C C9 9C        ...   cmpx c9
9C34 E0 9C           ..    subb 9c,x
9C36 F7 9D 0E        ...   stb 9d0e
9C39 9D 23 F6        .#.   jsr 23
9C3C 00              .     test 
9C3D 67 C4           g.    asr c4,x
9C3F 02              .     idiv 
9C40 27 08           '.    beq 08
9C42 CC AF 00        ...   ldd #af00
9C45 BD C0 BA        ...   jsr c0ba		;display:
9C48 20 06            .    bra 06
9C4A CC AF 0B        ...   ldd #af0b
9C4D BD C0 BA        ...   jsr c0ba		;display:
9C50 7E 9D 29        ~.)   jmp 9d29		;goto18
9C53 F6 00 67        ..g   ldab 0067
9C56 C4 10           ..    andb #10
9C58 27 08           '.    beq 08
9C5A CC AF 42        ..B   ldd #af42
9C5D BD C0 BA        ...   jsr c0ba		;display:
9C60 20 06            .    bra 06
9C62 CC AF 4D        ..M   ldd #af4d
9C65 BD C0 BA        ...   jsr c0ba		;display:
9C68 7E 9D 29        ~.)   jmp 9d29		;goto18
9C6B F6 00 67        ..g   ldab 0067
9C6E C4 20           .     andb #20
9C70 27 08           '.    beq 08
9C72 CC AF 58        ..X   ldd #af58
9C75 BD C0 BA        ...   jsr c0ba		;display:
9C78 20 06            .    bra 06
9C7A CC AF 63        ..c   ldd #af63
9C7D BD C0 BA        ...   jsr c0ba		;display:
9C80 7E 9D 29        ~.)   jmp 9d29		;goto18
9C83 F6 00 67        ..g   ldab 0067
9C86 C4 01           ..    andb #01
9C88 27 08           '.    beq 08
9C8A CC AE EA        ...   ldd #aeea
9C8D BD C0 BA        ...   jsr c0ba		;display:
9C90 20 06            .    bra 06
9C92 CC AE F5        ...   ldd #aef5
9C95 BD C0 BA        ...   jsr c0ba		;display:
9C98 7E 9D 29        ~.)   jmp 9d29		;goto18
9C9B F6 00 67        ..g   ldab 0067
9C9E C4 04           ..    andb #04
9CA0 27 08           '.    beq 08
9CA2 CC AF 16        ...   ldd #af16
9CA5 BD C0 BA        ...   jsr c0ba		;display:
9CA8 20 06            .    bra 06
9CAA CC AF 21        ..!   ldd #af21
9CAD BD C0 BA        ...   jsr c0ba		;display:
9CB0 20 77            w    bra 77
9CB2 F6 00 67        ..g   ldab 0067
9CB5 C4 08           ..    andb #08
9CB7 27 08           '.    beq 08
9CB9 CC AF 2C        ..,   ldd #af2c
9CBC BD C0 BA        ...   jsr c0ba		;display:
9CBF 20 06            .    bra 06
9CC1 CC AF 37        ..7   ldd #af37
9CC4 BD C0 BA        ...   jsr c0ba		;display:
9CC7 20 60            `    bra 60
9CC9 F6 00 67        ..g   ldab 0067
9CCC C4 40           .@    andb #40
9CCE 27 08           '.    beq 08
9CD0 CC AF 84        ...   ldd #af84
9CD3 BD C0 BA        ...   jsr c0ba		;display:
9CD6 20 06            .    bra 06
9CD8 CC AF 8F        ...   ldd #af8f
9CDB BD C0 BA        ...   jsr c0ba		;display:
9CDE 20 49            I    bra 49
9CE0 F6 0E 70        ..p   ldab 0e70
9CE3 C1 01           ..    cmpb #01
9CE5 26 08           &.    bne 08
9CE7 CC AE BE        ...   ldd #aebe
9CEA BD C0 BA        ...   jsr c0ba		;display:
9CED 20 06            .    bra 06
9CEF CC AE C9        ...   ldd #aec9
9CF2 BD C0 BA        ...   jsr c0ba		;display:
9CF5 20 32            2    bra 32
9CF7 F6 0E 6F        ..o   ldab 0e6f
9CFA C1 01           ..    cmpb #01
9CFC 26 08           &.    bne 08
9CFE CC AE D4        ...   ldd #aed4
9D01 BD C0 BA        ...   jsr c0ba		;display:
9D04 20 06            .    bra 06
9D06 CC AE DF        ...   ldd #aedf
9D09 BD C0 BA        ...   jsr c0ba		;display:
9D0C 20 1B            .    bra 1b
9D0E F6 00 67        ..g   ldab 0067
9D11 2C 08           ,.    bge 08
9D13 CC AF 6E        ..n   ldd #af6e
9D16 BD C0 BA        ...   jsr c0ba		;display:
9D19 20 06            .    bra 06
9D1B CC AF 79        ..y   ldd #af79
9D1E BD C0 BA        ...   jsr c0ba		;display:
9D21 20 06            .    bra 06
9D23 CC AF B0        ...   ldd #afb0
9D26 BD C0 BA        ...   jsr c0ba		;display:
goto18:
9D29 39              9     rts 

jump89:
9D2A BD BE 4B        ..K   jsr be4b		;jump1
9D2D F6 00 22        .."   ldab 0022
9D30 4F              O     clra 
9D31 BD E7 95        ...   jsr e795		;jump54
9D34 F6 00 5E        ..^   ldab 005e
9D37 F7 0B 99        ...   stb 0b99
9D3A F6 00 5F        .._   ldab 005f
9D3D F7 0B 98        ...   stb 0b98
9D40 C6 20           .     ldab #20
9D42 F7 0B 97        ...   stb 0b97
9D45 F6 00 24        ..$   ldab 0024
9D48 4F              O     clra 
9D49 BD E7 95        ...   jsr e795		;jump54
9D4C C6 50           .P    ldab #50
9D4E F7 0B 96        ...   stb 0b96
9D51 F6 00 5E        ..^   ldab 005e
9D54 F7 0B 95        ...   stb 0b95
9D57 F6 00 5F        .._   ldab 005f
9D5A F7 0B 94        ...   stb 0b94
9D5D C6 20           .     ldab #20
9D5F F7 0B 93        ...   stb 0b93
9D62 F6 00 2A        ..*   ldab 002a
9D65 4F              O     clra 
9D66 BD E7 95        ...   jsr e795		;jump54
9D69 C6 46           .F    ldab #46
9D6B F7 0B 92        ...   stb 0b92
9D6E F6 00 5E        ..^   ldab 005e
9D71 F7 0B 91        ...   stb 0b91
9D74 F6 00 5F        .._   ldab 005f
9D77 F7 0B 90        ...   stb 0b90
9D7A BD BE 01        ...   jsr be01		;jump70
9D7D 39              9     rts 

9D7E BD BE 4B        ..K   jsr be4b		;jump1
9D81 F6 00 60        ..`   ldab 0060
9D84 4F              O     clra 
9D85 C3 0A 90        ...   addd #0a90
9D88 8F              .     xgdx 
9D89 E6 00           ..    ldab 00,x
9D8B F7 0B 99        ...   stb 0b99
9D8E F6 00 60        ..`   ldab 0060
9D91 4F              O     clra 
9D92 C3 0A 91        ...   addd #0a91
9D95 8F              .     xgdx 
9D96 E6 00           ..    ldab 00,x
9D98 F7 0B 98        ...   stb 0b98
9D9B F6 00 60        ..`   ldab 0060
9D9E 4F              O     clra 
9D9F C3 0A 92        ...   addd #0a92
9DA2 8F              .     xgdx 
9DA3 E6 00           ..    ldab 00,x
9DA5 F7 0B 97        ...   stb 0b97
9DA8 F6 00 60        ..`   ldab 0060
9DAB 4F              O     clra 
9DAC C3 0A 94        ...   addd #0a94
9DAF 8F              .     xgdx 
9DB0 E6 00           ..    ldab 00,x
9DB2 F7 0B 95        ...   stb 0b95
9DB5 F6 00 60        ..`   ldab 0060
9DB8 4F              O     clra 
9DB9 C3 0A 95        ...   addd #0a95
9DBC 8F              .     xgdx 
9DBD E6 00           ..    ldab 00,x
9DBF F7 0B 94        ...   stb 0b94
9DC2 F6 00 60        ..`   ldab 0060
9DC5 4F              O     clra 
9DC6 C3 0A 96        ...   addd #0a96
9DC9 8F              .     xgdx 
9DCA E6 00           ..    ldab 00,x
9DCC F7 0B 93        ...   stb 0b93
9DCF C6 20           .     ldab #20
9DD1 F7 0B 92        ...   stb 0b92
9DD4 C6 20           .     ldab #20
9DD6 F7 0B 91        ...   stb 0b91
9DD9 F6 00 54        ..T   ldab 0054
9DDC 4F              O     clra 
9DDD C3 B4 8F        ...   addd #b48f
9DE0 8F              .     xgdx 
9DE1 E6 00           ..    ldab 00,x
9DE3 F7 0B 90        ...   stb 0b90
9DE6 F6 00 38        ..8   ldab 0038
9DE9 C1 03           ..    cmpb #03
9DEB 24 0F           $.    bcc 0f
9DED F6 00 60        ..`   ldab 0060
9DF0 4F              O     clra 
9DF1 C3 0A 93        ...   addd #0a93
9DF4 8F              .     xgdx 
9DF5 E6 00           ..    ldab 00,x
9DF7 F7 0B 96        ...   stb 0b96
9DFA 20 05            .    bra 05
9DFC C6 1F           ..    ldab #1f
9DFE F7 0B 96        ...   stb 0b96
9E01 BD BE 01        ...   jsr be01		;jump70
9E04 F6 00 38        ..8   ldab 0038
9E07 26 05           &.    bne 05
9E09 C6 05           ..    ldab #05
9E0B F7 00 38        ..8   stb 0038
9E0E 39              9     rts 

jump142:
9E0F BD BE 4B        ..K   jsr be4b		;jump1
9E12 F6 00 48        ..H   ldab 0048
9E15 26 15           &.    bne 15
9E17 F6 00 66        ..f   ldab 0066
9E1A 27 08           '.    beq 08
9E1C CC AF C6        ...   ldd #afc6
9E1F BD C0 BA        ...   jsr c0ba		;display:
9E22 20 06            .    bra 06
9E24 CC AF D1        ...   ldd #afd1
9E27 BD C0 BA        ...   jsr c0ba		;display:
9E2A 20 5C            \    bra 5c
9E2C C6 4D           .M    ldab #4d
9E2E F7 0B 99        ...   stb 0b99
9E31 C6 45           .E    ldab #45
9E33 F7 0B 98        ...   stb 0b98
9E36 C6 4E           .N    ldab #4e
9E38 F7 0B 97        ...   stb 0b97
9E3B C6 55           .U    ldab #55
9E3D F7 0B 96        ...   stb 0b96
9E40 C6 20           .     ldab #20
9E42 F7 0B 95        ...   stb 0b95
9E45 F6 00 48        ..H   ldab 0048
9E48 CB 30           .0    addb #30
9E4A F7 0B 94        ...   stb 0b94
9E4D F6 00 48        ..H   ldab 0048
9E50 C1 0A           ..    cmpb #0a
9E52 26 05           &.    bne 05
9E54 C6 30           .0    ldab #30
9E56 F7 0B 94        ...   stb 0b94
9E59 C6 20           .     ldab #20
9E5B F7 0B 93        ...   stb 0b93
9E5E C6 4F           .O    ldab #4f
9E60 F7 0B 92        ...   stb 0b92
9E63 F6 00 48        ..H   ldab 0048
9E66 4F              O     clra 
9E67 C3 00 68        ..h   addd #0068
9E6A 8F              .     xgdx 
9E6B E6 00           ..    ldab 00,x
9E6D 27 0C           '.    beq 0c
9E6F C6 4E           .N    ldab #4e
9E71 F7 0B 91        ...   stb 0b91
9E74 C6 20           .     ldab #20
9E76 F7 0B 90        ...   stb 0b90
9E79 20 0A            .    bra 0a
9E7B C6 46           .F    ldab #46
9E7D F7 0B 91        ...   stb 0b91
9E80 C6 46           .F    ldab #46
9E82 F7 0B 90        ...   stb 0b90
9E85 BD BE 01        ...   jsr be01		;jump70
9E88 39              9     rts 

jump100:
9E89 BD BE 4B        ..K   jsr be4b		;jump1
9E8C F6 00 48        ..H   ldab 0048
9E8F 4F              O     clra 
9E90 BD FE 16        ...   jsr fe16		;jump2
9E93 00              .     test 
9E94 01              .     nop 
9E95 00              .     test 
9E96 2D A1           -.    blt a1
9E98 C4 9E           ..    andb #9e
9E9A F5 9E FE        ...   bitb 9efe
9E9D 9F 07 9F        ...   sts 07
9EA0 10              .     sba 
9EA1 9F 19 9F        ...   sts 19
9EA4 3B              ;     rti 
9EA5 9F 44 9F        .D.   sts 44
9EA8 52              R     illegal 
9EA9 9F 5B 9F        .[.   sts 5b
9EAC 69              i     rol 
9EAD 9F 72 9F        .r.   sts 72
9EB0 80 9F           ..    suba #9f
9EB2 89 9F           ..    adca #9f
9EB4 97 9F A0        ...   sta 9f
9EB7 9F AE 9F        ...   sts ae
9EBA B7 9F C5        ...   sta 9fc5
9EBD 9F DD 9F        ...   sts dd
9EC0 EB A0           ..    addb a0,x
9EC2 03              .     fdiv 
9EC3 A0 11           ..    suba 11,x
9EC5 A0 29           .)    suba 29,x
9EC7 A0 37           .7    suba 37,x
9EC9 A0 54           .T    suba 54,x
9ECB A0 62           .b    suba 62,x
9ECD A0 7F           ..    suba 7f,x
9ECF A0 8D           ..    suba 8d,x
9ED1 A0 96           ..    suba 96,x
9ED3 A0 A4           ..    suba a4,x
9ED5 A0 AD           ..    suba ad,x
9ED7 A0 BC           ..    suba bc,x
9ED9 A0 C5           ..    suba c5,x
9EDB A0 D4           ..    suba d4,x
9EDD A0 DD           ..    suba dd,x
9EDF A0 EC           ..    suba ec,x
9EE1 A0 F5           ..    suba f5,x
9EE3 A1 04           ..    cmpa 04,x
9EE5 A1 26           .&    cmpa 26,x
9EE7 A1 34           .4    cmpa 34,x
9EE9 A1 55           .U    cmpa 55,x
9EEB A1 62           .b    cmpa 62,x
9EED A1 83           ..    cmpa 83,x
9EEF A1 90           ..    cmpa 90,x
9EF1 A1 B1           ..    cmpa b1,x
9EF3 A1 BE           ..    cmpa be,x
9EF5 CC B2 90        ...   ldd #b290
9EF8 BD C0 BA        ...   jsr c0ba		;display:
9EFB 7E A1 C4        ~..   jmp a1c4		;goto19
9EFE CC 0C BC        ...   ldd #0cbc
9F01 BD C0 BA        ...   jsr c0ba		;display:
9F04 7E A1 C4        ~..   jmp a1c4		;goto19
9F07 CC B2 A2        ...   ldd #b2a2
9F0A BD C0 BA        ...   jsr c0ba		;display:
9F0D 7E A1 C4        ~..   jmp a1c4		;goto19
9F10 CC 0C CC        ...   ldd #0ccc
9F13 BD C0 BA        ...   jsr c0ba		;display:
9F16 7E A1 C4        ~..   jmp a1c4		;goto19
9F19 C6 41           .A    ldab #41
9F1B F7 0B 99        ...   stb 0b99
9F1E C6 55           .U    ldab #55
9F20 F7 0B 98        ...   stb 0b98
9F23 C6 44           .D    ldab #44
9F25 F7 0B 97        ...   stb 0b97
9F28 C6 54           .T    ldab #54
9F2A F7 0B 96        ...   stb 0b96
9F2D CE 00 00        ...   ldx #0000
9F30 3C              <     pshx 
9F31 FC 0C E7        ...   ldd 0ce7
9F34 BD A2 D4        ...   jsr a2d4		;jump79
9F37 38              8     pulx 
9F38 7E A1 C4        ~..   jmp a1c4		;goto19
9F3B CC B2 C6        ...   ldd #b2c6
9F3E BD C0 BA        ...   jsr c0ba		;display:
9F41 7E A1 C4        ~..   jmp a1c4		;goto19
9F44 FE 0D E9        ...   ldx 0de9
9F47 3C              <     pshx 
9F48 FC 0D E7        ...   ldd 0de7
9F4B BD A3 41        ..A   jsr a341		;jump77
9F4E 38              8     pulx 
9F4F 7E A1 C4        ~..   jmp a1c4		;goto19
9F52 CC B2 D8        ...   ldd #b2d8
9F55 BD C0 BA        ...   jsr c0ba		;display:
9F58 7E A1 C4        ~..   jmp a1c4		;goto19
9F5B FE 0D ED        ...   ldx 0ded
9F5E 3C              <     pshx 
9F5F FC 0D EB        ...   ldd 0deb
9F62 BD A3 41        ..A   jsr a341		;jump77
9F65 38              8     pulx 
9F66 7E A1 C4        ~..   jmp a1c4		;goto19
9F69 CC B2 EA        ...   ldd #b2ea
9F6C BD C0 BA        ...   jsr c0ba		;display:
9F6F 7E A1 C4        ~..   jmp a1c4		;goto19
9F72 FE 0D F1        ...   ldx 0df1
9F75 3C              <     pshx 
9F76 FC 0D EF        ...   ldd 0def
9F79 BD A3 41        ..A   jsr a341		;jump77
9F7C 38              8     pulx 
9F7D 7E A1 C4        ~..   jmp a1c4		;goto19
9F80 CC B2 FC        ...   ldd #b2fc
9F83 BD C0 BA        ...   jsr c0ba		;display:
9F86 7E A1 C4        ~..   jmp a1c4		;goto19
9F89 FE 0D F9        ...   ldx 0df9
9F8C 3C              <     pshx 
9F8D FC 0D F7        ...   ldd 0df7
9F90 BD A3 41        ..A   jsr a341		;jump77
9F93 38              8     pulx 
9F94 7E A1 C4        ~..   jmp a1c4		;goto19
9F97 CC B3 0E        ...   ldd #b30e
9F9A BD C0 BA        ...   jsr c0ba		;display:
9F9D 7E A1 C4        ~..   jmp a1c4		;goto19
9FA0 FE 0D FD        ...   ldx 0dfd
9FA3 3C              <     pshx 
9FA4 FC 0D FB        ...   ldd 0dfb
9FA7 BD A3 41        ..A   jsr a341		;jump77
9FAA 38              8     pulx 
9FAB 7E A1 C4        ~..   jmp a1c4		;goto19
9FAE CC B3 35        ..5   ldd #b335
9FB1 BD C0 BA        ...   jsr c0ba		;display:
9FB4 7E A1 C4        ~..   jmp a1c4		;goto19
9FB7 FE 0D F5        ...   ldx 0df5
9FBA 3C              <     pshx 
9FBB FC 0D F3        ...   ldd 0df3
9FBE BD A3 41        ..A   jsr a341		;jump77
9FC1 38              8     pulx 
9FC2 7E A1 C4        ~..   jmp a1c4		;goto19
9FC5 C6 24           .$    ldab #24
9FC7 F7 0B 99        ...   stb 0b99
9FCA C6 31           .1    ldab #31
9FCC F7 0B 98        ...   stb 0b98
9FCF CE 00 00        ...   ldx #0000
9FD2 3C              <     pshx 
9FD3 FC 0C DD        ...   ldd 0cdd
9FD6 BD A2 D4        ...   jsr a2d4		;jump79
9FD9 38              8     pulx 
9FDA 7E A1 C4        ~..   jmp a1c4		;goto19
9FDD FE 0C F5        ...   ldx 0cf5
9FE0 3C              <     pshx 
9FE1 FC 0C F3        ...   ldd 0cf3
9FE4 BD A3 41        ..A   jsr a341		;jump77
9FE7 38              8     pulx 
9FE8 7E A1 C4        ~..   jmp a1c4		;goto19
9FEB C6 24           .$    ldab #24
9FED F7 0B 99        ...   stb 0b99
9FF0 C6 32           .2    ldab #32
9FF2 F7 0B 98        ...   stb 0b98
9FF5 CE 00 00        ...   ldx #0000
9FF8 3C              <     pshx 
9FF9 FC 0C DF        ...   ldd 0cdf
9FFC BD A2 D4        ...   jsr a2d4		;jump79
9FFF 38              8     pulx 
A000 7E A1 C4        ~..   jmp a1c4		;goto19
A003 FE 0C F9        ...   ldx 0cf9
A006 3C              <     pshx 
A007 FC 0C F7        ...   ldd 0cf7
A00A BD A3 41        ..A   jsr a341		;jump77
A00D 38              8     pulx 
A00E 7E A1 C4        ~..   jmp a1c4		;goto19
A011 C6 24           .$    ldab #24
A013 F7 0B 99        ...   stb 0b99
A016 C6 35           .5    ldab #35
A018 F7 0B 98        ...   stb 0b98
A01B CE 00 00        ...   ldx #0000
A01E 3C              <     pshx 
A01F FC 0C E1        ...   ldd 0ce1
A022 BD A2 D4        ...   jsr a2d4		;jump79
A025 38              8     pulx 
A026 7E A1 C4        ~..   jmp a1c4		;goto19
A029 FE 0C FD        ...   ldx 0cfd
A02C 3C              <     pshx 
A02D FC 0C FB        ...   ldd 0cfb
A030 BD A3 41        ..A   jsr a341		;jump77
A033 38              8     pulx 
A034 7E A1 C4        ~..   jmp a1c4		;goto19
A037 C6 24           .$    ldab #24
A039 F7 0B 99        ...   stb 0b99
A03C C6 31           .1    ldab #31
A03E F7 0B 98        ...   stb 0b98
A041 C6 30           .0    ldab #30
A043 F7 0B 97        ...   stb 0b97
A046 CE 00 00        ...   ldx #0000
A049 3C              <     pshx 
A04A FC 0C E3        ...   ldd 0ce3
A04D BD A2 D4        ...   jsr a2d4		;jump79
A050 38              8     pulx 
A051 7E A1 C4        ~..   jmp a1c4		;goto19
A054 FE 0D 01        ...   ldx 0d01
A057 3C              <     pshx 
A058 FC 0C FF        ...   ldd 0cff
A05B BD A3 41        ..A   jsr a341		;jump77
A05E 38              8     pulx 
A05F 7E A1 C4        ~..   jmp a1c4		;goto19
A062 C6 24           .$    ldab #24
A064 F7 0B 99        ...   stb 0b99
A067 C6 32           .2    ldab #32
A069 F7 0B 98        ...   stb 0b98
A06C C6 30           .0    ldab #30
A06E F7 0B 97        ...   stb 0b97
A071 CE 00 00        ...   ldx #0000
A074 3C              <     pshx 
A075 FC 0C E5        ...   ldd 0ce5
A078 BD A2 D4        ...   jsr a2d4		;jump79
A07B 38              8     pulx 
A07C 7E A1 C4        ~..   jmp a1c4		;goto19
A07F FE 0D 05        ...   ldx 0d05
A082 3C              <     pshx 
A083 FC 0D 03        ...   ldd 0d03
A086 BD A3 41        ..A   jsr a341		;jump77
A089 38              8     pulx 
A08A 7E A1 C4        ~..   jmp a1c4		;goto19
A08D CC B3 B7        ...   ldd #b3b7
A090 BD C0 BA        ...   jsr c0ba		;display:
A093 7E A1 C4        ~..   jmp a1c4		;goto19
A096 CE 00 01        ...   ldx #0001
A099 3C              <     pshx 
A09A FC 0C F1        ...   ldd 0cf1
A09D BD A2 D4        ...   jsr a2d4		;jump79
A0A0 38              8     pulx 
A0A1 7E A1 C4        ~..   jmp a1c4		;goto19
A0A4 CC B3 CE        ...   ldd #b3ce
A0A7 BD C0 BA        ...   jsr c0ba		;display:
A0AA 7E A1 C4        ~..   jmp a1c4		;goto19
A0AD FE 0C E9        ...   ldx 0ce9
A0B0 3C              <     pshx 
A0B1 F6 0C DC        ...   ldab 0cdc
A0B4 4F              O     clra 
A0B5 BD A3 1A        ...   jsr a31a		;jump93
A0B8 38              8     pulx 
A0B9 7E A1 C4        ~..   jmp a1c4		;goto19
A0BC CC B3 E0        ...   ldd #b3e0
A0BF BD C0 BA        ...   jsr c0ba		;display:
A0C2 7E A1 C4        ~..   jmp a1c4		;goto19
A0C5 FE 0C EB        ...   ldx 0ceb
A0C8 3C              <     pshx 
A0C9 F6 0C DB        ...   ldab 0cdb
A0CC 4F              O     clra 
A0CD BD A3 1A        ...   jsr a31a		;jump93
A0D0 38              8     pulx 
A0D1 7E A1 C4        ~..   jmp a1c4		;goto19
A0D4 CC B3 F2        ...   ldd #b3f2
A0D7 BD C0 BA        ...   jsr c0ba		;display:
A0DA 7E A1 C4        ~..   jmp a1c4		;goto19
A0DD FE 0C ED        ...   ldx 0ced
A0E0 3C              <     pshx 
A0E1 F6 0C DA        ...   ldab 0cda
A0E4 4F              O     clra 
A0E5 BD A3 1A        ...   jsr a31a		;jump93
A0E8 38              8     pulx 
A0E9 7E A1 C4        ~..   jmp a1c4		;goto19
A0EC CC B4 04        ...   ldd #b404
A0EF BD C0 BA        ...   jsr c0ba		;display:
A0F2 7E A1 C4        ~..   jmp a1c4		;goto19
A0F5 FE 0C EF        ...   ldx 0cef
A0F8 3C              <     pshx 
A0F9 F6 0C D9        ...   ldab 0cd9
A0FC 4F              O     clra 
A0FD BD A3 1A        ...   jsr a31a		;jump93
A100 38              8     pulx 
A101 7E A1 C4        ~..   jmp a1c4		;goto19
A104 C6 57           .W    ldab #57
A106 F7 0B 99        ...   stb 0b99
A109 C6 53           .S    ldab #53
A10B F7 0B 98        ...   stb 0b98
A10E C6 4E           .N    ldab #4e
A110 F7 0B 97        ...   stb 0b97
A113 C6 4B           .K    ldab #4b
A115 F7 0B 96        ...   stb 0b96
A118 CE 00 00        ...   ldx #0000
A11B 3C              <     pshx 
A11C FC 0D 07        ...   ldd 0d07
A11F BD A2 D4        ...   jsr a2d4		;jump79
A122 38              8     pulx 
A123 7E A1 C4        ~..   jmp a1c4		;goto19
A126 FE 0D 11        ...   ldx 0d11
A129 3C              <     pshx 
A12A FC 0D 0F        ...   ldd 0d0f
A12D BD A3 41        ..A   jsr a341		;jump77
A130 38              8     pulx 
A131 7E A1 C4        ~..   jmp a1c4		;goto19
A134 C6 53           .S    ldab #53
A136 F7 0B 99        ...   stb 0b99
A139 C6 48           .H    ldab #48
A13B F7 0B 98        ...   stb 0b98
A13E C6 50           .P    ldab #50
A140 F7 0B 97        ...   stb 0b97
A143 C6 52           .R    ldab #52
A145 F7 0B 96        ...   stb 0b96
A148 CE 00 00        ...   ldx #0000
A14B 3C              <     pshx 
A14C FC 0D 09        ...   ldd 0d09
A14F BD A2 D4        ...   jsr a2d4		;jump79
A152 38              8     pulx 
A153 20 6F            o    bra 6f
A155 FE 0D 15        ...   ldx 0d15
A158 3C              <     pshx 
A159 FC 0D 13        ...   ldd 0d13
A15C BD A3 41        ..A   jsr a341		;jump77
A15F 38              8     pulx 
A160 20 62            b    bra 62
A162 C6 50           .P    ldab #50
A164 F7 0B 99        ...   stb 0b99
A167 C6 52           .R    ldab #52
A169 F7 0B 98        ...   stb 0b98
A16C C6 4D           .M    ldab #4d
A16E F7 0B 97        ...   stb 0b97
A171 C6 4F           .O    ldab #4f
A173 F7 0B 96        ...   stb 0b96
A176 CE 00 00        ...   ldx #0000
A179 3C              <     pshx 
A17A FC 0D 0B        ...   ldd 0d0b
A17D BD A2 D4        ...   jsr a2d4		;jump79
A180 38              8     pulx 
A181 20 41            A    bra 41
A183 FE 0D 19        ...   ldx 0d19
A186 3C              <     pshx 
A187 FC 0D 17        ...   ldd 0d17
A18A BD A3 41        ..A   jsr a341		;jump77
A18D 38              8     pulx 
A18E 20 34            4    bra 34
A190 C6 46           .F    ldab #46
A192 F7 0B 99        ...   stb 0b99
A195 C6 52           .R    ldab #52
A197 F7 0B 98        ...   stb 0b98
A19A C6 45           .E    ldab #45
A19C F7 0B 97        ...   stb 0b97
A19F C6 45           .E    ldab #45
A1A1 F7 0B 96        ...   stb 0b96
A1A4 CE 00 00        ...   ldx #0000
A1A7 3C              <     pshx 
A1A8 FC 0D 0D        ...   ldd 0d0d
A1AB BD A2 D4        ...   jsr a2d4		;jump79
A1AE 38              8     pulx 
A1AF 20 13            .    bra 13
A1B1 FE 0D 1D        ...   ldx 0d1d
A1B4 3C              <     pshx 
A1B5 FC 0D 1B        ...   ldd 0d1b
A1B8 BD A3 41        ..A   jsr a341		;jump77
A1BB 38              8     pulx 
A1BC 20 06            .    bra 06
A1BE CC B4 84        ...   ldd #b484
A1C1 BD C0 BA        ...   jsr c0ba		;display:
goto19:
A1C4 F6 00 48        ..H   ldab 0048
A1C7 C1 2E           ..    cmpb #2e
A1C9 23 74           #t    bls 74
A1CB F6 00 48        ..H   ldab 0048
A1CE C1 92           ..    cmpb #92
A1D0 24 6D           $m    bcc 6d
A1D2 F6 00 48        ..H   ldab 0048
A1D5 4F              O     clra 
A1D6 05              .     asld 
A1D7 C3 08 6C        ..l   addd #086c
A1DA 8F              .     xgdx 
A1DB EC 00           ..    ldd 00,x
A1DD C4 80           ..    andb #80
A1DF 26 0E           &.    bne 0e
A1E1 7C 00 48        |.H   inc 0048
A1E4 F6 00 48        ..H   ldab 0048
A1E7 C1 92           ..    cmpb #92
A1E9 26 02           &.    bne 02
A1EB 20 02            .    bra 02
A1ED 20 E3            .    bra e3
A1EF F6 00 48        ..H   ldab 0048
A1F2 4F              O     clra 
A1F3 C3 FF D2        ...   addd #ffd2
A1F6 BD E7 95        ...   jsr e795		;jump54
A1F9 F6 00 5E        ..^   ldab 005e
A1FC F7 0B 99        ...   stb 0b99
A1FF F6 00 5F        .._   ldab 005f
A202 F7 0B 98        ...   stb 0b98
A205 F6 00 48        ..H   ldab 0048
A208 4F              O     clra 
A209 05              .     asld 
A20A C3 0C C3        ...   addd #0cc3
A20D 8F              .     xgdx 
A20E EC 00           ..    ldd 00,x
A210 BD E7 45        ..E   jsr e745		;jump55
A213 F6 00 57        ..W   ldab 0057
A216 F7 0B 94        ...   stb 0b94
A219 F6 00 5A        ..Z   ldab 005a
A21C F7 0B 93        ...   stb 0b93
A21F F6 00 5D        ..]   ldab 005d
A222 F7 0B 92        ...   stb 0b92
A225 F6 00 5E        ..^   ldab 005e
A228 F7 0B 91        ...   stb 0b91
A22B F6 00 5F        .._   ldab 005f
A22E F7 0B 90        ...   stb 0b90
A231 5F              _     clrb 
A232 4F              O     clra 
A233 8D 27           .'    bsr dest 27
A235 F6 00 48        ..H   ldab 0048
A238 C1 92           ..    cmpb #92
A23A 26 03           &.    bne 03
A23C 7F 00 2C        ..,   clr 002c
A23F F6 00 48        ..H   ldab 0048
A242 C1 92           ..    cmpb #92
A244 26 15           &.    bne 15
A246 F6 00 1F        ...   ldab 001f
A249 C4 10           ..    andb #10
A24B 27 08           '.    beq 08
A24D CC AF 9A        ...   ldd #af9a
A250 BD C0 BA        ...   jsr c0ba		;display:
A253 20 06            .    bra 06
A255 CC AF A5        ...   ldd #afa5
A258 BD C0 BA        ...   jsr c0ba		;display:
A25B 39              9     rts 

jump82:
A25C 37              7     pshb 
A25D 36              6     psha 
A25E F6 00 2C        ..,   ldab 002c
A261 26 6F           &o    bne 6f
A263 BD BE 69        ..i   jsr be69		;jump3
A266 30              0     tsx 
A267 E6 01           ..    ldab 01,x
A269 27 5F           '_    beq 5f
A26B F6 00 13        ...   ldab 0013
A26E C1 01           ..    cmpb #01
A270 23 58           #X    bls 58
A272 CE 10 28        ..(   ldx #1028
A275 1C 00           ..    bset add,x 00,x
A277 40              @     nega 
A278 CE 00 01        ...   ldx #0001
A27B 3C              <     pshx 
A27C F6 0B 90        ...   ldab 0b90
A27F 4F              O     clra 
A280 BD BD B5        ...   jsr bdb5		;jump78	
A283 38              8     pulx 
A284 CE 10 28        ..(   ldx #1028
A287 1D 00           ..    bclr add,x 00,x
A289 40              @     nega 
A28A F6 0B 91        ...   ldab 0b91
A28D F7 0B 90        ...   stb 0b90
A290 F6 00 13        ...   ldab 0013
A293 C1 02           ..    cmpb #02
A295 26 07           &.    bne 07
A297 C6 2E           ..    ldab #2e
A299 F7 0B 91        ...   stb 0b91
A29C 20 2C            ,    bra 2c
A29E F6 00 13        ...   ldab 0013
A2A1 C1 04           ..    cmpb #04
A2A3 26 0D           &.    bne 0d
A2A5 F6 0B 92        ...   ldab 0b92
A2A8 F7 0B 91        ...   stb 0b91
A2AB C6 2E           ..    ldab #2e
A2AD F7 0B 92        ...   stb 0b92
A2B0 20 18            .    bra 18
A2B2 F6 00 13        ...   ldab 0013
A2B5 C1 08           ..    cmpb #08
A2B7 26 11           &.    bne 11
A2B9 F6 0B 92        ...   ldab 0b92
A2BC F7 0B 91        ...   stb 0b91
A2BF F6 0B 93        ...   ldab 0b93
A2C2 F7 0B 92        ...   stb 0b92
A2C5 C6 2E           ..    ldab #2e
A2C7 F7 0B 93        ...   stb 0b93
A2CA BD BE 10        ...   jsr be10		;jump88
A2CD C6 03           ..    ldab #03
A2CF F7 00 2C        ..,   stb 002c
A2D2 38              8     pulx 
A2D3 39              9     rts 

jump79:
A2D4 37              7     pshb 
A2D5 36              6     psha 
A2D6 30              0     tsx 
A2D7 EC 00           ..    ldd 00,x
A2D9 BD E7 45        ..E   jsr e745		;jump55
A2DC F6 00 57        ..W   ldab 0057
A2DF F7 0B 94        ...   stb 0b94
A2E2 F6 00 5A        ..Z   ldab 005a
A2E5 F7 0B 93        ...   stb 0b93
A2E8 F6 00 5D        ..]   ldab 005d
A2EB F7 0B 92        ...   stb 0b92
A2EE F6 00 5E        ..^   ldab 005e
A2F1 F7 0B 91        ...   stb 0b91
A2F4 F6 00 5F        .._   ldab 005f
A2F7 F7 0B 90        ...   stb 0b90
A2FA 30              0     tsx 
A2FB E6 05           ..    ldab 05,x
A2FD 27 14           '.    beq 14
A2FF F6 00 17        ...   ldab 0017
A302 C4 08           ..    andb #08
A304 26 05           &.    bne 05
A306 C6 24           .$    ldab #24
A308 F7 0B 95        ...   stb 0b95
A30B CC 00 01        ...   ldd #0001
A30E BD A2 5C        ..\   jsr a25c		;jump82
A311 20 05            .    bra 05
A313 5F              _     clrb 
A314 4F              O     clra 
A315 BD A2 5C        ..\   jsr a25c		;jump82
A318 38              8     pulx 
A319 39              9     rts 

jump93:
A31A 37              7     pshb 
A31B 36              6     psha 
A31C 30              0     tsx 
A31D E6 01           ..    ldab 01,x
A31F 4F              O     clra 
A320 BD E7 95        ...   jsr e795		;jump54
A323 F6 00 5D        ..]   ldab 005d
A326 F7 0B 99        ...   stb 0b99
A329 F6 00 5E        ..^   ldab 005e
A32C F7 0B 98        ...   stb 0b98
A32F F6 00 5F        .._   ldab 005f
A332 F7 0B 97        ...   stb 0b97
A335 CE 00 01        ...   ldx #0001
A338 3C              <     pshx 
A339 30              0     tsx 
A33A EC 06           ..    ldd 06,x
A33C 8D 96           ..    bsr dest 96
A33E 38              8     pulx 
A33F 38              8     pulx 
A340 39              9     rts 

jump77:
A341 38              8     pulx 
A342 37              7     pshb 
A343 36              6     psha 
A344 3C              <     pshx 
A345 30              0     tsx 
A346 EC 04           ..    ldd 04,x
A348 37              7     pshb 
A349 36              6     psha 
A34A EC 02           ..    ldd 02,x
A34C BD E6 21        ..!   jsr e621		;jump56
A34F 38              8     pulx 
A350 C6 20           .     ldab #20
A352 F7 0B 99        ...   stb 0b99
A355 F6 00 17        ...   ldab 0017
A358 C4 08           ..    andb #08
A35A 26 05           &.    bne 05
A35C C6 24           .$    ldab #24
A35E F7 0B 98        ...   stb 0b98
A361 CC 00 01        ...   ldd #0001
A364 BD A2 5C        ..\   jsr a25c		;jump82
A367 38              8     pulx 
A368 31              1     ins 
A369 31              1     ins 
A36A 6E 00           n.    jmp 00,x

jump81:
A36C BD BE 4B        ..K   jsr be4b		;jump1
A36F CC AF DC        ...   ldd #afdc
A372 BD C0 BA        ...   jsr c0ba		;display:
A375 CC 01 F4        ...   ldd #01f4
A378 BD E5 71        ..q   jsr e571		;jump13
A37B 39              9     rts 

jump141:
A37C FC 00 52        ..R   ldd 0052
A37F FD 0B B5        ...   stad 0bb5
A382 8D E8           ..    bsr dest e8
A384 39              9     rts 

jump40:
A385 F6 00 1F        ...   ldab 001f
A388 C4 40           .@    andb #40
A38A 27 73           's    beq 73
A38C F6 00 06        ...   ldab 0006
A38F 4F              O     clra 
A390 05              .     asld 
A391 C3 08 00        ...   addd #0800
A394 8F              .     xgdx 
A395 EC 00           ..    ldd 00,x
A397 5F              _     clrb 
A398 84 C0           ..    anda #c0
A39A FA 00 28        ..(   orb 0028
A39D BA 00 27        ..'   ora 0027
A3A0 FD 00 27        ..'   stad 0027
A3A3 FC 00 27        ..'   ldd 0027
A3A6 37              7     pshb 
A3A7 36              6     psha 
A3A8 F6 00 06        ...   ldab 0006
A3AB 4F              O     clra 
A3AC 05              .     asld 
A3AD C3 08 00        ...   addd #0800
A3B0 38              8     pulx 
A3B1 8F              .     xgdx 
A3B2 ED 00           ..    stad 00,x
A3B4 FC 00 27        ..'   ldd 0027
A3B7 43              C     coma 
A3B8 53              S     comb 
A3B9 37              7     pshb 
A3BA 36              6     psha 
A3BB F6 00 06        ...   ldab 0006
A3BE 4F              O     clra 
A3BF 05              .     asld 
A3C0 C3 0B F1        ...   addd #0bf1
A3C3 38              8     pulx 
A3C4 8F              .     xgdx 
A3C5 ED 00           ..    stad 00,x
A3C7 F6 00 06        ...   ldab 0006
A3CA 4F              O     clra 
A3CB 05              .     asld 
A3CC C3 08 C8        ...   addd #08c8
A3CF 8F              .     xgdx 
A3D0 EC 00           ..    ldd 00,x
A3D2 C4 80           ..    andb #80
A3D4 84 F7           ..    anda #f7
A3D6 FA 00 47        ..G   orb 0047
A3D9 BA 00 46        ..F   ora 0046
A3DC FD 00 46        ..F   stad 0046
A3DF F6 00 19        ...   ldab 0019
A3E2 C4 10           ..    andb #10
A3E4 27 06           '.    beq 06
A3E6 CE 00 46        ..F   ldx #0046
A3E9 1C 00           ..    bset add,x 00,x
A3EB 08              .     inx 
A3EC FC 00 46        ..F   ldd 0046
A3EF 37              7     pshb 
A3F0 36              6     psha 
A3F1 F6 00 06        ...   ldab 0006
A3F4 4F              O     clra 
A3F5 05              .     asld 
A3F6 C3 08 C8        ...   addd #08c8
A3F9 38              8     pulx 
A3FA 8F              .     xgdx 
A3FB ED 00           ..    stad 00,x
A3FD 20 22            "    bra 22
A3FF CC 2A FF        .*.   ldd #2aff
A402 37              7     pshb 
A403 36              6     psha 
A404 F6 00 06        ...   ldab 0006
A407 4F              O     clra 
A408 05              .     asld 
A409 C3 08 00        ...   addd #0800
A40C 38              8     pulx 
A40D 8F              .     xgdx 
A40E ED 00           ..    stad 00,x
A410 CC D5 00        ...   ldd #d500
A413 37              7     pshb 
A414 36              6     psha 
A415 F6 00 06        ...   ldab 0006
A418 4F              O     clra 
A419 05              .     asld 
A41A C3 0B F1        ...   addd #0bf1
A41D 38              8     pulx 
A41E 8F              .     xgdx 
A41F ED 00           ..    stad 00,x
A421 F6 00 1F        ...   ldab 001f
A424 C4 40           .@    andb #40
A426 27 26           '&    beq 26
A428 F6 00 06        ...   ldab 0006
A42B 4F              O     clra 
A42C 05              .     asld 
A42D C3 08 00        ...   addd #0800
A430 8F              .     xgdx 
A431 EC 00           ..    ldd 00,x
A433 C4 FF           ..    andb #ff
A435 84 3F           .?    anda #3f
A437 FD 00 27        ..'   stad 0027
A43A F6 00 06        ...   ldab 0006
A43D 4F              O     clra 
A43E 05              .     asld 
A43F C3 08 C8        ...   addd #08c8
A442 8F              .     xgdx 
A443 EC 00           ..    ldd 00,x
A445 C4 7F           ..    andb #7f
A447 4F              O     clra 
A448 FD 00 46        ..F   stad 0046
A44B BD A3 6C        ..l   jsr a36c		;jump81
A44E 39              9     rts 

jump140:
A44F FC 00 0D        ...   ldd 000d
A452 FD 0B B7        ...   stad 0bb7
A455 BD A3 6C        ..l   jsr a36c		;jump81
A458 39              9     rts 

jump110:
A459 3C              <     pshx 
A45A F6 00 67        ..g   ldab 0067
A45D C4 02           ..    andb #02
A45F 27 08           '.    beq 08
A461 CE 00 67        ..g   ldx #0067
A464 1D 00           ..    bclr add,x 00,x
A466 4D              M     tsta 
A467 20 0C            .    bra 0c
A469 CE 00 67        ..g   ldx #0067
A46C 1D 00           ..    bclr add,x 00,x
A46E 10              .     sba 
A46F CE 00 67        ..g   ldx #0067
A472 1D 00           ..    bclr add,x 00,x
A474 20 F6            .    bra f6
A476 00              .     test 
A477 67 C4           g.    asr c4,x
A479 10              .     sba 
A47A 26 06           &.    bne 06
A47C CE 00 67        ..g   ldx #0067
A47F 1D 00           ..    bclr add,x 00,x
A481 20 F6            .    bra f6
A483 00              .     test 
A484 67 C4           g.    asr c4,x
A486 01              .     nop 
A487 27 14           '.    beq 14
A489 CE 00 67        ..g   ldx #0067
A48C 1D 00           ..    bclr add,x 00,x
A48E 02              .     idiv 
A48F CE 00 67        ..g   ldx #0067
A492 1D 00           ..    bclr add,x 00,x
A494 10              .     sba 
A495 CE 00 67        ..g   ldx #0067
A498 1D 00           ..    bclr add,x 00,x
A49A 20 20                 bra 20
A49C 06              .     tap 
A49D CE 00 67        ..g   ldx #0067
A4A0 1D 00           ..    bclr add,x 00,x
A4A2 08              .     inx 
A4A3 F6 00 67        ..g   ldab 0067
A4A6 C4 04           ..    andb #04
A4A8 27 12           '.    beq 12
A4AA CE 00 67        ..g   ldx #0067
A4AD 1D 00           ..    bclr add,x 00,x
A4AF 02              .     idiv 
A4B0 CE 00 67        ..g   ldx #0067
A4B3 1D 00           ..    bclr add,x 00,x
A4B5 10              .     sba 
A4B6 CE 00 67        ..g   ldx #0067
A4B9 1D 00           ..    bclr add,x 00,x
A4BB 20 F6            .    bra f6
A4BD 00              .     test 
A4BE 67 C4           g.    asr c4,x
A4C0 08              .     inx 
A4C1 26 06           &.    bne 06
A4C3 CE 00 67        ..g   ldx #0067
A4C6 1D 00           ..    bclr add,x 00,x
A4C8 40              @     nega 
A4C9 F6 00 67        ..g   ldab 0067
A4CC F7 0B C0        ...   stb 0bc0
A4CF F6 0E 70        ..p   ldab 0e70
A4D2 F7 0E 6E        ..n   stb 0e6e
A4D5 F6 0E 6D        ..m   ldab 0e6d
A4D8 26 39           &9    bne 39
A4DA F6 0E 6F        ..o   ldab 0e6f
A4DD 27 34           '4    beq 34
A4DF 7F 00 06        ...   clr 0006
A4E2 F6 00 06        ...   ldab 0006
A4E5 C1 3F           .?    cmpb #3f
A4E7 24 2A           $*    bcc 2a
A4E9 F6 00 06        ...   ldab 0006
A4EC 4F              O     clra 
A4ED CE 00 05        ...   ldx #0005
A4F0 02              .     idiv 
A4F1 5D              ]     tstb 
A4F2 27 1A           '.    beq 1a
A4F4 F6 00 06        ...   ldab 0006
A4F7 4F              O     clra 
A4F8 C3 08 00        ...   addd #0800
A4FB 30              0     tsx 
A4FC ED 00           ..    stad 00,x
A4FE EC 00           ..    ldd 00,x
A500 37              7     pshb 
A501 36              6     psha 
A502 F6 00 06        ...   ldab 0006
A505 4F              O     clra 
A506 05              .     asld 
A507 C3 08 C8        ...   addd #08c8
A50A 38              8     pulx 
A50B 8F              .     xgdx 
A50C ED 00           ..    stad 00,x
A50E 7C 00 06        |..   inc 0006
A511 20 CF            .    bra cf
A513 F6 0E 6F        ..o   ldab 0e6f
A516 F7 0E 6D        ..m   stb 0e6d
A519 BD A3 6C        ..l   jsr a36c		;jump81
A51C 38              8     pulx 
A51D 39              9     rts 

jump109:
A51E F6 00 22        .."   ldab 0022
A521 4F              O     clra 
A522 BD FE 16        ...   jsr fe16		;jump2
A525 00              .     test 
A526 01              .     nop 
A527 00              .     test 
A528 04              .     lsrd 
A529 A5 79           .y    bita 79,x
A52B A5 35           .5    bita 35,x
A52D A5 43           .C    bita 43,x
A52F A5 51           .Q    bita 51,x
A531 A5 5F           ._    bita 5f,x
A533 A5 6D           .m    bita 6d,x
A535 F6 00 24        ..$   ldab 0024
A538 F7 0B AF        ...   stb 0baf
A53B F6 00 2A        ..*   ldab 002a
A53E F7 0B B4        ...   stb 0bb4
A541 20 36            6    bra 36
A543 F6 00 24        ..$   ldab 0024
A546 F7 0B AE        ...   stb 0bae
A549 F6 00 2A        ..*   ldab 002a
A54C F7 0B B3        ...   stb 0bb3
A54F 20 28            (    bra 28
A551 F6 00 24        ..$   ldab 0024
A554 F7 0B AD        ...   stb 0bad
A557 F6 00 2A        ..*   ldab 002a
A55A F7 0B B2        ...   stb 0bb2
A55D 20 1A            .    bra 1a
A55F F6 00 24        ..$   ldab 0024
A562 F7 0B AC        ...   stb 0bac
A565 F6 00 2A        ..*   ldab 002a
A568 F7 0B B1        ...   stb 0bb1
A56B 20 0C            .    bra 0c
A56D F6 00 24        ..$   ldab 0024
A570 F7 0B AB        ...   stb 0bab
A573 F6 00 2A        ..*   ldab 002a
A576 F7 0B B0        ...   stb 0bb0
A579 BD A3 6C        ..l   jsr a36c		;jump81
A57C 39              9     rts 

jump139:
A57D 7F 00 54        ..T   clr 0054
A580 F6 00 54        ..T   ldab 0054
A583 C1 FF           ..    cmpb #ff
A585 24 1B           $.    bcc 1b
A587 F6 00 54        ..T   ldab 0054
A58A 4F              O     clra 
A58B C3 0A 90        ...   addd #0a90
A58E 8F              .     xgdx 
A58F E6 00           ..    ldab 00,x
A591 37              7     pshb 
A592 F6 00 54        ..T   ldab 0054
A595 4F              O     clra 
A596 C3 09 90        ...   addd #0990
A599 8F              .     xgdx 
A59A 33              3     pulb 
A59B E7 00           ..    stb 00,x
A59D 7C 00 54        |.T   inc 0054
A5A0 20 DE            .    bra de
A5A2 F6 00 61        ..a   ldab 0061
A5A5 F7 0B BF        ...   stb 0bbf
A5A8 BD A3 6C        ..l   jsr a36c		;jump81
A5AB 39              9     rts 

jump138:
A5AC 7F 00 54        ..T   clr 0054
A5AF F6 00 54        ..T   ldab 0054
A5B2 C1 10           ..    cmpb #10
A5B4 24 1B           $.    bcc 1b
A5B6 F6 00 54        ..T   ldab 0054
A5B9 4F              O     clra 
A5BA C3 0A 90        ...   addd #0a90
A5BD 8F              .     xgdx 
A5BE E6 00           ..    ldab 00,x
A5C0 37              7     pshb 
A5C1 F6 00 54        ..T   ldab 0054
A5C4 4F              O     clra 
A5C5 C3 0C B9        ...   addd #0cb9
A5C8 8F              .     xgdx 
A5C9 33              3     pulb 
A5CA E7 00           ..    stb 00,x
A5CC 7C 00 54        |.T   inc 0054
A5CF 20 DE            .    bra de
A5D1 BD A3 6C        ..l   jsr a36c		;jump81
A5D4 39              9     rts 

jump137:
A5D5 7F 00 54        ..T   clr 0054
A5D8 F6 00 54        ..T   ldab 0054
A5DB C1 10           ..    cmpb #10
A5DD 24 1B           $.    bcc 1b
A5DF F6 00 54        ..T   ldab 0054
A5E2 4F              O     clra 
A5E3 C3 0A 90        ...   addd #0a90
A5E6 8F              .     xgdx 
A5E7 E6 00           ..    ldab 00,x
A5E9 37              7     pshb 
A5EA F6 00 54        ..T   ldab 0054
A5ED 4F              O     clra 
A5EE C3 0C C9        ...   addd #0cc9
A5F1 8F              .     xgdx 
A5F2 33              3     pulb 
A5F3 E7 00           ..    stb 00,x
A5F5 7C 00 54        |.T   inc 0054
A5F8 20 DE            .    bra de
A5FA BD A3 6C        ..l   jsr a36c		;jump81
A5FD 39              9     rts 

jump136:
A5FE 7F 00 54        ..T   clr 0054
A601 F6 00 54        ..T   ldab 0054
A604 C1 05           ..    cmpb #05
A606 24 1B           $.    bcc 1b
A608 F6 00 54        ..T   ldab 0054
A60B 4F              O     clra 
A60C C3 00 62        ..b   addd #0062
A60F 8F              .     xgdx 
A610 E6 00           ..    ldab 00,x
A612 37              7     pshb 
A613 F6 00 54        ..T   ldab 0054
A616 4F              O     clra 
A617 C3 0B A4        ...   addd #0ba4
A61A 8F              .     xgdx 
A61B 33              3     pulb 
A61C E7 00           ..    stb 00,x
A61E 7C 00 54        |.T   inc 0054
A621 20 DE            .    bra de
A623 7F 00 54        ..T   clr 0054
A626 F6 00 54        ..T   ldab 0054
A629 C1 0B           ..    cmpb #0b
A62B 24 1B           $.    bcc 1b
A62D F6 00 54        ..T   ldab 0054
A630 4F              O     clra 
A631 C3 00 68        ..h   addd #0068
A634 8F              .     xgdx 
A635 E6 00           ..    ldab 00,x
A637 37              7     pshb 
A638 F6 00 54        ..T   ldab 0054
A63B 4F              O     clra 
A63C C3 0B E4        ...   addd #0be4
A63F 8F              .     xgdx 
A640 33              3     pulb 
A641 E7 00           ..    stb 00,x
A643 7C 00 54        |.T   inc 0054
A646 20 DE            .    bra de
A648 8D 04           ..    bsr dest 04
A64A BD A3 6C        ..l   jsr a36c		;jump81
A64D 39              9     rts 

jump135:
A64E BD AB 0C        ...   jsr ab0c		;jump126
A651 C6 20           .     ldab #20
A653 F7 0B A3        ...   stb 0ba3
A656 C6 20           .     ldab #20
A658 F7 0B 9A        ...   stb 0b9a
A65B C6 01           ..    ldab #01
A65D F7 00 54        ..T   stb 0054
goto21:
A660 F6 00 54        ..T   ldab 0054
A663 C1 09           ..    cmpb #09
A665 25 03           %.    bcs 03
A667 7E A7 1E        ~..   jmp a71e		;goto20
A66A F6 00 54        ..T   ldab 0054
A66D 4F              O     clra 
A66E BD FE 16        ...   jsr fe16		;jump2
A671 00              .     test 
A672 01              .     nop 
A673 00              .     test 
A674 07              .     tpa 
A675 A6 ED           ..    ldaa ed,x
A677 A6 87           ..    ldaa 87,x
A679 A6 95           ..    ldaa 95,x
A67B A6 A1           ..    ldaa a1,x
A67D A6 AF           ..    ldaa af,x
A67F A6 BB           ..    ldaa bb,x
A681 A6 C9           ..    ldaa c9,x
A683 A6 D5           ..    ldaa d5,x
A685 A6 E3           ..    ldaa e3,x
A687 F6 00 62        ..b   ldab 0062
A68A 54              T     lsrb 
A68B 54              T     lsrb 
A68C 54              T     lsrb 
A68D 54              T     lsrb 
A68E C8 04           ..    eorb #04
A690 F7 00 5F        .._   stb 005f
A693 20 58            X    bra 58
A695 F6 00 62        ..b   ldab 0062
A698 C4 0F           ..    andb #0f
A69A C8 0D           ..    eorb #0d
A69C F7 00 5F        .._   stb 005f
A69F 20 4C            L    bra 4c
A6A1 F6 00 63        ..c   ldab 0063
A6A4 54              T     lsrb 
A6A5 54              T     lsrb 
A6A6 54              T     lsrb 
A6A7 54              T     lsrb 
A6A8 C8 04           ..    eorb #04
A6AA F7 00 5F        .._   stb 005f
A6AD 20 3E            >    bra 3e
A6AF F6 00 63        ..c   ldab 0063
A6B2 C4 0F           ..    andb #0f
A6B4 C8 05           ..    eorb #05
A6B6 F7 00 5F        .._   stb 005f
A6B9 20 32            2    bra 32
A6BB F6 00 64        ..d   ldab 0064
A6BE 54              T     lsrb 
A6BF 54              T     lsrb 
A6C0 54              T     lsrb 
A6C1 54              T     lsrb 
A6C2 C8 05           ..    eorb #05
A6C4 F7 00 5F        .._   stb 005f
A6C7 20 24            $    bra 24
A6C9 F6 00 64        ..d   ldab 0064
A6CC C4 0F           ..    andb #0f
A6CE C8 04           ..    eorb #04
A6D0 F7 00 5F        .._   stb 005f
A6D3 20 18            .    bra 18
A6D5 F6 00 65        ..e   ldab 0065
A6D8 54              T     lsrb 
A6D9 54              T     lsrb 
A6DA 54              T     lsrb 
A6DB 54              T     lsrb 
A6DC C8 05           ..    eorb #05
A6DE F7 00 5F        .._   stb 005f
A6E1 20 0A            .    bra 0a
A6E3 F6 00 65        ..e   ldab 0065
A6E6 C4 0F           ..    andb #0f
A6E8 C8 03           ..    eorb #03
A6EA F7 00 5F        .._   stb 005f
A6ED F6 00 5F        .._   ldab 005f
A6F0 C1 09           ..    cmpb #09
A6F2 23 13           #.    bls 13
A6F4 F6 00 5F        .._   ldab 005f
A6F7 CB 37           .7    addb #37
A6F9 37              7     pshb 
A6FA F6 00 54        ..T   ldab 0054
A6FD 4F              O     clra 
A6FE C3 0B 9A        ...   addd #0b9a
A701 8F              .     xgdx 
A702 33              3     pulb 
A703 E7 00           ..    stb 00,x
A705 20 11            .    bra 11
A707 F6 00 5F        .._   ldab 005f
A70A CB 30           .0    addb #30
A70C 37              7     pshb 
A70D F6 00 54        ..T   ldab 0054
A710 4F              O     clra 
A711 C3 0B 9A        ...   addd #0b9a
A714 8F              .     xgdx 
A715 33              3     pulb 
A716 E7 00           ..    stb 00,x
A718 7C 00 54        |.T   inc 0054
A71B 7E A6 60        ~.`   jmp a660		;goto21
goto20:
A71E 39              9     rts 

jump134:
A71F F6 00 48        ..H   ldab 0048
A722 26 11           &.    bne 11
A724 F6 00 66        ..f   ldab 0066
A727 26 07           &.    bne 07
A729 C6 01           ..    ldab #01
A72B F7 00 66        ..f   stb 0066
A72E 20 03            .    bra 03
A730 7F 00 66        ..f   clr 0066
A733 20 29            )    bra 29
A735 F6 00 48        ..H   ldab 0048
A738 4F              O     clra 
A739 C3 00 68        ..h   addd #0068
A73C 8F              .     xgdx 
A73D E6 00           ..    ldab 00,x
A73F 26 10           &.    bne 10
A741 C6 01           ..    ldab #01
A743 37              7     pshb 
A744 F6 00 48        ..H   ldab 0048
A747 4F              O     clra 
A748 C3 00 68        ..h   addd #0068
A74B 8F              .     xgdx 
A74C 33              3     pulb 
A74D E7 00           ..    stb 00,x
A74F 20 0D            .    bra 0d
A751 5F              _     clrb 
A752 37              7     pshb 
A753 F6 00 48        ..H   ldab 0048
A756 4F              O     clra 
A757 C3 00 68        ..h   addd #0068
A75A 8F              .     xgdx 
A75B 33              3     pulb 
A75C E7 00           ..    stb 00,x
A75E 39              9     rts 

jump133:
A75F F6 00 48        ..H   ldab 0048
A762 4F              O     clra 
A763 BD FE 16        ...   jsr fe16		;jump2
A766 00              .     test 
A767 00              .     test 
A768 00              .     test 
A769 09              .     dex 
A76A A7 DC           ..    sta dc,x
A76C A7 80           ..    sta 80,x
A76E A7 87           ..    sta 87,x
A770 A7 8E           ..    sta 8e,x
A772 A7 95           ..    sta 95,x
A774 A7 9C           ..    sta 9c,x
A776 A7 A3           ..    sta a3,x
A778 A7 AA           ..    sta aa,x
A77A A7 B1           ..    sta b1,x
A77C A7 C4           ..    sta c4,x
A77E A7 D7           ..    sta d7,x
A780 C6 02           ..    ldab #02
A782 F7 00 02        ...   stb 0002
A785 20 55            U    bra 55
A787 C6 10           ..    ldab #10
A789 F7 00 02        ...   stb 0002
A78C 20 4E            N    bra 4e
A78E C6 20           .     ldab #20
A790 F7 00 02        ...   stb 0002
A793 20 47            G    bra 47
A795 C6 01           ..    ldab #01
A797 F7 00 02        ...   stb 0002
A79A 20 40            @    bra 40
A79C C6 04           ..    ldab #04
A79E F7 00 02        ...   stb 0002
A7A1 20 39            9    bra 39
A7A3 C6 08           ..    ldab #08
A7A5 F7 00 02        ...   stb 0002
A7A8 20 32            2    bra 32
A7AA C6 40           .@    ldab #40
A7AC F7 00 02        ...   stb 0002
A7AF 20 2B            +    bra 2b
A7B1 F6 0E 70        ..p   ldab 0e70
A7B4 C1 01           ..    cmpb #01
A7B6 26 05           &.    bne 05
A7B8 7F 0E 70        ..p   clr 0e70
A7BB 20 05            .    bra 05
A7BD C6 01           ..    ldab #01
A7BF F7 0E 70        ..p   stb 0e70
A7C2 20 18            .    bra 18
A7C4 F6 0E 6F        ..o   ldab 0e6f
A7C7 C1 01           ..    cmpb #01
A7C9 26 05           &.    bne 05
A7CB 7F 0E 6F        ..o   clr 0e6f
A7CE 20 05            .    bra 05
A7D0 C6 01           ..    ldab #01
A7D2 F7 0E 6F        ..o   stb 0e6f
A7D5 20 05            .    bra 05
A7D7 C6 80           ..    ldab #80
A7D9 F7 00 02        ...   stb 0002
A7DC F6 00 48        ..H   ldab 0048
A7DF C1 0A           ..    cmpb #0a
A7E1 24 2B           $+    bcc 2b
A7E3 F6 00 48        ..H   ldab 0048
A7E6 C1 07           ..    cmpb #07
A7E8 27 24           '$    beq 24
A7EA F6 00 48        ..H   ldab 0048
A7ED C1 08           ..    cmpb #08
A7EF 27 1D           '.    beq 1d
A7F1 F6 00 67        ..g   ldab 0067
A7F4 F4 00 02        ...   andb 0002
A7F7 26 0B           &.    bne 0b
A7F9 F6 00 02        ...   ldab 0002
A7FC FA 00 67        ..g   orb 0067
A7FF F7 00 67        ..g   stb 0067
A802 20 0A            .    bra 0a
A804 F6 00 02        ...   ldab 0002
A807 53              S     comb 
A808 F4 00 67        ..g   andb 0067
A80B F7 00 67        ..g   stb 0067
A80E 39              9     rts 

A80F 7F 00 54        ..T   clr 0054
A812 F6 00 54        ..T   ldab 0054
A815 C1 04           ..    cmpb #04
A817 24 4A           $J    bcc 4a
A819 CC 01 F4        ...   ldd #01f4
A81C BD E5 71        ..q   jsr e571		;jump13
A81F F6 0B 93        ...   ldab 0b93
A822 F7 00 5F        .._   stb 005f
A825 F6 0B 94        ...   ldab 0b94
A828 F7 00 5E        ..^   stb 005e
A82B F6 0B 95        ...   ldab 0b95
A82E F7 00 5D        ..]   stb 005d
A831 F6 0B 96        ...   ldab 0b96
A834 F7 00 5A        ..Z   stb 005a
A837 BD BE 4B        ..K   jsr be4b		;jump1
A83A BD BE 01        ...   jsr be01		;jump70
A83D CC 01 F4        ...   ldd #01f4
A840 BD E5 71        ..q   jsr e571		;jump13
A843 F6 00 5F        .._   ldab 005f
A846 F7 0B 93        ...   stb 0b93
A849 F6 00 5E        ..^   ldab 005e
A84C F7 0B 94        ...   stb 0b94
A84F F6 00 5D        ..]   ldab 005d
A852 F7 0B 95        ...   stb 0b95
A855 F6 00 5A        ..Z   ldab 005a
A858 F7 0B 96        ...   stb 0b96
A85B BD BE 01        ...   jsr be01		;jump70
A85E 7C 00 54        |.T   inc 0054
A861 20 AF            .    bra af
A863 39              9     rts 

jump99:
A864 F6 00 09        ...   ldab 0009
A867 C1 01           ..    cmpb #01
A869 26 20           &     bne 20
A86B F6 00 61        ..a   ldab 0061
A86E 4F              O     clra 
A86F C3 00 01        ...   addd #0001
A872 37              7     pshb 
A873 36              6     psha 
A874 F6 00 60        ..`   ldab 0060
A877 4F              O     clra 
A878 30              0     tsx 
A879 A3 00           ..    subd 00,x
A87B 31              1     ins 
A87C 31              1     ins 
A87D 2C 0A           ,.    bge 0a
A87F F6 00 60        ..`   ldab 0060
A882 C1 F9           ..    cmpb #f9
A884 24 03           $.    bcc 03
A886 7C 00 60        |.`   inc 0060
A889 20 0A            .    bra 0a
A88B F6 00 60        ..`   ldab 0060
A88E C1 09           ..    cmpb #09
A890 24 03           $.    bcc 03
A892 7C 00 60        |.`   inc 0060
A895 39              9     rts 

jump98:
A896 F6 00 36        ..6   ldab 0036
A899 26 39           &9    bne 39
A89B F6 00 54        ..T   ldab 0054
A89E C1 27           .'    cmpb #27
A8A0 24 20           $     bcc 20
A8A2 7C 00 54        |.T   inc 0054
A8A5 F6 00 09        ...   ldab 0009
A8A8 C1 01           ..    cmpb #01
A8AA 26 0C           &.    bne 0c
A8AC F6 00 54        ..T   ldab 0054
A8AF C1 27           .'    cmpb #27
A8B1 26 03           &.    bne 03
A8B3 7F 00 54        ..T   clr 0054
A8B6 20 0A            .    bra 0a
A8B8 F6 00 54        ..T   ldab 0054
A8BB C1 24           .$    cmpb #24
A8BD 26 03           &.    bne 03
A8BF 7F 00 54        ..T   clr 0054
A8C2 F6 00 54        ..T   ldab 0054
A8C5 4F              O     clra 
A8C6 C3 B4 8F        ...   addd #b48f
A8C9 8F              .     xgdx 
A8CA E6 00           ..    ldab 00,x
A8CC F7 0B 90        ...   stb 0b90
A8CF C6 02           ..    ldab #02
A8D1 F7 00 36        ..6   stb 0036
A8D4 39              9     rts 

jump97:
A8D5 F6 00 60        ..`   ldab 0060
A8D8 C1 00           ..    cmpb #00
A8DA 23 05           #.    bls 05
A8DC 7A 00 60        z.`   dec 0060
A8DF 20 03            .    bra 03
A8E1 7F 00 60        ..`   clr 0060
A8E4 39              9     rts 

jump96:
A8E5 F6 00 36        ..6   ldab 0036
A8E8 26 36           &6    bne 36
A8EA F6 00 54        ..T   ldab 0054
A8ED C1 00           ..    cmpb #00
A8EF 25 1D           %.    bcs 1d
A8F1 F6 00 54        ..T   ldab 0054
A8F4 26 15           &.    bne 15
A8F6 F6 00 09        ...   ldab 0009
A8F9 C1 01           ..    cmpb #01
A8FB 26 07           &.    bne 07
A8FD C6 26           .&    ldab #26
A8FF F7 00 54        ..T   stb 0054
A902 20 05            .    bra 05
A904 C6 23           .#    ldab #23
A906 F7 00 54        ..T   stb 0054
A909 20 03            .    bra 03
A90B 7A 00 54        z.T   dec 0054
A90E F6 00 54        ..T   ldab 0054
A911 4F              O     clra 
A912 C3 B4 8F        ...   addd #b48f
A915 8F              .     xgdx 
A916 E6 00           ..    ldab 00,x
A918 F7 0B 90        ...   stb 0b90
A91B C6 02           ..    ldab #02
A91D F7 00 36        ..6   stb 0036
A920 39              9     rts 

jump95:
A921 F6 0B 90        ...   ldab 0b90
A924 F7 0B 96        ...   stb 0b96
A927 F6 0B 96        ...   ldab 0b96
A92A 37              7     pshb 
A92B F6 00 60        ..`   ldab 0060
A92E 4F              O     clra 
A92F C3 0A 93        ...   addd #0a93
A932 8F              .     xgdx 
A933 33              3     pulb 
A934 E7 00           ..    stb 00,x
A936 F6 00 09        ...   ldab 0009
A939 C1 01           ..    cmpb #01
A93B 26 32           &2    bne 32
A93D F6 00 61        ..a   ldab 0061
A940 4F              O     clra 
A941 C3 00 01        ...   addd #0001
A944 37              7     pshb 
A945 36              6     psha 
A946 F6 00 60        ..`   ldab 0060
A949 4F              O     clra 
A94A 30              0     tsx 
A94B A3 00           ..    subd 00,x
A94D 31              1     ins 
A94E 31              1     ins 
A94F 26 0A           &.    bne 0a
A951 F6 00 60        ..`   ldab 0060
A954 C1 FA           ..    cmpb #fa
A956 24 03           $.    bcc 03
A958 7C 00 61        |.a   inc 0061
A95B F6 00 60        ..`   ldab 0060
A95E F1 00 61        ..a   cmpb 0061
A961 22 0A           ".    bhi 0a
A963 F6 00 60        ..`   ldab 0060
A966 C1 F9           ..    cmpb #f9
A968 24 03           $.    bcc 03
A96A 7C 00 60        |.`   inc 0060
A96D 20 0A            .    bra 0a
A96F F6 00 60        ..`   ldab 0060
A972 C1 09           ..    cmpb #09
A974 24 03           $.    bcc 03
A976 7C 00 60        |.`   inc 0060
A979 39              9     rts 

jump132:
A97A F6 00 60        ..`   ldab 0060
A97D 26 24           &$    bne 24
A97F F6 00 60        ..`   ldab 0060
A982 CB 03           ..    addb #03
A984 F7 00 54        ..T   stb 0054
A987 F6 00 54        ..T   ldab 0054
A98A C1 FF           ..    cmpb #ff
A98C 24 13           $.    bcc 13
A98E C6 20           .     ldab #20
A990 37              7     pshb 
A991 F6 00 54        ..T   ldab 0054
A994 4F              O     clra 
A995 C3 0A 90        ...   addd #0a90
A998 8F              .     xgdx 
A999 33              3     pulb 
A99A E7 00           ..    stb 00,x
A99C 7C 00 54        |.T   inc 0054
A99F 20 E6            .    bra e6
A9A1 20 22            "    bra 22
A9A3 F6 00 60        ..`   ldab 0060
A9A6 CB 04           ..    addb #04
A9A8 F7 00 54        ..T   stb 0054
A9AB F6 00 54        ..T   ldab 0054
A9AE C1 FF           ..    cmpb #ff
A9B0 24 13           $.    bcc 13
A9B2 C6 20           .     ldab #20
A9B4 37              7     pshb 
A9B5 F6 00 54        ..T   ldab 0054
A9B8 4F              O     clra 
A9B9 C3 0A 90        ...   addd #0a90
A9BC 8F              .     xgdx 
A9BD 33              3     pulb 
A9BE E7 00           ..    stb 00,x
A9C0 7C 00 54        |.T   inc 0054
A9C3 20 E6            .    bra e6
A9C5 F6 00 60        ..`   ldab 0060
A9C8 F7 00 61        ..a   stb 0061
A9CB 7F 00 54        ..T   clr 0054
A9CE 39              9     rts 

jump131:
A9CF C6 50           .P    ldab #50
A9D1 F7 09 90        ...   stb 0990
A9D4 C6 29           .)    ldab #29
A9D6 F7 09 91        ...   stb 0991
A9D9 C6 20           .     ldab #20
A9DB F7 09 92        ...   stb 0992
A9DE 7F 00 54        ..T   clr 0054
A9E1 F6 00 54        ..T   ldab 0054
A9E4 C1 FF           ..    cmpb #ff
A9E6 24 1B           $.    bcc 1b
A9E8 F6 00 54        ..T   ldab 0054
A9EB 4F              O     clra 
A9EC C3 09 90        ...   addd #0990
A9EF 8F              .     xgdx 
A9F0 E6 00           ..    ldab 00,x
A9F2 37              7     pshb 
A9F3 F6 00 54        ..T   ldab 0054
A9F6 4F              O     clra 
A9F7 C3 0A 90        ...   addd #0a90
A9FA 8F              .     xgdx 
A9FB 33              3     pulb 
A9FC E7 00           ..    stb 00,x
A9FE 7C 00 54        |.T   inc 0054
AA01 20 DE            .    bra de
AA03 F6 0B BF        ...   ldab 0bbf
AA06 F7 00 61        ..a   stb 0061
AA09 39              9     rts 

jump130:
AA0A C6 53           .S    ldab #53
AA0C F7 0C B9        ...   stb 0cb9
AA0F C6 29           .)    ldab #29
AA11 F7 0C BA        ...   stb 0cba
AA14 C6 20           .     ldab #20
AA16 F7 0C BB        ...   stb 0cbb
AA19 7F 00 54        ..T   clr 0054
AA1C F6 00 54        ..T   ldab 0054
AA1F C1 10           ..    cmpb #10
AA21 24 1B           $.    bcc 1b
AA23 F6 00 54        ..T   ldab 0054
AA26 4F              O     clra 
AA27 C3 0C B9        ...   addd #0cb9
AA2A 8F              .     xgdx 
AA2B E6 00           ..    ldab 00,x
AA2D 37              7     pshb 
AA2E F6 00 54        ..T   ldab 0054
AA31 4F              O     clra 
AA32 C3 0A 90        ...   addd #0a90
AA35 8F              .     xgdx 
AA36 33              3     pulb 
AA37 E7 00           ..    stb 00,x
AA39 7C 00 54        |.T   inc 0054
AA3C 20 DE            .    bra de
AA3E 39              9     rts 

jump129:
AA3F C6 49           .I    ldab #49
AA41 F7 0C C9        ...   stb 0cc9
AA44 C6 29           .)    ldab #29
AA46 F7 0C CA        ...   stb 0cca
AA49 C6 20           .     ldab #20
AA4B F7 0C CB        ...   stb 0ccb
AA4E 7F 00 54        ..T   clr 0054
AA51 F6 00 54        ..T   ldab 0054
AA54 C1 10           ..    cmpb #10
AA56 24 1B           $.    bcc 1b
AA58 F6 00 54        ..T   ldab 0054
AA5B 4F              O     clra 
AA5C C3 0C C9        ...   addd #0cc9
AA5F 8F              .     xgdx 
AA60 E6 00           ..    ldab 00,x
AA62 37              7     pshb 
AA63 F6 00 54        ..T   ldab 0054
AA66 4F              O     clra 
AA67 C3 0A 90        ...   addd #0a90
AA6A 8F              .     xgdx 
AA6B 33              3     pulb 
AA6C E7 00           ..    stb 00,x
AA6E 7C 00 54        |.T   inc 0054
AA71 20 DE            .    bra de
AA73 39              9     rts 

jump128:
AA74 7F 00 54        ..T   clr 0054
AA77 F6 00 54        ..T   ldab 0054
AA7A C1 0B           ..    cmpb #0b
AA7C 24 1B           $.    bcc 1b
AA7E F6 00 54        ..T   ldab 0054
AA81 4F              O     clra 
AA82 C3 0B E4        ...   addd #0be4
AA85 8F              .     xgdx 
AA86 E6 00           ..    ldab 00,x
AA88 37              7     pshb 
AA89 F6 00 54        ..T   ldab 0054
AA8C 4F              O     clra 
AA8D C3 00 68        ..h   addd #0068
AA90 8F              .     xgdx 
AA91 33              3     pulb 
AA92 E7 00           ..    stb 00,x
AA94 7C 00 54        |.T   inc 0054
AA97 20 DE            .    bra de
AA99 39              9     rts 

jump127:
AA9A F6 00 22        .."   ldab 0022
AA9D C1 06           ..    cmpb #06
AA9F 24 0F           $.    bcc 0f
AAA1 7C 00 22        |."   inc 0022
AAA4 F6 00 22        .."   ldab 0022
AAA7 C1 05           ..    cmpb #05
AAA9 23 05           #.    bls 05
AAAB C6 01           ..    ldab #01
AAAD F7 00 22        .."   stb 0022
AAB0 F6 00 22        .."   ldab 0022
AAB3 4F              O     clra 
AAB4 BD FE 16        ...   jsr fe16		;jump2
AAB7 00              .     test 
AAB8 01              .     nop 
AAB9 00              .     test 
AABA 04              .     lsrd 
AABB AB 0B           ..    adda 0b,x
AABD AA C7           ..    ora c7,x
AABF AA D5           ..    ora d5,x
AAC1 AA E3           ..    ora e3,x
AAC3 AA F1           ..    ora f1,x
AAC5 AA FF           ..    ora ff,x
AAC7 F6 0B AF        ...   ldab 0baf
AACA F7 00 24        ..$   stb 0024
AACD F6 0B B4        ...   ldab 0bb4
AAD0 F7 00 2A        ..*   stb 002a
AAD3 20 36            6    bra 36
AAD5 F6 0B AE        ...   ldab 0bae
AAD8 F7 00 24        ..$   stb 0024
AADB F6 0B B3        ...   ldab 0bb3
AADE F7 00 2A        ..*   stb 002a
AAE1 20 28            (    bra 28
AAE3 F6 0B AD        ...   ldab 0bad
AAE6 F7 00 24        ..$   stb 0024
AAE9 F6 0B B2        ...   ldab 0bb2
AAEC F7 00 2A        ..*   stb 002a
AAEF 20 1A            .    bra 1a
AAF1 F6 0B AC        ...   ldab 0bac
AAF4 F7 00 24        ..$   stb 0024
AAF7 F6 0B B1        ...   ldab 0bb1
AAFA F7 00 2A        ..*   stb 002a
AAFD 20 0C            .    bra 0c
AAFF F6 0B AB        ...   ldab 0bab
AB02 F7 00 24        ..$   stb 0024
AB05 F6 0B B0        ...   ldab 0bb0
AB08 F7 00 2A        ..*   stb 002a
AB0B 39              9     rts 

jump126:
AB0C C6 03           ..    ldab #03
AB0E F7 00 18        ...   stb 0018
AB11 7F 00 1C        ...   clr 001c
AB14 F6 00 1C        ...   ldab 001c
AB17 C1 04           ..    cmpb #04
AB19 24 6D           $m    bcc 6d
AB1B C6 07           ..    ldab #07
AB1D F7 00 55        ..U   stb 0055
AB20 7F 00 54        ..T   clr 0054
AB23 F6 00 54        ..T   ldab 0054
AB26 C1 08           ..    cmpb #08
AB28 24 56           $V    bcc 56
AB2A F6 00 1C        ...   ldab 001c
AB2D 4F              O     clra 
AB2E C3 0B A4        ...   addd #0ba4
AB31 8F              .     xgdx 
AB32 E6 00           ..    ldab 00,x
AB34 37              7     pshb 
AB35 F6 00 54        ..T   ldab 0054
AB38 4F              O     clra 
AB39 C3 B4 B7        ...   addd #b4b7
AB3C 8F              .     xgdx 
AB3D E6 00           ..    ldab 00,x
AB3F 30              0     tsx 
AB40 E4 00           ..    andb 00,x
AB42 31              1     ins 
AB43 27 1A           '.    beq 1a
AB45 F6 00 55        ..U   ldab 0055
AB48 4F              O     clra 
AB49 C3 B4 B7        ...   addd #b4b7
AB4C 8F              .     xgdx 
AB4D E6 00           ..    ldab 00,x
AB4F 37              7     pshb 
AB50 F6 00 18        ...   ldab 0018
AB53 4F              O     clra 
AB54 C3 00 62        ..b   addd #0062
AB57 8F              .     xgdx 
AB58 33              3     pulb 
AB59 EA 00           ..    orb 00,x
AB5B E7 00           ..    stb 00,x
AB5D 20 19            .    bra 19
AB5F F6 00 55        ..U   ldab 0055
AB62 4F              O     clra 
AB63 C3 B4 B7        ...   addd #b4b7
AB66 8F              .     xgdx 
AB67 E6 00           ..    ldab 00,x
AB69 53              S     comb 
AB6A 37              7     pshb 
AB6B F6 00 18        ...   ldab 0018
AB6E 4F              O     clra 
AB6F C3 00 62        ..b   addd #0062
AB72 8F              .     xgdx 
AB73 33              3     pulb 
AB74 E4 00           ..    andb 00,x
AB76 E7 00           ..    stb 00,x
AB78 7A 00 55        z.U   dec 0055
AB7B 7C 00 54        |.T   inc 0054
AB7E 20 A3            .    bra a3
AB80 7A 00 18        z..   dec 0018
AB83 7C 00 1C        |..   inc 001c
AB86 20 8C            .    bra 8c
AB88 39              9     rts 

jump7:
AB89 CC B0 8C        ...   ldd #b08c
AB8C BD C0 BA        ...   jsr c0ba		;display:
AB8F CC 01 F4        ...   ldd #01f4
AB92 BD E5 71        ..q   jsr e571		;jump13
AB95 BD D4 79        ..y   jsr d479		;jump67
AB98 BD E8 9A        ...   jsr e89a		;jump68
AB9B BD E9 EA        ...   jsr e9ea		;jump113
AB9E 7F 0B C1        ...   clr 0bc1
ABA1 39              9     rts 

jump8:
ABA2 7F 00 05        ...   clr 0005
goto24:
ABA5 F6 00 05        ...   ldab 0005
ABA8 C1 64           .d    cmpb #64
ABAA 25 03           %.    bcs 03
ABAC 7E AC 81        ~..   jmp ac81		;goto22
ABAF F6 00 05        ...   ldab 0005
ABB2 F7 00 06        ...   stb 0006
ABB5 F6 00 05        ...   ldab 0005
ABB8 4F              O     clra 
ABB9 CE 00 05        ...   ldx #0005
ABBC 02              .     idiv 
ABBD 5D              ]     tstb 
ABBE 26 03           &.    bne 03
ABC0 7E AC 7B        ~.{   jmp ac7b		;goto23
ABC3 F6 00 06        ...   ldab 0006
ABC6 BD D7 A0        ...   jsr d7a0		;jump10
ABC9 83 00 00        ...   subd #0000
ABCC 27 03           '.    beq 03
ABCE 7E AC 7B        ~.{   jmp ac7b		;goto23
ABD1 BD D3 11        ...   jsr d311		;jump30
ABD4 83 00 00        ...   subd #0000
ABD7 27 2B           '+    beq 2b
ABD9 F6 00 05        ...   ldab 0005
ABDC 4F              O     clra 
ABDD 05              .     asld 
ABDE C3 08 00        ...   addd #0800
ABE1 8F              .     xgdx 
ABE2 EC 00           ..    ldd 00,x
ABE4 C4 FF           ..    andb #ff
ABE6 84 3F           .?    anda #3f
ABE8 83 27 10        .'.   subd #2710
ABEB 24 15           $.    bcc 15
ABED CC 01 00        ...   ldd #0100
ABF0 37              7     pshb 
ABF1 36              6     psha 
ABF2 F6 00 05        ...   ldab 0005
ABF5 4F              O     clra 
ABF6 05              .     asld 
ABF7 C3 08 C8        ...   addd #08c8
ABFA 38              8     pulx 
ABFB 8F              .     xgdx 
ABFC AA 00           ..    ora 00,x
ABFE EA 01           ..    orb 01,x
AC00 ED 00           ..    stad 00,x
AC02 20 29            )    bra 29
AC04 F6 00 05        ...   ldab 0005
AC07 4F              O     clra 
AC08 05              .     asld 
AC09 C3 08 00        ...   addd #0800
AC0C 8F              .     xgdx 
AC0D EC 00           ..    ldd 00,x
AC0F C4 FF           ..    andb #ff
AC11 84 3F           .?    anda #3f
AC13 83 27 0F        .'.   subd #270f
AC16 23 15           #.    bls 15
AC18 CC 01 00        ...   ldd #0100
AC1B 37              7     pshb 
AC1C 36              6     psha 
AC1D F6 00 05        ...   ldab 0005
AC20 4F              O     clra 
AC21 05              .     asld 
AC22 C3 08 C8        ...   addd #08c8
AC25 38              8     pulx 
AC26 8F              .     xgdx 
AC27 AA 00           ..    ora 00,x
AC29 EA 01           ..    orb 01,x
AC2B ED 00           ..    stad 00,x
AC2D F6 00 05        ...   ldab 0005
AC30 4F              O     clra 
AC31 05              .     asld 
AC32 C3 08 C8        ...   addd #08c8
AC35 8F              .     xgdx 
AC36 EC 00           ..    ldd 00,x
AC38 84 01           ..    anda #01
AC3A 27 06           '.    beq 06
AC3C CE 00 19        ...   ldx #0019
AC3F 1C 00           ..    bset add,x 00,x
AC41 04              .     lsrd 
AC42 F6 00 05        ...   ldab 0005
AC45 4F              O     clra 
AC46 05              .     asld 
AC47 C3 08 00        ...   addd #0800
AC4A 8F              .     xgdx 
AC4B EC 00           ..    ldd 00,x
AC4D 84 80           ..    anda #80
AC4F 27 2A           '*    beq 2a
AC51 F6 00 05        ...   ldab 0005
AC54 4F              O     clra 
AC55 05              .     asld 
AC56 C3 08 C8        ...   addd #08c8
AC59 8F              .     xgdx 
AC5A EC 00           ..    ldd 00,x
AC5C 84 02           ..    anda #02
AC5E 27 06           '.    beq 06
AC60 CE 00 19        ...   ldx #0019
AC63 1C 00           ..    bset add,x 00,x
AC65 01              .     nop 
AC66 F6 00 05        ...   ldab 0005
AC69 4F              O     clra 
AC6A 05              .     asld 
AC6B C3 08 C8        ...   addd #08c8
AC6E 8F              .     xgdx 
AC6F EC 00           ..    ldd 00,x
AC71 84 04           ..    anda #04
AC73 27 06           '.    beq 06
AC75 CE 00 19        ...   ldx #0019
AC78 1C 00           ..    bset add,x 00,x
AC7A 02              .     idiv 
goto23:
AC7B 7C 00 05        |..   inc 0005
AC7E 7E AB A5        ~..   jmp aba5		;goto24
goto22:
AC81 F6 00 19        ...   ldab 0019
AC84 C4 07           ..    andb #07
AC86 26 0A           &.    bne 0a
AC88 F6 0B C1        ...   ldab 0bc1
AC8B C4 7F           ..    andb #7f
AC8D 26 03           &.    bne 03
AC8F 5F              _     clrb 
AC90 20 02            .    bra 02
AC92 C6 01           ..    ldab #01
AC94 5D              ]     tstb 
AC95 26 05           &.    bne 05
AC97 C6 0D           ..    ldab #0d
AC99 F7 00 48        ..H   stb 0048
AC9C 39              9     rts 

AC9D 08              .     inx 
AC9E 07              .     tpa 
AC9F 06              .     tap 
ACA0 05              .     asld 
ACA1 04              .     lsrd 
ACA2 03              .     fdiv 
ACA3 02              .     idiv 
ACA4 01              .     nop 
ACA5 0B              .     sev 
ACA6 0A              .     clv 
ACA7 09              .     dex 

ACA8 20 20                 bra 20
ACAA 50              P     negb 
ACAB 52              R     illegal 
ACAC 49              I     rola 
ACAD 43              C     coma 
ACAE 45              E     illegal 
ACAF 20 20                 bra 20
ACB1 20 00            .    bra 00
ACB3 20 20                 bra 20
ACB5 20 50            P    bra 50
ACB7 52              R     illegal 
ACB8 49              I     rola 
ACB9 58              X     aslb 
ACBA 20 20                 bra 20
ACBC 20 00            .    bra 00
ACBE 20 20                 bra 20
ACC0 50              P     negb 
ACC1 52              R     illegal 
ACC2 45              E     illegal 
ACC3 49              I     rola 
ACC4 53              S     comb 
ACC5 20 20                 bra 20
ACC7 20 00            .    bra 00
ACC9 20 20                 bra 20
ACCB 50              P     negb 
ACCC 52              R     illegal 
ACCD 45              E     illegal 
ACCE 43              C     coma 
ACCF 49              I     rola 
ACD0 4F              O     clra 
ACD1 20 20                 bra 20
ACD3 00              .     test 
ACD4 20 20                 bra 20
ACD6 43              C     coma 
ACD7 48              H     asla 
ACD8 41              A     illegal 
ACD9 4E              N     illegal 
ACDA 47              G     asra 
ACDB 45              E     illegal 
ACDC 20 20                 bra 20
ACDE 00              .     test 
ACDF 20 20                 bra 20
ACE1 43              C     coma 
ACE2 41              A     illegal 
ACE3 4D              M     tsta 
ACE4 42              B     illegal 
ACE5 49              I     rola 
ACE6 4F              O     clra 
ACE7 20 20                 bra 20
ACE9 00              .     test 
ACEA 20 4D            M    bra 4d
ACEC 4F              O     clra 
ACED 4E              N     illegal 
ACEE 4E              N     illegal 
ACEF 41              A     illegal 
ACF0 49              I     rola 
ACF1 45              E     illegal 
ACF2 20 20                 bra 20
ACF4 00              .     test 
ACF5 20 20                 bra 20
ACF7 54              T     lsrb 
ACF8 48              H     asla 
ACF9 41              A     illegal 
ACFA 4E              N     illegal 
ACFB 4B              K     illegal 
ACFC 53              S     comb 
ACFD 20 20                 bra 20
ACFF 00              .     test 
AD00 20 20                 bra 20
AD02 4D              M     tsta 
AD03 45              E     illegal 
AD04 52              R     illegal 
AD05 43              C     coma 
AD06 49              I     rola 
AD07 20 20                 bra 20
AD09 20 00            .    bra 00
AD0B 20 20                 bra 20
AD0D 44              D     lsra 
AD0E 41              A     illegal 
AD0F 4E              N     illegal 
AD10 4B              K     illegal 
AD11 45              E     illegal 
AD12 20 20                 bra 20
AD14 20 00            .    bra 00
AD16 20 20                 bra 20
AD18 47              G     asra 
AD19 52              R     illegal 
AD1A 41              A     illegal 
AD1B 43              C     coma 
AD1C 49              I     rola 
AD1D 41              A     illegal 
AD1E 53              S     comb 
AD1F 20 00            .    bra 00
AD21 20 20                 bra 20
AD23 53              S     comb 
AD24 45              E     illegal 
AD25 4C              L     inca 
AD26 45              E     illegal 
AD27 43              C     coma 
AD28 54              T     lsrb 
AD29 20 20                 bra 20
AD2B 00              .     test 
AD2C 20 20                 bra 20
AD2E 4F              O     clra 
AD2F 54              T     lsrb 
AD30 48              H     asla 
AD31 45              E     illegal 
AD32 52              R     illegal 
AD33 20 20                 bra 20
AD35 20 00            .    bra 00
AD37 20 20                 bra 20
AD39 20 49            I    bra 49
AD3B 54              T     lsrb 
AD3C 45              E     illegal 
AD3D 4D              M     tsta 
AD3E 20 20                 bra 20
AD40 20 00            .    bra 00
AD42 20 20                 bra 20
AD44 46              F     rora 
AD45 41              A     illegal 
AD46 49              I     rola 
AD47 54              T     lsrb 
AD48 45              E     illegal 
AD49 53              S     comb 
AD4A 20 20                 bra 20
AD4C 00              .     test 
AD4D 20 20                 bra 20
AD4F 41              A     illegal 
AD50 55              U     illegal 
AD51 54              T     lsrb 
AD52 52              R     illegal 
AD53 45              E     illegal 
AD54 20 20                 bra 20
AD56 20 00            .    bra 00
AD58 20 20                 bra 20
AD5A 43              C     coma 
AD5B 48              H     asla 
AD5C 4F              O     clra 
AD5D 49              I     rola 
AD5E 58              X     aslb 
AD5F 20 20                 bra 20
AD61 20 00            .    bra 00
AD63 20 20                 bra 20
AD65 42              B     illegal 
AD66 49              I     rola 
AD67 54              T     lsrb 
AD68 54              T     lsrb 
AD69 45              E     illegal 
AD6A 20 20                 bra 20
AD6C 20 00            .    bra 00
AD6E 20 20                 bra 20
AD70 41              A     illegal 
AD71 4E              N     illegal 
AD72 44              D     lsra 
AD73 45              E     illegal 
AD74 52              R     illegal 
AD75 45              E     illegal 
AD76 20 20                 bra 20
AD78 00              .     test 
AD79 20 20                 bra 20
AD7B 20 57            W    bra 57
AD7D 41              A     illegal 
AD7E 48              H     asla 
AD7F 4C              L     inca 
AD80 20 20                 bra 20
AD82 20 00            .    bra 00
AD84 20 20                 bra 20
AD86 50              P     negb 
AD87 55              U     illegal 
AD88 4C              L     inca 
AD89 53              S     comb 
AD8A 45              E     illegal 
AD8B 20 20                 bra 20
AD8D 20 00            .    bra 00
AD8F 20 20                 bra 20
AD91 45              E     illegal 
AD92 58              X     aslb 
AD93 54              T     lsrb 
AD94 52              R     illegal 
AD95 41              A     illegal 
AD96 20 20                 bra 20
AD98 20 00            .    bra 00
AD9A 20 53            S    bra 53
AD9C 45              E     illegal 
AD9D 4C              L     inca 
AD9E 45              E     illegal 
AD9F 43              C     coma 
ADA0 43              C     coma 
ADA1 49              I     rola 
ADA2 4F              O     clra 
ADA3 4E              N     illegal 
ADA4 00              .     test 
ADA5 20 45            E    bra 45
ADA7 58              X     aslb 
ADA8 41              A     illegal 
ADA9 43              C     coma 
ADAA 54              T     lsrb 
ADAB 20 43            C    bra 43
ADAD 4F              O     clra 
ADAE 49              I     rola 
ADAF 4E              N     illegal 
ADB0 53              S     comb 
ADB1 20 4F            O    bra 4f
ADB3 4E              N     illegal 
ADB4 4C              L     inca 
ADB5 59              Y     rolb 
ADB6 20 00            .    bra 00
ADB8 20 4D            M    bra 4d
ADBA 4F              O     clra 
ADBB 4E              N     illegal 
ADBC 4E              N     illegal 
ADBD 41              A     illegal 
ADBE 49              I     rola 
ADBF 45              E     illegal 
ADC0 20 45            E    bra 45
ADC2 58              X     aslb 
ADC3 41              A     illegal 
ADC4 43              C     coma 
ADC5 54              T     lsrb 
ADC6 45              E     illegal 
ADC7 20 53            S    bra 53
ADC9 45              E     illegal 
ADCA 55              U     illegal 
ADCB 4C              L     inca 
ADCC 45              E     illegal 
ADCD 4D              M     tsta 
ADCE 45              E     illegal 
ADCF 4E              N     illegal 
ADD0 54              T     lsrb 
ADD1 20 00            .    bra 00
ADD3 20 4E            N    bra 4e
ADD5 49              I     rola 
ADD6 43              C     coma 
ADD7 48              H     asla 
ADD8 54              T     lsrb 
ADD9 20 55            U    bra 55
ADDB 45              E     illegal 
ADDC 42              B     illegal 
ADDD 45              E     illegal 
ADDE 52              R     illegal 
ADDF 5A              Z     decb 
ADE0 41              A     illegal 
ADE1 48              H     asla 
ADE2 4C              L     inca 
ADE3 45              E     illegal 
ADE4 4E              N     illegal 
ADE5 20 00            .    bra 00
ADE7 20 49            I    bra 49
ADE9 4E              N     illegal 
ADEA 54              T     lsrb 
ADEB 52              R     illegal 
ADEC 4F              O     clra 
ADED 44              D     lsra 
ADEE 55              U     illegal 
ADEF 5A              Z     decb 
ADF0 43              C     coma 
ADF1 41              A     illegal 
ADF2 20 50            P    bra 50
ADF4 52              R     illegal 
ADF5 45              E     illegal 
ADF6 43              C     coma 
ADF7 49              I     rola 
ADF8 4F              O     clra 
ADF9 20 45            E    bra 45
ADFB 58              X     aslb 
ADFC 41              A     illegal 
ADFD 43              C     coma 
ADFE 54              T     lsrb 
ADFF 4F              O     clra 
AE00 20 00            .    bra 00
AE02 20 43            C    bra 43
AE04 4F              O     clra 
AE05 49              I     rola 
AE06 4E              N     illegal 
AE07 53              S     comb 
AE08 20 4F            O    bra 4f
AE0A 4E              N     illegal 
AE0B 4C              L     inca 
AE0C 59              Y     rolb 
AE0D 20 00            .    bra 00
AE0F 20 4D            M    bra 4d
AE11 4F              O     clra 
AE12 4E              N     illegal 
AE13 4E              N     illegal 
AE14 41              A     illegal 
AE15 49              I     rola 
AE16 45              E     illegal 
AE17 20 53            S    bra 53
AE19 45              E     illegal 
AE1A 55              U     illegal 
AE1B 4C              L     inca 
AE1C 45              E     illegal 
AE1D 4D              M     tsta 
AE1E 45              E     illegal 
AE1F 4E              N     illegal 
AE20 54              T     lsrb 
AE21 20 00            .    bra 00
AE23 20 4E            N    bra 4e
AE25 55              U     illegal 
AE26 52              R     illegal 
AE27 20 4D            M    bra 4d
AE29 55              U     illegal 
AE2A 4E              N     illegal 
AE2B 5A              Z     decb 
AE2C 45              E     illegal 
AE2D 4E              N     illegal 
AE2E 20 00            .    bra 00
AE30 20 4D            M    bra 4d
AE32 4F              O     clra 
AE33 4E              N     illegal 
AE34 45              E     illegal 
AE35 44              D     lsra 
AE36 41              A     illegal 
AE37 20 55            U    bra 55
AE39 4E              N     illegal 
AE3A 49              I     rola 
AE3B 43              C     coma 
AE3C 41              A     illegal 
AE3D 4D              M     tsta 
AE3E 45              E     illegal 
AE3F 4E              N     illegal 
AE40 54              T     lsrb 
AE41 45              E     illegal 
AE42 20 00            .    bra 00
AE44 20 4D            M    bra 4d
AE46 41              A     illegal 
AE47 43              C     coma 
AE48 48              H     asla 
AE49 49              I     rola 
AE4A 4E              N     illegal 
AE4B 45              E     illegal 
AE4C 20 4F            O    bra 4f
AE4E 55              U     illegal 
AE4F 54              T     lsrb 
AE50 20 4F            O    bra 4f
AE52 46              F     rora 
AE53 20 4F            O    bra 4f
AE55 52              R     illegal 
AE56 44              D     lsra 
AE57 45              E     illegal 
AE58 52              R     illegal 
AE59 20 00            .    bra 00
AE5B 20 48            H    bra 48
AE5D 4F              O     clra 
AE5E 52              R     illegal 
AE5F 53              S     comb 
AE60 20 44            D    bra 44
AE62 45              E     illegal 
AE63 20 53            S    bra 53
AE65 45              E     illegal 
AE66 52              R     illegal 
AE67 56              V     rorb 
AE68 49              I     rola 
AE69 43              C     coma 
AE6A 45              E     illegal 
AE6B 20 00            .    bra 00
AE6D 20 41            A    bra 41
AE6F 55              U     illegal 
AE70 53              S     comb 
AE71 53              S     comb 
AE72 45              E     illegal 
AE73 52              R     illegal 
AE74 20 42            B    bra 42
AE76 45              E     illegal 
AE77 54              T     lsrb 
AE78 52              R     illegal 
AE79 49              I     rola 
AE7A 45              E     illegal 
AE7B 42              B     illegal 
AE7C 20 00            .    bra 00
AE7E 20 46            F    bra 46
AE80 55              U     illegal 
AE81 45              E     illegal 
AE82 52              R     illegal 
AE83 41              A     illegal 
AE84 20 44            D    bra 44
AE86 45              E     illegal 
AE87 20 53            S    bra 53
AE89 45              E     illegal 
AE8A 52              R     illegal 
AE8B 56              V     rorb 
AE8C 49              I     rola 
AE8D 43              C     coma 
AE8E 49              I     rola 
AE8F 4F              O     clra 
AE90 20 00            .    bra 00

AE92 2D 20           -     blt 20
AE94 20 20                 bra 20
AE96 20 20                 bra 20
AE98 20 20                 bra 20
AE9A 20 20                 bra 20
AE9C 00              .     test 

AE9D 53              S     comb 
AE9E 45              E     illegal 
AE9F 54              T     lsrb 
AEA0 20 50            P    bra 50
AEA2 52              R     illegal 
AEA3 49              I     rola 
AEA4 43              C     coma 
AEA5 45              E     illegal 
AEA6 3F              ?     swi 
AEA7 00              .     test 

AEA8 20 20                 bra 20
AEAA 53              S     comb 
AEAB 59              Y     rolb 
AEAC 53              S     comb 
AEAD 54              T     lsrb 
AEAE 45              E     illegal 
AEAF 4D              M     tsta 
AEB0 20 20                 bra 20
AEB2 00              .     test 

AEB3 20 20                 bra 20
AEB5 20 20                 bra 20
AEB7 4F              O     clra 
AEB8 4B              K     illegal 
AEB9 20 20                 bra 20
AEBB 20 20                 bra 20
AEBD 00              .     test 

AEBE 24 35           $5    bcc 35
AEC0 20 45            E    bra 45
AEC2 4E              N     illegal 
AEC3 41              A     illegal 
AEC4 42              B     illegal 
AEC5 4C              L     inca 
AEC6 45              E     illegal 
AEC7 20 00            .    bra 00

AEC9 24 35           $5    bcc 35
AECB 20 44            D    bra 44
AECD 49              I     rola 
AECE 53              S     comb 
AECF 41              A     illegal 
AED0 42              B     illegal 
AED1 4C              L     inca 
AED2 45              E     illegal 
AED3 00              .     test 

AED4 53              S     comb 
AED5 4F              O     clra 
AED6 44              D     lsra 
AED7 41              A     illegal 
AED8 20 56            V    bra 56
AEDA 45              E     illegal 
AEDB 4E              N     illegal 
AEDC 44              D     lsra 
AEDD 20 00            .    bra 00
AEDF 53              S     comb 
AEE0 4E              N     illegal 
AEE1 41              A     illegal 
AEE2 43              C     coma 
AEE3 4B              K     illegal 
AEE4 20 56            V    bra 56
AEE6 45              E     illegal 
AEE7 4E              N     illegal 
AEE8 44              D     lsra 
AEE9 00              .     test 

AEEA 44              D     lsra 
AEEB 55              U     illegal 
AEEC 4D              M     tsta 
AEED 42              B     illegal 
AEEE 20 4D            M    bra 4d
AEF0 45              E     illegal 
AEF1 43              C     coma 
AEF2 20 59            Y    bra 59
AEF4 00              .     test 
AEF5 44              D     lsra 
AEF6 55              U     illegal 
AEF7 4D              M     tsta 
AEF8 42              B     illegal 
AEF9 20 4D            M    bra 4d
AEFB 45              E     illegal 
AEFC 43              C     coma 
AEFD 20 4E            N    bra 4e
AEFF 00              .     test 
AF00 4C              L     inca 
AF01 4E              N     illegal 
AF02 4B              K     illegal 
AF03 20 4D            M    bra 4d
AF05 53              S     comb 
AF06 54              T     lsrb 
AF07 52              R     illegal 
AF08 20 59            Y    bra 59
AF0A 00              .     test 
AF0B 4C              L     inca 
AF0C 4E              N     illegal 
AF0D 4B              K     illegal 
AF0E 20 4D            M    bra 4d
AF10 53              S     comb 
AF11 54              T     lsrb 
AF12 52              R     illegal 
AF13 20 4E            N    bra 4e
AF15 00              .     test 
AF16 43              C     coma 
AF17 41              A     illegal 
AF18 52              R     illegal 
AF19 44              D     lsra 
AF1A 20 52            R    bra 52
AF1C 44              D     lsra 
AF1D 52              R     illegal 
AF1E 20 59            Y    bra 59
AF20 00              .     test 
AF21 43              C     coma 
AF22 41              A     illegal 
AF23 52              R     illegal 
AF24 44              D     lsra 
AF25 20 52            R    bra 52
AF27 44              D     lsra 
AF28 52              R     illegal 
AF29 20 4E            N    bra 4e
AF2B 00              .     test 
AF2C 42              B     illegal 
AF2D 49              I     rola 
AF2E 4C              L     inca 
AF2F 4C              L     inca 
AF30 20 56            V    bra 56
AF32 41              A     illegal 
AF33 4C              L     inca 
AF34 20 59            Y    bra 59
AF36 00              .     test 
AF37 42              B     illegal 
AF38 49              I     rola 
AF39 4C              L     inca 
AF3A 4C              L     inca 
AF3B 20 56            V    bra 56
AF3D 41              A     illegal 
AF3E 4C              L     inca 
AF3F 20 4E            N    bra 4e
AF41 00              .     test 
AF42 50              P     negb 
AF43 52              R     illegal 
AF44 43              C     coma 
AF45 20 48            H    bra 48
AF47 4F              O     clra 
AF48 4C              L     inca 
AF49 44              D     lsra 
AF4A 20 59            Y    bra 59
AF4C 00              .     test 
AF4D 50              P     negb 
AF4E 52              R     illegal 
AF4F 43              C     coma 
AF50 20 48            H    bra 48
AF52 4F              O     clra 
AF53 4C              L     inca 
AF54 44              D     lsra 
AF55 20 4E            N    bra 4e
AF57 00              .     test 
AF58 50              P     negb 
AF59 52              R     illegal 
AF5A 43              C     coma 
AF5B 20 44            D    bra 44
AF5D 49              I     rola 
AF5E 53              S     comb 
AF5F 50              P     negb 
AF60 20 59            Y    bra 59
AF62 00              .     test 
AF63 50              P     negb 
AF64 52              R     illegal 
AF65 43              C     coma 
AF66 20 44            D    bra 44
AF68 49              I     rola 
AF69 53              S     comb 
AF6A 50              P     negb 
AF6B 20 4E            N    bra 4e
AF6D 00              .     test 
AF6E 46              F     rora 
AF6F 52              R     illegal 
AF70 45              E     illegal 
AF71 45              E     illegal 
AF72 20 56            V    bra 56
AF74 4E              N     illegal 
AF75 44              D     lsra 
AF76 20 59            Y    bra 59
AF78 00              .     test 
AF79 46              F     rora 
AF7A 52              R     illegal 
AF7B 45              E     illegal 
AF7C 45              E     illegal 
AF7D 20 56            V    bra 56
AF7F 4E              N     illegal 
AF80 44              D     lsra 
AF81 20 4E            N    bra 4e
AF83 00              .     test 
AF84 24 31           $1    bcc 31
AF86 20 43            C    bra 43
AF88 48              H     asla 
AF89 4E              N     illegal 
AF8A 47              G     asra 
AF8B 52              R     illegal 
AF8C 20 59            Y    bra 59
AF8E 00              .     test 
AF8F 24 31           $1    bcc 31
AF91 20 43            C    bra 43
AF93 48              H     asla 
AF94 4E              N     illegal 
AF95 47              G     asra 
AF96 52              R     illegal 
AF97 20 4E            N    bra 4e
AF99 00              .     test 
AF9A 43              C     coma 
AF9B 4C              L     inca 
AF9C 52              R     illegal 
AF9D 20 4D            M    bra 4d
AF9F 49              I     rola 
AFA0 53              S     comb 
AFA1 20 20                 bra 20
AFA3 59              Y     rolb 
AFA4 00              .     test 
AFA5 43              C     coma 
AFA6 4C              L     inca 
AFA7 52              R     illegal 
AFA8 20 4D            M    bra 4d
AFAA 49              I     rola 
AFAB 53              S     comb 
AFAC 20 20                 bra 20
AFAE 4E              N     illegal 
AFAF 00              .     test 
AFB0 48              H     asla 
AFB1 49              I     rola 
AFB2 54              T     lsrb 
AFB3 20 4B            K    bra 4b
AFB5 45              E     illegal 
AFB6 59              Y     rolb 
AFB7 22 30           "0    bhi 30
AFB9 22 00           ".    bhi 00
AFBB 45              E     illegal 
AFBC 4E              N     illegal 
AFBD 54              T     lsrb 
AFBE 45              E     illegal 
AFBF 52              R     illegal 
AFC0 20 43            C    bra 43
AFC2 4F              O     clra 
AFC3 44              D     lsra 
AFC4 45              E     illegal 
AFC5 00              .     test 
AFC6 53              S     comb 
AFC7 45              E     illegal 
AFC8 43              C     coma 
AFC9 55              U     illegal 
AFCA 52              R     illegal 
AFCB 45              E     illegal 
AFCC 20 4F            O    bra 4f
AFCE 4E              N     illegal 
AFCF 20 00            .    bra 00
AFD1 53              S     comb 
AFD2 45              E     illegal 
AFD3 43              C     coma 
AFD4 55              U     illegal 
AFD5 52              R     illegal 
AFD6 45              E     illegal 
AFD7 20 4F            O    bra 4f
AFD9 46              F     rora 
AFDA 46              F     rora 
AFDB 00              .     test 
AFDC 2D 20           -     blt 20
AFDE 53              S     comb 
AFDF 41              A     illegal 
AFE0 56              V     rorb 
AFE1 49              I     rola 
AFE2 4E              N     illegal 
AFE3 47              G     asra 
AFE4 20 2D            -    bra 2d
AFE6 00              .     test 
AFE7 2D 50           -P    blt 50
AFE9 52              R     illegal 
AFEA 49              I     rola 
AFEB 4E              N     illegal 
AFEC 54              T     lsrb 
AFED 49              I     rola 
AFEE 4E              N     illegal 
AFEF 47              G     asra 
AFF0 2D 00           -.    blt 00

AFF2 4F              O     clra 
AFF3 56              V     rorb 
AFF4 45              E     illegal 
AFF5 52              R     illegal 
AFF6 20 43            C    bra 43
AFF8 52              R     illegal 
AFF9 4E              N     illegal 
AFFA 54              T     lsrb 
AFFB 3D              =     mul 
AFFC 00              .     test 

AFFD 48              H     asla 
AFFE 4F              O     clra 
AFFF 4D              M     tsta 
B000 45              E     illegal 
B001 20 46            F    bra 46
B003 41              A     illegal 
B004 49              I     rola 
B005 4C              L     inca 
B006 3D              =     mul 
B007 00              .     test 
B008 43              C     coma 
B009 48              H     asla 
B00A 4B              K     illegal 
B00B 20 50            P    bra 50
B00D 52              R     illegal 
B00E 49              I     rola 
B00F 43              C     coma 
B010 45              E     illegal 
B011 53              S     comb 
B012 00              .     test 

B013 48              H     asla 
B014 49              I     rola 
B015 54              T     lsrb 
B016 20 52            R    bra 52
B018 45              E     illegal 
B019 53              S     comb 
B01A 45              E     illegal 
B01B 54              T     lsrb 
B01C 20 00            .    bra 00

B01E 43              C     coma 
B01F 4F              O     clra 
B020 49              I     rola 
B021 4E              N     illegal 
B022 20 20                 bra 20
B024 20 4A            J    bra 4a
B026 41              A     illegal 
B027 4D              M     tsta 
B028 00              .     test 

B029 42              B     illegal 
B02A 41              A     illegal 
B02B 44              D     lsra 
B02C 20 53            S    bra 53
B02E 45              E     illegal 
B02F 4E              N     illegal 
B030 53              S     comb 
B031 4F              O     clra 
B032 52              R     illegal 
B033 00              .     test 

B034 43              C     coma 
B035 48              H     asla 
B036 47              G     asra 
B037 52              R     illegal 
B038 20 50            P    bra 50
B03A 57              W     asrb 
B03B 52              R     illegal 
B03C 55              U     illegal 
B03D 50              P     negb 
B03E 00              .     test 

B03F 4C              L     inca 
B040 49              I     rola 
B041 4E              N     illegal 
B042 4B              K     illegal 
B043 20 50            P    bra 50
B045 57              W     asrb 
B046 52              R     illegal 
B047 55              U     illegal 
B048 50              P     negb 
B049 00              .     test 

B04A 43              C     coma 
B04B 41              A     illegal 
B04C 52              R     illegal 
B04D 44              D     lsra 
B04E 20 50            P    bra 50
B050 57              W     asrb 
B051 52              R     illegal 
B052 55              U     illegal 
B053 50              P     negb 
B054 00              .     test 

B055 42              B     illegal 
B056 49              I     rola 
B057 4C              L     inca 
B058 4C              L     inca 
B059 20 45            E    bra 45
B05B 52              R     illegal 
B05C 52              R     illegal 
B05D 4F              O     clra 
B05E 52              R     illegal 
B05F 00              .     test 

B060 54              T     lsrb 
B061 55              U     illegal 
B062 42              B     illegal 
B063 45              E     illegal 
B064 20 45            E    bra 45
B066 52              R     illegal 
B067 52              R     illegal 
B068 4F              O     clra 
B069 52              R     illegal 
B06A 00              .     test 
B06B 4E              N     illegal 
B06C 4F              O     clra 
B06D 20 20                 bra 20
B06F 43              C     coma 
B070 48              H     asla 
B071 41              A     illegal 
B072 4E              N     illegal 
B073 47              G     asra 
B074 45              E     illegal 
B075 00              .     test 
B076 20 4E            N    bra 4e
B078 4F              O     clra 
B079 20 46            F    bra 46
B07B 49              I     rola 
B07C 56              V     rorb 
B07D 45              E     illegal 
B07E 53              S     comb 
B07F 20 00            .    bra 00

B081 4E              N     illegal 
B082 4F              O     clra 
B083 20 20                 bra 20
B085 41              A     illegal 
B086 43              C     coma 
B087 43              C     coma 
B088 45              E     illegal 
B089 53              S     comb 
B08A 53              S     comb 
B08B 00              .     test 
B08C 4E              N     illegal 
B08D 4F              O     clra 
B08E 20 20                 bra 20
B090 45              E     illegal 
B091 52              R     illegal 
B092 52              R     illegal 
B093 4F              O     clra 
B094 52              R     illegal 
B095 53              S     comb 
B096 00              .     test 
B097 54              T     lsrb 
B098 45              E     illegal 
B099 53              S     comb 
B09A 54              T     lsrb 
B09B 20 20                 bra 20
B09D 56              V     rorb 
B09E 45              E     illegal 
B09F 4E              N     illegal 
B0A0 44              D     lsra 
B0A1 00              .     test 
B0A2 4D              M     tsta 
B0A3 54              T     lsrb 
B0A4 52              R     illegal 
B0A5 20 46            F    bra 46
B0A7 55              U     illegal 
B0A8 4E              N     illegal 
B0A9 43              C     coma 
B0AA 54              T     lsrb 
B0AB 3F              ?     swi 
B0AC 00              .     test 
B0AD 50              P     negb 
B0AE 52              R     illegal 
B0AF 4F              O     clra 
B0B0 47              G     asra 
B0B1 52              R     illegal 
B0B2 41              A     illegal 
B0B3 4D              M     tsta 
B0B4 20 20                 bra 20
B0B6 3F              ?     swi 
B0B7 00              .     test 
B0B8 53              S     comb 
B0B9 45              E     illegal 
B0BA 4C              L     inca 
B0BB 45              E     illegal 
B0BC 43              C     coma 
B0BD 54              T     lsrb 
B0BE 49              I     rola 
B0BF 4F              O     clra 
B0C0 4E              N     illegal 
B0C1 3F              ?     swi 
B0C2 00              .     test 

B0C3 14              .     bset 
B0C4 00              .     test 
B0C5 14              .     bset 
B0C6 C8 05           ..    eorb #05
B0C8 00              .     test 
B0C9 05              .     asld 
B0CA 00              .     test 
B0CB 02              .     idiv 
B0CC 00              .     test 
B0CD 02              .     idiv 
B0CE 00              .     test 
B0CF 01              .     nop 
B0D0 00              .     test 
B0D1 01              .     nop 
B0D2 00              .     test 
B0D3 01              .     nop 
B0D4 0C              .     clc 
B0D5 17              .     tba 
B0D6 22 2E           ".    bhi 2e
B0D8 39              9     rts 

B0D9 44              D     lsra 
B0DA 4F              O     clra 
B0DB 0B              .     sev 
B0DC 16              .     tab 
B0DD 21 2C           !,    brn 2c
B0DF 38              8     pulx 
B0E0 43              C     coma 
B0E1 4E              N     illegal 
B0E2 59              Y     rolb 
B0E3 15              .     bclr 
B0E4 20 2B            +    bra 2b
B0E6 36              6     psha 
B0E7 42              B     illegal 
B0E8 4D              M     tsta 
B0E9 58              X     aslb 
B0EA 63 1F           c.    com 1f,x
B0EC 2A 35           *5    bpl 35
B0EE 40              @     nega 
B0EF 4C              L     inca 
B0F0 57              W     asrb 
B0F1 62              b     illegal 
B0F2 09              .     dex 
B0F3 29 34           )4    bvs 34
B0F5 3F              ?     swi 
B0F6 4A              J     deca 
B0F7 56              V     rorb 
B0F8 61              a     illegal 
B0F9 08              .     inx 
B0FA 13              .     brclr 
B0FB 33              3     pulb 
B0FC 3E              >     wai 
B0FD 49              I     rola 
B0FE 54              T     lsrb 
B0FF 60 07           `.    neg 07,x
B101 12              .     brset 
B102 1D 3D           .=    bclr add,x 3d,x
B104 48              H     asla 
B105 53              S     comb 
B106 5E              ^     illegal 
B107 06              .     tap 
B108 11              .     cba 
B109 1C 27           .'    bset add,x 27,x
B10B 47              G     asra 
B10C 52              R     illegal 
B10D 5D              ]     tstb 
B10E 04              .     lsrd 
B10F 10              .     sba 
B110 1B              .     aba 
B111 26 31           &1    bne 31
B113 51              Q     illegal 
B114 5C              \     incb 
B115 03              .     fdiv 
B116 0E              .     cli 
B117 1A              .     illegal 
B118 25 30           %0    bcs 30
B11A 3B              ;     rti 
B11B 5B              [     illegal 
B11C 02              .     idiv 
B11D 0D              .     sec 
B11E 18              .     illegal 
B11F 24 2F           $/    bcc 2f
B121 3A              :     abx 
B122 45              E     illegal 

table of powers of 2

B123 00 01 02 04 08
B128 10 20 40 80

B12C 00
B12D 00              .     test 
B12E 2A 2A           **    bpl 2a	;"**ERROR***"
B130 45              E     illegal 
B131 52              R     illegal 
B132 52              R     illegal 
B133 4F              O     clra 
B134 52              R     illegal 
B135 2A 2A           **    bpl 2a
B137 2A 00           *.    bpl 00

B139 2A 2A           **    bpl 2a
B13B 2A 2A           **    bpl 2a
B13D 2A 2A           **    bpl 2a
B13F 2A 2A           **    bpl 2a
B141 2A 2A           **    bpl 2a
B143 00              .     test 
B144 4F              O     clra 
B145 4F              O     clra 
B146 4F              O     clra 
B147 4F              O     clra 
B148 4F              O     clra 
B149 4F              O     clra 
B14A 4F              O     clra 
B14B 4F              O     clra 
B14C 4F              O     clra 
B14D 4F              O     clra 
B14E 00              .     test 
B14F 52              R     illegal 
B150 41              A     illegal 
B151 4D              M     tsta 
B152 20 20                 bra 20
B154 20 46            F    bra 46
B156 41              A     illegal 
B157 49              I     rola 
B158 4C              L     inca 
B159 00              .     test 
B15A 50              P     negb 
B15B 32              2     pula 
B15C 20 20                 bra 20
B15E 20 20                 bra 20
B160 46              F     rora 
B161 41              A     illegal 
B162 49              I     rola 
B163 4C              L     inca 
B164 00              .     test 
B165 50              P     negb 
B166 32              2     pula 
B167 2D 50           -P    blt 50
B169 33              3     pulb 
B16A 20 46            F    bra 46
B16C 41              A     illegal 
B16D 49              I     rola 
B16E 4C              L     inca 
B16F 00              .     test 
B170 50              P     negb 
B171 32              2     pula 
B172 2D 50           -P    blt 50
B174 38              8     pulx 
B175 20 46            F    bra 46
B177 41              A     illegal 
B178 49              I     rola 
B179 4C              L     inca 
B17A 00              .     test 
B17B 50              P     negb 
B17C 32              2     pula 
B17D 2D 50           -P    blt 50
B17F 39              9     rts 

B180 20 46            F    bra 46
B182 41              A     illegal 
B183 49              I     rola 
B184 4C              L     inca 
B185 00              .     test 
B186 50              P     negb 
B187 33              3     pulb 
B188 2D 50           -P    blt 50
B18A 34              4     des 
B18B 20 46            F    bra 46
B18D 41              A     illegal 
B18E 49              I     rola 
B18F 4C              L     inca 
B190 00              .     test 
B191 50              P     negb 
B192 35              5     txs 
B193 20 20                 bra 20
B195 20 20                 bra 20
B197 46              F     rora 
B198 41              A     illegal 
B199 49              I     rola 
B19A 4C              L     inca 
B19B 00              .     test 
B19C 50              P     negb 
B19D 35              5     txs 
B19E 2D 50           -P    blt 50
B1A0 37              7     pshb 
B1A1 20 46            F    bra 46
B1A3 41              A     illegal 
B1A4 49              I     rola 
B1A5 4C              L     inca 
B1A6 00              .     test 
B1A7 50              P     negb 
B1A8 37              7     pshb 
B1A9 20 20                 bra 20
B1AB 20 20                 bra 20
B1AD 46              F     rora 
B1AE 41              A     illegal 
B1AF 49              I     rola 
B1B0 4C              L     inca 
B1B1 00              .     test 
B1B2 50              P     negb 
B1B3 37              7     pshb 
B1B4 2D 50           -P    blt 50
B1B6 38              8     pulx 
B1B7 20 46            F    bra 46
B1B9 41              A     illegal 
B1BA 49              I     rola 
B1BB 4C              L     inca 
B1BC 00              .     test 
B1BD 2D 20           -     blt 20
B1BF 50              P     negb 
B1C0 41              A     illegal 
B1C1 53              S     comb 
B1C2 53              S     comb 
B1C3 45              E     illegal 
B1C4 44              D     lsra 
B1C5 20 2D            -    bra 2d
B1C7 00              .     test 
B1C8 09              .     dex 
B1C9 12              .     brset 
B1CA 1B              .     aba 
B1CB 24 2C           $,    bcc 2c
B1CD 35              5     txs 
B1CE 3E              >     wai 
B1CF 47              G     asra 
B1D0 54              T     lsrb 
B1D1 48              H     asla 
B1D2 41              A     illegal 
B1D3 4E              N     illegal 
B1D4 4B              K     illegal 
B1D5 20 59            Y    bra 59
B1D7 4F              O     clra 
B1D8 55              U     illegal 
B1D9 20 46            F    bra 46
B1DB 4F              O     clra 
B1DC 52              R     illegal 
B1DD 20 59            Y    bra 59
B1DF 4F              O     clra 
B1E0 55              U     illegal 
B1E1 52              R     illegal 
B1E2 20 50            P    bra 50
B1E4 41              A     illegal 
B1E5 54              T     lsrb 
B1E6 52              R     illegal 
B1E7 4F              O     clra 
B1E8 4E              N     illegal 
B1E9 41              A     illegal 
B1EA 47              G     asra 
B1EB 45              E     illegal 
B1EC 20 20                 bra 20
B1EE 00              .     test 

B1EF 20 52            R    bra 52
B1F1 4F              O     clra 
B1F2 57              W     asrb 
B1F3 45              E     illegal 
B1F4 20 49            I    bra 49
B1F6 4E              N     illegal 
B1F7 54              T     lsrb 
B1F8 20 00            .    bra 00

B1FA 43              C     coma 
B1FB 4F              O     clra 
B1FC 50              P     negb 
B1FD 59              Y     rolb 
B1FE 52              R     illegal 
B1FF 49              I     rola 
B200 47              G     asra 
B201 48              H     asla 
B202 54              T     lsrb 
B203 20 00            .    bra 00

B205 20 20                 bra 20
B207 20 31            1    bra 31
B209 39              9     rts 
B20A 39              9     rts 
B20B 33              3     pulb 
B20C 20 20                 bra 20
B20E 20 00            .    bra 00

B210 35              5     txs 
B211 39              9     rts 
B212 30              0     tsx 
B213 30              0     tsx 
B214 20 56            V    bra 56
B216 45              E     illegal 
B217 52              R     illegal 
B218 20 35            5    bra 35
B21A 00              .     test 

B21B 20 20                 bra 20
B21D 20 20                 bra 20
B21F 20 20                 bra 20
B221 20 20                 bra 20
B223 20 52            R    bra 52
B225 4F              O     clra 
B226 57              W     asrb 
B227 45              E     illegal 
B228 20 20                 bra 20
B22A 49              I     rola 
B22B 4E              N     illegal 
B22C 54              T     lsrb 
B22D 45              E     illegal 
B22E 52              R     illegal 
B22F 4E              N     illegal 
B230 41              A     illegal 
B231 54              T     lsrb 
B232 49              I     rola 
B233 4F              O     clra 
B234 4E              N     illegal 
B235 41              A     illegal 
B236 4C              L     inca 
B237 00              .     test 
B238 20 20                 bra 20
B23A 20 20                 bra 20
B23C 20 20                 bra 20
B23E 20 20                 bra 20
B240 2D 2D           --    blt 2d
B242 2D 2D           --    blt 2d
B244 2D 2D           --    blt 2d
B246 2D 2D           --    blt 2d
B248 2D 2D           --    blt 2d
B24A 2D 2D           --    blt 2d
B24C 2D 2D           --    blt 2d
B24E 2D 2D           --    blt 2d
B250 2D 2D           --    blt 2d
B252 2D 2D           --    blt 2d
B254 2D 00           -.    blt 00
B256 20 20                 bra 20
B258 20 20                 bra 20
B25A 20 20                 bra 20
B25C 20 20                 bra 20
B25E 35              5     txs 
B25F 39              9     rts 
B260 30              0     tsx 
B261 30              0     tsx 
B262 20 53            S    bra 53
B264 4F              O     clra 
B265 46              F     rora 
B266 54              T     lsrb 
B267 57              W     asrb 
B268 41              A     illegal 
B269 52              R     illegal 
B26A 45              E     illegal 
B26B 20 56            V    bra 56
B26D 45              E     illegal 
B26E 52              R     illegal 
B26F 2E 20           .     bgt 20
B271 35              5     txs 
B272 20 20                 bra 20
B274 00              .     test 
B275 20 20                 bra 20
B277 20 20                 bra 20
B279 20 20                 bra 20
B27B 20 20                 bra 20
B27D 20 20                 bra 20
B27F 20 41            A    bra 41
B281 43              C     coma 
B282 43              C     coma 
B283 4F              O     clra 
B284 55              U     illegal 
B285 4E              N     illegal 
B286 54              T     lsrb 
B287 20 20                 bra 20
B289 52              R     illegal 
B28A 45              E     illegal 
B28B 43              C     coma 
B28C 4F              O     clra 
B28D 52              R     illegal 
B28E 44              D     lsra 
B28F 00              .     test 
B290 53              S     comb 
B291 45              E     illegal 
B292 52              R     illegal 
B293 49              I     rola 
B294 41              A     illegal 
B295 4C              L     inca 
B296 20 20                 bra 20
B298 23 20           #     bls 20
B29A 20 20                 bra 20
B29C 20 20                 bra 20
B29E 20 2D            -    bra 2d
B2A0 20 00            .    bra 00
B2A2 4D              M     tsta 
B2A3 41              A     illegal 
B2A4 43              C     coma 
B2A5 48              H     asla 
B2A6 49              I     rola 
B2A7 4E              N     illegal 
B2A8 45              E     illegal 
B2A9 20 49            I    bra 49
B2AB 44              D     lsra 
B2AC 20 20                 bra 20
B2AE 20 20                 bra 20
B2B0 20 2D            -    bra 2d
B2B2 20 00            .    bra 00
B2B4 41              A     illegal 
B2B5 55              U     illegal 
B2B6 44              D     lsra 
B2B7 49              I     rola 
B2B8 54              T     lsrb 
B2B9 20 4E            N    bra 4e
B2BB 55              U     illegal 
B2BC 4D              M     tsta 
B2BD 42              B     illegal 
B2BE 45              E     illegal 
B2BF 52              R     illegal 
B2C0 20 20                 bra 20
B2C2 20 20                 bra 20
B2C4 20 00            .    bra 00
B2C6 53              S     comb 
B2C7 41              A     illegal 
B2C8 4C              L     inca 
B2C9 45              E     illegal 
B2CA 53              S     comb 
B2CB 20 20                 bra 20
B2CD 28 52           (R    bvc 52
B2CF 29 20           )     bvs 20
B2D1 20 20                 bra 20
B2D3 20 20                 bra 20
B2D5 2D 20           -     blt 20
B2D7 00              .     test 
B2D8 53              S     comb 
B2D9 41              A     illegal 
B2DA 4C              L     inca 
B2DB 45              E     illegal 
B2DC 53              S     comb 
B2DD 20 20                 bra 20
B2DF 28 4E           (N    bvc 4e
B2E1 29 20           )     bvs 20
B2E3 20 20                 bra 20
B2E5 20 20                 bra 20
B2E7 2D 20           -     blt 20
B2E9 00              .     test 
B2EA 42              B     illegal 
B2EB 41              A     illegal 
B2EC 47              G     asra 
B2ED 20 54            T    bra 54
B2EF 4F              O     clra 
B2F0 54              T     lsrb 
B2F1 41              A     illegal 
B2F2 4C              L     inca 
B2F3 20 20                 bra 20
B2F5 20 20                 bra 20
B2F7 20 20                 bra 20
B2F9 2D 20           -     blt 20
B2FB 00              .     test 
B2FC 43              C     coma 
B2FD 41              A     illegal 
B2FE 53              S     comb 
B2FF 48              H     asla 
B300 20 42            B    bra 42
B302 4F              O     clra 
B303 58              X     aslb 
B304 20 20                 bra 20
B306 20 20                 bra 20
B308 20 20                 bra 20
B30A 20 2D            -    bra 2d
B30C 20 00            .    bra 00
B30E 43              C     coma 
B30F 41              A     illegal 
B310 52              R     illegal 
B311 44              D     lsra 
B312 20 53            S    bra 53
B314 41              A     illegal 
B315 4C              L     inca 
B316 45              E     illegal 
B317 53              S     comb 
B318 20 20                 bra 20
B31A 20 20                 bra 20
B31C 20 2D            -    bra 2d
B31E 20 00            .    bra 00
B320 42              B     illegal 
B321 49              I     rola 
B322 4C              L     inca 
B323 4C              L     inca 
B324 53              S     comb 
B325 20 49            I    bra 49
B327 4E              N     illegal 
B328 20 53            S    bra 53
B32A 54              T     lsrb 
B32B 41              A     illegal 
B32C 43              C     coma 
B32D 4B              K     illegal 
B32E 45              E     illegal 
B32F 52              R     illegal 
B330 00              .     test 
B331 20 20                 bra 20
B333 20 20                 bra 20
B335 42              B     illegal 
B336 49              I     rola 
B337 4C              L     inca 
B338 4C              L     inca 
B339 20 54            T    bra 54
B33B 4F              O     clra 
B33C 54              T     lsrb 
B33D 41              A     illegal 
B33E 4C              L     inca 
B33F 20 2D            -    bra 2d
B341 20 20                 bra 20
B343 20 20                 bra 20
B345 20 20                 bra 20
B347 20 20                 bra 20
B349 00              .     test 
B34A 20 20                 bra 20
B34C 20 20                 bra 20
B34E 4F              O     clra 
B34F 4E              N     illegal 
B350 45              E     illegal 
B351 53              S     comb 
B352 20 20                 bra 20
B354 20 20                 bra 20
B356 20 20                 bra 20
B358 20 2D            -    bra 2d
B35A 20 00            .    bra 00
B35C 20 20                 bra 20
B35E 20 20                 bra 20
B360 54              T     lsrb 
B361 57              W     asrb 
B362 4F              O     clra 
B363 53              S     comb 
B364 20 20                 bra 20
B366 20 20                 bra 20
B368 20 20                 bra 20
B36A 20 2D            -    bra 2d
B36C 20 00            .    bra 00
B36E 20 20                 bra 20
B370 20 20                 bra 20
B372 46              F     rora 
B373 49              I     rola 
B374 56              V     rorb 
B375 45              E     illegal 
B376 53              S     comb 
B377 20 20                 bra 20
B379 20 20                 bra 20
B37B 20 20                 bra 20
B37D 2D 20           -     blt 20
B37F 00              .     test 
B380 20 20                 bra 20
B382 20 20                 bra 20
B384 54              T     lsrb 
B385 45              E     illegal 
B386 4E              N     illegal 
B387 53              S     comb 
B388 20 20                 bra 20
B38A 20 20                 bra 20
B38C 20 20                 bra 20
B38E 20 2D            -    bra 2d
B390 20 00            .    bra 00
B392 20 20                 bra 20
B394 20 20                 bra 20
B396 54              T     lsrb 
B397 57              W     asrb 
B398 45              E     illegal 
B399 4E              N     illegal 
B39A 54              T     lsrb 
B39B 49              I     rola 
B39C 45              E     illegal 
B39D 53              S     comb 
B39E 20 20                 bra 20
B3A0 20 2D            -    bra 2d
B3A2 20 00            .    bra 00
B3A4 43              C     coma 
B3A5 4F              O     clra 
B3A6 49              I     rola 
B3A7 4E              N     illegal 
B3A8 53              S     comb 
B3A9 20 49            I    bra 49
B3AB 4E              N     illegal 
B3AC 20 54            T    bra 54
B3AE 55              U     illegal 
B3AF 42              B     illegal 
B3B0 45              E     illegal 
B3B1 53              S     comb 
B3B2 00              .     test 
B3B3 20 20                 bra 20
B3B5 20 20                 bra 20
B3B7 54              T     lsrb 
B3B8 55              U     illegal 
B3B9 42              B     illegal 
B3BA 45              E     illegal 
B3BB 20 54            T    bra 54
B3BD 4F              O     clra 
B3BE 54              T     lsrb 
B3BF 41              A     illegal 
B3C0 4C              L     inca 
B3C1 20 2D            -    bra 2d
B3C3 20 20                 bra 20
B3C5 20 20                 bra 20
B3C7 20 20                 bra 20
B3C9 00              .     test 
B3CA 20 20                 bra 20
B3CC 20 20                 bra 20
B3CE 24 31           $1    bcc 31
B3D0 20 43            C    bra 43
B3D2 4F              O     clra 
B3D3 49              I     rola 
B3D4 4E              N     illegal 
B3D5 20 20                 bra 20
B3D7 20 20                 bra 20
B3D9 2D 20           -     blt 20
B3DB 00              .     test 
B3DC 20 20                 bra 20
B3DE 20 20                 bra 20
B3E0 51              Q     illegal 
B3E1 55              U     illegal 
B3E2 41              A     illegal 
B3E3 52              R     illegal 
B3E4 54              T     lsrb 
B3E5 45              E     illegal 
B3E6 52              R     illegal 
B3E7 53              S     comb 
B3E8 20 20                 bra 20
B3EA 20 2D            -    bra 2d
B3EC 20 00            .    bra 00
B3EE 20 20                 bra 20
B3F0 20 20                 bra 20
B3F2 44              D     lsra 
B3F3 49              I     rola 
B3F4 4D              M     tsta 
B3F5 45              E     illegal 
B3F6 53              S     comb 
B3F7 20 20                 bra 20
B3F9 20 20                 bra 20
B3FB 20 20                 bra 20
B3FD 2D 20           -     blt 20
B3FF 00              .     test 
B400 20 20                 bra 20
B402 20 20                 bra 20
B404 4E              N     illegal 
B405 49              I     rola 
B406 43              C     coma 
B407 4B              K     illegal 
B408 45              E     illegal 
B409 4C              L     inca 
B40A 53              S     comb 
B40B 20 20                 bra 20
B40D 20 20                 bra 20
B40F 2D 20           -     blt 20
B411 00              .     test 
B412 57              W     asrb 
B413 49              I     rola 
B414 4E              N     illegal 
B415 2D 53           -S    blt 53
B417 4E              N     illegal 
B418 4B              K     illegal 
B419 20 56            V    bra 56
B41B 45              E     illegal 
B41C 4E              N     illegal 
B41D 44              D     lsra 
B41E 53              S     comb 
B41F 20 20                 bra 20
B421 2D 20           -     blt 20
B423 00              .     test 
B424 53              S     comb 
B425 48              H     asla 
B426 4F              O     clra 
B427 50              P     negb 
B428 50              P     negb 
B429 45              E     illegal 
B42A 52              R     illegal 
B42B 20 56            V    bra 56
B42D 45              E     illegal 
B42E 4E              N     illegal 
B42F 44              D     lsra 
B430 53              S     comb 
B431 20 20                 bra 20
B433 2D 20           -     blt 20
B435 00              .     test 
B436 50              P     negb 
B437 52              R     illegal 
B438 4F              O     clra 
B439 4D              M     tsta 
B43A 4F              O     clra 
B43B 20 56            V    bra 56
B43D 45              E     illegal 
B43E 4E              N     illegal 
B43F 44              D     lsra 
B440 53              S     comb 
B441 20 20                 bra 20
B443 20 20                 bra 20
B445 2D 20           -     blt 20
B447 00              .     test 
B448 46              F     rora 
B449 52              R     illegal 
B44A 45              E     illegal 
B44B 45              E     illegal 
B44C 20 20                 bra 20
B44E 56              V     rorb 
B44F 45              E     illegal 
B450 4E              N     illegal 
B451 44              D     lsra 
B452 53              S     comb 
B453 20 20                 bra 20
B455 20 20                 bra 20
B457 2D 20           -     blt 20
B459 00              .     test 
B45A 56              V     rorb 
B45B 45              E     illegal 
B45C 4E              N     illegal 
B45D 44              D     lsra 
B45E 53              S     comb 
B45F 20 50            P    bra 50
B461 45              E     illegal 
B462 52              R     illegal 
B463 20 50            P    bra 50
B465 52              R     illegal 
B466 4F              O     clra 
B467 44              D     lsra 
B468 55              U     illegal 
B469 43              C     coma 
B46A 54              T     lsrb 
B46B 20 43            C    bra 43
B46D 4F              O     clra 
B46E 44              D     lsra 
B46F 45              E     illegal 
B470 00              .     test 

B471 20 20                 bra 20
B473 20 20                 bra 20
B475 43              C     coma 
B476 4F              O     clra 
B477 44              D     lsra 
B478 45              E     illegal 
B479 20 00            .    bra 00
B47B 56              V     rorb 
B47C 45              E     illegal 
B47D 4E              N     illegal 
B47E 44              D     lsra 
B47F 53              S     comb 
B480 20 2D            -    bra 2d
B482 20 00            .    bra 00
B484 56              V     rorb 
B485 45              E     illegal 
B486 4E              N     illegal 
B487 44              D     lsra 
B488 53              S     comb 
B489 2F 43           /C    ble 43
B48B 4F              O     clra 
B48C 44              D     lsra 
B48D 45              E     illegal 
B48E 00              .     test 
B48F 41              A     illegal 
B490 42              B     illegal 
B491 43              C     coma 
B492 44              D     lsra 
B493 45              E     illegal 
B494 46              F     rora 
B495 47              G     asra 
B496 48              H     asla 
B497 49              I     rola 
B498 4A              J     deca 
B499 4B              K     illegal 
B49A 4C              L     inca 
B49B 4D              M     tsta 
B49C 4E              N     illegal 
B49D 4F              O     clra 
B49E 50              P     negb 
B49F 51              Q     illegal 
B4A0 52              R     illegal 
B4A1 53              S     comb 
B4A2 54              T     lsrb 
B4A3 55              U     illegal 
B4A4 56              V     rorb 
B4A5 57              W     asrb 
B4A6 58              X     aslb 
B4A7 59              Y     rolb 
B4A8 5A              Z     decb 
B4A9 30              0     tsx 
B4AA 31              1     ins 
B4AB 32              2     pula 
B4AC 33              3     pulb 
B4AD 34              4     des 
B4AE 35              5     txs 
B4AF 36              6     psha 
B4B0 37              7     pshb 
B4B1 38              8     pulx 
B4B2 39              9     rts 
B4B3 24 2A           $*    bcc 2a
B4B5 20 00            .    bra 00
B4B7 01              .     nop 
B4B8 02              .     idiv 
B4B9 04              .     lsrd 
B4BA 08              .     inx 
B4BB 10              .     sba 
B4BC 20 40            @    bra 40
B4BE 80 FF           ..    suba #ff
B4C0 FF FF FF        ...   stx ffff
B4C3 FF FF FF        ...   stx ffff
B4C6 FF FF FF        ...   stx ffff
B4C9 FF FF FF        ...   stx ffff
B4CC FF FF FF        ...   stx ffff
B4CF FF FF FF        ...   stx ffff
B4D2 FF FF FF        ...   stx ffff
B4D5 FF FF FF        ...   stx ffff
B4D8 FF FF FF        ...   stx ffff
B4DB FF FF FF        ...   stx ffff
B4DE FF FF FF        ...   stx ffff
B4E1 FF FF FF        ...   stx ffff
B4E4 FF FF FF        ...   stx ffff
B4E7 FF FF FF        ...   stx ffff
B4EA FF FF FF        ...   stx ffff
B4ED FF FF FF        ...   stx ffff
B4F0 FF FF FF        ...   stx ffff
B4F3 FF FF FF        ...   stx ffff
B4F6 FF FF FF        ...   stx ffff
B4F9 FF FF FF        ...   stx ffff
B4FC FF FF FF        ...   stx ffff
B4FF FF FF FF        ...   stx ffff
B502 FF FF FF        ...   stx ffff
B505 FF FF FF        ...   stx ffff
B508 FF FF FF        ...   stx ffff
B50B FF FF FF        ...   stx ffff
B50E FF FF FF        ...   stx ffff
B511 FF FF FF        ...   stx ffff
B514 FF FF FF        ...   stx ffff
B517 FF FF FF        ...   stx ffff
B51A FF FF FF        ...   stx ffff
B51D FF FF FF        ...   stx ffff
B520 FF FF FF        ...   stx ffff
B523 FF FF FF        ...   stx ffff
B526 FF FF FF        ...   stx ffff
B529 FF FF FF        ...   stx ffff
B52C FF FF FF        ...   stx ffff
B52F FF FF FF        ...   stx ffff
B532 FF FF FF        ...   stx ffff
B535 FF FF FF        ...   stx ffff
B538 FF FF FF        ...   stx ffff
B53B FF FF FF        ...   stx ffff
B53E FF FF FF        ...   stx ffff
B541 FF FF FF        ...   stx ffff
B544 FF FF FF        ...   stx ffff
B547 FF FF FF        ...   stx ffff
B54A FF FF FF        ...   stx ffff
B54D FF FF FF        ...   stx ffff
B550 FF FF FF        ...   stx ffff
B553 FF FF FF        ...   stx ffff
B556 FF FF FF        ...   stx ffff
B559 FF FF FF        ...   stx ffff
B55C FF FF FF        ...   stx ffff
B55F FF FF FF        ...   stx ffff
B562 FF FF FF        ...   stx ffff
B565 FF FF FF        ...   stx ffff
B568 FF FF FF        ...   stx ffff
B56B FF FF FF        ...   stx ffff
B56E FF FF FF        ...   stx ffff
B571 FF FF FF        ...   stx ffff
B574 FF FF FF        ...   stx ffff
B577 FF FF FF        ...   stx ffff
B57A FF FF FF        ...   stx ffff
B57D FF FF FF        ...   stx ffff
B580 FF FF FF        ...   stx ffff
B583 FF FF FF        ...   stx ffff
B586 FF FF FF        ...   stx ffff
B589 FF FF FF        ...   stx ffff
B58C FF FF FF        ...   stx ffff
B58F FF FF FF        ...   stx ffff
B592 FF FF FF        ...   stx ffff
B595 FF FF FF        ...   stx ffff
B598 FF FF FF        ...   stx ffff
B59B FF FF FF        ...   stx ffff
B59E FF FF FF        ...   stx ffff
B5A1 FF FF FF        ...   stx ffff
B5A4 FF FF FF        ...   stx ffff
B5A7 FF FF FF        ...   stx ffff
B5AA FF FF FF        ...   stx ffff
B5AD FF FF FF        ...   stx ffff
B5B0 FF FF FF        ...   stx ffff
B5B3 FF FF FF        ...   stx ffff
B5B6 FF FF FF        ...   stx ffff
B5B9 FF FF FF        ...   stx ffff
B5BC FF FF FF        ...   stx ffff
B5BF FF FF FF        ...   stx ffff
B5C2 FF FF FF        ...   stx ffff
B5C5 FF FF FF        ...   stx ffff
B5C8 FF FF FF        ...   stx ffff
B5CB FF FF FF        ...   stx ffff
B5CE FF FF FF        ...   stx ffff
B5D1 FF FF FF        ...   stx ffff
B5D4 FF FF FF        ...   stx ffff
B5D7 FF FF FF        ...   stx ffff
B5DA FF FF FF        ...   stx ffff
B5DD FF FF FF        ...   stx ffff
B5E0 FF FF FF        ...   stx ffff
B5E3 FF FF FF        ...   stx ffff
B5E6 FF FF FF        ...   stx ffff
B5E9 FF FF FF        ...   stx ffff
B5EC FF FF FF        ...   stx ffff
B5EF FF FF FF        ...   stx ffff
B5F2 FF FF FF        ...   stx ffff
B5F5 FF FF FF        ...   stx ffff
B5F8 FF FF FF        ...   stx ffff
B5FB FF FF FF        ...   stx ffff
B5FE FF FF FF        ...   stx ffff
B601 FF FF FF        ...   stx ffff
B604 FF FF FF        ...   stx ffff
B607 FF FF FF        ...   stx ffff
B60A FF FF FF        ...   stx ffff
B60D FF FF FF        ...   stx ffff
B610 FF FF FF        ...   stx ffff
B613 FF FF FF        ...   stx ffff
B616 FF FF FF        ...   stx ffff
B619 FF FF FF        ...   stx ffff
B61C FF FF FF        ...   stx ffff
B61F FF FF FF        ...   stx ffff
B622 FF FF FF        ...   stx ffff
B625 FF FF FF        ...   stx ffff
B628 FF FF FF        ...   stx ffff
B62B FF FF FF        ...   stx ffff
B62E FF FF FF        ...   stx ffff
B631 FF FF FF        ...   stx ffff
B634 FF FF FF        ...   stx ffff
B637 FF FF FF        ...   stx ffff
B63A FF FF FF        ...   stx ffff
B63D FF FF FF        ...   stx ffff
B640 FF FF FF        ...   stx ffff
B643 FF FF FF        ...   stx ffff
B646 FF FF FF        ...   stx ffff
B649 FF FF FF        ...   stx ffff
B64C FF FF FF        ...   stx ffff
B64F FF FF FF        ...   stx ffff
B652 FF FF FF        ...   stx ffff
B655 FF FF FF        ...   stx ffff
B658 FF FF FF        ...   stx ffff
B65B FF FF FF        ...   stx ffff
B65E FF FF FF        ...   stx ffff
B661 FF FF FF        ...   stx ffff
B664 FF FF FF        ...   stx ffff
B667 FF FF FF        ...   stx ffff
B66A FF FF FF        ...   stx ffff
B66D FF FF FF        ...   stx ffff
B670 FF FF FF        ...   stx ffff
B673 FF FF FF        ...   stx ffff
B676 FF FF FF        ...   stx ffff
B679 FF FF FF        ...   stx ffff
B67C FF FF FF        ...   stx ffff
B67F FF FF FF        ...   stx ffff
B682 FF FF FF        ...   stx ffff
B685 FF FF FF        ...   stx ffff
B688 FF FF FF        ...   stx ffff
B68B FF FF FF        ...   stx ffff
B68E FF FF FF        ...   stx ffff
B691 FF FF FF        ...   stx ffff
B694 FF FF FF        ...   stx ffff
B697 FF FF FF        ...   stx ffff
B69A FF FF FF        ...   stx ffff
B69D FF FF FF        ...   stx ffff
B6A0 FF FF FF        ...   stx ffff
B6A3 FF FF FF        ...   stx ffff
B6A6 FF FF FF        ...   stx ffff
B6A9 FF FF FF        ...   stx ffff
B6AC FF FF FF        ...   stx ffff
B6AF FF FF FF        ...   stx ffff
B6B2 FF FF FF        ...   stx ffff
B6B5 FF FF FF        ...   stx ffff
B6B8 FF FF FF        ...   stx ffff
B6BB FF FF FF        ...   stx ffff
B6BE FF FF FF        ...   stx ffff
B6C1 FF FF FF        ...   stx ffff
B6C4 FF FF FF        ...   stx ffff
B6C7 FF FF FF        ...   stx ffff
B6CA FF FF FF        ...   stx ffff
B6CD FF FF FF        ...   stx ffff
B6D0 FF FF FF        ...   stx ffff
B6D3 FF FF FF        ...   stx ffff
B6D6 FF FF FF        ...   stx ffff
B6D9 FF FF FF        ...   stx ffff
B6DC FF FF FF        ...   stx ffff
B6DF FF FF FF        ...   stx ffff
B6E2 FF FF FF        ...   stx ffff
B6E5 FF FF FF        ...   stx ffff
B6E8 FF FF FF        ...   stx ffff
B6EB FF FF FF        ...   stx ffff
B6EE FF FF FF        ...   stx ffff
B6F1 FF FF FF        ...   stx ffff
B6F4 FF FF FF        ...   stx ffff
B6F7 FF FF FF        ...   stx ffff
B6FA FF FF FF        ...   stx ffff
B6FD FF FF FF        ...   stx ffff
B700 FF FF FF        ...   stx ffff
B703 FF FF FF        ...   stx ffff
B706 FF FF FF        ...   stx ffff
B709 FF FF FF        ...   stx ffff
B70C FF FF FF        ...   stx ffff
B70F FF FF FF        ...   stx ffff
B712 FF FF FF        ...   stx ffff
B715 FF FF FF        ...   stx ffff
B718 FF FF FF        ...   stx ffff
B71B FF FF FF        ...   stx ffff
B71E FF FF FF        ...   stx ffff
B721 FF FF FF        ...   stx ffff
B724 FF FF FF        ...   stx ffff
B727 FF FF FF        ...   stx ffff
B72A FF FF FF        ...   stx ffff
B72D FF FF FF        ...   stx ffff
B730 FF FF FF        ...   stx ffff
B733 FF FF FF        ...   stx ffff
B736 FF FF FF        ...   stx ffff
B739 FF FF FF        ...   stx ffff
B73C FF FF FF        ...   stx ffff
B73F FF FF FF        ...   stx ffff
B742 FF FF FF        ...   stx ffff
B745 FF FF FF        ...   stx ffff
B748 FF FF FF        ...   stx ffff
B74B FF FF FF        ...   stx ffff
B74E FF FF FF        ...   stx ffff
B751 FF FF FF        ...   stx ffff
B754 FF FF FF        ...   stx ffff
B757 FF FF FF        ...   stx ffff
B75A FF FF FF        ...   stx ffff
B75D FF FF FF        ...   stx ffff
B760 FF FF FF        ...   stx ffff
B763 FF FF FF        ...   stx ffff
B766 FF FF FF        ...   stx ffff
B769 FF FF FF        ...   stx ffff
B76C FF FF FF        ...   stx ffff
B76F FF FF FF        ...   stx ffff
B772 FF FF FF        ...   stx ffff
B775 FF FF FF        ...   stx ffff
B778 FF FF FF        ...   stx ffff
B77B FF FF FF        ...   stx ffff
B77E FF FF FF        ...   stx ffff
B781 FF FF FF        ...   stx ffff
B784 FF FF FF        ...   stx ffff
B787 FF FF FF        ...   stx ffff
B78A FF FF FF        ...   stx ffff
B78D FF FF FF        ...   stx ffff
B790 FF FF FF        ...   stx ffff
B793 FF FF FF        ...   stx ffff
B796 FF FF FF        ...   stx ffff
B799 FF FF FF        ...   stx ffff
B79C FF FF FF        ...   stx ffff
B79F FF FF FF        ...   stx ffff
B7A2 FF FF FF        ...   stx ffff
B7A5 FF FF FF        ...   stx ffff
B7A8 FF FF FF        ...   stx ffff
B7AB FF FF FF        ...   stx ffff
B7AE FF FF FF        ...   stx ffff
B7B1 FF FF FF        ...   stx ffff
B7B4 FF FF FF        ...   stx ffff
B7B7 FF FF FF        ...   stx ffff
B7BA FF FF FF        ...   stx ffff
B7BD FF FF FF        ...   stx ffff
B7C0 FF FF FF        ...   stx ffff
B7C3 FF FF FF        ...   stx ffff
B7C6 FF FF FF        ...   stx ffff
B7C9 FF FF FF        ...   stx ffff
B7CC FF FF FF        ...   stx ffff
B7CF FF FF FF        ...   stx ffff
B7D2 FF FF FF        ...   stx ffff
B7D5 FF FF FF        ...   stx ffff
B7D8 FF FF FF        ...   stx ffff
B7DB FF FF FF        ...   stx ffff
B7DE FF FF FF        ...   stx ffff
B7E1 FF FF FF        ...   stx ffff
B7E4 FF FF FF        ...   stx ffff
B7E7 FF FF FF        ...   stx ffff
B7EA FF FF FF        ...   stx ffff
B7ED FF FF FF        ...   stx ffff
B7F0 FF FF FF        ...   stx ffff
B7F3 FF FF FF        ...   stx ffff
B7F6 FF FF FF        ...   stx ffff
B7F9 FF FF FF        ...   stx ffff
B7FC FF FF FF        ...   stx ffff
B7FF FF 34 F6        .4.   stx 34f6
B802 0E              .     cli 
B803 6D C1           m.    tst c1,x
B805 01              .     nop 
B806 27 03           '.    beq 03
B808 7E B9 2A        ~.*   jmp b92a		;goto25
B80B F6 0E 02        ...   ldab 0e02
B80E C1 05           ..    cmpb #05
B810 27 03           '.    beq 03
B812 7E B9 2A        ~.*   jmp b92a		;goto25
B815 F6 0E 03        ...   ldab 0e03
B818 C1 05           ..    cmpb #05
B81A 27 03           '.    beq 03
B81C 7E B9 2A        ~.*   jmp b92a		;goto25
B81F F6 0E 04        ...   ldab 0e04
B822 C1 05           ..    cmpb #05
B824 27 03           '.    beq 03
B826 7E B9 2A        ~.*   jmp b92a		;goto25
B829 F6 00 12        ...   ldab 0012
B82C C4 20           .     andb #20
B82E 27 03           '.    beq 03
B830 7E B9 2A        ~.*   jmp b92a		;goto25
B833 30              0     tsx 
B834 6F 00           o.    clr 00,x
B836 F6 0E 6A        ..j   ldab 0e6a
B839 C1 00           ..    cmpb #00
B83B 22 33           "3    bhi 33
B83D C6 64           .d    ldab #64
B83F F7 0E 6A        ..j   stb 0e6a
B842 7C 0E 05        |..   inc 0e05
B845 F6 0E 05        ...   ldab 0e05
B848 C1 64           .d    cmpb #64
B84A 23 03           #.    bls 03
B84C 7F 0E 05        ...   clr 0e05
B84F F6 0E 05        ...   ldab 0e05
B852 BD D4 68        ..h   jsr d468		;jump51
B855 83 00 00        ...   subd #0000
B858 27 0B           '.    beq 0b
B85A F6 0E 05        ...   ldab 0e05
B85D BD D7 A0        ...   jsr d7a0		;jump10
B860 83 00 00        ...   subd #0000
B863 27 09           '.    beq 09
B865 30              0     tsx 
B866 6C 00           l.    inc 00,x
B868 E6 00           ..    ldab 00,x
B86A C1 6E           .n    cmpb #6e
B86C 2D D4           -.    blt d4
B86E 20 0A            .    bra 0a
B870 F6 0E 6A        ..j   ldab 0e6a
B873 C1 00           ..    cmpb #00
B875 23 03           #.    bls 03
B877 7A 0E 6A        z.j   dec 0e6a
B87A F6 0E 05        ...   ldab 0e05
B87D BD D4 68        ..h   jsr d468		;jump51
B880 83 00 00        ...   subd #0000
B883 26 03           &.    bne 03
B885 7E B9 0A        ~..   jmp b90a		;goto26
B888 F6 0E 05        ...   ldab 0e05
B88B BD D7 A0        ...   jsr d7a0		;jump10
B88E 83 00 00        ...   subd #0000
B891 26 77           &w    bne 77
B893 BD BE 4B        ..K   jsr be4b		;jump1
B896 C6 20           .     ldab #20
B898 F7 0B 9A        ...   stb 0b9a
B89B C6 20           .     ldab #20
B89D F7 0B 99        ...   stb 0b99
B8A0 C6 20           .     ldab #20
B8A2 F7 0B 98        ...   stb 0b98
B8A5 F6 0E 05        ...   ldab 0e05
B8A8 4F              O     clra 
B8A9 CE 00 0A        ...   ldx #000a
B8AC 02              .     idiv 
B8AD 8F              .     xgdx 
B8AE CB 30           .0    addb #30
B8B0 F7 0B 97        ...   stb 0b97
B8B3 F6 0E 05        ...   ldab 0e05
B8B6 4F              O     clra 
B8B7 CE 00 0A        ...   ldx #000a
B8BA 02              .     idiv 
B8BB CB 30           .0    addb #30
B8BD F7 0B 96        ...   stb 0b96
B8C0 C6 20           .     ldab #20
B8C2 F7 0B 95        ...   stb 0b95
B8C5 C6 20           .     ldab #20
B8C7 F7 0B 94        ...   stb 0b94
B8CA F6 0E 05        ...   ldab 0e05
B8CD 4F              O     clra 
B8CE C3 0E 06        ...   addd #0e06
B8D1 8F              .     xgdx 
B8D2 E6 00           ..    ldab 00,x
B8D4 4F              O     clra 
B8D5 CE 00 0A        ...   ldx #000a
B8D8 02              .     idiv 
B8D9 8F              .     xgdx 
B8DA CB 30           .0    addb #30
B8DC F7 0B 93        ...   stb 0b93
B8DF F6 0E 05        ...   ldab 0e05
B8E2 4F              O     clra 
B8E3 C3 0E 06        ...   addd #0e06
B8E6 8F              .     xgdx 
B8E7 E6 00           ..    ldab 00,x
B8E9 4F              O     clra 
B8EA CE 00 0A        ...   ldx #000a
B8ED 02              .     idiv 
B8EE CB 30           .0    addb #30
B8F0 F7 0B 92        ...   stb 0b92
B8F3 C6 20           .     ldab #20
B8F5 F7 0B 91        ...   stb 0b91
B8F8 C6 20           .     ldab #20
B8FA F7 0B 90        ...   stb 0b90
B8FD 7F 00 2C        ..,   clr 002c
B900 BD BE 01        ...   jsr be01		;jump70
B903 C6 0A           ..    ldab #0a
B905 F7 00 2C        ..,   stb 002c
B908 20 1D            .    bra 1d
goto26:
B90A BD BE 4B        ..K   jsr be4b		;jump1
B90D C6 57           .W    ldab #57
B90F F7 0B 96        ...   stb 0b96
B912 C6 42           .B    ldab #42
B914 F7 0B 95        ...   stb 0b95
B917 C6 42           .B    ldab #42
B919 F7 0B 94        ...   stb 0b94
B91C 7F 00 2C        ..,   clr 002c
B91F BD BE 01        ...   jsr be01		;jump70
B922 C6 0A           ..    ldab #0a
B924 F7 00 2C        ..,   stb 002c
B927 7E BA 81        ~..   jmp ba81		;goto27
goto25:
B92A F6 0E 6D        ..m   ldab 0e6d
B92D C1 01           ..    cmpb #01
B92F 27 03           '.    beq 03
B931 7E BA 81        ~..   jmp ba81		;goto27
B934 F6 0E 00        ...   ldab 0e00
B937 C1 05           ..    cmpb #05
B939 27 03           '.    beq 03
B93B 7E BA 81        ~..   jmp ba81		;goto27
B93E F6 0E 01        ...   ldab 0e01
B941 C1 05           ..    cmpb #05
B943 27 03           '.    beq 03
B945 7E BA 81        ~..   jmp ba81		;goto27
B948 F6 0E 02        ...   ldab 0e02
B94B C1 05           ..    cmpb #05
B94D 27 03           '.    beq 03
B94F 7E BA 81        ~..   jmp ba81		;goto27
B952 F6 00 12        ...   ldab 0012
B955 C4 20           .     andb #20
B957 26 03           &.    bne 03
B959 7E BA 81        ~..   jmp ba81		;goto27
B95C F6 0E 04        ...   ldab 0e04
B95F C1 05           ..    cmpb #05
B961 26 79           &y    bne 79
B963 F6 0E 03        ...   ldab 0e03
B966 C1 05           ..    cmpb #05
B968 26 72           &r    bne 72
B96A 30              0     tsx 
B96B 6F 00           o.    clr 00,x
B96D 30              0     tsx 
B96E E6 00           ..    ldab 00,x
B970 C1 64           .d    cmpb #64
B972 2C 16           ,.    bge 16
B974 5F              _     clrb 
B975 37              7     pshb 
B976 30              0     tsx 
B977 E6 01           ..    ldab 01,x
B979 4F              O     clra 
B97A 5D              ]     tstb 
B97B 2C 01           ,.    bge 01
B97D 4A              J     deca 
B97E C3 0E 06        ...   addd #0e06
B981 8F              .     xgdx 
B982 33              3     pulb 
B983 E7 00           ..    stb 00,x
B985 30              0     tsx 
B986 6C 00           l.    inc 00,x
B988 20 E3            .    bra e3
B98A 7F 0D FF        ...   clr 0dff
B98D 7F 0E 00        ...   clr 0e00
B990 7F 0E 01        ...   clr 0e01
B993 7F 0E 02        ...   clr 0e02
B996 7F 0E 03        ...   clr 0e03
B999 7F 0E 04        ...   clr 0e04
B99C C6 41           .A    ldab #41
B99E F7 0B 99        ...   stb 0b99
B9A1 C6 4C           .L    ldab #4c
B9A3 F7 0B 98        ...   stb 0b98
B9A6 C6 4C           .L    ldab #4c
B9A8 F7 0B 97        ...   stb 0b97
B9AB C6 20           .     ldab #20
B9AD F7 0B 96        ...   stb 0b96
B9B0 C6 43           .C    ldab #43
B9B2 F7 0B 95        ...   stb 0b95
B9B5 C6 4C           .L    ldab #4c
B9B7 F7 0B 94        ...   stb 0b94
B9BA C6 45           .E    ldab #45
B9BC F7 0B 93        ...   stb 0b93
B9BF C6 41           .A    ldab #41
B9C1 F7 0B 92        ...   stb 0b92
B9C4 C6 52           .R    ldab #52
B9C6 F7 0B 91        ...   stb 0b91
B9C9 C6 20           .     ldab #20
B9CB F7 0B 90        ...   stb 0b90
B9CE 7F 00 2C        ..,   clr 002c
B9D1 BD BE 01        ...   jsr be01		;jump70
B9D4 C6 1E           ..    ldab #1e
B9D6 F7 00 2C        ..,   stb 002c
B9D9 7E BA 81        ~..   jmp ba81		;goto27
B9DC F6 0E 04        ...   ldab 0e04
B9DF C1 01           ..    cmpb #01
B9E1 27 0A           '.    beq 0a
B9E3 F6 0E 04        ...   ldab 0e04
B9E6 C1 02           ..    cmpb #02
B9E8 27 03           '.    beq 03
B9EA 7E BA 81        ~..   jmp ba81		;goto27
B9ED F6 0E 03        ...   ldab 0e03
B9F0 C1 00           ..    cmpb #00
B9F2 22 03           ".    bhi 03
B9F4 7E BA 81        ~..   jmp ba81		;goto27
B9F7 F6 0E 03        ...   ldab 0e03
B9FA C1 05           ..    cmpb #05
B9FC 26 03           &.    bne 03
B9FE 7E BA 81        ~..   jmp ba81		;goto27
BA01 F6 0E 03        ...   ldab 0e03
BA04 C1 07           ..    cmpb #07
BA06 24 79           $y    bcc 79
BA08 F6 0E 03        ...   ldab 0e03
BA0B 86 0A           ..    ldaa #0a
BA0D 3D              =     mul 
BA0E FB 0E 04        ...   addb 0e04
BA11 30              0     tsx 
BA12 E7 00           ..    stb 00,x
BA14 E6 00           ..    ldab 00,x
BA16 C1 64           .d    cmpb #64
BA18 2C 67           ,g    bge 67
BA1A E6 00           ..    ldab 00,x
BA1C 2D 63           -c    blt 63
BA1E 5F              _     clrb 
BA1F 37              7     pshb 
BA20 30              0     tsx 
BA21 E6 01           ..    ldab 01,x
BA23 4F              O     clra 
BA24 5D              ]     tstb 
BA25 2C 01           ,.    bge 01
BA27 4A              J     deca 
BA28 C3 0E 06        ...   addd #0e06
BA2B 8F              .     xgdx 
BA2C 33              3     pulb 
BA2D E7 00           ..    stb 00,x
BA2F F6 0E 03        ...   ldab 0e03
BA32 CB 30           .0    addb #30
BA34 F7 0B 99        ...   stb 0b99
BA37 F6 0E 04        ...   ldab 0e04
BA3A CB 30           .0    addb #30
BA3C F7 0B 98        ...   stb 0b98
BA3F C6 20           .     ldab #20
BA41 F7 0B 97        ...   stb 0b97
BA44 C6 43           .C    ldab #43
BA46 F7 0B 96        ...   stb 0b96
BA49 C6 4C           .L    ldab #4c
BA4B F7 0B 95        ...   stb 0b95
BA4E C6 45           .E    ldab #45
BA50 F7 0B 94        ...   stb 0b94
BA53 C6 41           .A    ldab #41
BA55 F7 0B 93        ...   stb 0b93
BA58 C6 52           .R    ldab #52
BA5A F7 0B 92        ...   stb 0b92
BA5D C6 45           .E    ldab #45
BA5F F7 0B 91        ...   stb 0b91
BA62 C6 44           .D    ldab #44
BA64 F7 0B 90        ...   stb 0b90
BA67 7F 0E 00        ...   clr 0e00
BA6A 7F 0E 01        ...   clr 0e01
BA6D 7F 0E 02        ...   clr 0e02
BA70 7F 0E 03        ...   clr 0e03
BA73 7F 0E 04        ...   clr 0e04
BA76 7F 00 2C        ..,   clr 002c
BA79 BD BE 01        ...   jsr be01		;jump70
BA7C C6 1E           ..    ldab #1e
BA7E F7 00 2C        ..,   stb 002c
goto27:
BA81 31              1     ins 
BA82 39              9     rts 

jump125:
BA83 F6 18 00        ...   ldab 1800
BA86 C4 7F           ..    andb #7f
BA88 F7 00 23        ..#   stb 0023
BA8B F6 00 23        ..#   ldab 0023
BA8E F1 00 12        ...   cmpb 0012
BA91 27 06           '.    beq 06
BA93 F6 00 23        ..#   ldab 0023
BA96 F7 00 12        ...   stb 0012
BA99 F6 20 00        . .   ldab 2000
BA9C F1 00 20        ..    cmpb 0020
BA9F 27 08           '.    beq 08
BAA1 F6 20 00        . .   ldab 2000
BAA4 F7 00 20        ..    stb 0020
BAA7 20 0E            .    bra 0e
BAA9 F6 00 20        ..    ldab 0020
BAAC F7 00 11        ...   stb 0011
BAAF F6 00 11        ...   ldab 0011
BAB2 C4 03           ..    andb #03
BAB4 F7 00 2B        ..+   stb 002b
BAB7 F6 00 1B        ...   ldab 001b
BABA C4 04           ..    andb #04
BABC 37              7     pshb 
BABD F6 10 0A        ...   ldab 100a
BAC0 C4 04           ..    andb #04
BAC2 30              0     tsx 
BAC3 E0 00           ..    subb 00,x
BAC5 31              1     ins 
BAC6 27 17           '.    beq 17
BAC8 F6 10 0A        ...   ldab 100a
BACB C4 04           ..    andb #04
BACD 27 08           '.    beq 08
BACF CE 00 1B        ...   ldx #001b
BAD2 1C 00           ..    bset add,x 00,x
BAD4 04              .     lsrd 
BAD5 20 06            .    bra 06
BAD7 CE 00 1B        ...   ldx #001b
BADA 1D 00           ..    bclr add,x 00,x
BADC 04              .     lsrd 
BADD 20 15            .    bra 15
BADF F6 10 0A        ...   ldab 100a
BAE2 C4 04           ..    andb #04
BAE4 27 08           '.    beq 08
BAE6 CE 00 1D        ...   ldx #001d
BAE9 1C 00           ..    bset add,x 00,x
BAEB 04              .     lsrd 
BAEC 20 06            .    bra 06
BAEE CE 00 1D        ...   ldx #001d
BAF1 1D 00           ..    bclr add,x 00,x
BAF3 04              .     lsrd 
BAF4 CE 10 08        ...   ldx #1008
BAF7 1D 00           ..    bclr add,x 00,x
BAF9 20 CE            .    bra ce
BAFB 10              .     sba 
BAFC 28 1C           (.    bvc 1c
BAFE 00              .     test 
BAFF 40              @     nega 
BB00 CE 00 14        ...   ldx #0014
BB03 1C 00           ..    bset add,x 00,x
BB05 04              .     lsrd 
BB06 F6 00 14        ...   ldab 0014
BB09 F7 38 00        .8.   stb 3800
BB0C CE 00 14        ...   ldx #0014
BB0F 1D 00           ..    bclr add,x 00,x
BB11 04              .     lsrd 
BB12 F6 00 14        ...   ldab 0014
BB15 F7 38 00        .8.   stb 3800
BB18 CE 00 00        ...   ldx #0000
BB1B 3C              <     pshx 
BB1C 5F              _     clrb 
BB1D 4F              O     clra 
BB1E BD BD B5        ...   jsr bdb5		;jump78	
BB21 38              8     pulx 
BB22 F7 00 5B        ..[   stb 005b
BB25 CE 10 28        ..(   ldx #1028
BB28 1D 00           ..    bclr add,x 00,x
BB2A 40              @     nega 
BB2B CE 10 08        ...   ldx #1008
BB2E 1C 00           ..    bset add,x 00,x
BB30 20 CE            .    bra ce
BB32 10              .     sba 
BB33 28 1C           (.    bvc 1c
BB35 00              .     test 
BB36 40              @     nega 
BB37 CE 00 14        ...   ldx #0014
BB3A 1C 00           ..    bset add,x 00,x
BB3C 04              .     lsrd 
BB3D F6 00 14        ...   ldab 0014
BB40 F7 38 00        .8.   stb 3800
BB43 CE 00 14        ...   ldx #0014
BB46 1D 00           ..    bclr add,x 00,x
BB48 04              .     lsrd 
BB49 F6 00 14        ...   ldab 0014
BB4C F7 38 00        .8.   stb 3800
BB4F CE 00 00        ...   ldx #0000
BB52 3C              <     pshx 
BB53 5F              _     clrb 
BB54 4F              O     clra 
BB55 BD BD B5        ...   jsr bdb5		;jump78	
BB58 38              8     pulx 
BB59 F7 00 56        ..V   stb 0056
BB5C CE 10 28        ..(   ldx #1028
BB5F 1D 00           ..    bclr add,x 00,x
BB61 40              @     nega 
BB62 39              9     rts 
BB63 F6 00 00        ...   ldab 0000
BB66 27 03           '.    beq 03
BB68 7E BD B4        ~..   jmp bdb4		;goto28
BB6B F6 00 2E        ...   ldab 002e
BB6E 27 03           '.    beq 03
BB70 7E BD B4        ~..   jmp bdb4		;goto28
BB73 BD BA 83        ...   jsr ba83		;jump125
BB76 F6 00 5B        ..[   ldab 005b
BB79 F1 00 56        ..V   cmpb 0056
BB7C 26 0B           &.    bne 0b
BB7E F6 00 56        ..V   ldab 0056
BB81 27 06           '.    beq 06
BB83 7F 00 29        ..)   clr 0029
BB86 7E BD AF        ~..   jmp bdaf		;goto29
BB89 F6 00 5B        ..[   ldab 005b
BB8C 27 34           '4    beq 34
BB8E F6 00 5B        ..[   ldab 005b
BB91 F7 00 5F        .._   stb 005f
BB94 7F 00 1C        ...   clr 001c
BB97 F6 00 1C        ...   ldab 001c
BB9A C1 08           ..    cmpb #08
BB9C 24 15           $.    bcc 15
BB9E F6 00 5F        .._   ldab 005f
BBA1 C4 01           ..    andb #01
BBA3 27 02           '.    beq 02
BBA5 20 0C            .    bra 0c
BBA7 F6 00 5F        .._   ldab 005f
BBAA 54              T     lsrb 
BBAB F7 00 5F        .._   stb 005f
BBAE 7C 00 1C        |..   inc 001c
BBB1 20 E4            .    bra e4
BBB3 F6 00 1C        ...   ldab 001c
BBB6 4F              O     clra 
BBB7 C3 AC 9D        ...   addd #ac9d
BBBA 8F              .     xgdx 
BBBB E6 00           ..    ldab 00,x
BBBD F7 00 16        ...   stb 0016
BBC0 20 3C            <    bra 3c
BBC2 F6 00 56        ..V   ldab 0056
BBC5 27 34           '4    beq 34
BBC7 F6 00 56        ..V   ldab 0056
BBCA F7 00 5F        .._   stb 005f
BBCD 7F 00 1C        ...   clr 001c
BBD0 F6 00 1C        ...   ldab 001c
BBD3 C1 08           ..    cmpb #08
BBD5 24 15           $.    bcc 15
BBD7 F6 00 5F        .._   ldab 005f
BBDA C4 01           ..    andb #01
BBDC 27 02           '.    beq 02
BBDE 20 0C            .    bra 0c
BBE0 F6 00 5F        .._   ldab 005f
BBE3 54              T     lsrb 
BBE4 F7 00 5F        .._   stb 005f
BBE7 7C 00 1C        |..   inc 001c
BBEA 20 E4            .    bra e4
BBEC F6 00 1C        ...   ldab 001c
BBEF 4F              O     clra 
BBF0 C3 AC A0        ...   addd #aca0
BBF3 8F              .     xgdx 
BBF4 E6 00           ..    ldab 00,x
BBF6 F7 00 16        ...   stb 0016
BBF9 20 03            .    bra 03
BBFB 7F 00 16        ...   clr 0016
BBFE F6 00 5B        ..[   ldab 005b
BC01 FA 00 56        ..V   orb 0056
BC04 F7 00 5F        .._   stb 005f
BC07 7F 00 5E        ..^   clr 005e
BC0A 7F 00 1C        ...   clr 001c
BC0D F6 00 1C        ...   ldab 001c
BC10 C1 08           ..    cmpb #08
BC12 24 16           $.    bcc 16
BC14 F6 00 5F        .._   ldab 005f
BC17 C4 01           ..    andb #01
BC19 27 03           '.    beq 03
BC1B 7C 00 5E        |.^   inc 005e
BC1E F6 00 5F        .._   ldab 005f
BC21 54              T     lsrb 
BC22 F7 00 5F        .._   stb 005f
BC25 7C 00 1C        |..   inc 001c
BC28 20 E3            .    bra e3
BC2A F6 00 5E        ..^   ldab 005e
BC2D C1 01           ..    cmpb #01
BC2F 22 05           ".    bhi 05
BC31 F6 00 16        ...   ldab 0016
BC34 26 0F           &.    bne 0f
BC36 7F 00 29        ..)   clr 0029
BC39 CE 00 1B        ...   ldx #001b
BC3C 1C 00           ..    bset add,x 00,x
BC3E 10              .     sba 
BC3F 7F 00 37        ..7   clr 0037
BC42 7E BD AF        ~..   jmp bdaf		;goto29
BC45 F6 00 16        ...   ldab 0016
BC48 F1 00 29        ..)   cmpb 0029
BC4B 27 2D           '-    beq 2d
BC4D F6 00 16        ...   ldab 0016
BC50 F7 00 29        ..)   stb 0029
BC53 F6 0E 00        ...   ldab 0e00
BC56 F7 0D FF        ...   stb 0dff
BC59 F6 0E 01        ...   ldab 0e01
BC5C F7 0E 00        ...   stb 0e00
BC5F F6 0E 02        ...   ldab 0e02
BC62 F7 0E 01        ...   stb 0e01
BC65 F6 0E 03        ...   ldab 0e03
BC68 F7 0E 02        ...   stb 0e02
BC6B F6 0E 04        ...   ldab 0e04
BC6E F7 0E 03        ...   stb 0e03
BC71 F6 00 29        ..)   ldab 0029
BC74 F7 0E 04        ...   stb 0e04
BC77 7E BD AF        ~..   jmp bdaf		;goto29
BC7A F6 00 12        ...   ldab 0012
BC7D C4 20           .     andb #20
BC7F 27 3B           ';    beq 3b
BC81 F6 00 29        ..)   ldab 0029
BC84 27 33           '3    beq 33
BC86 F6 00 1B        ...   ldab 001b
BC89 C4 10           ..    andb #10
BC8B 27 2C           ',    beq 2c
BC8D F6 00 42        ..B   ldab 0042
BC90 27 15           '.    beq 15
BC92 F6 00 09        ...   ldab 0009
BC95 27 08           '.    beq 08
BC97 F6 00 29        ..)   ldab 0029
BC9A F7 00 0A        ...   stb 000a
BC9D 20 06            .    bra 06
BC9F F6 00 29        ..)   ldab 0029
BCA2 F7 00 09        ...   stb 0009
BCA5 20 06            .    bra 06
BCA7 F6 00 29        ..)   ldab 0029
BCAA F7 00 42        ..B   stb 0042
BCAD BD E5 AA        ...   jsr e5aa		;jump12
BCB0 7F 00 2C        ..,   clr 002c
BCB3 CE 00 1B        ...   ldx #001b
BCB6 1D 00           ..    bclr add,x 00,x
BCB8 10              .     sba 
BCB9 7E BD AF        ~..   jmp bdaf		;goto29
BCBC F6 00 1B        ...   ldab 001b
BCBF C4 10           ..    andb #10
BCC1 26 03           &.    bne 03
BCC3 7E BD 91        ~..   jmp bd91		;goto30
BCC6 F6 00 21        ..!   ldab 0021
BCC9 C4 20           .     andb #20
BCCB 27 03           '.    beq 03
BCCD 7E BD 91        ~..   jmp bd91		;goto30
BCD0 F6 00 29        ..)   ldab 0029
BCD3 C1 0B           ..    cmpb #0b
BCD5 26 03           &.    bne 03
BCD7 7E BD 5C        ~.\   jmp bd5c		;goto31
BCDA 7F 00 49        ..I   clr 0049
BCDD F6 00 3A        ..:   ldab 003a
BCE0 26 32           &2    bne 32
BCE2 F6 00 29        ..)   ldab 0029
BCE5 C1 0A           ..    cmpb #0a
BCE7 26 05           &.    bne 05
BCE9 7F 00 1E        ...   clr 001e
BCEC 20 06            .    bra 06
BCEE F6 00 29        ..)   ldab 0029
BCF1 F7 00 1E        ...   stb 001e
BCF4 C6 32           .2    ldab #32
BCF6 F7 00 3A        ..:   stb 003a
BCF9 BD BE 4B        ..K   jsr be4b		;jump1
BCFC F6 00 1E        ...   ldab 001e
BCFF CB 30           .0    addb #30
BD01 F7 0B 95        ...   stb 0b95
BD04 BD E5 AA        ...   jsr e5aa		;jump12
BD07 7F 00 2C        ..,   clr 002c
BD0A BD BE 01        ...   jsr be01		;jump70
BD0D C6 32           .2    ldab #32
BD0F F7 00 2C        ..,   stb 002c
BD12 20 46            F    bra 46
BD14 F6 00 29        ..)   ldab 0029
BD17 C1 05           ..    cmpb #05
BD19 27 07           '.    beq 07
BD1B F6 00 29        ..)   ldab 0029
BD1E C1 0A           ..    cmpb #0a
BD20 26 05           &.    bne 05
BD22 7F 00 2C        ..,   clr 002c
BD25 20 2D            -    bra 2d
BD27 F6 00 1E        ...   ldab 001e
BD2A 58              X     aslb 
BD2B 58              X     aslb 
BD2C 58              X     aslb 
BD2D F7 00 06        ...   stb 0006
BD30 F6 00 1E        ...   ldab 001e
BD33 58              X     aslb 
BD34 FB 00 06        ...   addb 0006
BD37 F7 00 06        ...   stb 0006
BD3A F6 00 29        ..)   ldab 0029
BD3D FB 00 06        ...   addb 0006
BD40 F7 00 06        ...   stb 0006
BD43 F6 00 29        ..)   ldab 0029
BD46 CB 30           .0    addb #30
BD48 F7 0B 94        ...   stb 0b94
BD4B BD BE 01        ...   jsr be01		;jump70
BD4E CE 00 1B        ...   ldx #001b
BD51 1D 00           ..    bclr add,x 00,x
BD53 40              @     nega 
BD54 7F 00 3A        ..:   clr 003a
BD57 BD E5 AA        ...   jsr e5aa		;jump12
BD5A 20 2D            -    bra 2d
goto31:
BD5C C6 19           ..    ldab #19
BD5E F7 00 37        ..7   stb 0037
BD61 F6 00 3A        ..:   ldab 003a
BD64 27 08           '.    beq 08
BD66 7F 00 3A        ..:   clr 003a
BD69 7F 00 2C        ..,   clr 002c
BD6C 20 18            .    bra 18
BD6E 7C 00 49        |.I   inc 0049
BD71 F6 00 49        ..I   ldab 0049
BD74 C1 02           ..    cmpb #02
BD76 25 0E           %.    bcs 0e
BD78 7F 00 49        ..I   clr 0049
BD7B CE 00 1B        ...   ldx #001b
BD7E 1C 00           ..    bset add,x 00,x
BD80 01              .     nop 
BD81 C6 3C           .<    ldab #3c
BD83 F7 00 4F        ..O   stb 004f
BD86 BD E5 AA        ...   jsr e5aa		;jump12
BD89 CE 00 1B        ...   ldx #001b
BD8C 1D 00           ..    bclr add,x 00,x
BD8E 10              .     sba 
BD8F 20 1E            .    bra 1e
goto30:
BD91 F6 00 1B        ...   ldab 001b
BD94 C4 10           ..    andb #10
BD96 26 17           &.    bne 17
BD98 F6 00 29        ..)   ldab 0029
BD9B C1 0B           ..    cmpb #0b
BD9D 26 10           &.    bne 10
BD9F F6 00 37        ..7   ldab 0037
BDA2 26 0B           &.    bne 0b
BDA4 BD DD D5        ...   jsr ddd5		;jump120
BDA7 7F 00 49        ..I   clr 0049
BDAA C6 14           ..    ldab #14
BDAC F7 00 2C        ..,   stb 002c
goto29:
BDAF C6 01           ..    ldab #01
BDB1 F7 00 00        ...   stb 0000
goto28:
BDB4 39              9     rts 

jump78:
BDB5 37              7     pshb 
BDB6 36              6     psha 
BDB7 3C              <     pshx 
BDB8 5F              _     clrb 
BDB9 4F              O     clra 
BDBA 30              0     tsx 
BDBB ED 00           ..    stad 00,x
BDBD E6 07           ..    ldab 07,x
BDBF 27 0E           '.    beq 0e
BDC1 CE 00 14        ...   ldx #0014
BDC4 1C 00           ..    bset add,x 00,x
BDC6 08              .     inx 
BDC7 F6 00 14        ...   ldab 0014
BDCA F7 38 00        .8.   stb 3800
BDCD 20 0C            .    bra 0c
BDCF CE 00 14        ...   ldx #0014
BDD2 1D 00           ..    bclr add,x 00,x
BDD4 08              .     inx 
BDD5 F6 00 14        ...   ldab 0014
BDD8 F7 38 00        .8.   stb 3800
BDDB 30              0     tsx 
BDDC E6 03           ..    ldab 03,x
BDDE F7 10 2A        ..*   stb 102a
BDE1 F6 10 29        ..)   ldab 1029
BDE4 2D 0F           -.    blt 0f
BDE6 38              8     pulx 
BDE7 08              .     inx 
BDE8 3C              <     pshx 
BDE9 30              0     tsx 
BDEA EC 00           ..    ldd 00,x
BDEC 83 FF FF        ...   subd #ffff
BDEF 26 02           &.    bne 02
BDF1 20 02            .    bra 02
BDF3 20 EC            .    bra ec
BDF5 F6 10 2A        ..*   ldab 102a
BDF8 30              0     tsx 
BDF9 E7 03           ..    stb 03,x
BDFB E6 03           ..    ldab 03,x
BDFD 4F              O     clra 
BDFE 38              8     pulx 
BDFF 38              8     pulx 
BE00 39              9     rts 

jump70:
BE01 F6 00 2C        ..,   ldab 002c
BE04 26 09           &.    bne 09
BE06 8D 61           .a    bsr dest 61
BE08 8D 06           ..    bsr dest 06
BE0A C6 03           ..    ldab #03
BE0C F7 00 2C        ..,   stb 002c
BE0F 39              9     rts 

jump88:
BE10 CE 10 28        ..(   ldx #1028
BE13 1C 00           ..    bset add,x 00,x
BE15 40              @     nega 
BE16 7F 00 1C        ...   clr 001c
BE19 F6 00 1C        ...   ldab 001c
BE1C C1 0A           ..    cmpb #0a
BE1E 24 19           $.    bcc 19
BE20 CE 00 01        ...   ldx #0001
BE23 3C              <     pshx 
BE24 F6 00 1C        ...   ldab 001c
BE27 4F              O     clra 
BE28 C3 0B 90        ...   addd #0b90
BE2B 8F              .     xgdx 
BE2C E6 00           ..    ldab 00,x
BE2E C4 7F           ..    andb #7f
BE30 4F              O     clra 
BE31 8D 82           ..    bsr dest 82
BE33 38              8     pulx 
BE34 7C 00 1C        |..   inc 001c
BE37 20 E0            .    bra e0
BE39 CE 00 01        ...   ldx #0001
BE3C 3C              <     pshx 
BE3D CC 00 FF        ...   ldd #00ff
BE40 BD BD B5        ...   jsr bdb5		;jump78	
BE43 38              8     pulx 
BE44 CE 10 28        ..(   ldx #1028
BE47 1D 00           ..    bclr add,x 00,x
BE49 40              @     nega 
BE4A 39              9     rts 

jump1:
BE4B 7F 00 1C        ...   clr 001c
BE4E F6 00 1C        ...   ldab 001c
BE51 C1 0A           ..    cmpb #0a
BE53 24 13           $.    bcc 13
BE55 C6 20           .     ldab #20
BE57 37              7     pshb 
BE58 F6 00 1C        ...   ldab 001c
BE5B 4F              O     clra 
BE5C C3 0B 90        ...   addd #0b90
BE5F 8F              .     xgdx 
BE60 33              3     pulb 
BE61 E7 00           ..    stb 00,x
BE63 7C 00 1C        |..   inc 001c
BE66 20 E6            .    bra e6
BE68 39              9     rts 

jump3:
BE69 CE 10 00        ...   ldx #1000		;
BE6C 1D 00           ..    bclr add,x 00,x	;
BE6E 80 BD           ..    suba #bd		;
BE70 E5 96           ..    bitb 96,x
BE72 CE 10 00        ...   ldx #1000
BE75 1C 00           ..    bset add,x 00,x
BE77 80 CE           ..    suba #ce
BE79 10              .     sba 
BE7A 28 1C           (.    bvc 1c
BE7C 00              .     test 
BE7D 40              @     nega 
BE7E BD E5 96        ...   jsr e596		;jump115
BE81 CE 00 01        ...   ldx #0001
BE84 3C              <     pshx 
BE85 CC 00 CA        ...   ldd #00ca
BE88 BD BD B5        ...   jsr bdb5		;jump78	
BE8B 38              8     pulx 
BE8C CE 00 01        ...   ldx #0001
BE8F 3C              <     pshx 
BE90 CC 00 E0        ...   ldd #00e0
BE93 BD BD B5        ...   jsr bdb5		;jump78	
BE96 38              8     pulx 
BE97 CE 10 28        ..(   ldx #1028
BE9A 1D 00           ..    bclr add,x 00,x
BE9C 40              @     nega 
BE9D 39              9     rts 

jump50:
BE9E 37              7     pshb 
BE9F 36              6     psha 
BEA0 8D A9           ..    bsr dest a9
BEA2 30              0     tsx 
BEA3 E6 05           ..    ldab 05,x
BEA5 26 1B           &.    bne 1b
BEA7 F6 00 06        ...   ldab 0006
BEAA 4F              O     clra 
BEAB CE 00 0A        ...   ldx #000a
BEAE 02              .     idiv 
BEAF 8F              .     xgdx 
BEB0 CB 30           .0    addb #30
BEB2 F7 0B 99        ...   stb 0b99
BEB5 F6 00 06        ...   ldab 0006
BEB8 4F              O     clra 
BEB9 CE 00 0A        ...   ldx #000a
BEBC 02              .     idiv 
BEBD CB 30           .0    addb #30
BEBF F7 0B 98        ...   stb 0b98
BEC2 30              0     tsx 
BEC3 EC 00           ..    ldd 00,x
BEC5 C4 FF           ..    andb #ff
BEC7 84 3F           .?    anda #3f
BEC9 ED 00           ..    stad 00,x
BECB EC 00           ..    ldd 00,x
BECD CE 03 E8        ...   ldx #03e8
BED0 02              .     idiv 
BED1 8F              .     xgdx 
BED2 CB 30           .0    addb #30
BED4 37              7     pshb 
BED5 30              0     tsx 
BED6 E6 06           ..    ldab 06,x
BED8 4F              O     clra 
BED9 C3 0B 95        ...   addd #0b95
BEDC 8F              .     xgdx 
BEDD 33              3     pulb 
BEDE E7 00           ..    stb 00,x
BEE0 30              0     tsx 
BEE1 EC 00           ..    ldd 00,x
BEE3 CE 03 E8        ...   ldx #03e8
BEE6 02              .     idiv 
BEE7 CE 00 64        ..d   ldx #0064
BEEA 02              .     idiv 
BEEB 8F              .     xgdx 
BEEC CB 30           .0    addb #30
BEEE 37              7     pshb 
BEEF 30              0     tsx 
BEF0 E6 06           ..    ldab 06,x
BEF2 4F              O     clra 
BEF3 C3 0B 94        ...   addd #0b94
BEF6 8F              .     xgdx 
BEF7 33              3     pulb 
BEF8 E7 00           ..    stb 00,x
BEFA 30              0     tsx 
BEFB EC 00           ..    ldd 00,x
BEFD CE 00 64        ..d   ldx #0064
BF00 02              .     idiv 
BF01 CE 00 0A        ...   ldx #000a
BF04 02              .     idiv 
BF05 8F              .     xgdx 
BF06 CB 30           .0    addb #30
BF08 37              7     pshb 
BF09 30              0     tsx 
BF0A E6 06           ..    ldab 06,x
BF0C 4F              O     clra 
BF0D C3 0B 93        ...   addd #0b93
BF10 8F              .     xgdx 
BF11 33              3     pulb 
BF12 E7 00           ..    stb 00,x
BF14 30              0     tsx 
BF15 EC 00           ..    ldd 00,x
BF17 CE 00 0A        ...   ldx #000a
BF1A 02              .     idiv 
BF1B CB 30           .0    addb #30
BF1D 37              7     pshb 
BF1E 30              0     tsx 
BF1F E6 06           ..    ldab 06,x
BF21 4F              O     clra 
BF22 C3 0B 92        ...   addd #0b92
BF25 8F              .     xgdx 
BF26 33              3     pulb 
BF27 E7 00           ..    stb 00,x
BF29 F6 00 13        ...   ldab 0013
BF2C C1 01           ..    cmpb #01
BF2E 22 03           ".    bhi 03
BF30 7E BF DE        ~..   jmp bfde		;goto32
BF33 30              0     tsx 
BF34 E6 05           ..    ldab 05,x
BF36 4F              O     clra 
BF37 C3 0B 92        ...   addd #0b92
BF3A 8F              .     xgdx 
BF3B E6 00           ..    ldab 00,x
BF3D 37              7     pshb 
BF3E 30              0     tsx 
BF3F E6 06           ..    ldab 06,x
BF41 4F              O     clra 
BF42 C3 0B 91        ...   addd #0b91
BF45 8F              .     xgdx 
BF46 33              3     pulb 
BF47 E7 00           ..    stb 00,x
BF49 30              0     tsx 
BF4A E6 05           ..    ldab 05,x
BF4C 4F              O     clra 
BF4D C3 0B 93        ...   addd #0b93
BF50 8F              .     xgdx 
BF51 E6 00           ..    ldab 00,x
BF53 37              7     pshb 
BF54 30              0     tsx 
BF55 E6 06           ..    ldab 06,x
BF57 4F              O     clra 
BF58 C3 0B 92        ...   addd #0b92
BF5B 8F              .     xgdx 
BF5C 33              3     pulb 
BF5D E7 00           ..    stb 00,x
BF5F F6 00 13        ...   ldab 0013
BF62 C1 02           ..    cmpb #02
BF64 26 10           &.    bne 10
BF66 C6 2E           ..    ldab #2e
BF68 37              7     pshb 
BF69 30              0     tsx 
BF6A E6 06           ..    ldab 06,x
BF6C 4F              O     clra 
BF6D C3 0B 93        ...   addd #0b93
BF70 8F              .     xgdx 
BF71 33              3     pulb 
BF72 E7 00           ..    stb 00,x
BF74 20 68            h    bra 68
BF76 C1 04           ..    cmpb #04
BF78 26 26           &&    bne 26
BF7A 30              0     tsx 
BF7B E6 05           ..    ldab 05,x
BF7D 4F              O     clra 
BF7E C3 0B 94        ...   addd #0b94
BF81 8F              .     xgdx 
BF82 E6 00           ..    ldab 00,x
BF84 37              7     pshb 
BF85 30              0     tsx 
BF86 E6 06           ..    ldab 06,x
BF88 4F              O     clra 
BF89 C3 0B 93        ...   addd #0b93
BF8C 8F              .     xgdx 
BF8D 33              3     pulb 
BF8E E7 00           ..    stb 00,x
BF90 C6 2E           ..    ldab #2e
BF92 37              7     pshb 
BF93 30              0     tsx 
BF94 E6 06           ..    ldab 06,x
BF96 4F              O     clra 
BF97 C3 0B 94        ...   addd #0b94
BF9A 8F              .     xgdx 
BF9B 33              3     pulb 
BF9C E7 00           ..    stb 00,x
BF9E 20 3E            >    bra 3e
BFA0 C1 08           ..    cmpb #08
BFA2 26 3A           &:    bne 3a
BFA4 30              0     tsx 
BFA5 E6 05           ..    ldab 05,x
BFA7 4F              O     clra 
BFA8 C3 0B 94        ...   addd #0b94
BFAB 8F              .     xgdx 
BFAC E6 00           ..    ldab 00,x
BFAE 37              7     pshb 
BFAF 30              0     tsx 
BFB0 E6 06           ..    ldab 06,x
BFB2 4F              O     clra 
BFB3 C3 0B 93        ...   addd #0b93
BFB6 8F              .     xgdx 
BFB7 33              3     pulb 
BFB8 E7 00           ..    stb 00,x
BFBA 30              0     tsx 
BFBB E6 05           ..    ldab 05,x
BFBD 4F              O     clra 
BFBE C3 0B 95        ...   addd #0b95
BFC1 8F              .     xgdx 
BFC2 E6 00           ..    ldab 00,x
BFC4 37              7     pshb 
BFC5 30              0     tsx 
BFC6 E6 06           ..    ldab 06,x
BFC8 4F              O     clra 
BFC9 C3 0B 94        ...   addd #0b94
BFCC 8F              .     xgdx 
BFCD 33              3     pulb 
BFCE E7 00           ..    stb 00,x
BFD0 C6 2E           ..    ldab #2e
BFD2 37              7     pshb 
BFD3 30              0     tsx 
BFD4 E6 06           ..    ldab 06,x
BFD6 4F              O     clra 
BFD7 C3 0B 95        ...   addd #0b95
BFDA 8F              .     xgdx 
BFDB 33              3     pulb 
BFDC E7 00           ..    stb 00,x
goto32:
BFDE F6 0B C0        ...   ldab 0bc0
BFE1 C4 01           ..    andb #01
BFE3 26 0E           &.    bne 0e
BFE5 F6 0B C0        ...   ldab 0bc0
BFE8 C4 04           ..    andb #04
BFEA 26 07           &.    bne 07
BFEC F6 0B C0        ...   ldab 0bc0
BFEF C4 08           ..    andb #08
BFF1 27 15           '.    beq 15
BFF3 F6 00 17        ...   ldab 0017
BFF6 C4 08           ..    andb #08
BFF8 26 0E           &.    bne 0e
BFFA C6 24           .$    ldab #24
BFFC 37              7     pshb 
BFFD 30              0     tsx 
BFFE E6 06           ..    ldab 06,x
C000 4F              O     clra 
C001 C3 0B 96        ...   addd #0b96
C004 8F              .     xgdx 
C005 33              3     pulb 
C006 E7 00           ..    stb 00,x
C008 BD BE 69        ..i   jsr be69		;jump3
C00B F6 00 13        ...   ldab 0013
C00E C1 01           ..    cmpb #01
C010 23 17           #.    bls 17
C012 CE 10 28        ..(   ldx #1028
C015 1C 00           ..    bset add,x 00,x
C017 40              @     nega 
C018 CE 00 01        ...   ldx #0001
C01B 3C              <     pshx 
C01C CC 00 20        ..    ldd #0020
C01F BD BD B5        ...   jsr bdb5		;jump78	
C022 38              8     pulx 
C023 CE 10 28        ..(   ldx #1028
C026 1D 00           ..    bclr add,x 00,x
C028 40              @     nega 
C029 BD BE 10        ...   jsr be10		;jump88
C02C 38              8     pulx 
C02D 39              9     rts 

jump71:
C02E 37              7     pshb 
C02F 36              6     psha 
C030 3C              <     pshx 
C031 F6 00 2C        ..,   ldab 002c
C034 27 03           '.    beq 03
C036 7E C0 B7        ~..   jmp c0b7		;goto33
C039 FC 00 03        ...   ldd 0003
C03C 30              0     tsx 
C03D A3 02           ..    subd 02,x
C03F 25 0A           %.    bcs 0a
C041 E6 07           ..    ldab 07,x
C043 4F              O     clra 
C044 E3 02           ..    addd 02,x
C046 B3 00 03        ...   subd 0003
C049 24 06           $.    bcc 06
C04B 30              0     tsx 
C04C EC 02           ..    ldd 02,x
C04E FD 00 03        ...   stad 0003
C051 FE 00 03        ...   ldx 0003
C054 08              .     inx 
C055 FF 00 03        ...   stx 0003
C058 09              .     dex 
C059 8F              .     xgdx 
C05A 30              0     tsx 
C05B ED 00           ..    stad 00,x
C05D C6 09           ..    ldab #09
C05F F7 00 18        ...   stb 0018
C062 F6 00 18        ...   ldab 0018
C065 C1 FF           ..    cmpb #ff
C067 24 46           $F    bcc 46
C069 30              0     tsx 
C06A E6 07           ..    ldab 07,x
C06C 4F              O     clra 
C06D E3 02           ..    addd 02,x
C06F A3 00           ..    subd 00,x
C071 24 05           $.    bcc 05
C073 30              0     tsx 
C074 EC 02           ..    ldd 02,x
C076 ED 00           ..    stad 00,x
C078 30              0     tsx 
C079 EE 00           ..    ldx 00,x
C07B E6 00           ..    ldab 00,x
C07D C1 40           .@    cmpb #40
C07F 24 13           $.    bcc 13
C081 30              0     tsx 
C082 EE 00           ..    ldx 00,x
C084 E6 00           ..    ldab 00,x
C086 37              7     pshb 
C087 F6 00 18        ...   ldab 0018
C08A 4F              O     clra 
C08B C3 0B 90        ...   addd #0b90
C08E 8F              .     xgdx 
C08F 33              3     pulb 
C090 E7 00           ..    stb 00,x
C092 20 13            .    bra 13
C094 30              0     tsx 
C095 EE 00           ..    ldx 00,x
C097 E6 00           ..    ldab 00,x
C099 CB C0           ..    addb #c0
C09B 37              7     pshb 
C09C F6 00 18        ...   ldab 0018
C09F 4F              O     clra 
C0A0 C3 0B 90        ...   addd #0b90
C0A3 8F              .     xgdx 
C0A4 33              3     pulb 
C0A5 E7 00           ..    stb 00,x
C0A7 38              8     pulx 
C0A8 08              .     inx 
C0A9 3C              <     pshx 
C0AA 7A 00 18        z..   dec 0018
C0AD 20 B3            .    bra b3
C0AF BD BE 01        ...   jsr be01		;jump70
C0B2 C6 03           ..    ldab #03
C0B4 F7 00 2C        ..,   stb 002c
goto33:
C0B7 38              8     pulx 
C0B8 38              8     pulx 
C0B9 39              9     rts 

;Display a string.  D points to the string, which is NULL-terminated

display:
C0BA 37              7     pshb 
C0BB 36              6     psha 
C0BC F6 00 1F        ...   ldab 001f
C0BF C4 04           ..    andb #04
C0C1 26 03           &.    bne 03
C0C3 7F 00 2C        ..,   clr 002c
C0C6 F6 00 2C        ..,   ldab 002c
C0C9 26 13           &.    bne 13
C0CB 5F              _     clrb 
C0CC 4F              O     clra 
C0CD FD 00 03        ...   stad 0003
C0D0 7F 00 2C        ..,   clr 002c
C0D3 CE 00 09        ...   ldx #0009
C0D6 3C              <     pshx 
C0D7 30              0     tsx 
C0D8 EC 02           ..    ldd 02,x
C0DA BD C0 2E        ...   jsr c02e		;jump71
C0DD 38              8     pulx 
C0DE 38              8     pulx 
C0DF 39              9     rts 

jump94:
C0E0 F6 00 15        ...   ldab 0015
C0E3 2C 02           ,.    bge 02
C0E5 20 F9            .    bra f9
C0E7 CE 00 1D        ...   ldx #001d
C0EA 1C 00           ..    bset add,x 00,x
C0EC 20 BD            .    bra bd
C0EE D7 30 83        .0.   stab 30
C0F1 00              .     test 
C0F2 00              .     test 
C0F3 27 09           '.    beq 09
C0F5 CE 00 1D        ...   ldx #001d
C0F8 1D 00           ..    bclr add,x 00,x
C0FA 20 5F            _    bra 5f
C0FC 4F              O     clra 
C0FD 39              9     rts 

C0FE CE 00 14        ...   ldx #0014
C101 1C 00           ..    bset add,x 00,x
C103 01              .     nop 
C104 F6 00 14        ...   ldab 0014
C107 F7 38 00        .8.   stb 3800
C10A F6 00 15        ...   ldab 0015
C10D C4 1F           ..    andb #1f
C10F 26 03           &.    bne 03
C111 7E C3 1B        ~..   jmp c31b		;goto34
C114 C6 14           ..    ldab #14
C116 F7 00 31        ..1   stb 0031
C119 F6 00 15        ...   ldab 0015
C11C 2D 15           -.    blt 15
C11E F6 00 31        ..1   ldab 0031
C121 C1 0A           ..    cmpb #0a
C123 22 0C           ".    bhi 0c
C125 BD C5 D3        ...   jsr c5d3		;jump6
C128 CE 00 1D        ...   ldx #001d
C12B 1D 00           ..    bclr add,x 00,x
C12D 20 5F            _    bra 5f
C12F 4F              O     clra 
C130 39              9     rts 

C131 20 E6            .    bra e6
C133 FC 00 3E        ..>   ldd 003e
C136 F3 00 40        ..@   addd 0040
C139 FD 00 40        ..@   stad 0040
C13C 5F              _     clrb 
C13D 4F              O     clra 
C13E FD 00 3E        ..>   stad 003e
C141 F6 00 15        ...   ldab 0015
C144 C4 1F           ..    andb #1f
C146 4F              O     clra 
C147 BD FE 32        ..2   jsr fe32		;jump25
C14A C1 64           .d    cmpb #64
C14C 00              .     test 
C14D 01              .     nop 
C14E C1 BA           ..    cmpb #ba
C150 00              .     test 
C151 01              .     nop 
C152 C2 10           ..    sbcb #10
C154 00              .     test 
C155 02              .     idiv 
C156 C2 66           .f    sbcb #66
C158 00              .     test 
C159 04              .     lsrd 
C15A C2 BB           ..    sbcb #bb
C15C 00              .     test 
C15D 08              .     inx 
C15E C3 0E FF        ...   addd #0eff
C161 F0 00 00        ...   subb 0000
C164 FC 0C DD        ...   ldd 0cdd
C167 83 FF FF        ...   subd #ffff
C16A 26 05           &.    bne 05
C16C 5F              _     clrb 
C16D 4F              O     clra 
C16E FD 0C DD        ...   stad 0cdd
C171 FE 0C DD        ...   ldx 0cdd
C174 08              .     inx 
C175 FF 0C DD        ...   stx 0cdd
C178 CE 00 64        ..d   ldx #0064
C17B 3C              <     pshx 
C17C CC 00 00        ...   ldd #0000
C17F 37              7     pshb 
C180 36              6     psha 
C181 CC 0C F3        ...   ldd #0cf3
C184 BD FB DF        ...   jsr fbdf		;jump46
C187 31              1     ins 
C188 31              1     ins 
C189 FE 0C F5        ...   ldx 0cf5
C18C 3C              <     pshx 
C18D FC 0C F3        ...   ldd 0cf3
C190 37              7     pshb 
C191 36              6     psha 
C192 CE FF 9C        ...   ldx #ff9c
C195 3C              <     pshx 
C196 CC 00 63        ..c   ldd #0063
C199 30              0     tsx 
C19A A3 02           ..    subd 02,x
C19C 32              2     pula 
C19D 33              3     pulb 
C19E 26 02           &.    bne 02
C1A0 A3 04           ..    subd 04,x
C1A2 38              8     pulx 
C1A3 38              8     pulx 
C1A4 24 11           $.    bcc 11
C1A6 CE 00 64        ..d   ldx #0064
C1A9 3C              <     pshx 
C1AA CC FF 9C        ...   ldd #ff9c
C1AD 37              7     pshb 
C1AE 36              6     psha 
C1AF CC 0C F3        ...   ldd #0cf3
C1B2 BD FB DF        ...   jsr fbdf		;jump46
C1B5 31              1     ins 
C1B6 31              1     ins 
C1B7 7E C3 0E        ~..   jmp c30e		;goto35
C1BA FC 0C DF        ...   ldd 0cdf
C1BD 83 FF FF        ...   subd #ffff
C1C0 26 05           &.    bne 05
C1C2 5F              _     clrb 
C1C3 4F              O     clra 
C1C4 FD 0C DF        ...   stad 0cdf
C1C7 FE 0C DF        ...   ldx 0cdf
C1CA 08              .     inx 
C1CB FF 0C DF        ...   stx 0cdf
C1CE CE 00 C8        ...   ldx #00c8
C1D1 3C              <     pshx 
C1D2 CC 00 00        ...   ldd #0000
C1D5 37              7     pshb 
C1D6 36              6     psha 
C1D7 CC 0C F7        ...   ldd #0cf7
C1DA BD FB DF        ...   jsr fbdf		;jump46
C1DD 31              1     ins 
C1DE 31              1     ins 
C1DF FE 0C F9        ...   ldx 0cf9
C1E2 3C              <     pshx 
C1E3 FC 0C F7        ...   ldd 0cf7
C1E6 37              7     pshb 
C1E7 36              6     psha 
C1E8 CE FF 38        ..8   ldx #ff38
C1EB 3C              <     pshx 
C1EC CC 00 C7        ...   ldd #00c7
C1EF 30              0     tsx 
C1F0 A3 02           ..    subd 02,x
C1F2 32              2     pula 
C1F3 33              3     pulb 
C1F4 26 02           &.    bne 02
C1F6 A3 04           ..    subd 04,x
C1F8 38              8     pulx 
C1F9 38              8     pulx 
C1FA 24 11           $.    bcc 11
C1FC CE 00 C8        ...   ldx #00c8
C1FF 3C              <     pshx 
C200 CC FF 38        ..8   ldd #ff38
C203 37              7     pshb 
C204 36              6     psha 
C205 CC 0C F7        ...   ldd #0cf7
C208 BD FB DF        ...   jsr fbdf		;jump46
C20B 31              1     ins 
C20C 31              1     ins 
C20D 7E C3 0E        ~..   jmp c30e		;goto35
C210 FC 0C E1        ...   ldd 0ce1
C213 83 FF FF        ...   subd #ffff
C216 26 05           &.    bne 05
C218 5F              _     clrb 
C219 4F              O     clra 
C21A FD 0C E1        ...   stad 0ce1
C21D FE 0C E1        ...   ldx 0ce1
C220 08              .     inx 
C221 FF 0C E1        ...   stx 0ce1
C224 CE 01 F4        ...   ldx #01f4
C227 3C              <     pshx 
C228 CC 00 00        ...   ldd #0000
C22B 37              7     pshb 
C22C 36              6     psha 
C22D CC 0C FB        ...   ldd #0cfb
C230 BD FB DF        ...   jsr fbdf		;jump46
C233 31              1     ins 
C234 31              1     ins 
C235 FE 0C FD        ...   ldx 0cfd
C238 3C              <     pshx 
C239 FC 0C FB        ...   ldd 0cfb
C23C 37              7     pshb 
C23D 36              6     psha 
C23E CE FE 0C        ...   ldx #fe0c
C241 3C              <     pshx 
C242 CC 01 F3        ...   ldd #01f3
C245 30              0     tsx 
C246 A3 02           ..    subd 02,x
C248 32              2     pula 
C249 33              3     pulb 
C24A 26 02           &.    bne 02
C24C A3 04           ..    subd 04,x
C24E 38              8     pulx 
C24F 38              8     pulx 
C250 24 11           $.    bcc 11
C252 CE 01 F4        ...   ldx #01f4
C255 3C              <     pshx 
C256 CC FE 0C        ...   ldd #fe0c
C259 37              7     pshb 
C25A 36              6     psha 
C25B CC 0C FB        ...   ldd #0cfb
C25E BD FB DF        ...   jsr fbdf		;jump46
C261 31              1     ins 
C262 31              1     ins 
C263 7E C3 0E        ~..   jmp c30e		;goto35
C266 FC 0C E3        ...   ldd 0ce3
C269 83 FF FF        ...   subd #ffff
C26C 26 05           &.    bne 05
C26E 5F              _     clrb 
C26F 4F              O     clra 
C270 FD 0C E3        ...   stad 0ce3
C273 FE 0C E3        ...   ldx 0ce3
C276 08              .     inx 
C277 FF 0C E3        ...   stx 0ce3
C27A CE 03 E8        ...   ldx #03e8
C27D 3C              <     pshx 
C27E CC 00 00        ...   ldd #0000
C281 37              7     pshb 
C282 36              6     psha 
C283 CC 0C FF        ...   ldd #0cff
C286 BD FB DF        ...   jsr fbdf		;jump46
C289 31              1     ins 
C28A 31              1     ins 
C28B FE 0D 01        ...   ldx 0d01
C28E 3C              <     pshx 
C28F FC 0C FF        ...   ldd 0cff
C292 37              7     pshb 
C293 36              6     psha 
C294 CE FC 18        ...   ldx #fc18
C297 3C              <     pshx 
C298 CC 03 E7        ...   ldd #03e7
C29B 30              0     tsx 
C29C A3 02           ..    subd 02,x
C29E 32              2     pula 
C29F 33              3     pulb 
C2A0 26 02           &.    bne 02
C2A2 A3 04           ..    subd 04,x
C2A4 38              8     pulx 
C2A5 38              8     pulx 
C2A6 24 11           $.    bcc 11
C2A8 CE 03 E8        ...   ldx #03e8
C2AB 3C              <     pshx 
C2AC CC FC 18        ...   ldd #fc18
C2AF 37              7     pshb 
C2B0 36              6     psha 
C2B1 CC 0C FF        ...   ldd #0cff
C2B4 BD FB DF        ...   jsr fbdf		;jump46
C2B7 31              1     ins 
C2B8 31              1     ins 
C2B9 20 53            S    bra 53
C2BB FC 0C E5        ...   ldd 0ce5
C2BE 83 C3 4F        ..O   subd #c34f
C2C1 26 05           &.    bne 05
C2C3 5F              _     clrb 
C2C4 4F              O     clra 
C2C5 FD 0C E5        ...   stad 0ce5
C2C8 FE 0C E5        ...   ldx 0ce5
C2CB 08              .     inx 
C2CC FF 0C E5        ...   stx 0ce5
C2CF CE 07 D0        ...   ldx #07d0
C2D2 3C              <     pshx 
C2D3 CC 00 00        ...   ldd #0000
C2D6 37              7     pshb 
C2D7 36              6     psha 
C2D8 CC 0D 03        ...   ldd #0d03
C2DB BD FB DF        ...   jsr fbdf		;jump46
C2DE 31              1     ins 
C2DF 31              1     ins 
C2E0 FE 0D 05        ...   ldx 0d05
C2E3 3C              <     pshx 
C2E4 FC 0D 03        ...   ldd 0d03
C2E7 37              7     pshb 
C2E8 36              6     psha 
C2E9 CE D9 30        ..0   ldx #d930
C2EC 3C              <     pshx 
C2ED CC 05 F5        ...   ldd #05f5
C2F0 30              0     tsx 
C2F1 A3 02           ..    subd 02,x
C2F3 32              2     pula 
C2F4 33              3     pulb 
C2F5 26 02           &.    bne 02
C2F7 A3 04           ..    subd 04,x
C2F9 38              8     pulx 
C2FA 38              8     pulx 
C2FB 24 11           $.    bcc 11
C2FD CE 26 D0        .&.   ldx #26d0
C300 3C              <     pshx 
C301 CC FA 0A        ...   ldd #fa0a
C304 37              7     pshb 
C305 36              6     psha 
C306 CC 0D 03        ...   ldd #0d03
C309 BD FB DF        ...   jsr fbdf		;jump46
C30C 31              1     ins 
C30D 31              1     ins 
goto35:
C30E F6 00 15        ...   ldab 0015
C311 2C 02           ,.    bge 02
C313 20 F9            .    bra f9
C315 CE 00 15        ...   ldx #0015
C318 1D 00           ..    bclr add,x 00,x
C31A 1F CE 00 1D     ....  brclr ce,x 00 1d
;goto34
; c31b???
C31E 1D 00           ..    bclr add,x 00,x
C320 20 CC            .    bra cc
C322 00              .     test 
C323 01              .     nop 
C324 39              9     rts 

jump64:
C325 F6 10 00        ...   ldab 1000
C328 C4 01           ..    andb #01
C32A 26 13           &.    bne 13
C32C 7C 00 7D        |.}   inc 007d
C32F F6 00 7D        ..}   ldab 007d
C332 C1 05           ..    cmpb #05
C334 23 03           #.    bls 03
C336 7F 00 7D        ..}   clr 007d
C339 7F 00 7E        ..~   clr 007e
C33C 7E C4 2F        ~./   jmp c42f		;goto36
C33F 7C 00 7E        |.~   inc 007e
C342 F6 00 7E        ..~   ldab 007e
C345 C1 07           ..    cmpb #07
C347 23 17           #.    bls 17
C349 7F 00 7D        ..}   clr 007d
C34C 7F 00 7E        ..~   clr 007e
C34F 7F 00 7C        ..|   clr 007c
C352 F6 00 15        ...   ldab 0015
C355 2C 03           ,.    bge 03
C357 7F 00 2C        ..,   clr 002c
C35A CE 00 15        ...   ldx #0015
C35D 1D 00           ..    bclr add,x 00,x
C35F 80 F6           ..    suba #f6
C361 00              .     test 
C362 7E 26 03        ~&.   jmp 2603		;goto37 ???
C365 7E C4 2F        ~./   jmp c42f		;goto36
C368 F6 00 7D        ..}   ldab 007d
C36B C1 01           ..    cmpb #01
C36D 22 03           ".    bhi 03
C36F 7E C4 2F        ~./   jmp c42f		;goto36
C372 CE 00 15        ...   ldx #0015
C375 1C 00           ..    bset add,x 00,x
C377 80 CE           ..    suba #ce
C379 0B              .     sev 
C37A C1 1D           ..    cmpb #1d
C37C 00              .     test 
C37D 08              .     inx 
C37E F6 00 1D        ...   ldab 001d
C381 C4 20           .     andb #20
C383 27 03           '.    beq 03
C385 7E C4 29        ~.)   jmp c429		;goto38
C388 C6 02           ..    ldab #02
C38A F7 00 2C        ..,   stb 002c
C38D 7F 00 3A        ..:   clr 003a
C390 CE 00 1B        ...   ldx #001b
C393 1D 00           ..    bclr add,x 00,x
C395 40              @     nega 
C396 7F 00 25        ..%   clr 0025
C399 CE 00 21        ..!   ldx #0021
C39C 1D 00           ..    bclr add,x 00,x
C39E 08              .     inx 
C39F CE 00 14        ...   ldx #0014
C3A2 1D 00           ..    bclr add,x 00,x
C3A4 01              .     nop 
C3A5 F6 00 14        ...   ldab 0014
C3A8 F7 38 00        .8.   stb 3800
C3AB 7C 00 7C        |.|   inc 007c
C3AE CE 00 15        ...   ldx #0015
C3B1 1D 00           ..    bclr add,x 00,x
C3B3 1F F6 00 7C     ...|  brclr f6,x 00 7c
C3B7 C1 01           ..    cmpb #01
C3B9 26 0E           &.    bne 0e
C3BB CE 00 15        ...   ldx #0015
C3BE 1C 00           ..    bset add,x 00,x
C3C0 01              .     nop 
C3C1 CC 00 64        ..d   ldd #0064
C3C4 FD 00 3E        ..>   stad 003e
C3C7 20 60            `    bra 60
C3C9 F6 00 7C        ..|   ldab 007c
C3CC C1 02           ..    cmpb #02
C3CE 26 0E           &.    bne 0e
C3D0 CE 00 15        ...   ldx #0015
C3D3 1C 00           ..    bset add,x 00,x
C3D5 02              .     idiv 
C3D6 CC 00 C8        ...   ldd #00c8
C3D9 FD 00 3E        ..>   stad 003e
C3DC 20 4B            K    bra 4b
C3DE F6 00 7C        ..|   ldab 007c
C3E1 C1 02           ..    cmpb #02
C3E3 23 15           #.    bls 15
C3E5 F6 00 7C        ..|   ldab 007c
C3E8 C1 06           ..    cmpb #06
C3EA 24 0E           $.    bcc 0e
C3EC CE 00 15        ...   ldx #0015
C3EF 1C 00           ..    bset add,x 00,x
C3F1 04              .     lsrd 
C3F2 CC 01 F4        ...   ldd #01f4
C3F5 FD 00 3E        ..>   stad 003e
C3F8 20 2F            /    bra 2f
C3FA F6 00 7C        ..|   ldab 007c
C3FD C1 05           ..    cmpb #05
C3FF 23 15           #.    bls 15
C401 F6 00 7C        ..|   ldab 007c
C404 C1 0B           ..    cmpb #0b
C406 24 0E           $.    bcc 0e
C408 CE 00 15        ...   ldx #0015
C40B 1C 00           ..    bset add,x 00,x
C40D 08              .     inx 
C40E CC 03 E8        ...   ldd #03e8
C411 FD 00 3E        ..>   stad 003e
C414 20 13            .    bra 13
C416 F6 00 7C        ..|   ldab 007c
C419 C1 0B           ..    cmpb #0b
C41B 23 0C           #.    bls 0c
C41D CE 00 15        ...   ldx #0015
C420 1C 00           ..    bset add,x 00,x
C422 10              .     sba 
C423 CC 07 D0        ...   ldd #07d0
C426 FD 00 3E        ..>   stad 003e
goto38:
C429 7F 00 7D        ..}   clr 007d
C42C 7F 00 7E        ..~   clr 007e
goto36:
C42F 39              9     rts 

jump26:
C430 F6 00 01        ...   ldab 0001
C433 53              S     comb 
C434 F4 00 1A        ...   andb 001a
C437 F7 00 1A        ...   stb 001a
C43A F6 00 1A        ...   ldab 001a
C43D F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C440 CC 00 64        ..d   ldd #0064
C443 BD E5 71        ..q   jsr e571		;jump13
C446 F6 00 01        ...   ldab 0001
C449 FA 00 1A        ...   orb 001a
C44C F7 00 1A        ...   stb 001a
C44F F6 00 1A        ...   ldab 001a
C452 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C455 39              9     rts 

jump47:
C456 FC 00 40        ..@   ldd 0040
C459 26 03           &.    bne 03
C45B 7E C5 C6        ~..   jmp c5c6		;goto39
C45E CE 00 1A        ...   ldx #001a
C461 1C 00           ..    bset add,x 00,x
C463 01              .     nop 
C464 F6 00 1A        ...   ldab 001a
C467 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C46A CC 03 E8        ...   ldd #03e8
C46D BD E5 71        ..q   jsr e571		;jump13
goto42:
C470 FC 00 40        ..@   ldd 0040
C473 83 00 00        ...   subd #0000
C476 22 03           ".    bhi 03
C478 7E C5 B0        ~..   jmp c5b0		;goto40
C47B F6 00 1B        ...   ldab 001b
C47E C4 20           .     andb #20
C480 27 1B           '.    beq 1b
C482 FC 00 40        ..@   ldd 0040
C485 83 03 E8        ...   subd #03e8
C488 25 13           %.    bcs 13
C48A C6 28           .(    ldab #28
C48C F7 00 01        ...   stb 0001
C48F CC FC 18        ...   ldd #fc18
C492 F3 00 40        ..@   addd 0040
C495 FD 00 40        ..@   stad 0040
C498 8D 96           ..    bsr dest 96
C49A 7E C5 7C        ~.|   jmp c57c		;goto41
C49D F6 00 1B        ...   ldab 001b
C4A0 C4 20           .     andb #20
C4A2 27 1C           '.    beq 1c
C4A4 FC 00 40        ..@   ldd 0040
C4A7 83 00 64        ..d   subd #0064
C4AA 25 14           %.    bcs 14
C4AC C6 30           .0    ldab #30
C4AE F7 00 01        ...   stb 0001
C4B1 CC FF 9C        ...   ldd #ff9c
C4B4 F3 00 40        ..@   addd 0040
C4B7 FD 00 40        ..@   stad 0040
C4BA BD C4 30        ..0   jsr c430		;jump26
C4BD 7E C5 7C        ~.|   jmp c57c		;goto41
C4C0 FC 0B DD        ...   ldd 0bdd
C4C3 27 3C           '<    beq 3c
C4C5 FC 00 40        ..@   ldd 0040
C4C8 B3 0B DD        ...   subd 0bdd
C4CB 25 34           %4    bcs 34
C4CD F6 0C DB        ...   ldab 0cdb
C4D0 26 07           &.    bne 07
C4D2 F6 00 1B        ...   ldab 001b
C4D5 C4 20           .     andb #20
C4D7 27 28           '(    beq 28
C4D9 FC 0B DD        ...   ldd 0bdd
C4DC 53              S     comb 
C4DD 43              C     coma 
C4DE F9 00 41        ..A   adcb 0041
C4E1 B9 00 40        ..@   adca 0040
C4E4 FD 00 40        ..@   stad 0040
C4E7 F6 0B DA        ...   ldab 0bda
C4EA F7 00 01        ...   stb 0001
C4ED BD C4 30        ..0   jsr c430		;jump26
C4F0 F6 00 1B        ...   ldab 001b
C4F3 C4 20           .     andb #20
C4F5 26 08           &.    bne 08
C4F7 F6 0C DB        ...   ldab 0cdb
C4FA 27 03           '.    beq 03
C4FC 7A 0C DB        z..   dec 0cdb
C4FF 20 7B            {    bra 7b
C501 FC 0B DF        ...   ldd 0bdf
C504 27 3C           '<    beq 3c
C506 FC 00 40        ..@   ldd 0040
C509 B3 0B DF        ...   subd 0bdf
C50C 25 34           %4    bcs 34
C50E F6 0C DA        ...   ldab 0cda
C511 26 07           &.    bne 07
C513 F6 00 1B        ...   ldab 001b
C516 C4 20           .     andb #20
C518 27 28           '(    beq 28
C51A FC 0B DF        ...   ldd 0bdf
C51D 53              S     comb 
C51E 43              C     coma 
C51F F9 00 41        ..A   adcb 0041
C522 B9 00 40        ..@   adca 0040
C525 FD 00 40        ..@   stad 0040
C528 F6 0B DB        ...   ldab 0bdb
C52B F7 00 01        ...   stb 0001
C52E BD C4 30        ..0   jsr c430		;jump26
C531 F6 00 1B        ...   ldab 001b
C534 C4 20           .     andb #20
C536 26 08           &.    bne 08
C538 F6 0C DA        ...   ldab 0cda
C53B 27 03           '.    beq 03
C53D 7A 0C DA        z..   dec 0cda
C540 20 3A            :    bra 3a
C542 FC 0B E1        ...   ldd 0be1
C545 27 30           '0    beq 30
C547 FC 00 40        ..@   ldd 0040
C54A B3 0B E1        ...   subd 0be1
C54D 25 28           %(    bcs 28
C54F FC 0B E1        ...   ldd 0be1
C552 53              S     comb 
C553 43              C     coma 
C554 F9 00 41        ..A   adcb 0041
C557 B9 00 40        ..@   adca 0040
C55A FD 00 40        ..@   stad 0040
C55D F6 0B DC        ...   ldab 0bdc
C560 F7 00 01        ...   stb 0001
C563 BD C4 30        ..0   jsr c430		;jump26
C566 F6 00 1B        ...   ldab 001b
C569 C4 20           .     andb #20
C56B 26 08           &.    bne 08
C56D F6 0C D9        ...   ldab 0cd9
C570 27 03           '.    beq 03
C572 7A 0C D9        z..   dec 0cd9
C575 20 05            .    bra 05
C577 5F              _     clrb 
C578 4F              O     clra 
C579 FD 00 40        ..@   stad 0040
goto41:
C57C CC 01 2C        ..,   ldd #012c
C57F BD E5 71        ..q   jsr e571		;jump13
C582 F6 00 1B        ...   ldab 001b
C585 C4 20           .     andb #20
C587 26 24           &$    bne 24
C589 CE 00 1A        ...   ldx #001a
C58C 1D 00           ..    bclr add,x 00,x
C58E 01              .     nop 
C58F F6 00 1A        ...   ldab 001a
C592 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C595 CC 00 0C        ...   ldd #000c
C598 BD E5 71        ..q   jsr e571		;jump13
C59B CE 00 1A        ...   ldx #001a
C59E 1C 00           ..    bset add,x 00,x
C5A0 01              .     nop 
C5A1 F6 00 1A        ...   ldab 001a
C5A4 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C5A7 CC 00 23        ..#   ldd #0023
C5AA BD E5 71        ..q   jsr e571		;jump13
C5AD 7E C4 70        ~.p   jmp c470		;goto42
goto40:
C5B0 F6 0B C0        ...   ldab 0bc0
C5B3 C4 04           ..    andb #04
C5B5 27 0F           '.    beq 0f
C5B7 F6 00 1B        ...   ldab 001b
C5BA C4 20           .     andb #20
C5BC 27 08           '.    beq 08
C5BE C6 38           .8    ldab #38
C5C0 F7 00 01        ...   stb 0001
C5C3 BD C4 30        ..0   jsr c430		;jump26
goto39:
C5C6 CE 00 21        ..!   ldx #0021
C5C9 1D 00           ..    bclr add,x 00,x
C5CB 10              .     sba 
C5CC CE 00 1B        ...   ldx #001b
C5CF 1D 00           ..    bclr add,x 00,x
C5D1 20 39            9    bra 39

jump6:
C5D3 F6 00 15        ...   ldab 0015
C5D6 C4 1F           ..    andb #1f
C5D8 26 03           &.    bne 03
C5DA 7E C6 7C        ~.|   jmp c67c		;goto43
C5DD CC 00 C8        ...   ldd #00c8
C5E0 BD E5 71        ..q   jsr e571		;jump13
C5E3 FC 00 3E        ..>   ldd 003e
C5E6 27 58           'X    beq 58
C5E8 F6 00 15        ...   ldab 0015
C5EB C4 1F           ..    andb #1f
C5ED 4F              O     clra 
C5EE BD FE 32        ..2   jsr fe32		;jump25
C5F1 C6 0B           ..    ldab #0b
C5F3 00              .     test 
C5F4 01              .     nop 
C5F5 C6 16           ..    ldab #16
C5F7 00              .     test 
C5F8 01              .     nop 
C5F9 C6 21           .!    ldab #21
C5FB 00              .     test 
C5FC 02              .     idiv 
C5FD C6 2C           .,    ldab #2c
C5FF 00              .     test 
C600 04              .     lsrd 
C601 C6 37           .7    ldab #37
C603 00              .     test 
C604 08              .     inx 
C605 C6 40           .@    ldab #40
C607 FF F0 00        ...   stx f000
C60A 00              .     test 
C60B CC FF 9C        ...   ldd #ff9c
C60E F3 00 3E        ..>   addd 003e
C611 FD 00 3E        ..>   stad 003e
C614 20 2A            *    bra 2a
C616 CC FF 38        ..8   ldd #ff38
C619 F3 00 3E        ..>   addd 003e
C61C FD 00 3E        ..>   stad 003e
C61F 20 1F            .    bra 1f
C621 CC FE 0C        ...   ldd #fe0c
C624 F3 00 3E        ..>   addd 003e
C627 FD 00 3E        ..>   stad 003e
C62A 20 14            .    bra 14
C62C CC FC 18        ...   ldd #fc18
C62F F3 00 3E        ..>   addd 003e
C632 FD 00 3E        ..>   stad 003e
C635 20 09            .    bra 09
C637 CC F8 30        ..0   ldd #f830
C63A F3 00 3E        ..>   addd 003e
C63D FD 00 3E        ..>   stad 003e
C640 CE 00 14        ...   ldx #0014
C643 1D 00           ..    bclr add,x 00,x
C645 02              .     idiv 
C646 F6 00 14        ...   ldab 0014
C649 F7 38 00        .8.   stb 3800
C64C CC 00 1E        ...   ldd #001e
C64F BD E5 71        ..q   jsr e571		;jump13
C652 CE 00 15        ...   ldx #0015
C655 1D 00           ..    bclr add,x 00,x
C657 1F CE 00 14     ....  brclr ce,x 00 14
C65B 1C 00           ..    bset add,x 00,x
C65D 02              .     idiv 
C65E F6 00 14        ...   ldab 0014
C661 F7 38 00        .8.   stb 3800
C664 CC 00 19        ...   ldd #0019
C667 BD E5 71        ..q   jsr e571		;jump13
C66A CE 00 14        ...   ldx #0014
C66D 1C 00           ..    bset add,x 00,x
C66F 01              .     nop 
C670 F6 00 14        ...   ldab 0014
C673 F7 38 00        .8.   stb 3800
C676 CC 00 19        ...   ldd #0019
C679 BD E5 71        ..q   jsr e571		;jump13
goto43:
C67C 39              9     rts 

jump124:
C67D F6 00 30        ..0   ldab 0030
C680 26 55           &U    bne 55
C682 F6 0B C0        ...   ldab 0bc0
C685 C4 04           ..    andb #04
C687 26 4E           &N    bne 4e
C689 C6 27           .'    ldab #27
C68B F7 10 2D        ..-   stb 102d
C68E C6 07           ..    ldab #07
C690 F7 10 2B        ..+   stb 102b
C693 CE 00 1A        ...   ldx #001a
C696 1C 00           ..    bset add,x 00,x
C698 04              .     lsrd 
C699 F6 00 1A        ...   ldab 001a
C69C F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C69F CC 00 14        ...   ldd #0014
C6A2 BD E5 71        ..q   jsr e571		;jump13
C6A5 F6 0B C0        ...   ldab 0bc0
C6A8 C4 01           ..    andb #01
C6AA 27 06           '.    beq 06
C6AC CE 0B C1        ...   ldx #0bc1
C6AF 1C 00           ..    bset add,x 00,x
C6B1 01              .     nop 
C6B2 F6 0B C0        ...   ldab 0bc0
C6B5 C4 04           ..    andb #04
C6B7 27 06           '.    beq 06
C6B9 CE 0B C1        ...   ldx #0bc1
C6BC 1C 00           ..    bset add,x 00,x
C6BE 02              .     idiv 
C6BF CE 00 1A        ...   ldx #001a
C6C2 1D 00           ..    bclr add,x 00,x
C6C4 04              .     lsrd 
C6C5 F6 00 1A        ...   ldab 001a
C6C8 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C6CB C6 96           ..    ldab #96
C6CD F7 00 30        ..0   stb 0030
C6D0 C6 1E           ..    ldab #1e
C6D2 F7 00 35        ..5   stb 0035
C6D5 20 5D            ]    bra 5d
C6D7 F6 18 00        ...   ldab 1800
C6DA 2D 2A           -*    blt 2a
C6DC F6 00 07        ...   ldab 0007
C6DF 26 25           &%    bne 25
C6E1 CE 00 1A        ...   ldx #001a
C6E4 1D 00           ..    bclr add,x 00,x
C6E6 02              .     idiv 
C6E7 F6 00 1A        ...   ldab 001a
C6EA F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C6ED C6 64           .d    ldab #64
C6EF F7 00 2D        ..-   stb 002d
C6F2 CC 00 32        ..2   ldd #0032
C6F5 BD E5 71        ..q   jsr e571		;jump13
C6F8 CE 00 1A        ...   ldx #001a
C6FB 1C 00           ..    bset add,x 00,x
C6FD 02              .     idiv 
C6FE F6 00 1A        ...   ldab 001a
C701 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C704 20 2E            .    bra 2e
C706 F6 00 2D        ..-   ldab 002d
C709 26 29           &)    bne 29
C70B CE 00 1A        ...   ldx #001a
C70E 1D 00           ..    bclr add,x 00,x
C710 01              .     nop 
C711 F6 00 1A        ...   ldab 001a
C714 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C717 CC 00 0C        ...   ldd #000c
C71A BD E5 71        ..q   jsr e571		;jump13
C71D CE 00 1A        ...   ldx #001a
C720 1C 00           ..    bset add,x 00,x
C722 01              .     nop 
C723 F6 00 1A        ...   ldab 001a
C726 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C729 CC 00 23        ..#   ldd #0023
C72C BD E5 71        ..q   jsr e571		;jump13
C72F C6 64           .d    ldab #64
C731 F7 00 2D        ..-   stb 002d
C734 F6 00 21        ..!   ldab 0021
C737 C4 10           ..    andb #10
C739 27 45           'E    beq 45
C73B F6 00 11        ...   ldab 0011
C73E C4 04           ..    andb #04
C740 26 1A           &.    bne 1a
C742 FC 00 40        ..@   ldd 0040
C745 F3 00 3E        ..>   addd 003e
C748 B3 00 50        ..P   subd 0050
C74B 25 0F           %.    bcs 0f
C74D F6 0B C0        ...   ldab 0bc0
C750 C4 04           ..    andb #04
C752 26 08           &.    bne 08
C754 CE 00 21        ..!   ldx #0021
C757 1D 00           ..    bclr add,x 00,x
C759 10              .     sba 
C75A 20 24            $    bra 24
C75C F6 0B C0        ...   ldab 0bc0
C75F C4 08           ..    andb #08
C761 27 03           '.    beq 03
C763 BD C5 D3        ...   jsr c5d3		;jump6
C766 FC 0B DD        ...   ldd 0bdd
C769 26 12           &.    bne 12
C76B FC 0B DF        ...   ldd 0bdf
C76E 26 0D           &.    bne 0d
C770 FC 0B E1        ...   ldd 0be1
C773 26 08           &.    bne 08
C775 CE 00 21        ..!   ldx #0021
C778 1D 00           ..    bclr add,x 00,x
C77A 10              .     sba 
C77B 20 03            .    bra 03
C77D BD C4 56        ..V   jsr c456		;jump47
C780 F6 00 15        ...   ldab 0015
C783 2D 08           -.    blt 08
C785 CE 00 21        ..!   ldx #0021
C788 1C 00           ..    bset add,x 00,x
C78A 08              .     inx 
C78B 20 06            .    bra 06
C78D CE 00 21        ..!   ldx #0021
C790 1D 00           ..    bclr add,x 00,x
C792 08              .     inx 
C793 39              9     rts 

;proc(a,b) var local
;local=0
;while b<>0
;  
;endwhile
;return local

C794 37              7     pshb 
C795 36              6     psha 
C796 34              4     des 		;stack: 0=local, 1=A, 2=B
C797 30              0     tsx 
C798 6F 00           o.    clr 00,x
loop:
C79A 30              0     tsx 
C79B E6 02           ..    ldab 02,x
C79D 27 1A           '.    beq exit	;c7b9
C79F 30              0     tsx 
C7A0 1F 02 01 0D     ....  brclr 02,x 01 skip	;c7b1
C7A4 30              0     tsx 
C7A5 E6 00           ..    ldab 00,x
C7A7 27 05           '.    beq else	;c7ae
C7A9 30              0     tsx 
C7AA 6A 00           j.    dec 00,x
C7AC 20 03            .    bra skip	;c7b1
else:
C7AE 30              0     tsx 
C7AF 6C 00           l.    inc 00,x
skip:
C7B1 30              0     tsx 
C7B2 E6 02           ..    ldab 02,x
C7B4 54              T     lsrb 
C7B5 E7 02           ..    stb 02,x
C7B7 20 E1            .    bra loop	;c79a
exit:
C7B9 30              0     tsx 
C7BA E6 00           ..    ldab 00,x
C7BC 4F              O     clra 
C7BD 38              8     pulx 
C7BE 31              1     ins 
C7BF 39              9     rts 

jump49:
C7C0 F6 10 2F        ../   ldab 102f		;sci data
C7C3 4F              O     clra 
C7C4 8D CE           ..    bsr c794
C7C6 83 00 00        ...   subd #0000
C7C9 27 05           '.    beq 05
C7CB F6 10 2C        ..,   ldab 102c
C7CE 2D 10           -.    blt 10
C7D0 F6 10 2F        ../   ldab 102f		;sci data
C7D3 4F              O     clra 
C7D4 8D BE           ..    bsr c794
C7D6 83 00 00        ...   subd #0000
C7D9 26 09           &.    bne 09
C7DB F6 10 2C        ..,   ldab 102c
C7DE 2D 04           -.    blt 04
C7E0 CC 00 01        ...   ldd #0001
C7E3 39              9     rts 

C7E4 5F              _     clrb 
C7E5 4F              O     clra 
C7E6 39              9     rts 


;this might send a byte to the serial port
jump37:
C7E7 37              7     pshb 
C7E8 36              6     psha 
C7E9 C6 32           .2    ldab #32
C7EB F7 00 31        ..1   stb 0031
C7EE 30              0     tsx 
C7EF E6 01           ..    ldab 01,x
C7F1 F7 00 43        ..C   stb 0043
C7F4 F6 10 2E        ...   ldab 102e	;SCI status
C7F7 2D 02           -.    blt 02
C7F9 20 F9            .    bra f9
C7FB 30              0     tsx 
C7FC E6 01           ..    ldab 01,x
C7FE 4F              O     clra 
C7FF 8D 93           ..    bsr dest 93
C801 83 00 00        ...   subd #0000
C804 27 08           '.    beq 08
C806 CE 10 2C        ..,   ldx #102c
C809 1C 00           ..    bset add,x 00,x
C80B 40              @     nega 
C80C 20 06            .    bra 06
C80E CE 10 2C        ..,   ldx #102c	;SCI control 1
C811 1D 00           ..    bclr add,x 00,x
C813 40              @     nega 
C814 30              0     tsx 
C815 E6 01           ..    ldab 01,x
C817 F7 10 2F        ../   stb 102f		;sci data
C81A 38              8     pulx 
C81B 39              9     rts 


C81C 34              4     des 
C81D F6 0B C0        ...   ldab 0bc0
C820 C4 01           ..    andb #01
C822 26 0A           &.    bne 0a
C824 F6 0B C0        ...   ldab 0bc0
C827 C4 04           ..    andb #04
C829 26 03           &.    bne 03
C82B 7E CB FF        ~..   jmp cbff		;goto44
C82E C6 96           ..    ldab #96
C830 F7 00 30        ..0   stb 0030
C833 F6 10 2E        ...   ldab 102e
C836 C4 02           ..    andb #02
C838 26 0B           &.    bne 0b
C83A F6 10 2F        ../   ldab 102f		;sci data
C83D F1 00 26        ..&   cmpb 0026
C840 26 03           &.    bne 03
C842 7E C9 1B        ~..   jmp c91b		;goto45
C845 F6 10 2F        ../   ldab 102f		;sci data
C848 C4 63           .c    andb #63
C84A C1 23           .#    cmpb #23
C84C 27 03           '.    beq 03
C84E 7E C8 E7        ~..   jmp c8e7		;goto46
C851 F6 00 1A        ...   ldab 001a
C854 C4 01           ..    andb #01
C856 26 03           &.    bne 03
C858 7E C8 E7        ~..   jmp c8e7		;goto46
C85B F6 00 21        ..!   ldab 0021
C85E C4 F8           ..    andb #f8
C860 37              7     pshb 
C861 F6 10 2F        ../   ldab 102f		;sci data
C864 C4 1C           ..    andb #1c
C866 54              T     lsrb 
C867 54              T     lsrb 
C868 30              0     tsx 
C869 EA 00           ..    orb 00,x
C86B 31              1     ins 
C86C F7 00 21        ..!   stb 0021
C86F F6 00 21        ..!   ldab 0021
C872 F4 0B D7        ...   andb 0bd7
C875 27 08           '.    beq 08
C877 CE 00 17        ...   ldx #0017
C87A 1C 00           ..    bset add,x 00,x
C87C 20 20                 bra 20
C87E 18              .     illegal 
C87F F6 00 17        ...   ldab 0017
C882 C4 20           .     andb #20
C884 27 11           '.    beq 11
C886 CE 00 17        ...   ldx #0017
C889 1D 00           ..    bclr add,x 00,x
C88B 20 C6            .    bra c6
C88D 04              .     lsrd 
C88E F7 0C DB        ...   stb 0cdb
C891 CE 0B C1        ...   ldx #0bc1
C894 1C 00           ..    bset add,x 00,x
C896 40              @     nega 
C897 F6 00 21        ..!   ldab 0021
C89A F4 0B D8        ...   andb 0bd8
C89D 27 08           '.    beq 08
C89F CE 00 17        ...   ldx #0017
C8A2 1C 00           ..    bset add,x 00,x
C8A4 40              @     nega 
C8A5 20 18            .    bra 18
C8A7 F6 00 17        ...   ldab 0017
C8AA C4 40           .@    andb #40
C8AC 27 11           '.    beq 11
C8AE CE 00 17        ...   ldx #0017
C8B1 1D 00           ..    bclr add,x 00,x
C8B3 40              @     nega 
C8B4 C6 04           ..    ldab #04
C8B6 F7 0C DA        ...   stb 0cda
C8B9 CE 0B C1        ...   ldx #0bc1
C8BC 1C 00           ..    bset add,x 00,x
C8BE 40              @     nega 
C8BF F6 00 21        ..!   ldab 0021
C8C2 F4 0B D9        ...   andb 0bd9
C8C5 27 08           '.    beq 08
C8C7 CE 00 17        ...   ldx #0017
C8CA 1C 00           ..    bset add,x 00,x
C8CC 80 20           .     suba #20
C8CE 16              .     tab 
C8CF F6 00 17        ...   ldab 0017
C8D2 2C 11           ,.    bge 11
C8D4 CE 00 17        ...   ldx #0017
C8D7 1D 00           ..    bclr add,x 00,x
C8D9 80 C6           ..    suba #c6
C8DB 04              .     lsrd 
C8DC F7 0C D9        ...   stb 0cd9
C8DF CE 0B C1        ...   ldx #0bc1
C8E2 1C 00           ..    bset add,x 00,x
C8E4 40              @     nega 
C8E5 20 31            1    bra 31
goto46:
C8E7 F6 10 2F        ../   ldab 102f		;sci data
C8EA F7 00 26        ..&   stb 0026
C8ED CE 00 1A        ...   ldx #001a
C8F0 1C 00           ..    bset add,x 00,x
C8F2 02              .     idiv 
C8F3 F6 00 1A        ...   ldab 001a
C8F6 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C8F9 F6 00 3D        ..=   ldab 003d
C8FC C4 04           ..    andb #04
C8FE 26 18           &.    bne 18
C900 CC 00 03        ...   ldd #0003
C903 BD E5 71        ..q   jsr e571		;jump13
C906 CE 00 1A        ...   ldx #001a
C909 1D 00           ..    bclr add,x 00,x
C90B 02              .     idiv 
C90C F6 00 1A        ...   ldab 001a
C90F F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
C912 7C 00 3D        |.=   inc 003d
C915 7E CE 46        ~.F   jmp ce46		;goto47
C918 7E CB DA        ~..   jmp cbda		;goto48
goto45:
C91B F6 00 08        ...   ldab 0008
C91E 26 03           &.    bne 03
C920 7E CA 5F        ~._   jmp ca5f		;goto49
C923 F6 00 17        ...   ldab 0017
C926 C4 04           ..    andb #04
C928 26 03           &.    bne 03
C92A 7E CA 5F        ~._   jmp ca5f		;goto49
C92D F6 00 17        ...   ldab 0017
C930 C4 08           ..    andb #08
C932 26 03           &.    bne 03
C934 7E CA 5F        ~._   jmp ca5f		;goto49
C937 F6 10 2F        ../   ldab 102f		;sci data
C93A C1 FF           ..    cmpb #ff
C93C 27 07           '.    beq 07
C93E F6 00 08        ...   ldab 0008
C941 C1 0E           ..    cmpb #0e
C943 25 12           %.    bcs 12
C945 CE 00 17        ...   ldx #0017
C948 1D 00           ..    bclr add,x 00,x
C94A 04              .     lsrd 
C94B 7F 00 08        ...   clr 0008
C94E BD CE 48        ..H   jsr ce48		;jump48
C951 7F 00 2C        ..,   clr 002c
C954 7E CA 5C        ~.\   jmp ca5c		;goto50
C957 F6 00 08        ...   ldab 0008
C95A 4F              O     clra 
C95B BD FE 16        ...   jsr fe16		;jump2
C95E 00              .     test 
C95F 01              .     nop 
C960 00              .     test 
C961 0C              .     clc 
C962 CA 59           .Y    orab #59
C964 C9 7E           .~    adcb #7e
C966 C9 87           ..    adcb #87
C968 C9 DF           ..    adcb #df
C96A C9 F3           ..    adcb #f3
C96C CA 07           ..    orab #07
C96E CA 1B           ..    orab #1b
C970 CA 23           .#    orab #23
C972 CA 2B           .+    orab #2b
C974 CA 33           .3    orab #33
C976 CA 3B           .;    orab #3b
C978 CA 43           .C    orab #43
C97A CA 4B           .K    orab #4b
C97C CA 53           .S    orab #53
C97E F6 10 2F        ../   ldab 102f		;sci data
C981 F7 0B C2        ...   stb 0bc2
C984 7E CA 59        ~.Y   jmp ca59		;goto51
C987 F6 10 2F        ../   ldab 102f		;sci data
C98A F7 0B E3        ...   stb 0be3
C98D F6 10 2F        ../   ldab 102f		;sci data
C990 C4 0F           ..    andb #0f
C992 C1 01           ..    cmpb #01
C994 26 07           &.    bne 07
C996 C6 02           ..    ldab #02
C998 F7 00 13        ...   stb 0013
C99B 20 1B            .    bra 1b
C99D C1 02           ..    cmpb #02
C99F 26 07           &.    bne 07
C9A1 C6 04           ..    ldab #04
C9A3 F7 00 13        ...   stb 0013
C9A6 20 10            .    bra 10
C9A8 C1 03           ..    cmpb #03
C9AA 26 07           &.    bne 07
C9AC C6 08           ..    ldab #08
C9AE F7 00 13        ...   stb 0013
C9B1 20 05            .    bra 05
C9B3 C6 01           ..    ldab #01
C9B5 F7 00 13        ...   stb 0013
C9B8 F6 10 2F        ../   ldab 102f		;sci data
C9BB C4 F0           ..    andb #f0
C9BD 54              T     lsrb 
C9BE 54              T     lsrb 
C9BF 54              T     lsrb 
C9C0 54              T     lsrb 
C9C1 F7 0B C3        ...   stb 0bc3
C9C4 F6 0B C2        ...   ldab 0bc2
C9C7 F7 0B AA        ...   stb 0baa
;;0baa^:=pow(10,0bc3^)	0bc3 is the top 4 bits from the serial port.
;while 0bc3^<>0 
;  obaa^:=0baa^*10
;  obc3^:=0bc3-1
;endwhile
C9CA F6 0B C3        ...   ldab 0bc3
C9CD 27 0E           '.    beq c9dd
C9CF F6 0B AA        ...   ldab 0baa
C9D2 86 0A           ..    ldaa #0a
C9D4 3D              =     mul 
C9D5 F7 0B AA        ...   stb 0baa
C9D8 7A 0B C3        z..   dec 0bc3
C9DB 20 ED            .    bra c9ca

C9DD 20 7A            z    bra 7a
C9DF F6 10 2F        ../   ldab 102f		;sci data
C9E2 F7 0B C4        ...   stb 0bc4
C9E5 F6 0B C4        ...   ldab 0bc4
C9E8 C1 80           ..    cmpb #80
C9EA 26 05           &.    bne 05
C9EC C6 0F           ..    ldab #0f
C9EE F7 0B C4        ...   stb 0bc4
C9F1 20 66            f    bra 66
C9F3 F6 10 2F        ../   ldab 102f		;sci data
C9F6 F7 0B C5        ...   stb 0bc5
C9F9 F6 0B C5        ...   ldab 0bc5
C9FC C1 80           ..    cmpb #80
C9FE 26 05           &.    bne 05
CA00 C6 0F           ..    ldab #0f
CA02 F7 0B C5        ...   stb 0bc5
CA05 20 52            R    bra 52
CA07 F6 10 2F        ../   ldab 102f		;sci data
CA0A F7 0B C6        ...   stb 0bc6
CA0D F6 0B C6        ...   ldab 0bc6
CA10 C1 80           ..    cmpb #80
CA12 26 05           &.    bne ca19
CA14 C6 0F           ..    ldab #0f
CA16 F7 0B C6        ...   stb 0bc6
CA19 20 3E            >    bra 3e
;!!!where does this lot get called?
CA1B F6 10 2F        ../   ldab 102f		;sci data
CA1E F7 0B C7        ...   stb 0bc7
CA21 20 36            6    bra 36
CA23 F6 10 2F        ../   ldab 102f		;sci data
CA26 F7 0B C8        ...   stb 0bc8
CA29 20 2E            .    bra 2e
CA2B F6 10 2F        ../   ldab 102f		;sci data
CA2E F7 0B C9        ...   stb 0bc9
CA31 20 26            &    bra 26
CA33 F6 10 2F        ../   ldab 102f		;sci data
CA36 F7 0B CA        ...   stb 0bca
CA39 20 1E            .    bra 1e
CA3B F6 10 2F        ../   ldab 102f		;sci data
CA3E F7 0B CB        ...   stb 0bcb
CA41 20 16            .    bra 16
CA43 F6 10 2F        ../   ldab 102f		;sci data
CA46 F7 0B CC        ...   stb 0bcc
CA49 20 0E            .    bra 0e
CA4B F6 10 2F        ../   ldab 102f		;sci data
CA4E F7 0B CD        ...   stb 0bcd
CA51 20 06            .    bra 06
CA53 F6 10 2F        ../   ldab 102f		;sci data
CA56 F7 0B CE        ...   stb 0bce
goto51:
CA59 7C 00 08        |..   inc 0008
goto50:
CA5C 7E CB DA        ~..   jmp cbda		;goto48
goto49:
CA5F F6 10 2F        ../   ldab 102f		;sci data
CA62 C4 EF           ..    andb #ef
CA64 C1 E2           ..    cmpb #e2
CA66 26 1A           &.    bne 1a
CA68 CE 00 17        ...   ldx #0017
CA6B 1C 00           ..    bset add,x 00,x
CA6D 08              .     inx 
CA6E CE 00 17        ...   ldx #0017
CA71 1C 00           ..    bset add,x 00,x
CA73 04              .     lsrd 
CA74 C6 01           ..    ldab #01
CA76 F7 00 08        ...   stb 0008
CA79 CE 0B C1        ...   ldx #0bc1
CA7C 1D 00           ..    bclr add,x 00,x
CA7E 01              .     nop 
CA7F 7E CB DA        ~..   jmp cbda		;goto48
CA82 F6 10 2F        ../   ldab 102f		;sci data
CA85 C4 02           ..    andb #02
CA87 27 03           '.    beq 03
CA89 7E CB 6C        ~.l   jmp cb6c		;goto52
CA8C F6 10 2F        ../   ldab 102f		;sci data
CA8F 2C 38           ,8    bge 38
CA91 F6 00 17        ...   ldab 0017
CA94 C4 08           ..    andb #08
CA96 26 31           &1    bne 31
CA98 F6 10 2F        ../   ldab 102f		;sci data
CA9B C4 78           .x    andb #78
CA9D 54              T     lsrb 
CA9E 54              T     lsrb 
CA9F 54              T     lsrb 
CAA0 4F              O     clra 
CAA1 C3 0B C7        ...   addd #0bc7
CAA4 8F              .     xgdx 
CAA5 E6 00           ..    ldab 00,x
CAA7 4F              O     clra 
CAA8 37              7     pshb 
CAA9 36              6     psha 
CAAA F6 0B AA        ...   ldab 0baa
CAAD 4F              O     clra 
CAAE BD FB BA        ...   jsr fbba		;jump38
CAB1 F3 00 40        ..@   addd 0040
CAB4 FD 00 40        ..@   stad 0040
CAB7 CE 00 1B        ...   ldx #001b
CABA 1C 00           ..    bset add,x 00,x
CABC 20 CE            .    bra ce
CABE 00              .     test 
CABF 1B              .     aba 
CAC0 1D 00           ..    bclr add,x 00,x
CAC2 40              @     nega 
CAC3 7F 00 25        ..%   clr 0025
CAC6 7E CB 64        ~.d   jmp cb64		;goto53
CAC9 F6 00 17        ...   ldab 0017
CACC C4 08           ..    andb #08
CACE 26 4A           &J    bne 4a
CAD0 F6 10 2F        ../   ldab 102f		;sci data
CAD3 C4 60           .`    andb #60
CAD5 54              T     lsrb 
CAD6 54              T     lsrb 
CAD7 54              T     lsrb 
CAD8 4F              O     clra 
CAD9 C3 0B C7        ...   addd #0bc7
CADC 8F              .     xgdx 
CADD E6 00           ..    ldab 00,x
CADF 4F              O     clra 
CAE0 37              7     pshb 
CAE1 36              6     psha 
CAE2 F6 0B AA        ...   ldab 0baa
CAE5 4F              O     clra 
CAE6 BD FB BA        ...   jsr fbba		;jump38
CAE9 FD 0B BD        ...   stad 0bbd
CAEC FC 0B BD        ...   ldd 0bbd
CAEF F3 00 40        ..@   addd 0040
CAF2 FD 00 40        ..@   stad 0040
CAF5 F6 00 21        ..!   ldab 0021
CAF8 C4 F8           ..    andb #f8
CAFA 37              7     pshb 
CAFB F6 10 2F        ../   ldab 102f		;sci data
CAFE C4 1C           ..    andb #1c
CB00 54              T     lsrb 
CB01 54              T     lsrb 
CB02 30              0     tsx 
CB03 EA 00           ..    orb 00,x
CB05 31              1     ins 
CB06 F7 00 21        ..!   stb 0021
CB09 F6 10 2F        ../   ldab 102f		;sci data
CB0C F7 0B BC        ...   stb 0bbc
CB0F CE 00 1B        ...   ldx #001b
CB12 1D 00           ..    bclr add,x 00,x
CB14 40              @     nega 
CB15 7F 00 25        ..%   clr 0025
CB18 20 4A            J    bra 4a
CB1A F6 10 2F        ../   ldab 102f		;sci data
CB1D C4 E0           ..    andb #e0
CB1F 54              T     lsrb 
CB20 54              T     lsrb 
CB21 54              T     lsrb 
CB22 54              T     lsrb 
CB23 54              T     lsrb 
CB24 4F              O     clra 
CB25 C3 0B C7        ...   addd #0bc7
CB28 8F              .     xgdx 
CB29 E6 00           ..    ldab 00,x
CB2B 4F              O     clra 
CB2C 37              7     pshb 
CB2D 36              6     psha 
CB2E F6 0B AA        ...   ldab 0baa
CB31 4F              O     clra 
CB32 BD FB BA        ...   jsr fbba		;jump38
CB35 FD 0B BD        ...   stad 0bbd
CB38 FC 0B BD        ...   ldd 0bbd
CB3B F3 00 40        ..@   addd 0040
CB3E FD 00 40        ..@   stad 0040
CB41 F6 00 21        ..!   ldab 0021
CB44 C4 F8           ..    andb #f8
CB46 37              7     pshb 
CB47 F6 10 2F        ../   ldab 102f		;sci data
CB4A C4 1C           ..    andb #1c
CB4C 54              T     lsrb 
CB4D 54              T     lsrb 
CB4E 30              0     tsx 
CB4F EA 00           ..    orb 00,x
CB51 31              1     ins 
CB52 F7 00 21        ..!   stb 0021
CB55 F6 10 2F        ../   ldab 102f		;sci data
CB58 F7 0B BC        ...   stb 0bbc
CB5B CE 00 1B        ...   ldx #001b
CB5E 1D 00           ..    bclr add,x 00,x
CB60 40              @     nega 
CB61 7F 00 25        ..%   clr 0025
goto53:
CB64 7F 00 2C        ..,   clr 002c
CB67 7F 00 3A        ..:   clr 003a
CB6A 20 6E            n    bra 6e
goto52:
CB6C F6 10 2F        ../   ldab 102f		;sci data
CB6F C4 03           ..    andb #03
CB71 C1 02           ..    cmpb #02
CB73 26 0F           &.    bne 0f
CB75 F6 00 21        ..!   ldab 0021
CB78 C4 08           ..    andb #08
CB7A 27 08           '.    beq 08
CB7C CE 00 21        ..!   ldx #0021
CB7F 1C 00           ..    bset add,x 00,x
CB81 10              .     sba 
CB82 20 56            V    bra 56
CB84 F6 10 2F        ../   ldab 102f		;sci data
CB87 C4 EF           ..    andb #ef
CB89 C1 E3           ..    cmpb #e3
CB8B 26 08           &.    bne 08
CB8D CE 0B C1        ...   ldx #0bc1
CB90 1D 00           ..    bclr add,x 00,x
CB92 02              .     idiv 
CB93 20 45            E    bra 45
CB95 F6 10 2F        ../   ldab 102f		;sci data
CB98 C4 EF           ..    andb #ef
CB9A C1 63           .c    cmpb #63
CB9C 26 1C           &.    bne 1c
CB9E CE 0B C1        ...   ldx #0bc1
CBA1 1D 00           ..    bclr add,x 00,x
CBA3 01              .     nop 
CBA4 F6 0B C0        ...   ldab 0bc0
CBA7 C4 01           ..    andb #01
CBA9 27 0D           '.    beq 0d
CBAB F6 0B C0        ...   ldab 0bc0
CBAE C4 04           ..    andb #04
CBB0 27 06           '.    beq 06
CBB2 CE 0B C1        ...   ldx #0bc1
CBB5 1D 00           ..    bclr add,x 00,x
CBB7 02              .     idiv 
CBB8 20 20                 bra 20
CBBA F6 10 2F        ../   ldab 102f		;sci data
CBBD C4 EF           ..    andb #ef
CBBF C1 67           .g    cmpb #67
CBC1 26 08           &.    bne 08
CBC3 CE 0B C1        ...   ldx #0bc1
CBC6 1C 00           ..    bset add,x 00,x
CBC8 20 20                 bra 20
CBCA 0F              .     sei 
CBCB F6 10 2F        ../   ldab 102f		;sci data
CBCE C4 EF           ..    andb #ef
CBD0 C1 27           .'    cmpb #27
CBD2 26 06           &.    bne 06
CBD4 CE 0B C1        ...   ldx #0bc1
CBD7 1C 00           ..    bset add,x 00,x
CBD9 10              .     sba 
goto48:
CBDA CE 00 1A        ...   ldx #001a
CBDD 1C 00           ..    bset add,x 00,x
CBDF 02              .     idiv 
CBE0 F6 00 1A        ...   ldab 001a
CBE3 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
CBE6 F6 00 17        ...   ldab 0017
CBE9 C4 04           ..    andb #04
CBEB 26 05           &.    bne 05
CBED C6 06           ..    ldab #06
CBEF F7 00 07        ...   stb 0007
CBF2 F6 00 26        ..&   ldab 0026
CBF5 53              S     comb 
CBF6 F7 00 26        ..&   stb 0026
CBF9 7F 00 3D        ..=   clr 003d
CBFC 7E CE 46        ~.F   jmp ce46		;goto47
goto44:
CBFF F6 0B C0        ...   ldab 0bc0
CC02 C4 02           ..    andb #02
CC04 26 03           &.    bne 03
CC06 7E CE 3B        ~.;   jmp ce3b		;goto54
CC09 F6 10 2F        ../   ldab 102f		;sci data
CC0C C4 E0           ..    andb #e0
CC0E C1 20           .     cmpb #20
CC10 27 03           '.    beq 03
CC12 7E CE 3B        ~.;   jmp ce3b		;goto54
CC15 C6 32           .2    ldab #32
CC17 F7 00 2F        ../   stb 002f
CC1A CE 00 1B        ...   ldx #001b
CC1D 1C 00           ..    bset add,x 00,x
CC1F 02              .     idiv 
CC20 F6 10 2E        ...   ldab 102e
CC23 C4 02           ..    andb #02
CC25 26 08           &.    bne 08
CC27 BD C7 C0        ...   jsr c7c0		;jump49
CC2A 83 00 00        ...   subd #0000
CC2D 26 0F           &.    bne 0f
CC2F F6 10 2F        ../   ldab 102f		;sci data
CC32 30              0     tsx 
CC33 E7 00           ..    stb 00,x
CC35 CC 00 FF        ...   ldd #00ff
CC38 BD C7 E7        ...   jsr c7e7		;jump37
CC3B 7E CE 39        ~.9   jmp ce39		;goto55
CC3E F6 10 2F        ../   ldab 102f		;sci data
CC41 C1 3F           .?    cmpb #3f
CC43 26 0A           &.    bne 0a
CC45 F6 00 43        ..C   ldab 0043
CC48 4F              O     clra 
CC49 BD C7 E7        ...   jsr c7e7		;jump37
CC4C 7E CE 39        ~.9   jmp ce39		;goto55
CC4F F6 00 08        ...   ldab 0008
CC52 26 03           &.    bne 03
CC54 7E CD 94        ~..   jmp cd94		;goto56
CC57 F6 10 2F        ../   ldab 102f		;sci data
CC5A C4 F0           ..    andb #f0
CC5C C1 20           .     cmpb #20
CC5E 27 03           '.    beq 03
CC60 7E CD 94        ~..   jmp cd94		;goto56
CC63 F6 10 2F        ../   ldab 102f		;sci data
CC66 C4 0F           ..    andb #0f
CC68 30              0     tsx 
CC69 E7 00           ..    stb 00,x
CC6B F6 00 08        ...   ldab 0008
CC6E 4F              O     clra 
CC6F BD FE 16        ...   jsr fe16		;jump2
CC72 00              .     test 
CC73 01              .     nop 
CC74 00              .     test 
CC75 07              .     tpa 
CC76 CD              .     illegal 
CC77 7A CC 88        z..   dec cc88
CC7A CC A2 CC        ...   ldd #a2cc
CC7D CA CC           ..    orab #cc
CC7F EE CD           ..    ldx cd,x
CC81 19              .     daa 
CC82 CD              .     illegal 
CC83 21 CD           !.    brn cd
CC85 30              0     tsx 
CC86 CD              .     illegal 
CC87 38              8     pulx 
CC88 F6 00 17        ...   ldab 0017
CC8B C4 01           ..    andb #01
CC8D 27 09           '.    beq 09
CC8F 30              0     tsx 
CC90 E6 00           ..    ldab 00,x
CC92 4F              O     clra 
CC93 FD 00 76        ..v   stad 0076
CC96 20 07            .    bra 07
CC98 30              0     tsx 
CC99 E6 00           ..    ldab 00,x
CC9B 4F              O     clra 
CC9C FD 00 40        ..@   stad 0040
CC9F 7E CD 7A        ~.z   jmp cd7a		;goto57
CCA2 F6 00 17        ...   ldab 0017
CCA5 C4 01           ..    andb #01
CCA7 27 10           '.    beq 10
CCA9 30              0     tsx 
CCAA E6 00           ..    ldab 00,x
CCAC 4F              O     clra 
CCAD 05              .     asld 
CCAE 05              .     asld 
CCAF 05              .     asld 
CCB0 05              .     asld 
CCB1 F3 00 76        ..v   addd 0076
CCB4 FD 00 76        ..v   stad 0076
CCB7 20 0E            .    bra 0e
CCB9 30              0     tsx 
CCBA E6 00           ..    ldab 00,x
CCBC 4F              O     clra 
CCBD 05              .     asld 
CCBE 05              .     asld 
CCBF 05              .     asld 
CCC0 05              .     asld 
CCC1 F3 00 40        ..@   addd 0040
CCC4 FD 00 40        ..@   stad 0040
CCC7 7E CD 7A        ~.z   jmp cd7a		;goto57
CCCA F6 00 17        ...   ldab 0017
CCCD C4 01           ..    andb #01
CCCF 27 0E           '.    beq 0e
CCD1 30              0     tsx 
CCD2 E6 00           ..    ldab 00,x
CCD4 4F              O     clra 
CCD5 17              .     tba 
CCD6 5F              _     clrb 
CCD7 F3 00 76        ..v   addd 0076
CCDA FD 00 76        ..v   stad 0076
CCDD 20 0C            .    bra 0c
CCDF 30              0     tsx 
CCE0 E6 00           ..    ldab 00,x
CCE2 4F              O     clra 
CCE3 17              .     tba 
CCE4 5F              _     clrb 
CCE5 F3 00 40        ..@   addd 0040
CCE8 FD 00 40        ..@   stad 0040
CCEB 7E CD 7A        ~.z   jmp cd7a		;goto57
CCEE F6 00 17        ...   ldab 0017
CCF1 C4 01           ..    andb #01
CCF3 27 12           '.    beq 12
CCF5 30              0     tsx 
CCF6 E6 00           ..    ldab 00,x
CCF8 4F              O     clra 
CCF9 17              .     tba 
CCFA 48              H     asla 
CCFB 48              H     asla 
CCFC 48              H     asla 
CCFD 48              H     asla 
CCFE 5F              _     clrb 
CCFF F3 00 76        ..v   addd 0076
CD02 FD 00 76        ..v   stad 0076
CD05 20 10            .    bra 10
CD07 30              0     tsx 
CD08 E6 00           ..    ldab 00,x
CD0A 4F              O     clra 
CD0B 17              .     tba 
CD0C 48              H     asla 
CD0D 48              H     asla 
CD0E 48              H     asla 
CD0F 48              H     asla 
CD10 5F              _     clrb 
CD11 F3 00 40        ..@   addd 0040
CD14 FD 00 40        ..@   stad 0040
CD17 20 61            a    bra 61
CD19 30              0     tsx 
CD1A E6 00           ..    ldab 00,x
CD1C F7 0B AA        ...   stb 0baa
CD1F 20 59            Y    bra 59
CD21 30              0     tsx 
CD22 E6 00           ..    ldab 00,x
CD24 58              X     aslb 
CD25 58              X     aslb 
CD26 58              X     aslb 
CD27 58              X     aslb 
CD28 FB 0B AA        ...   addb 0baa
CD2B F7 0B AA        ...   stb 0baa
CD2E 20 4A            J    bra 4a
CD30 30              0     tsx 
CD31 E6 00           ..    ldab 00,x
CD33 F7 00 13        ...   stb 0013
CD36 20 42            B    bra 42
CD38 F6 10 2F        ../   ldab 102f		;sci data
CD3B C4 01           ..    andb #01
CD3D 27 08           '.    beq 08
CD3F CE 00 21        ..!   ldx #0021
CD42 1C 00           ..    bset add,x 00,x
CD44 40              @     nega 
CD45 20 06            .    bra 06
CD47 CE 00 21        ..!   ldx #0021
CD4A 1D 00           ..    bclr add,x 00,x
CD4C 40              @     nega 
CD4D F6 00 17        ...   ldab 0017
CD50 C4 01           ..    andb #01
CD52 27 17           '.    beq 17
CD54 F6 0B AA        ...   ldab 0baa
CD57 4F              O     clra 
CD58 37              7     pshb 
CD59 36              6     psha 
CD5A FC 00 76        ..v   ldd 0076
CD5D BD FB BA        ...   jsr fbba		;jump38
CD60 FD 00 76        ..v   stad 0076
CD63 CE 00 17        ...   ldx #0017
CD66 1D 00           ..    bclr add,x 00,x
CD68 01              .     nop 
CD69 20 0F            .    bra 0f
CD6B F6 0B AA        ...   ldab 0baa
CD6E 4F              O     clra 
CD6F 37              7     pshb 
CD70 36              6     psha 
CD71 FC 00 40        ..@   ldd 0040
CD74 BD FB BA        ...   jsr fbba		;jump38
CD77 FD 00 40        ..@   stad 0040
goto57:
CD7A F6 00 08        ...   ldab 0008
CD7D C1 09           ..    cmpb #09
CD7F 26 08           &.    bne 08
CD81 7F 00 2C        ..,   clr 002c
CD84 7F 00 08        ...   clr 0008
CD87 20 03            .    bra 03
CD89 7C 00 08        |..   inc 0008
CD8C 5F              _     clrb 
CD8D 4F              O     clra 
CD8E BD C7 E7        ...   jsr c7e7		;jump37
CD91 7E CE 39        ~.9   jmp ce39		;goto55
goto56:
CD94 F6 10 2F        ../   ldab 102f		;sci data
CD97 4F              O     clra 
CD98 BD FE 32        ..2   jsr fe32		;jump25
CD9B CD              .     illegal 
CD9C B1 00 31        ..1   cmpa 0031
CD9F CD              .     illegal 
CDA0 DB 00 01        ...   addb 00
CDA3 CE 20 00        . .   ldx #2000
CDA6 01              .     nop 
CDA7 CE 28 00        .(.   ldx #2800
CDAA 05              .     asld 
CDAB CE 34 FF        .4.   ldx #34ff
CDAE C8 00           ..    eorb #00
CDB0 00              .     test 
CDB1 F6 00 21        ..!   ldab 0021
CDB4 2C 08           ,.    bge 08
CDB6 CC 00 80        ...   ldd #0080
CDB9 BD C7 E7        ...   jsr c7e7		;jump37
CDBC 20 1B            .    bra 1b
CDBE 5F              _     clrb 
CDBF 4F              O     clra 
CDC0 BD C7 E7        ...   jsr c7e7		;jump37
CDC3 F6 00 1F        ...   ldab 001f
CDC6 C4 08           ..    andb #08
CDC8 27 0C           '.    beq 0c
CDCA F6 0B C0        ...   ldab 0bc0
CDCD C4 10           ..    andb #10
CDCF 27 05           '.    beq 05
CDD1 7C 00 75        |.u   inc 0075
CDD4 20 03            .    bra 03
CDD6 7F 00 75        ..u   clr 0075
CDD9 20 5E            ^    bra 5e
CDDB F6 00 1D        ...   ldab 001d
CDDE C4 02           ..    andb #02
CDE0 27 07           '.    beq 07
CDE2 F6 00 19        ...   ldab 0019
CDE5 C4 08           ..    andb #08
CDE7 26 19           &.    bne 19
CDE9 CC 00 FE        ...   ldd #00fe
CDEC BD C7 E7        ...   jsr c7e7		;jump37
CDEF F6 00 1F        ...   ldab 001f
CDF2 C4 08           ..    andb #08
CDF4 27 0A           '.    beq 0a
CDF6 F6 0B C0        ...   ldab 0bc0
CDF9 C4 10           ..    andb #10
CDFB 27 03           '.    beq 03
CDFD 7C 00 75        |.u   inc 0075
CE00 20 1C            .    bra 1c
CE02 FC 00 44        ..D   ldd 0044
CE05 37              7     pshb 
CE06 36              6     psha 
CE07 F6 0B AA        ...   ldab 0baa
CE0A 4F              O     clra 
CE0B 38              8     pulx 
CE0C 8F              .     xgdx 
CE0D 02              .     idiv 
CE0E 8F              .     xgdx 
CE0F BD C7 E7        ...   jsr c7e7		;jump37
CE12 CE 00 19        ...   ldx #0019
CE15 1D 00           ..    bclr add,x 00,x
CE17 08              .     inx 
CE18 CE 00 1F        ...   ldx #001f
CE1B 1C 00           ..    bset add,x 00,x
CE1D 08              .     inx 
CE1E 20 19            .    bra 19
CE20 CE 00 1D        ...   ldx #001d
CE23 1D 00           ..    bclr add,x 00,x
CE25 02              .     idiv 
CE26 20 11            .    bra 11
CE28 C6 01           ..    ldab #01
CE2A F7 00 08        ...   stb 0008
CE2D 5F              _     clrb 
CE2E 4F              O     clra 
CE2F BD C7 E7        ...   jsr c7e7		;jump37
CE32 20 05            .    bra 05
CE34 5F              _     clrb 
CE35 4F              O     clra 
CE36 BD C7 E7        ...   jsr c7e7		;jump37
goto55:
CE39 20 0B            .    bra 0b
goto54:
CE3B F6 10 2E        ...   ldab 102e
CE3E 30              0     tsx 
CE3F E7 00           ..    stb 00,x
CE41 F6 10 2F        ../   ldab 102f		;sci data
CE44 E7 00           ..    stb 00,x
goto47:
CE46 31              1     ins 
CE47 3B              ;     rti 

jump48:
CE48 F6 0B C4        ...   ldab 0bc4
CE4B F1 0B C5        ...   cmpb 0bc5
CE4E 25 53           %S    bcs 53
CE50 F6 0B C5        ...   ldab 0bc5
CE53 F1 0B C6        ...   cmpb 0bc6
CE56 25 4B           %K    bcs 4b
CE58 F6 0B C4        ...   ldab 0bc4
CE5B 4F              O     clra 
CE5C C3 0B C7        ...   addd #0bc7
CE5F 8F              .     xgdx 
CE60 E6 00           ..    ldab 00,x
CE62 4F              O     clra 
CE63 FD 0B DD        ...   stad 0bdd
CE66 F6 0B C5        ...   ldab 0bc5
CE69 4F              O     clra 
CE6A C3 0B C7        ...   addd #0bc7
CE6D 8F              .     xgdx 
CE6E E6 00           ..    ldab 00,x
CE70 4F              O     clra 
CE71 FD 0B DF        ...   stad 0bdf
CE74 F6 0B C6        ...   ldab 0bc6
CE77 4F              O     clra 
CE78 C3 0B C7        ...   addd #0bc7
CE7B 8F              .     xgdx 
CE7C E6 00           ..    ldab 00,x
CE7E 4F              O     clra 
CE7F FD 0B E1        ...   stad 0be1
CE82 C6 01           ..    ldab #01
CE84 F7 0B D7        ...   stb 0bd7
CE87 C6 04           ..    ldab #04
CE89 F7 0B D8        ...   stb 0bd8
CE8C C6 02           ..    ldab #02
CE8E F7 0B D9        ...   stb 0bd9
CE91 C6 20           .     ldab #20
CE93 F7 0B DA        ...   stb 0bda
CE96 C6 08           ..    ldab #08
CE98 F7 0B DB        ...   stb 0bdb
CE9B C6 10           ..    ldab #10
CE9D F7 0B DC        ...   stb 0bdc
CEA0 7E D0 66        ~.f   jmp d066		;goto58
CEA3 F6 0B C4        ...   ldab 0bc4
CEA6 F1 0B C6        ...   cmpb 0bc6
CEA9 25 53           %S    bcs 53
CEAB F6 0B C6        ...   ldab 0bc6
CEAE F1 0B C5        ...   cmpb 0bc5
CEB1 25 4B           %K    bcs 4b
CEB3 F6 0B C4        ...   ldab 0bc4
CEB6 4F              O     clra 
CEB7 C3 0B C7        ...   addd #0bc7
CEBA 8F              .     xgdx 
CEBB E6 00           ..    ldab 00,x
CEBD 4F              O     clra 
CEBE FD 0B DD        ...   stad 0bdd
CEC1 F6 0B C6        ...   ldab 0bc6
CEC4 4F              O     clra 
CEC5 C3 0B C7        ...   addd #0bc7
CEC8 8F              .     xgdx 
CEC9 E6 00           ..    ldab 00,x
CECB 4F              O     clra 
CECC FD 0B DF        ...   stad 0bdf
CECF F6 0B C5        ...   ldab 0bc5
CED2 4F              O     clra 
CED3 C3 0B C7        ...   addd #0bc7
CED6 8F              .     xgdx 
CED7 E6 00           ..    ldab 00,x
CED9 4F              O     clra 
CEDA FD 0B E1        ...   stad 0be1
CEDD C6 01           ..    ldab #01
CEDF F7 0B D7        ...   stb 0bd7
CEE2 C6 02           ..    ldab #02
CEE4 F7 0B D8        ...   stb 0bd8
CEE7 C6 04           ..    ldab #04
CEE9 F7 0B D9        ...   stb 0bd9
CEEC C6 20           .     ldab #20
CEEE F7 0B DA        ...   stb 0bda
CEF1 C6 10           ..    ldab #10
CEF3 F7 0B DB        ...   stb 0bdb
CEF6 C6 08           ..    ldab #08
CEF8 F7 0B DC        ...   stb 0bdc
CEFB 7E D0 66        ~.f   jmp d066		;goto58
CEFE F6 0B C5        ...   ldab 0bc5
CF01 F1 0B C4        ...   cmpb 0bc4
CF04 25 53           %S    bcs 53
CF06 F6 0B C4        ...   ldab 0bc4
CF09 F1 0B C6        ...   cmpb 0bc6
CF0C 25 4B           %K    bcs 4b
CF0E F6 0B C5        ...   ldab 0bc5
CF11 4F              O     clra 
CF12 C3 0B C7        ...   addd #0bc7
CF15 8F              .     xgdx 
CF16 E6 00           ..    ldab 00,x
CF18 4F              O     clra 
CF19 FD 0B DD        ...   stad 0bdd
CF1C F6 0B C4        ...   ldab 0bc4
CF1F 4F              O     clra 
CF20 C3 0B C7        ...   addd #0bc7
CF23 8F              .     xgdx 
CF24 E6 00           ..    ldab 00,x
CF26 4F              O     clra 
CF27 FD 0B DF        ...   stad 0bdf
CF2A F6 0B C6        ...   ldab 0bc6
CF2D 4F              O     clra 
CF2E C3 0B C7        ...   addd #0bc7
CF31 8F              .     xgdx 
CF32 E6 00           ..    ldab 00,x
CF34 4F              O     clra 
CF35 FD 0B E1        ...   stad 0be1
CF38 C6 04           ..    ldab #04
CF3A F7 0B D7        ...   stb 0bd7
CF3D C6 01           ..    ldab #01
CF3F F7 0B D8        ...   stb 0bd8
CF42 C6 02           ..    ldab #02
CF44 F7 0B D9        ...   stb 0bd9
CF47 C6 08           ..    ldab #08
CF49 F7 0B DA        ...   stb 0bda
CF4C C6 20           .     ldab #20
CF4E F7 0B DB        ...   stb 0bdb
CF51 C6 10           ..    ldab #10
CF53 F7 0B DC        ...   stb 0bdc
CF56 7E D0 66        ~.f   jmp d066		;goto58
CF59 F6 0B C5        ...   ldab 0bc5
CF5C F1 0B C6        ...   cmpb 0bc6
CF5F 25 53           %S    bcs 53
CF61 F6 0B C6        ...   ldab 0bc6
CF64 F1 0B C4        ...   cmpb 0bc4
CF67 25 4B           %K    bcs 4b
CF69 F6 0B C5        ...   ldab 0bc5
CF6C 4F              O     clra 
CF6D C3 0B C7        ...   addd #0bc7
CF70 8F              .     xgdx 
CF71 E6 00           ..    ldab 00,x
CF73 4F              O     clra 
CF74 FD 0B DD        ...   stad 0bdd
CF77 F6 0B C6        ...   ldab 0bc6
CF7A 4F              O     clra 
CF7B C3 0B C7        ...   addd #0bc7
CF7E 8F              .     xgdx 
CF7F E6 00           ..    ldab 00,x
CF81 4F              O     clra 
CF82 FD 0B DF        ...   stad 0bdf
CF85 F6 0B C4        ...   ldab 0bc4
CF88 4F              O     clra 
CF89 C3 0B C7        ...   addd #0bc7
CF8C 8F              .     xgdx 
CF8D E6 00           ..    ldab 00,x
CF8F 4F              O     clra 
CF90 FD 0B E1        ...   stad 0be1
CF93 C6 04           ..    ldab #04
CF95 F7 0B D7        ...   stb 0bd7
CF98 C6 02           ..    ldab #02
CF9A F7 0B D8        ...   stb 0bd8
CF9D C6 01           ..    ldab #01
CF9F F7 0B D9        ...   stb 0bd9
CFA2 C6 08           ..    ldab #08
CFA4 F7 0B DA        ...   stb 0bda
CFA7 C6 10           ..    ldab #10
CFA9 F7 0B DB        ...   stb 0bdb
CFAC C6 20           .     ldab #20
CFAE F7 0B DC        ...   stb 0bdc
CFB1 7E D0 66        ~.f   jmp d066		;goto58
CFB4 F6 0B C6        ...   ldab 0bc6
CFB7 F1 0B C4        ...   cmpb 0bc4
CFBA 25 52           %R    bcs 52
CFBC F6 0B C4        ...   ldab 0bc4
CFBF F1 0B C5        ...   cmpb 0bc5
CFC2 25 4A           %J    bcs 4a
CFC4 F6 0B C6        ...   ldab 0bc6
CFC7 4F              O     clra 
CFC8 C3 0B C7        ...   addd #0bc7
CFCB 8F              .     xgdx 
CFCC E6 00           ..    ldab 00,x
CFCE 4F              O     clra 
CFCF FD 0B DD        ...   stad 0bdd
CFD2 F6 0B C4        ...   ldab 0bc4
CFD5 4F              O     clra 
CFD6 C3 0B C7        ...   addd #0bc7
CFD9 8F              .     xgdx 
CFDA E6 00           ..    ldab 00,x
CFDC 4F              O     clra 
CFDD FD 0B DF        ...   stad 0bdf
CFE0 F6 0B C5        ...   ldab 0bc5
CFE3 4F              O     clra 
CFE4 C3 0B C7        ...   addd #0bc7
CFE7 8F              .     xgdx 
CFE8 E6 00           ..    ldab 00,x
CFEA 4F              O     clra 
CFEB FD 0B E1        ...   stad 0be1
CFEE C6 02           ..    ldab #02
CFF0 F7 0B D7        ...   stb 0bd7
CFF3 C6 01           ..    ldab #01
CFF5 F7 0B D8        ...   stb 0bd8
CFF8 C6 04           ..    ldab #04
CFFA F7 0B D9        ...   stb 0bd9
CFFD C6 10           ..    ldab #10
CFFF F7 0B DA        ...   stb 0bda
D002 C6 20           .     ldab #20
D004 F7 0B DB        ...   stb 0bdb
D007 C6 08           ..    ldab #08
D009 F7 0B DC        ...   stb 0bdc
D00C 20 58            X    bra 58
D00E F6 0B C6        ...   ldab 0bc6
D011 F1 0B C5        ...   cmpb 0bc5
D014 25 50           %P    bcs 50
D016 F6 0B C5        ...   ldab 0bc5
D019 F1 0B C4        ...   cmpb 0bc4
D01C 25 48           %H    bcs 48
D01E F6 0B C6        ...   ldab 0bc6
D021 4F              O     clra 
D022 C3 0B C7        ...   addd #0bc7
D025 8F              .     xgdx 
D026 E6 00           ..    ldab 00,x
D028 4F              O     clra 
D029 FD 0B DD        ...   stad 0bdd
D02C F6 0B C5        ...   ldab 0bc5
D02F 4F              O     clra 
D030 C3 0B C7        ...   addd #0bc7
D033 8F              .     xgdx 
D034 E6 00           ..    ldab 00,x
D036 4F              O     clra 
D037 FD 0B DF        ...   stad 0bdf
D03A F6 0B C4        ...   ldab 0bc4
D03D 4F              O     clra 
D03E C3 0B C7        ...   addd #0bc7
D041 8F              .     xgdx 
D042 E6 00           ..    ldab 00,x
D044 4F              O     clra 
D045 FD 0B E1        ...   stad 0be1
D048 C6 02           ..    ldab #02
D04A F7 0B D7        ...   stb 0bd7
D04D C6 04           ..    ldab #04
D04F F7 0B D8        ...   stb 0bd8
D052 C6 01           ..    ldab #01
D054 F7 0B D9        ...   stb 0bd9
D057 C6 10           ..    ldab #10
D059 F7 0B DA        ...   stb 0bda
D05C C6 08           ..    ldab #08
D05E F7 0B DB        ...   stb 0bdb
D061 C6 20           .     ldab #20
D063 F7 0B DC        ...   stb 0bdc
goto58:
D066 F6 0B AA        ...   ldab 0baa
D069 4F              O     clra 
D06A 37              7     pshb 
D06B 36              6     psha 
D06C FC 0B DD        ...   ldd 0bdd
D06F BD FB BA        ...   jsr fbba		;jump38
D072 FD 0B DD        ...   stad 0bdd
D075 F6 0B AA        ...   ldab 0baa
D078 4F              O     clra 
D079 37              7     pshb 
D07A 36              6     psha 
D07B FC 0B DF        ...   ldd 0bdf
D07E BD FB BA        ...   jsr fbba		;jump38
D081 FD 0B DF        ...   stad 0bdf
D084 F6 0B AA        ...   ldab 0baa
D087 4F              O     clra 
D088 37              7     pshb 
D089 36              6     psha 
D08A FC 0B E1        ...   ldd 0be1
D08D BD FB BA        ...   jsr fbba		;jump38
D090 FD 0B E1        ...   stad 0be1
D093 39              9     rts 

jump66:
D094 34              4     des 
D095 30              0     tsx 
D096 6F 00           o.    clr 00,x
D098 30              0     tsx 
D099 E6 00           ..    ldab 00,x
D09B C1 10           ..    cmpb #10
D09D 24 1B           $.    bcc 1b
D09F 30              0     tsx 
D0A0 E6 00           ..    ldab 00,x
D0A2 4F              O     clra 
D0A3 C3 B0 C3        ...   addd #b0c3
D0A6 8F              .     xgdx 
D0A7 E6 00           ..    ldab 00,x
D0A9 37              7     pshb 
D0AA 30              0     tsx 
D0AB E6 01           ..    ldab 01,x
D0AD 4F              O     clra 
D0AE C3 0B C7        ...   addd #0bc7
D0B1 8F              .     xgdx 
D0B2 33              3     pulb 
D0B3 E7 00           ..    stb 00,x
D0B5 30              0     tsx 
D0B6 6C 00           l.    inc 00,x
D0B8 20 DE            .    bra de
D0BA C6 05           ..    ldab #05
D0BC F7 0B AA        ...   stb 0baa
D0BF C6 04           ..    ldab #04
D0C1 F7 00 13        ...   stb 0013
D0C4 C6 01           ..    ldab #01
D0C6 F7 0B C4        ...   stb 0bc4
D0C9 C6 03           ..    ldab #03
D0CB F7 0B C5        ...   stb 0bc5
D0CE C6 02           ..    ldab #02
D0D0 F7 0B C6        ...   stb 0bc6
D0D3 CC 00 19        ...   ldd #0019
D0D6 FD 0B DD        ...   stad 0bdd
D0D9 CC 00 0A        ...   ldd #000a
D0DC FD 0B DF        ...   stad 0bdf
D0DF CC 00 05        ...   ldd #0005
D0E2 FD 0B E1        ...   stad 0be1
D0E5 C6 01           ..    ldab #01
D0E7 F7 0B D7        ...   stb 0bd7
D0EA C6 02           ..    ldab #02
D0EC F7 0B D8        ...   stb 0bd8
D0EF C6 04           ..    ldab #04
D0F1 F7 0B D9        ...   stb 0bd9
D0F4 C6 20           .     ldab #20
D0F6 F7 0B DA        ...   stb 0bda
D0F9 C6 10           ..    ldab #10
D0FB F7 0B DB        ...   stb 0bdb
D0FE C6 08           ..    ldab #08
D100 F7 0B DC        ...   stb 0bdc
D103 31              1     ins 
D104 39              9     rts 

jump32:
D105 34              4     des 
D106 CE 00 1A        ...   ldx #001a
D109 1C 00           ..    bset add,x 00,x
D10B 80 F6           ..    suba #f6
D10D 00              .     test 
D10E 1A              .     illegal 
D10F F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
D112 C6 C8           ..    ldab #c8
D114 30              0     tsx 
D115 E7 00           ..    stb 00,x
D117 30              0     tsx 
D118 E6 00           ..    ldab 00,x
D11A C1 00           ..    cmpb #00
D11C 23 05           #.    bls 05
D11E 30              0     tsx 
D11F 6A 00           j.    dec 00,x
D121 20 F4            .    bra f4
D123 CE 10 00        ...   ldx #1000
D126 1C 00           ..    bset add,x 00,x
D128 40              @     nega 
D129 CE 00 14        ...   ldx #0014
D12C 1D 00           ..    bclr add,x 00,x
D12E 20 CE            .    bra ce
D130 00              .     test 
D131 14              .     bset 
D132 1D 00           ..    bclr add,x 00,x
D134 40              @     nega 
D135 F6 00 14        ...   ldab 0014
D138 F7 38 00        .8.   stb 3800
D13B 31              1     ins 
D13C 39              9     rts 

jump34:
D13D F6 00 06        ...   ldab 0006
D140 4F              O     clra 
D141 CE 00 0A        ...   ldx #000a
D144 02              .     idiv 
D145 8F              .     xgdx 
D146 CB 01           ..    addb #01
D148 F7 00 5F        .._   stb 005f
D14B F6 00 06        ...   ldab 0006
D14E 4F              O     clra 
D14F CE 00 0A        ...   ldx #000a
D152 02              .     idiv 
D153 F7 00 5E        ..^   stb 005e
D156 F6 00 5E        ..^   ldab 005e
D159 C1 05           ..    cmpb #05
D15B 23 03           #.    bls 03
D15D 7A 00 5E        z.^   dec 005e
D160 F6 00 5E        ..^   ldab 005e
D163 4F              O     clra 
D164 C3 B1 23        ..#   addd #b123
D167 8F              .     xgdx 
D168 E6 00           ..    ldab 00,x
D16A 4F              O     clra 
D16B 37              7     pshb 
D16C 36              6     psha 
D16D F6 00 5F        .._   ldab 005f
D170 4F              O     clra 
D171 C3 B1 23        ..#   addd #b123
D174 8F              .     xgdx 
D175 E6 00           ..    ldab 00,x
D177 4F              O     clra 
D178 17              .     tba 
D179 5F              _     clrb 
D17A 30              0     tsx 
D17B E3 00           ..    addd 00,x
D17D 38              8     pulx 
D17E FD 00 58        ..X   stad 0058
D181 7F 00 1C        ...   clr 001c
D184 F6 00 1C        ...   ldab 001c
D187 C1 10           ..    cmpb #10
D189 24 37           $7    bcc 37
D18B F6 00 58        ..X   ldab 0058
D18E 2C 0E           ,.    bge 0e
D190 CE 00 14        ...   ldx #0014
D193 1C 00           ..    bset add,x 00,x
D195 10              .     sba 
D196 F6 00 14        ...   ldab 0014
D199 F7 38 00        .8.   stb 3800
D19C 20 0C            .    bra 0c
D19E CE 00 14        ...   ldx #0014
D1A1 1D 00           ..    bclr add,x 00,x
D1A3 10              .     sba 
D1A4 F6 00 14        ...   ldab 0014
D1A7 F7 38 00        .8.   stb 3800
D1AA CE 10 00        ...   ldx #1000
D1AD 1C 00           ..    bset add,x 00,x
D1AF 20 CE            .    bra ce
D1B1 10              .     sba 
D1B2 00              .     test 
D1B3 1D 00           ..    bclr add,x 00,x
D1B5 20 FC            .    bra fc
D1B7 00              .     test 
D1B8 58              X     aslb 
D1B9 05              .     asld 
D1BA FD 00 58        ..X   stad 0058
D1BD 7C 00 1C        |..   inc 001c
D1C0 20 C2            .    bra c2
D1C2 F6 00 5F        .._   ldab 005f
D1C5 C1 09           ..    cmpb #09
D1C7 26 0E           &.    bne 0e
D1C9 CE 00 14        ...   ldx #0014
D1CC 1C 00           ..    bset add,x 00,x
D1CE 20 F6            .    bra f6
D1D0 00              .     test 
D1D1 14              .     bset 
D1D2 F7 38 00        .8.   stb 3800
D1D5 20 13            .    bra 13
D1D7 F6 00 5F        .._   ldab 005f
D1DA C1 0A           ..    cmpb #0a
D1DC 26 0C           &.    bne 0c
D1DE CE 00 14        ...   ldx #0014
D1E1 1C 00           ..    bset add,x 00,x
D1E3 40              @     nega 
D1E4 F6 00 14        ...   ldab 0014
D1E7 F7 38 00        .8.   stb 3800
D1EA CE 10 00        ...   ldx #1000
D1ED 1D 00           ..    bclr add,x 00,x
D1EF 40              @     nega 
D1F0 CE 00 1A        ...   ldx #001a
D1F3 1D 00           ..    bclr add,x 00,x
D1F5 80 F6           ..    suba #f6
D1F7 00              .     test 
D1F8 1A              .     illegal 
D1F9 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
D1FC 39              9     rts 

jump90:
D1FD CC 40 00        .@.   ldd #4000
D200 37              7     pshb 
D201 36              6     psha 
D202 F6 00 06        ...   ldab 0006
D205 4F              O     clra 
D206 05              .     asld 
D207 C3 08 00        ...   addd #0800
D20A 38              8     pulx 
D20B 8F              .     xgdx 
D20C AA 00           ..    ora 00,x
D20E EA 01           ..    orb 01,x
D210 ED 00           ..    stad 00,x
D212 39              9     rts 

jump45:
D213 F6 00 1F        ...   ldab 001f
D216 C4 04           ..    andb #04
D218 26 12           &.    bne 12
D21A F6 0B C0        ...   ldab 0bc0
D21D C4 10           ..    andb #10
D21F 26 0B           &.    bne 0b
D221 CE 00 00        ...   ldx #0000
D224 3C              <     pshx 
D225 FC 00 44        ..D   ldd 0044
D228 BD BE 9E        ...   jsr be9e		;jump50
D22B 38              8     pulx 
D22C BD D3 11        ...   jsr d311		;jump30
D22F 83 00 00        ...   subd #0000
D232 27 13           '.    beq 13
D234 F6 00 06        ...   ldab 0006
D237 BD D4 68        ..h   jsr d468		;jump51
D23A 83 00 00        ...   subd #0000
D23D 27 05           '.    beq 05
D23F 8D BC           ..    bsr dest bc
D241 BD D6 3C        ..<   jsr d63c		;jump33
D244 5F              _     clrb 
D245 4F              O     clra 
D246 39              9     rts 

D247 BD D1 3D        ..=   jsr d13d		;jump34
D24A C6 3C           .<    ldab #3c
D24C F7 00 2E        ...   stb 002e
D24F C6 41           .A    ldab #41
D251 F7 00 31        ..1   stb 0031
D254 CE 00 1B        ...   ldx #001b
D257 1D 00           ..    bclr add,x 00,x
D259 08              .     inx 
goto59:
D25A F6 00 1B        ...   ldab 001b
D25D C4 08           ..    andb #08
D25F 26 22           &"    bne 22
D261 F6 00 2E        ...   ldab 002e
D264 C1 35           .5    cmpb #35
D266 23 10           #.    bls 10
D268 BD D5 70        ..p   jsr d570		;jump35
D26B 83 00 00        ...   subd #0000
D26E 27 06           '.    beq 06
D270 CE 00 1B        ...   ldx #001b
D273 1C 00           ..    bset add,x 00,x
D275 08              .     inx 
D276 20 0B            .    bra 0b
D278 BD D1 05        ...   jsr d105		;jump32
D27B 8D 80           ..    bsr dest 80
D27D BD D6 3C        ..<   jsr d63c		;jump33
D280 5F              _     clrb 
D281 4F              O     clra 
D282 39              9     rts 

D283 F6 00 2E        ...   ldab 002e
D286 C1 36           .6    cmpb #36
D288 24 56           $V    bcc 56
D28A BD D5 F7        ...   jsr d5f7		;jump108
D28D 83 00 00        ...   subd #0000
D290 27 4E           'N    beq 4e
D292 BD D5 B0        ...   jsr d5b0		;jump31
D295 83 00 00        ...   subd #0000
D298 27 46           'F    beq 46
D29A BD D1 05        ...   jsr d105		;jump32
D29D F6 00 2E        ...   ldab 002e
D2A0 C1 32           .2    cmpb #32
D2A2 22 07           ".    bhi 07
D2A4 8D 6B           .k    bsr dest 6b
D2A6 83 00 00        ...   subd #0000
D2A9 27 0C           '.    beq 0c
D2AB BD D1 05        ...   jsr d105		;jump32
D2AE BD D1 FD        ...   jsr d1fd		;jump90
D2B1 BD D6 3C        ..<   jsr d63c		;jump33
D2B4 5F              _     clrb 
D2B5 4F              O     clra 
D2B6 39              9     rts 

D2B7 F6 00 06        ...   ldab 0006
D2BA 4F              O     clra 
D2BB C3 0E 06        ...   addd #0e06
D2BE 8F              .     xgdx 
D2BF 6C 00           l.    inc 00,x
D2C1 F6 00 06        ...   ldab 0006
D2C4 4F              O     clra 
D2C5 C3 0E 06        ...   addd #0e06
D2C8 8F              .     xgdx 
D2C9 E6 00           ..    ldab 00,x
D2CB C1 64           .d    cmpb #64
D2CD 23 0D           #.    bls 0d
D2CF 5F              _     clrb 
D2D0 37              7     pshb 
D2D1 F6 00 06        ...   ldab 0006
D2D4 4F              O     clra 
D2D5 C3 0E 06        ...   addd #0e06
D2D8 8F              .     xgdx 
D2D9 33              3     pulb 
D2DA E7 00           ..    stb 00,x
D2DC CC 00 01        ...   ldd #0001
D2DF 39              9     rts 

D2E0 F6 00 2E        ...   ldab 002e
D2E3 26 14           &.    bne 14
D2E5 BD D5 F7        ...   jsr d5f7		;jump108
D2E8 83 00 00        ...   subd #0000
D2EB 26 0C           &.    bne 0c
D2ED BD D1 05        ...   jsr d105		;jump32
D2F0 BD D1 FD        ...   jsr d1fd		;jump90
D2F3 BD D6 26        ..&   jsr d626		;jump91
D2F6 5F              _     clrb 
D2F7 4F              O     clra 
D2F8 39              9     rts 

D2F9 F6 00 2E        ...   ldab 002e
D2FC 26 0C           &.    bne 0c
D2FE BD D1 05        ...   jsr d105		;jump32
D301 BD D1 FD        ...   jsr d1fd		;jump90
D304 BD D6 3C        ..<   jsr d63c		;jump33
D307 5F              _     clrb 
D308 4F              O     clra 
D309 39              9     rts 

D30A 7E D2 5A        ~.Z   jmp d25a		;goto59
D30D BD E6 1B        ...   jsr e61b		;jump4
D310 39              9     rts 

jump30:
D311 BD D1 3D        ..=   jsr d13d		;jump34
D314 7F 00 1C        ...   clr 001c
D317 7F 00 18        ...   clr 0018
D31A F6 10 0A        ...   ldab 100a
D31D C4 02           ..    andb #02
D31F 27 13           '.    beq 13
D321 7C 00 1C        |..   inc 001c
D324 F6 00 1C        ...   ldab 001c
D327 C1 FF           ..    cmpb #ff
D329 26 07           &.    bne 07
D32B BD D1 05        ...   jsr d105		;jump32
D32E CC 00 01        ...   ldd #0001
D331 39              9     rts 

D332 20 10            .    bra 10
D334 7C 00 18        |..   inc 0018
D337 F6 00 18        ...   ldab 0018
D33A C1 08           ..    cmpb #08
D33C 26 06           &.    bne 06
D33E BD D1 05        ...   jsr d105		;jump32
D341 5F              _     clrb 
D342 4F              O     clra 
D343 39              9     rts 

jump43:
D344 20 D4            .    bra d4
D346 3C              <     pshx 
D347 3C              <     pshx 
D348 3C              <     pshx 
D349 34              4     des 
D34A F6 0E 6D        ..m   ldab 0e6d
D34D C1 01           ..    cmpb #01
D34F 27 03           '.    beq 03
D351 7E D4 07        ~..   jmp d407		;goto60
D354 F6 00 06        ...   ldab 0006
D357 4F              O     clra 
D358 05              .     asld 
D359 C3 08 C8        ...   addd #08c8
D35C 8F              .     xgdx 
D35D EC 00           ..    ldd 00,x
D35F C4 7F           ..    andb #7f
D361 4F              O     clra 
D362 FD 0E 6B        ..k   stad 0e6b
D365 7F 00 06        ...   clr 0006
goto62:
D368 F6 00 06        ...   ldab 0006
D36B C1 3E           .>    cmpb #3e
D36D 26 03           &.    bne 03
D36F 7E D4 02        ~..   jmp d402		;goto61
D372 BD 97 6F        ..o   jsr 976f		;jump41
D375 F6 00 06        ...   ldab 0006
D378 4F              O     clra 
D379 05              .     asld 
D37A C3 08 C8        ...   addd #08c8
D37D 8F              .     xgdx 
D37E EC 00           ..    ldd 00,x
D380 C4 7F           ..    andb #7f
D382 4F              O     clra 
D383 B3 0E 6B        ..k   subd 0e6b
D386 26 77           &w    bne 77
D388 F6 00 06        ...   ldab 0006
D38B 4F              O     clra 
D38C CE 00 0A        ...   ldx #000a
D38F 02              .     idiv 
D390 30              0     tsx 
D391 E7 01           ..    stb 01,x
D393 E6 01           ..    ldab 01,x
D395 C1 01           ..    cmpb #01
D397 26 07           &.    bne 07
D399 C6 40           .@    ldab #40
D39B 30              0     tsx 
D39C E7 00           ..    stb 00,x
D39E 20 0C            .    bra 0c
D3A0 30              0     tsx 
D3A1 E6 01           ..    ldab 01,x
D3A3 C1 02           ..    cmpb #02
D3A5 26 05           &.    bne 05
D3A7 C6 80           ..    ldab #80
D3A9 30              0     tsx 
D3AA E7 00           ..    stb 00,x
D3AC BD D1 3D        ..=   jsr d13d		;jump34
D3AF 5F              _     clrb 
D3B0 4F              O     clra 
D3B1 30              0     tsx 
D3B2 ED 03           ..    stad 03,x
;look at the home sensors for this row.  If it is 0, increment 03/04 else
;decrement 03/04.   Do this 10 times.
D3B4 CC 00 0A        ...   ldd #000a
D3B7 ED 05           ..    stad 05,x
loop:
D3B9 30              0     tsx 
D3BA EC 05           ..    ldd 05,x
D3BC 2F 23           /#    ble 23
D3BE F6 28 00        .(.   ldab 2800	;snack motor home sensors
D3C1 30              0     tsx 
D3C2 E4 00           ..    andb 00,x
D3C4 26 09           &.    bne d3cf
D3C6 30              0     tsx 
D3C7 6C 04           l.    inc 04,x
D3C9 26 0C           &.    bne 0c
D3CB 6C 03           l.    inc 03,x
D3CD 20 08            .    bra d3d7
D3CF 30              0     tsx 
D3D0 EC 03           ..    ldd 03,x
D3D2 83 00 01        ...   subd #0001
D3D5 ED 03           ..    stad 03,x
D3D7 30              0     tsx 
D3D8 EC 05           ..    ldd 05,x
D3DA 83 00 01        ...   subd #0001
D3DD ED 05           ..    stad 05,x
D3DF 20 D8            .    bra loop	;d3b9
D3E1 BD D1 05        ...   jsr d105		;jump32
D3E4 30              0     tsx 
D3E5 EC 03           ..    ldd 03,x
D3E7 2C 16           ,.    bge 16
D3E9 F6 00 06        ...   ldab 0006
D3EC 8D 7A           .z    bsr dest 7a
D3EE 83 00 00        ...   subd #0000
D3F1 27 0C           '.    beq 0c
D3F3 BD D5 61        ..a   jsr d561		;jump42
D3F6 83 00 00        ...   subd #0000
D3F9 26 04           &.    bne 04
D3FB 5F              _     clrb 
D3FC 4F              O     clra 
D3FD 20 64            d    bra 64
D3FF 7E D3 68        ~.h   jmp d368		;goto62
goto61:
D402 CC 00 01        ...   ldd #0001
D405 20 5C            \    bra 5c
goto60:
D407 F6 00 06        ...   ldab 0006
D40A C1 13           ..    cmpb #13
D40C 27 1C           '.    beq 1c
D40E F6 00 06        ...   ldab 0006
D411 C1 1D           ..    cmpb #1d
D413 27 15           '.    beq 15
D415 F6 00 06        ...   ldab 0006
D418 C1 27           .'    cmpb #27
D41A 27 0E           '.    beq 0e
D41C F6 00 06        ...   ldab 0006
D41F C1 31           .1    cmpb #31
D421 27 07           '.    beq 07
D423 F6 00 06        ...   ldab 0006
D426 C1 3B           .;    cmpb #3b
D428 26 37           &7    bne 37
D42A BD D1 3D        ..=   jsr d13d		;jump34
D42D 7F 00 1C        ...   clr 001c
D430 7F 00 18        ...   clr 0018
D433 F6 18 00        ...   ldab 1800
D436 C4 40           .@    andb #40
D438 26 14           &.    bne 14
D43A 7C 00 1C        |..   inc 001c
D43D F6 00 1C        ...   ldab 001c
D440 C1 FF           ..    cmpb #ff
D442 26 08           &.    bne 08
D444 BD D1 05        ...   jsr d105		;jump32
D447 CC 00 01        ...   ldd #0001
D44A 20 17            .    bra 17
D44C 20 11            .    bra 11
D44E 7C 00 18        |..   inc 0018
D451 F6 00 18        ...   ldab 0018
D454 C1 08           ..    cmpb #08
D456 26 07           &.    bne 07
D458 BD D1 05        ...   jsr d105		;jump32
D45B 5F              _     clrb 
D45C 4F              O     clra 
D45D 20 04            .    bra 04
D45F 20 D2            .    bra d2
D461 5F              _     clrb 
D462 4F              O     clra 
D463 38              8     pulx 
D464 38              8     pulx 
D465 38              8     pulx 
D466 31              1     ins 
D467 39              9     rts 

jump51:
D468 37              7     pshb 
D469 30              0     tsx 
D46A E6 00           ..    ldab 00,x
D46C 4F              O     clra 
D46D 05              .     asld 
D46E C3 08 00        ...   addd #0800
D471 8F              .     xgdx 
D472 EC 00           ..    ldd 00,x
D474 5F              _     clrb 
D475 84 80           ..    anda #80
D477 31              1     ins 
D478 39              9     rts 

jump67:
D479 7F 00 1C        ...   clr 001c
D47C F6 00 1C        ...   ldab 001c
D47F C1 64           .d    cmpb #64
D481 24 1A           $.    bcc 1a
D483 CC FF 7F        ...   ldd #ff7f
D486 37              7     pshb 
D487 36              6     psha 
D488 F6 00 1C        ...   ldab 001c
D48B 4F              O     clra 
D48C 05              .     asld 
D48D C3 08 C8        ...   addd #08c8
D490 38              8     pulx 
D491 8F              .     xgdx 
D492 A4 00           ..    anda 00,x
D494 E4 01           ..    andb 01,x
D496 ED 00           ..    stad 00,x
D498 7C 00 1C        |..   inc 001c
D49B 20 DF            .    bra df
D49D 7F 00 05        ...   clr 0005
goto65:
D4A0 F6 00 05        ...   ldab 0005
D4A3 C1 50           .P    cmpb #50
D4A5 25 03           %.    bcs 03
D4A7 7E D5 60        ~.`   jmp d560		;goto63
D4AA F6 00 05        ...   ldab 0005
D4AD 4F              O     clra 
D4AE C3 B0 D3        ...   addd #b0d3
D4B1 8F              .     xgdx 
D4B2 E6 00           ..    ldab 00,x
D4B4 F7 00 06        ...   stb 0006
D4B7 BD D3 11        ...   jsr d311		;jump30
D4BA 83 00 00        ...   subd #0000
D4BD 27 03           '.    beq 03
D4BF 7E D5 45        ~.E   jmp d545		;goto64
D4C2 F6 00 06        ...   ldab 0006
D4C5 BD D7 A0        ...   jsr d7a0		;jump10
D4C8 83 00 00        ...   subd #0000
D4CB 26 78           &x    bne 78
D4CD BD D5 B0        ...   jsr d5b0		;jump31
D4D0 83 00 00        ...   subd #0000
D4D3 26 06           &.    bne 06
D4D5 BD D1 FD        ...   jsr d1fd		;jump90
D4D8 BD D6 3C        ..<   jsr d63c		;jump33
D4DB CC 80 00        ...   ldd #8000
D4DE 37              7     pshb 
D4DF 36              6     psha 
D4E0 F6 00 06        ...   ldab 0006
D4E3 4F              O     clra 
D4E4 05              .     asld 
D4E5 C3 08 00        ...   addd #0800
D4E8 38              8     pulx 
D4E9 8F              .     xgdx 
D4EA AA 00           ..    ora 00,x
D4EC EA 01           ..    orb 01,x
D4EE ED 00           ..    stad 00,x
D4F0 F6 00 06        ...   ldab 0006
D4F3 4F              O     clra 
D4F4 05              .     asld 
D4F5 C3 08 00        ...   addd #0800
D4F8 8F              .     xgdx 
D4F9 EC 00           ..    ldd 00,x
D4FB C4 FF           ..    andb #ff
D4FD 84 3F           .?    anda #3f
D4FF 83 27 0F        .'.   subd #270f
D502 23 1A           #.    bls 1a
D504 BD D1 FD        ...   jsr d1fd		;jump90
D507 CC 01 00        ...   ldd #0100
D50A 37              7     pshb 
D50B 36              6     psha 
D50C F6 00 06        ...   ldab 0006
D50F 4F              O     clra 
D510 05              .     asld 
D511 C3 08 C8        ...   addd #08c8
D514 38              8     pulx 
D515 8F              .     xgdx 
D516 AA 00           ..    ora 00,x
D518 EA 01           ..    orb 01,x
D51A ED 00           ..    stad 00,x
D51C 20 25            %    bra 25
D51E F6 00 06        ...   ldab 0006
D521 4F              O     clra 
D522 05              .     asld 
D523 C3 08 C8        ...   addd #08c8
D526 8F              .     xgdx 
D527 EC 00           ..    ldd 00,x
D529 C4 7F           ..    andb #7f
D52B F7 00 06        ...   stb 0006
D52E CC 00 80        ...   ldd #0080
D531 37              7     pshb 
D532 36              6     psha 
D533 F6 00 06        ...   ldab 0006
D536 4F              O     clra 
D537 05              .     asld 
D538 C3 08 C8        ...   addd #08c8
D53B 38              8     pulx 
D53C 8F              .     xgdx 
D53D AA 00           ..    ora 00,x
D53F EA 01           ..    orb 01,x
D541 ED 00           ..    stad 00,x
D543 20 15            .    bra 15
goto64:
D545 CC 7F FF        ...   ldd #7fff
D548 37              7     pshb 
D549 36              6     psha 
D54A F6 00 06        ...   ldab 0006
D54D 4F              O     clra 
D54E 05              .     asld 
D54F C3 08 00        ...   addd #0800
D552 38              8     pulx 
D553 8F              .     xgdx 
D554 A4 00           ..    anda 00,x
D556 E4 01           ..    andb 01,x
D558 ED 00           ..    stad 00,x
D55A 7C 00 05        |..   inc 0005
D55D 7E D4 A0        ~..   jmp d4a0		;goto65
goto63:
D560 39              9     rts 

jump42:
D561 F6 00 06        ...   ldab 0006
D564 4F              O     clra 
D565 05              .     asld 
D566 C3 08 00        ...   addd #0800
D569 8F              .     xgdx 
D56A EC 00           ..    ldd 00,x
D56C 5F              _     clrb 
D56D 84 40           .@    anda #40
D56F 39              9     rts 

;Look at the home sensor for the row of this slot.  If it isn't 0 within 5
;loops, return 1 (fault?), else return 0 (OK?).
;005f is the row number in this routine.  scratch variable?
;looks like 0006 is the slot number.
;this is called twice
jump35:
D570 F6 00 06        ...   ldab 0006
D573 4F              O     clra 
D574 CE 00 0A        ...   ldx #000a
D577 02              .     idiv 
D578 F7 00 5F        .._   stb 005f
D57B F6 00 5F        .._   ldab 005f
D57E C1 05           ..    cmpb #05
D580 23 03           #.    bls d585
D582 7A 00 5F        z._   dec 005f
D585 F6 00 5F        .._   ldab 005f	;row number
D588 4F              O     clra 
D589 C3 B1 23        ..#   addd #b123
D58C 8F              .     xgdx 
D58D E6 00           ..    ldab 00,x	;get bit for this row
D58F F7 00 5E        ..^   stb 005e
D592 7F 00 1C        ...   clr 001c
loop:
D595 F6 28 00        .(.   ldab 2800	;snack motor home sensors
D598 F4 00 5E        ..^   andb 005e	;check home sensor for this row
D59B 27 10           '.    beq d5ad
D59D 7C 00 1C        |..   inc 001c
D5A0 F6 00 1C        ...   ldab 001c
D5A3 C1 05           ..    cmpb #05
D5A5 26 04           &.    bne d5ab
D5A7 CC 00 01        ...   ldd #0001
D5AA 39              9     rts 

D5AB 20 E8            .    bra loop	;d595
D5AD 5F              _     clrb 
D5AE 4F              O     clra 
D5AF 39              9     rts 

;This does almost the same - check the home sensor for this row (slot number
;is in 0006) up to 5 times.  If it is 1, return 0.  If it stays 0, return 0.
;(this is the reverse of the routine above).  But also check bit 0 of 100a.
;If it is ever 0, return 0.
;005f is the row number
;005e is the bit mask for this row
jump31:
D5B0 F6 00 06        ...   ldab 0006
D5B3 4F              O     clra 
D5B4 CE 00 0A        ...   ldx #000a
D5B7 02              .     idiv 
D5B8 F7 00 5F        .._   stb 005f
D5BB F6 00 5F        .._   ldab 005f
D5BE C1 05           ..    cmpb #05
D5C0 23 03           #.    bls 03
D5C2 7A 00 5F        z._   dec 005f
D5C5 F6 00 5F        .._   ldab 005f
D5C8 4F              O     clra 
D5C9 C3 B1 23        ..#   addd #b123
D5CC 8F              .     xgdx 
D5CD E6 00           ..    ldab 00,x
D5CF F7 00 5E        ..^   stb 005e
D5D2 7F 00 1C        ...   clr 001c
loop:
D5D5 F6 28 00        .(.   ldab 2800	;snack motor home sensors
D5D8 F4 00 5E        ..^   andb 005e
D5DB 26 17           &.    bne d5f4
D5DD F6 10 0A        ...   ldab 100a
D5E0 C4 01           ..    andb #01
D5E2 27 10           '.    beq d5f4
D5E4 7C 00 1C        |..   inc 001c
D5E7 F6 00 1C        ...   ldab 001c
D5EA C1 05           ..    cmpb #05
D5EC 26 04           &.    bne d5f2
D5EE CC 00 01        ...   ldd #0001
D5F1 39              9     rts 

D5F2 20 E1            .    bra loop	;d5d5
D5F4 5F              _     clrb 
D5F5 4F              O     clra 
D5F6 39              9     rts 

jump108:
D5F7 C6 07           ..    ldab #07
D5F9 F7 00 1C        ...   stb 001c
D5FC 7F 00 18        ...   clr 0018
D5FF F6 00 1C        ...   ldab 001c
D602 27 1E           '.    beq 1e
D604 7A 00 1C        z..   dec 001c
D607 F6 10 0A        ...   ldab 100a
D60A C4 01           ..    andb #01
D60C 26 12           &.    bne 12
D60E C6 07           ..    ldab #07
D610 F7 00 1C        ...   stb 001c
D613 7C 00 18        |..   inc 0018
D616 F6 00 18        ...   ldab 0018
D619 C1 05           ..    cmpb #05
D61B 26 03           &.    bne 03
D61D 5F              _     clrb 
D61E 4F              O     clra 
D61F 39              9     rts 

D620 20 DD            .    bra dd
D622 CC 00 01        ...   ldd #0001
D625 39              9     rts 

jump91:
D626 CC 02 00        ...   ldd #0200
D629 37              7     pshb 
D62A 36              6     psha 
D62B F6 00 06        ...   ldab 0006
D62E 4F              O     clra 
D62F 05              .     asld 
D630 C3 08 C8        ...   addd #08c8
D633 38              8     pulx 
D634 8F              .     xgdx 
D635 AA 00           ..    ora 00,x
D637 EA 01           ..    orb 01,x
D639 ED 00           ..    stad 00,x
D63B 39              9     rts 

jump33:
D63C CC 04 00        ...   ldd #0400
D63F 37              7     pshb 
D640 36              6     psha 
D641 F6 00 06        ...   ldab 0006
D644 4F              O     clra 
D645 05              .     asld 
D646 C3 08 C8        ...   addd #08c8
D649 38              8     pulx 
D64A 8F              .     xgdx 
D64B AA 00           ..    ora 00,x
D64D EA 01           ..    orb 01,x
D64F ED 00           ..    stad 00,x
D651 39              9     rts 

jump5:
D652 CE 00 1A        ...   ldx #001a
D655 1C 00           ..    bset add,x 00,x
D657 01              .     nop 
D658 F6 00 1A        ...   ldab 001a
D65B F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
D65E CE 00 14        ...   ldx #0014
D661 1D 00           ..    bclr add,x 00,x
D663 01              .     nop 
D664 F6 00 14        ...   ldab 0014
D667 F7 38 00        .8.   stb 3800
D66A 39              9     rts 

jump107:
D66B F6 0B C0        ...   ldab 0bc0
D66E C4 08           ..    andb #08
D670 27 07           '.    beq 07
D672 F6 00 7D        ..}   ldab 007d
D675 27 02           '.    beq 02
D677 20 F9            .    bra f9
D679 F6 00 15        ...   ldab 0015
D67C 2D 77           -w    blt 77
D67E FC 00 40        ..@   ldd 0040
D681 F3 00 3E        ..>   addd 003e
D684 B3 00 0F        ...   subd 000f
D687 24 4F           $O    bcc 4f
D689 F6 0B C0        ...   ldab 0bc0
D68C C4 01           ..    andb #01
D68E 26 07           &.    bne 07
D690 F6 0B C0        ...   ldab 0bc0
D693 C4 04           ..    andb #04
D695 27 0C           '.    beq 0c
D697 CE 00 1A        ...   ldx #001a
D69A 1D 00           ..    bclr add,x 00,x
D69C 01              .     nop 
D69D F6 00 1A        ...   ldab 001a
D6A0 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
D6A3 F6 0B C0        ...   ldab 0bc0
D6A6 C4 08           ..    andb #08
D6A8 27 20           '     beq 20
D6AA F6 0C D9        ...   ldab 0cd9
D6AD C1 03           ..    cmpb #03
D6AF 23 19           #.    bls 19
D6B1 FC 0B EF        ...   ldd 0bef
D6B4 83 00 64        ..d   subd #0064
D6B7 25 11           %.    bcs 11
D6B9 CE 00 14        ...   ldx #0014
D6BC 1C 00           ..    bset add,x 00,x
D6BE 02              .     idiv 
D6BF F6 00 14        ...   ldab 0014
D6C2 F7 38 00        .8.   stb 3800
D6C5 BD C0 E0        ...   jsr c0e0		;jump94
D6C8 20 0C            .    bra 0c
D6CA CE 00 14        ...   ldx #0014
D6CD 1D 00           ..    bclr add,x 00,x
D6CF 01              .     nop 
D6D0 F6 00 14        ...   ldab 0014
D6D3 F7 38 00        .8.   stb 3800
D6D6 20 05            .    bra 05
D6D8 BD D6 52        ..R   jsr d652		;jump5
D6DB 8D 53           .S    bsr dest 53
D6DD F6 0B C0        ...   ldab 0bc0
D6E0 C4 08           ..    andb #08
D6E2 27 11           '.    beq 11
D6E4 F6 0B C0        ...   ldab 0bc0
D6E7 C4 40           .@    andb #40
D6E9 27 0A           '.    beq 0a
D6EB F6 00 15        ...   ldab 0015
D6EE C4 01           ..    andb #01
D6F0 27 03           '.    beq 03
D6F2 BD C0 E0        ...   jsr c0e0		;jump94
D6F5 39              9     rts 

jump106:
D6F6 F6 0C DB        ...   ldab 0cdb
D6F9 4F              O     clra 
D6FA 37              7     pshb 
D6FB 36              6     psha 
D6FC FC 0B DD        ...   ldd 0bdd
D6FF BD FB BA        ...   jsr fbba		;jump38
D702 FD 0C EB        ...   stad 0ceb
D705 F6 0C DA        ...   ldab 0cda
D708 4F              O     clra 
D709 37              7     pshb 
D70A 36              6     psha 
D70B FC 0B DF        ...   ldd 0bdf
D70E BD FB BA        ...   jsr fbba		;jump38
D711 FD 0C ED        ...   stad 0ced
D714 F6 0C D9        ...   ldab 0cd9
D717 4F              O     clra 
D718 37              7     pshb 
D719 36              6     psha 
D71A FC 0B E1        ...   ldd 0be1
D71D BD FB BA        ...   jsr fbba		;jump38
D720 FD 0C EF        ...   stad 0cef
D723 FC 0C EB        ...   ldd 0ceb
D726 F3 0C ED        ...   addd 0ced
D729 F3 0C EF        ...   addd 0cef
D72C FD 0B EF        ...   stad 0bef
D72F 39              9     rts 

D730 F6 00 15        ...   ldab 0015
D733 C4 1F           ..    andb #1f
D735 27 49           'I    beq 49
D737 FC 00 40        ..@   ldd 0040
D73A F3 00 3E        ..>   addd 003e
D73D B3 0B EF        ...   subd 0bef
D740 23 19           #.    bls 19
D742 CC B0 6B        ..k   ldd #b06b
D745 BD C0 BA        ...   jsr c0ba		;display:
D748 CC 01 F4        ...   ldd #01f4
D74B BD E5 71        ..q   jsr e571		;jump13
D74E BD C5 D3        ...   jsr c5d3		;jump6
D751 CC 01 F4        ...   ldd #01f4
D754 BD E5 71        ..q   jsr e571		;jump13
D757 CC 00 01        ...   ldd #0001
D75A 39              9     rts 

D75B F6 00 15        ...   ldab 0015
D75E C4 04           ..    andb #04
D760 27 1E           '.    beq 1e
D762 F6 0E 6E        ..n   ldab 0e6e
D765 26 19           &.    bne 19
D767 CC B0 76        ..v   ldd #b076
D76A BD C0 BA        ...   jsr c0ba		;display:
D76D CC 01 F4        ...   ldd #01f4
D770 BD E5 71        ..q   jsr e571		;jump13
D773 BD C5 D3        ...   jsr c5d3		;jump6
D776 CC 01 F4        ...   ldd #01f4
D779 BD E5 71        ..q   jsr e571		;jump13
D77C CC 00 01        ...   ldd #0001
D77F 39              9     rts 

D780 5F              _     clrb 
D781 4F              O     clra 
D782 39              9     rts 

jump44:
D783 F6 00 06        ...   ldab 0006
D786 4F              O     clra 
D787 CE 00 0A        ...   ldx #000a
D78A 02              .     idiv 
D78B C1 04           ..    cmpb #04
D78D 26 0E           &.    bne 0e
D78F F6 00 1D        ...   ldab 001d
D792 C4 04           ..    andb #04
D794 27 03           '.    beq 03
D796 5F              _     clrb 
D797 4F              O     clra 
D798 39              9     rts 

D799 CC 00 01        ...   ldd #0001
D79C 39              9     rts 

D79D 5F              _     clrb 
D79E 4F              O     clra 
D79F 39              9     rts 

jump10:
D7A0 37              7     pshb 
D7A1 30              0     tsx 
D7A2 E6 00           ..    ldab 00,x
D7A4 C1 09           ..    cmpb #09
D7A6 27 18           '.    beq 18
D7A8 E6 00           ..    ldab 00,x
D7AA C1 45           .E    cmpb #45
D7AC 27 12           '.    beq 12
D7AE E6 00           ..    ldab 00,x
D7B0 C1 4F           .O    cmpb #4f
D7B2 27 0C           '.    beq 0c
D7B4 E6 00           ..    ldab 00,x
D7B6 C1 59           .Y    cmpb #59
D7B8 27 06           '.    beq 06
D7BA E6 00           ..    ldab 00,x
D7BC C1 63           .c    cmpb #63
D7BE 26 05           &.    bne 05
D7C0 CC 00 01        ...   ldd #0001
D7C3 20 38            8    bra 38
D7C5 F6 0E 6D        ..m   ldab 0e6d
D7C8 C1 01           ..    cmpb #01
D7CA 26 2F           &/    bne 2f
D7CC 30              0     tsx 
D7CD E6 00           ..    ldab 00,x
D7CF 4F              O     clra 
D7D0 CE 00 0A        ...   ldx #000a
D7D3 02              .     idiv 
D7D4 C1 01           ..    cmpb #01
D7D6 27 11           '.    beq 11
D7D8 30              0     tsx 
D7D9 E6 00           ..    ldab 00,x
D7DB 4F              O     clra 
D7DC CE 00 0A        ...   ldx #000a
D7DF 02              .     idiv 
D7E0 C1 02           ..    cmpb #02
D7E2 27 05           '.    beq 05
D7E4 CC 00 01        ...   ldd #0001
D7E7 20 14            .    bra 14
D7E9 30              0     tsx 
D7EA E6 00           ..    ldab 00,x
D7EC C1 0B           ..    cmpb #0b
D7EE 25 06           %.    bcs 06
D7F0 E6 00           ..    ldab 00,x
D7F2 C1 3E           .>    cmpb #3e
D7F4 23 05           #.    bls 05
D7F6 CC 00 01        ...   ldd #0001
D7F9 20 02            .    bra 02
D7FB 5F              _     clrb 
D7FC 4F              O     clra 
D7FD 31              1     ins 
D7FE 39              9     rts 

jump80:
D7FF 7F 00 1C        ...   clr 001c
D802 F6 00 1C        ...   ldab 001c
D805 4F              O     clra 
D806 B3 00 4D        ..M   subd 004d
D809 24 05           $.    bcc 05
D80B 7C 00 1C        |..   inc 001c
D80E 20 F2            .    bra f2
D810 F6 00 1C        ...   ldab 001c
D813 4F              O     clra 
D814 05              .     asld 
D815 C3 0D 1F        ...   addd #0d1f
D818 8F              .     xgdx 
D819 EC 00           ..    ldd 00,x
D81B 83 FF FF        ...   subd #ffff
D81E 26 10           &.    bne 10
D820 5F              _     clrb 
D821 4F              O     clra 
D822 37              7     pshb 
D823 36              6     psha 
D824 F6 00 1C        ...   ldab 001c
D827 4F              O     clra 
D828 05              .     asld 
D829 C3 0D 1F        ...   addd #0d1f
D82C 38              8     pulx 
D82D 8F              .     xgdx 
D82E ED 00           ..    stad 00,x
D830 F6 00 1C        ...   ldab 001c
D833 4F              O     clra 
D834 05              .     asld 
D835 C3 0D 1F        ...   addd #0d1f
D838 8F              .     xgdx 
D839 6C 01           l.    inc 01,x
D83B 26 02           &.    bne 02
D83D 6C 00           l.    inc 00,x
D83F 39              9     rts 

jump105:
D840 FC 00 44        ..D   ldd 0044
D843 26 03           &.    bne 03
D845 7E D8 D2        ~..   jmp d8d2		;goto66
D848 FC 00 44        ..D   ldd 0044
D84B 37              7     pshb 
D84C 36              6     psha 
D84D 4F              O     clra 
D84E 5F              _     clrb 
D84F 37              7     pshb 
D850 36              6     psha 
D851 CC 0D EB        ...   ldd #0deb
D854 BD FB DF        ...   jsr fbdf		;jump46
D857 31              1     ins 
D858 31              1     ins 
D859 FE 0D ED        ...   ldx 0ded
D85C 3C              <     pshx 
D85D FC 0D EB        ...   ldd 0deb
D860 37              7     pshb 
D861 36              6     psha 
D862 CE E1 00        ...   ldx #e100
D865 3C              <     pshx 
D866 CC 05 F5        ...   ldd #05f5
D869 37              7     pshb 
D86A 36              6     psha 
D86B 30              0     tsx 
D86C EC 06           ..    ldd 06,x
D86E A3 02           ..    subd 02,x
D870 EC 04           ..    ldd 04,x
D872 E2 01           ..    sbcb 01,x
D874 A2 00           ..    sbca 00,x
D876 38              8     pulx 
D877 38              8     pulx 
D878 38              8     pulx 
D879 38              8     pulx 
D87A 25 11           %.    bcs 11
D87C CE 1F 00        ...   ldx #1f00
D87F 3C              <     pshx 
D880 CC FA 0A        ...   ldd #fa0a
D883 37              7     pshb 
D884 36              6     psha 
D885 CC 0D EB        ...   ldd #0deb
D888 BD FB DF        ...   jsr fbdf		;jump46
D88B 31              1     ins 
D88C 31              1     ins 
D88D FC 00 44        ..D   ldd 0044
D890 37              7     pshb 
D891 36              6     psha 
D892 4F              O     clra 
D893 5F              _     clrb 
D894 37              7     pshb 
D895 36              6     psha 
D896 CC 0D E7        ...   ldd #0de7
D899 BD FB DF        ...   jsr fbdf		;jump46
D89C 31              1     ins 
D89D 31              1     ins 
D89E FE 0D E9        ...   ldx 0de9
D8A1 3C              <     pshx 
D8A2 FC 0D E7        ...   ldd 0de7
D8A5 37              7     pshb 
D8A6 36              6     psha 
D8A7 CE E1 00        ...   ldx #e100
D8AA 3C              <     pshx 
D8AB CC 05 F5        ...   ldd #05f5
D8AE 37              7     pshb 
D8AF 36              6     psha 
D8B0 30              0     tsx 
D8B1 EC 06           ..    ldd 06,x
D8B3 A3 02           ..    subd 02,x
D8B5 EC 04           ..    ldd 04,x
D8B7 E2 01           ..    sbcb 01,x
D8B9 A2 00           ..    sbca 00,x
D8BB 38              8     pulx 
D8BC 38              8     pulx 
D8BD 38              8     pulx 
D8BE 38              8     pulx 
D8BF 25 11           %.    bcs 11
D8C1 CE 1F 00        ...   ldx #1f00
D8C4 3C              <     pshx 
D8C5 CC FA 0A        ...   ldd #fa0a
D8C8 37              7     pshb 
D8C9 36              6     psha 
D8CA CC 0D E7        ...   ldd #0de7
D8CD BD FB DF        ...   jsr fbdf		;jump46
D8D0 31              1     ins 
D8D1 31              1     ins 
goto66:
D8D2 39              9     rts 

jump84:
D8D3 7F 00 18        ...   clr 0018
D8D6 F6 00 18        ...   ldab 0018
D8D9 C1 10           ..    cmpb #10
D8DB 24 0B           $.    bcc 0b
D8DD CC 00 32        ..2   ldd #0032
D8E0 BD E5 71        ..q   jsr e571		;jump13
D8E3 7C 00 18        |..   inc 0018
D8E6 20 EE            .    bra ee
D8E8 39              9     rts 

jump92:
D8E9 F6 00 2B        ..+   ldab 002b
D8EC C1 00           ..    cmpb #00
D8EE 26 18           &.    bne 18
D8F0 CC AD 84        ...   ldd #ad84
D8F3 BD C0 BA        ...   jsr c0ba		;display:
D8F6 8D DB           ..    bsr dest db
D8F8 CC AD 8F        ...   ldd #ad8f
D8FB BD C0 BA        ...   jsr c0ba		;display:
D8FE 8D D3           ..    bsr dest d3
D900 CC AD 9A        ...   ldd #ad9a
D903 BD C0 BA        ...   jsr c0ba		;display:
D906 20 4E            N    bra 4e
D908 C1 01           ..    cmpb #01
D90A 26 18           &.    bne 18
D90C CC AD 63        ..c   ldd #ad63
D90F BD C0 BA        ...   jsr c0ba		;display:
D912 8D BF           ..    bsr dest bf
D914 CC AD 6E        ..n   ldd #ad6e
D917 BD C0 BA        ...   jsr c0ba		;display:
D91A 8D B7           ..    bsr dest b7
D91C CC AD 79        ..y   ldd #ad79
D91F BD C0 BA        ...   jsr c0ba		;display:
D922 20 32            2    bra 32
D924 C1 02           ..    cmpb #02
D926 26 18           &.    bne 18
D928 CC AD 42        ..B   ldd #ad42
D92B BD C0 BA        ...   jsr c0ba		;display:
D92E 8D A3           ..    bsr dest a3
D930 CC AD 4D        ..M   ldd #ad4d
D933 BD C0 BA        ...   jsr c0ba		;display:
D936 8D 9B           ..    bsr dest 9b
D938 CC AD 58        ..X   ldd #ad58
D93B BD C0 BA        ...   jsr c0ba		;display:
D93E 20 16            .    bra 16
D940 CC AD 21        ..!   ldd #ad21
D943 BD C0 BA        ...   jsr c0ba		;display:
D946 8D 8B           ..    bsr dest 8b
D948 CC AD 2C        ..,   ldd #ad2c
D94B BD C0 BA        ...   jsr c0ba		;display:
D94E 8D 83           ..    bsr dest 83
D950 CC AD 37        ..7   ldd #ad37
D953 BD C0 BA        ...   jsr c0ba		;display:
D956 BD D8 D3        ...   jsr d8d3		;jump84
D959 39              9     rts 

jump123:
D95A F6 00 06        ...   ldab 0006
D95D F1 0B AF        ...   cmpb 0baf
D960 26 0D           &.    bne 0d
D962 BD D7 FF        ...   jsr d7ff		;jump80
D965 F6 0B B4        ...   ldab 0bb4
D968 F7 00 06        ...   stb 0006
D96B 8D 55           .U    bsr dest 55
D96D 20 52            R    bra 52
D96F F6 00 06        ...   ldab 0006
D972 F1 0B AE        ...   cmpb 0bae
D975 26 0D           &.    bne 0d
D977 BD D7 FF        ...   jsr d7ff		;jump80
D97A F6 0B B3        ...   ldab 0bb3
D97D F7 00 06        ...   stb 0006
D980 8D 40           .@    bsr dest 40
D982 20 3D            =    bra 3d
D984 F6 00 06        ...   ldab 0006
D987 F1 0B AD        ...   cmpb 0bad
D98A 26 0D           &.    bne 0d
D98C BD D7 FF        ...   jsr d7ff		;jump80
D98F F6 0B B2        ...   ldab 0bb2
D992 F7 00 06        ...   stb 0006
D995 8D 2B           .+    bsr dest 2b
D997 20 28            (    bra 28
D999 F6 00 06        ...   ldab 0006
D99C F1 0B AC        ...   cmpb 0bac
D99F 26 0D           &.    bne 0d
D9A1 BD D7 FF        ...   jsr d7ff		;jump80
D9A4 F6 0B B1        ...   ldab 0bb1
D9A7 F7 00 06        ...   stb 0006
D9AA 8D 16           ..    bsr dest 16
D9AC 20 13            .    bra 13
D9AE F6 00 06        ...   ldab 0006
D9B1 F1 0B AB        ...   cmpb 0bab
D9B4 26 0B           &.    bne 0b
D9B6 BD D7 FF        ...   jsr d7ff		;jump80
D9B9 F6 0B B0        ...   ldab 0bb0
D9BC F7 00 06        ...   stb 0006
D9BF 8D 01           ..    bsr dest 01
D9C1 39              9     rts 

D9C2 CC 03 E8        ...   ldd #03e8
D9C5 BD E5 71        ..q   jsr e571		;jump13
D9C8 BD D7 83        ...   jsr d783		;jump44
D9CB 83 00 00        ...   subd #0000
D9CE 27 0B           '.    beq 0b
D9D0 7F 00 06        ...   clr 0006
D9D3 5F              _     clrb 
D9D4 4F              O     clra 
D9D5 FD 00 4D        ..M   stad 004d
D9D8 7E DA 63        ~.c   jmp da63		;goto67
D9DB BD D2 13        ...   jsr d213		;jump45
D9DE 83 00 00        ...   subd #0000
D9E1 26 0A           &.    bne 0a
D9E3 7F 00 06        ...   clr 0006
D9E6 5F              _     clrb 
D9E7 4F              O     clra 
D9E8 FD 00 4D        ..M   stad 004d
D9EB 20 76            v    bra 76
D9ED FC 0D 0B        ...   ldd 0d0b
D9F0 83 FF FF        ...   subd #ffff
D9F3 26 05           &.    bne 05
D9F5 5F              _     clrb 
D9F6 4F              O     clra 
D9F7 FD 0D 0B        ...   stad 0d0b
D9FA FE 0D 0B        ...   ldx 0d0b
D9FD 08              .     inx 
D9FE FF 0D 0B        ...   stx 0d0b
DA01 F6 00 06        ...   ldab 0006
DA04 4F              O     clra 
DA05 05              .     asld 
DA06 C3 08 C8        ...   addd #08c8
DA09 8F              .     xgdx 
DA0A EC 00           ..    ldd 00,x
DA0C C4 7F           ..    andb #7f
DA0E 4F              O     clra 
DA0F FD 00 4D        ..M   stad 004d
DA12 F6 00 06        ...   ldab 0006
DA15 4F              O     clra 
DA16 05              .     asld 
DA17 C3 08 00        ...   addd #0800
DA1A 8F              .     xgdx 
DA1B EC 00           ..    ldd 00,x
DA1D C4 FF           ..    andb #ff
DA1F 84 3F           .?    anda #3f
DA21 37              7     pshb 
DA22 36              6     psha 
DA23 4F              O     clra 
DA24 5F              _     clrb 
DA25 37              7     pshb 
DA26 36              6     psha 
DA27 CC 0D 17        ...   ldd #0d17
DA2A BD FB DF        ...   jsr fbdf		;jump46
DA2D 31              1     ins 
DA2E 31              1     ins 
DA2F FE 0D 19        ...   ldx 0d19
DA32 3C              <     pshx 
DA33 FC 0D 17        ...   ldd 0d17
DA36 37              7     pshb 
DA37 36              6     psha 
DA38 CE E1 00        ...   ldx #e100
DA3B 3C              <     pshx 
DA3C CC 05 F5        ...   ldd #05f5
DA3F 37              7     pshb 
DA40 36              6     psha 
DA41 30              0     tsx 
DA42 EC 06           ..    ldd 06,x
DA44 A3 02           ..    subd 02,x
DA46 EC 04           ..    ldd 04,x
DA48 E2 01           ..    sbcb 01,x
DA4A A2 00           ..    sbca 00,x
DA4C 38              8     pulx 
DA4D 38              8     pulx 
DA4E 38              8     pulx 
DA4F 38              8     pulx 
DA50 25 11           %.    bcs 11
DA52 CE 1F 00        ...   ldx #1f00
DA55 3C              <     pshx 
DA56 CC FA 0A        ...   ldd #fa0a
DA59 37              7     pshb 
DA5A 36              6     psha 
DA5B CC 0D 17        ...   ldd #0d17
DA5E BD FB DF        ...   jsr fbdf		;jump46
DA61 31              1     ins 
DA62 31              1     ins 
goto67:
DA63 39              9     rts 

jump122:
DA64 FE 0B B9        ...   ldx 0bb9
DA67 08              .     inx 
DA68 FF 0B B9        ...   stx 0bb9
DA6B FC 0B B5        ...   ldd 0bb5
DA6E CE 00 32        ..2   ldx #0032
DA71 02              .     idiv 
DA72 8F              .     xgdx 
DA73 37              7     pshb 
DA74 36              6     psha 
DA75 CC 00 C3        ...   ldd #00c3
DA78 BD FB BA        ...   jsr fbba		;jump38
DA7B B3 0B B9        ...   subd 0bb9
DA7E 24 05           $.    bcc 05
DA80 5F              _     clrb 
DA81 4F              O     clra 
DA82 FD 0B B9        ...   stad 0bb9
DA85 FC 0B B9        ...   ldd 0bb9
DA88 37              7     pshb 
DA89 36              6     psha 
DA8A CC 00 0A        ...   ldd #000a
DA8D BD FB BA        ...   jsr fbba		;jump38
DA90 37              7     pshb 
DA91 36              6     psha 
DA92 FC 0B B5        ...   ldd 0bb5
DA95 CE 00 32        ..2   ldx #0032
DA98 02              .     idiv 
DA99 8F              .     xgdx 
DA9A 38              8     pulx 
DA9B 8F              .     xgdx 
DA9C 02              .     idiv 
DA9D 8F              .     xgdx 
DA9E 37              7     pshb 
DA9F 36              6     psha 
DAA0 CC 00 0A        ...   ldd #000a
DAA3 BD FB BA        ...   jsr fbba		;jump38
DAA6 CE 00 0A        ...   ldx #000a
DAA9 02              .     idiv 
DAAA 8F              .     xgdx 
DAAB FD 00 4A        ..J   stad 004a
DAAE FC 00 4A        ..J   ldd 004a
DAB1 BD FE 32        ..2   jsr fe32		;jump25
DAB4 DA CA 00        ...   orab ca
DAB7 6E DA           n.    jmp da,x
DAB9 CA 02           ..    orab #02
DABB 80 DA           ..    suba #da
DABD CA 01           ..    orab #01
DABF C2 DA           ..    sbcb #da
DAC1 CA 01           ..    orab #01
DAC3 C2 DB           ..    sbcb #db
DAC5 43              C     coma 
DAC6 F9 8E 00        ...   adcb 8e00
DAC9 00              .     test 
DACA FC 0D 07        ...   ldd 0d07
DACD 83 FF FF        ...   subd #ffff
DAD0 26 05           &.    bne 05
DAD2 5F              _     clrb 
DAD3 4F              O     clra 
DAD4 FD 0D 07        ...   stad 0d07
DAD7 FE 0D 07        ...   ldx 0d07
DADA 08              .     inx 
DADB FF 0D 07        ...   stx 0d07
DADE F6 00 06        ...   ldab 0006
DAE1 4F              O     clra 
DAE2 05              .     asld 
DAE3 C3 08 00        ...   addd #0800
DAE6 8F              .     xgdx 
DAE7 EC 00           ..    ldd 00,x
DAE9 C4 FF           ..    andb #ff
DAEB 84 3F           .?    anda #3f
DAED 37              7     pshb 
DAEE 36              6     psha 
DAEF 4F              O     clra 
DAF0 5F              _     clrb 
DAF1 37              7     pshb 
DAF2 36              6     psha 
DAF3 CC 0D 0F        ...   ldd #0d0f
DAF6 BD FB DF        ...   jsr fbdf		;jump46
DAF9 31              1     ins 
DAFA 31              1     ins 
DAFB FE 0D 11        ...   ldx 0d11
DAFE 3C              <     pshx 
DAFF FC 0D 0F        ...   ldd 0d0f
DB02 37              7     pshb 
DB03 36              6     psha 
DB04 CE E1 00        ...   ldx #e100
DB07 3C              <     pshx 
DB08 CC 05 F5        ...   ldd #05f5
DB0B 37              7     pshb 
DB0C 36              6     psha 
DB0D 30              0     tsx 
DB0E EC 06           ..    ldd 06,x
DB10 A3 02           ..    subd 02,x
DB12 EC 04           ..    ldd 04,x
DB14 E2 01           ..    sbcb 01,x
DB16 A2 00           ..    sbca 00,x
DB18 38              8     pulx 
DB19 38              8     pulx 
DB1A 38              8     pulx 
DB1B 38              8     pulx 
DB1C 25 11           %.    bcs 11
DB1E CE 1F 00        ...   ldx #1f00
DB21 3C              <     pshx 
DB22 CC FA 0A        ...   ldd #fa0a
DB25 37              7     pshb 
DB26 36              6     psha 
DB27 CC 0D 0F        ...   ldd #0d0f
DB2A BD FB DF        ...   jsr fbdf		;jump46
DB2D 31              1     ins 
DB2E 31              1     ins 
DB2F 5F              _     clrb 
DB30 4F              O     clra 
DB31 FD 00 44        ..D   stad 0044
DB34 CE 00 1B        ...   ldx #001b
DB37 1D 00           ..    bclr add,x 00,x
DB39 01              .     nop 
DB3A 7F 00 4F        ..O   clr 004f
DB3D CE 00 11        ...   ldx #0011
DB40 1C 00           ..    bset add,x 00,x
DB42 10              .     sba 
DB43 39              9     rts 

jump104:
DB44 F6 00 2B        ..+   ldab 002b
DB47 C1 00           ..    cmpb #00
DB49 26 08           &.    bne 08
DB4B CC AC DF        ...   ldd #acdf
DB4E BD C0 BA        ...   jsr c0ba		;display:
DB51 20 1E            .    bra 1e
DB53 C1 01           ..    cmpb #01
DB55 26 08           &.    bne 08
DB57 CC AC D4        ...   ldd #acd4
DB5A BD C0 BA        ...   jsr c0ba		;display:
DB5D 20 12            .    bra 12
DB5F C1 02           ..    cmpb #02
DB61 26 08           &.    bne 08
DB63 CC AC EA        ...   ldd #acea
DB66 BD C0 BA        ...   jsr c0ba		;display:
DB69 20 06            .    bra 06
DB6B CC AC D4        ...   ldd #acd4
DB6E BD C0 BA        ...   jsr c0ba		;display:
DB71 BD D8 D3        ...   jsr d8d3		;jump84
DB74 CE 00 01        ...   ldx #0001
DB77 3C              <     pshx 
DB78 FC 00 40        ..@   ldd 0040
DB7B F3 00 3E        ..>   addd 003e
DB7E BD BE 9E        ...   jsr be9e		;jump50
DB81 38              8     pulx 
DB82 FC 00 40        ..@   ldd 0040
DB85 27 05           '.    beq 05
DB87 BD C4 56        ..V   jsr c456		;jump47
DB8A 20 06            .    bra 06
DB8C CC 02 58        ..X   ldd #0258
DB8F BD E5 71        ..q   jsr e571		;jump13
DB92 FC 00 3E        ..>   ldd 003e
DB95 27 03           '.    beq 03
DB97 BD C5 D3        ...   jsr c5d3		;jump6
DB9A F6 00 2B        ..+   ldab 002b
DB9D C1 00           ..    cmpb #00
DB9F 26 08           &.    bne 08
DBA1 CC AD 16        ...   ldd #ad16
DBA4 BD C0 BA        ...   jsr c0ba		;display:
DBA7 20 1E            .    bra 1e
DBA9 C1 01           ..    cmpb #01
DBAB 26 08           &.    bne 08
DBAD CC AD 0B        ...   ldd #ad0b
DBB0 BD C0 BA        ...   jsr c0ba		;display:
DBB3 20 12            .    bra 12
DBB5 C1 02           ..    cmpb #02
DBB7 26 08           &.    bne 08
DBB9 CC AD 00        ...   ldd #ad00
DBBC BD C0 BA        ...   jsr c0ba		;display:
DBBF 20 06            .    bra 06
DBC1 CC AC F5        ...   ldd #acf5
DBC4 BD C0 BA        ...   jsr c0ba		;display:
DBC7 BD D8 D3        ...   jsr d8d3		;jump84
DBCA 39              9     rts 

jump121:
DBCB F6 00 1B        ...   ldab 001b
DBCE C4 01           ..    andb #01
DBD0 26 03           &.    bne 03
DBD2 7E DC 98        ~..   jmp dc98		;goto68
DBD5 F6 00 06        ...   ldab 0006
DBD8 4F              O     clra 
DBD9 05              .     asld 
DBDA C3 08 C8        ...   addd #08c8
DBDD 8F              .     xgdx 
DBDE EC 00           ..    ldd 00,x
DBE0 84 08           ..    anda #08
DBE2 26 03           &.    bne 03
DBE4 7E DC 98        ~..   jmp dc98		;goto68
DBE7 BD D7 FF        ...   jsr d7ff		;jump80
DBEA CC 03 E8        ...   ldd #03e8
DBED BD E5 71        ..q   jsr e571		;jump13
DBF0 BD D3 46        ..F   jsr d346		;jump43
DBF3 83 00 00        ...   subd #0000
DBF6 26 08           &.    bne 08
DBF8 BD D7 83        ...   jsr d783		;jump44
DBFB 83 00 00        ...   subd #0000
DBFE 27 17           '.    beq 17
DC00 5F              _     clrb 
DC01 4F              O     clra 
DC02 FD 00 4D        ..M   stad 004d
DC05 FC 00 44        ..D   ldd 0044
DC08 F3 0B B7        ...   addd 0bb7
DC0B 04              .     lsrd 
DC0C F3 00 40        ..@   addd 0040
DC0F B3 0B B7        ...   subd 0bb7
DC12 FD 00 40        ..@   stad 0040
DC15 20 78            x    bra 78
DC17 BD D2 13        ...   jsr d213		;jump45
DC1A 83 00 00        ...   subd #0000
DC1D 26 17           &.    bne 17
DC1F 5F              _     clrb 
DC20 4F              O     clra 
DC21 FD 00 4D        ..M   stad 004d
DC24 FC 00 44        ..D   ldd 0044
DC27 F3 0B B7        ...   addd 0bb7
DC2A 04              .     lsrd 
DC2B F3 00 40        ..@   addd 0040
DC2E B3 0B B7        ...   subd 0bb7
DC31 FD 00 40        ..@   stad 0040
DC34 20 59            Y    bra 59
DC36 FC 0D 09        ...   ldd 0d09
DC39 83 FF FF        ...   subd #ffff
DC3C 26 05           &.    bne 05
DC3E 5F              _     clrb 
DC3F 4F              O     clra 
DC40 FD 0D 09        ...   stad 0d09
DC43 FE 0D 09        ...   ldx 0d09
DC46 08              .     inx 
DC47 FF 0D 09        ...   stx 0d09
DC4A FC 0B B7        ...   ldd 0bb7
DC4D 37              7     pshb 
DC4E 36              6     psha 
DC4F 4F              O     clra 
DC50 5F              _     clrb 
DC51 37              7     pshb 
DC52 36              6     psha 
DC53 CC 0D 13        ...   ldd #0d13
DC56 BD FB DF        ...   jsr fbdf		;jump46
DC59 31              1     ins 
DC5A 31              1     ins 
DC5B FE 0D 15        ...   ldx 0d15
DC5E 3C              <     pshx 
DC5F FC 0D 13        ...   ldd 0d13
DC62 37              7     pshb 
DC63 36              6     psha 
DC64 CE E1 00        ...   ldx #e100
DC67 3C              <     pshx 
DC68 CC 05 F5        ...   ldd #05f5
DC6B 37              7     pshb 
DC6C 36              6     psha 
DC6D 30              0     tsx 
DC6E EC 06           ..    ldd 06,x
DC70 A3 02           ..    subd 02,x
DC72 EC 04           ..    ldd 04,x
DC74 E2 01           ..    sbcb 01,x
DC76 A2 00           ..    sbca 00,x
DC78 38              8     pulx 
DC79 38              8     pulx 
DC7A 38              8     pulx 
DC7B 38              8     pulx 
DC7C 25 11           %.    bcs 11
DC7E CE 1F 00        ...   ldx #1f00
DC81 3C              <     pshx 
DC82 CC FA 0A        ...   ldd #fa0a
DC85 37              7     pshb 
DC86 36              6     psha 
DC87 CC 0D 13        ...   ldd #0d13
DC8A BD FB DF        ...   jsr fbdf		;jump46
DC8D 31              1     ins 
DC8E 31              1     ins 
DC8F CE 00 1B        ...   ldx #001b
DC92 1D 00           ..    bclr add,x 00,x
DC94 01              .     nop 
DC95 7F 00 4F        ..O   clr 004f
goto68:
DC98 F6 00 11        ...   ldab 0011
DC9B C4 08           ..    andb #08
DC9D 26 03           &.    bne 03
DC9F BD D9 5A        ..Z   jsr d95a		;jump123
DCA2 39              9     rts 

jump29:
DCA3 CE 0B BC        ...   ldx #0bbc
DCA6 1D 00           ..    bclr add,x 00,x
DCA8 1C F6           ..    bset add,x f6,x
DCAA 0B              .     sev 
DCAB BC C4 03        ...   cmpx c403
DCAE 26 48           &H    bne 48
DCB0 FC 0B BD        ...   ldd 0bbd
DCB3 37              7     pshb 
DCB4 36              6     psha 
DCB5 4F              O     clra 
DCB6 5F              _     clrb 
DCB7 37              7     pshb 
DCB8 36              6     psha 
DCB9 CC 0D F7        ...   ldd #0df7
DCBC BD FB DF        ...   jsr fbdf		;jump46
DCBF 31              1     ins 
DCC0 31              1     ins 
DCC1 FE 0D F9        ...   ldx 0df9
DCC4 3C              <     pshx 
DCC5 FC 0D F7        ...   ldd 0df7
DCC8 37              7     pshb 
DCC9 36              6     psha 
DCCA CE E1 00        ...   ldx #e100
DCCD 3C              <     pshx 
DCCE CC 05 F5        ...   ldd #05f5
DCD1 37              7     pshb 
DCD2 36              6     psha 
DCD3 30              0     tsx 
DCD4 EC 06           ..    ldd 06,x
DCD6 A3 02           ..    subd 02,x
DCD8 EC 04           ..    ldd 04,x
DCDA E2 01           ..    sbcb 01,x
DCDC A2 00           ..    sbca 00,x
DCDE 38              8     pulx 
DCDF 38              8     pulx 
DCE0 38              8     pulx 
DCE1 38              8     pulx 
DCE2 25 11           %.    bcs 11
DCE4 CE 1F 00        ...   ldx #1f00
DCE7 3C              <     pshx 
DCE8 CC FA 0A        ...   ldd #fa0a
DCEB 37              7     pshb 
DCEC 36              6     psha 
DCED CC 0D F7        ...   ldd #0df7
DCF0 BD FB DF        ...   jsr fbdf		;jump46
DCF3 31              1     ins 
DCF4 31              1     ins 
DCF5 7E DD D4        ~..   jmp ddd4		;goto69
DCF8 F6 0B BC        ...   ldab 0bbc
DCFB C4 03           ..    andb #03
DCFD C1 01           ..    cmpb #01
DCFF 27 03           '.    beq 03
DD01 7E DD D4        ~..   jmp ddd4		;goto69
DD04 F6 0B BC        ...   ldab 0bbc
DD07 C4 E0           ..    andb #e0
DD09 54              T     lsrb 
DD0A 54              T     lsrb 
DD0B 54              T     lsrb 
DD0C 54              T     lsrb 
DD0D 54              T     lsrb 
DD0E F7 0B BC        ...   stb 0bbc
DD11 F6 0B BC        ...   ldab 0bbc
DD14 F1 0B C4        ...   cmpb 0bc4
DD17 26 33           &3    bne 33
DD19 F6 0B D7        ...   ldab 0bd7
DD1C C1 01           ..    cmpb #01
DD1E 26 0A           &.    bne 0a
DD20 7C 0C DB        |..   inc 0cdb
DD23 C6 03           ..    ldab #03
DD25 F7 00 48        ..H   stb 0048
DD28 20 20                 bra 20
DD2A F6 0B D8        ...   ldab 0bd8
DD2D C1 01           ..    cmpb #01
DD2F 26 0A           &.    bne 0a
DD31 7C 0C DA        |..   inc 0cda
DD34 C6 02           ..    ldab #02
DD36 F7 00 48        ..H   stb 0048
DD39 20 0F            .    bra 0f
DD3B F6 0B D9        ...   ldab 0bd9
DD3E C1 01           ..    cmpb #01
DD40 26 08           &.    bne 08
DD42 7C 0C D9        |..   inc 0cd9
DD45 C6 01           ..    ldab #01
DD47 F7 00 48        ..H   stb 0048
DD4A 20 74            t    bra 74
DD4C F6 0B BC        ...   ldab 0bbc
DD4F F1 0B C5        ...   cmpb 0bc5
DD52 26 33           &3    bne 33
DD54 F6 0B D7        ...   ldab 0bd7
DD57 C1 04           ..    cmpb #04
DD59 26 0A           &.    bne 0a
DD5B 7C 0C DB        |..   inc 0cdb
DD5E C6 03           ..    ldab #03
DD60 F7 00 48        ..H   stb 0048
DD63 20 20                 bra 20
DD65 F6 0B D8        ...   ldab 0bd8
DD68 C1 04           ..    cmpb #04
DD6A 26 0A           &.    bne 0a
DD6C 7C 0C DA        |..   inc 0cda
DD6F C6 02           ..    ldab #02
DD71 F7 00 48        ..H   stb 0048
DD74 20 0F            .    bra 0f
DD76 F6 0B D9        ...   ldab 0bd9
DD79 C1 04           ..    cmpb #04
DD7B 26 08           &.    bne 08
DD7D 7C 0C D9        |..   inc 0cd9
DD80 C6 01           ..    ldab #01
DD82 F7 00 48        ..H   stb 0048
DD85 20 39            9    bra 39
DD87 F6 0B BC        ...   ldab 0bbc
DD8A F1 0B C6        ...   cmpb 0bc6
DD8D 26 31           &1    bne 31
DD8F F6 0B D7        ...   ldab 0bd7
DD92 C1 02           ..    cmpb #02
DD94 26 0A           &.    bne 0a
DD96 7C 0C DB        |..   inc 0cdb
DD99 C6 03           ..    ldab #03
DD9B F7 00 48        ..H   stb 0048
DD9E 20 20                 bra 20
DDA0 F6 0B D8        ...   ldab 0bd8
DDA3 C1 02           ..    cmpb #02
DDA5 26 0A           &.    bne 0a
DDA7 7C 0C DA        |..   inc 0cda
DDAA C6 02           ..    ldab #02
DDAC F7 00 48        ..H   stb 0048
DDAF 20 0F            .    bra 0f
DDB1 F6 0B D9        ...   ldab 0bd9
DDB4 C1 02           ..    cmpb #02
DDB6 26 08           &.    bne 08
DDB8 7C 0C D9        |..   inc 0cd9
DDBB C6 01           ..    ldab #01
DDBD F7 00 48        ..H   stb 0048
DDC0 F6 00 17        ...   ldab 0017
DDC3 C4 08           ..    andb #08
DDC5 26 0D           &.    bne 0d
DDC7 F6 0B BC        ...   ldab 0bbc
DDCA 26 08           &.    bne 08
DDCC 7C 0C DC        |..   inc 0cdc
DDCF C6 04           ..    ldab #04
DDD1 F7 00 48        ..H   stb 0048
goto69:
DDD4 39              9     rts 

jump120:
DDD5 BD BE 69        ..i   jsr be69		;jump3
DDD8 C6 03           ..    ldab #03
DDDA F7 00 1C        ...   stb 001c
DDDD F6 00 1C        ...   ldab 001c
DDE0 C1 0D           ..    cmpb #0d
DDE2 24 1C           $.    bcc 1c
DDE4 F6 00 1C        ...   ldab 001c
DDE7 4F              O     clra 
DDE8 C3 0C B9        ...   addd #0cb9
DDEB 8F              .     xgdx 
DDEC E6 00           ..    ldab 00,x
DDEE 37              7     pshb 
DDEF CC 0B 9C        ...   ldd #0b9c
DDF2 F0 00 1C        ...   subb 001c
DDF5 82 00           ..    sbca #00
DDF7 8F              .     xgdx 
DDF8 33              3     pulb 
DDF9 E7 00           ..    stb 00,x
DDFB 7C 00 1C        |..   inc 001c
DDFE 20 DD            .    bra dd
DE00 BD BE 10        ...   jsr be10		;jump88
DE03 C6 14           ..    ldab #14
DE05 F7 00 2C        ..,   stb 002c
DE08 39              9     rts 

dump119:
DE09 CC 0B 9A        ...   ldd #0b9a
DE0C BD C0 BA        ...   jsr c0ba		;display:
DE0F C6 14           ..    ldab #14
DE11 F7 00 2C        ..,   stb 002c
DE14 39              9     rts 

dump118:
DE15 F6 00 1B        ...   ldab 001b
DE18 C4 01           ..    andb #01
DE1A 26 03           &.    bne 03
DE1C 7E DE A4        ~..   jmp dea4		;goto70
DE1F F6 00 06        ...   ldab 0006
DE22 4F              O     clra 
DE23 05              .     asld 
DE24 C3 08 C8        ...   addd #08c8
DE27 8F              .     xgdx 
DE28 EC 00           ..    ldd 00,x
DE2A 84 08           ..    anda #08
DE2C 27 76           'v    beq 76
DE2E F6 00 11        ...   ldab 0011
DE31 C4 08           ..    andb #08
DE33 26 33           &3    bne 33
DE35 F6 00 06        ...   ldab 0006
DE38 F1 0B AF        ...   cmpb 0baf
DE3B 27 20           '     beq 20
DE3D F6 00 06        ...   ldab 0006
DE40 F1 0B AE        ...   cmpb 0bae
DE43 27 18           '.    beq 18
DE45 F6 00 06        ...   ldab 0006
DE48 F1 0B AD        ...   cmpb 0bad
DE4B 27 10           '.    beq 10
DE4D F6 00 06        ...   ldab 0006
DE50 F1 0B AC        ...   cmpb 0bac
DE53 27 08           '.    beq 08
DE55 F6 00 06        ...   ldab 0006
DE58 F1 0B AB        ...   cmpb 0bab
DE5B 26 0B           &.    bne 0b
DE5D CE 00 1B        ...   ldx #001b
DE60 1D 00           ..    bclr add,x 00,x
DE62 01              .     nop 
DE63 7F 00 4F        ..O   clr 004f
DE66 20 3C            <    bra 3c
DE68 FC 00 44        ..D   ldd 0044
DE6B B3 0B B7        ...   subd 0bb7
DE6E 23 2B           #+    bls 2b
DE70 FC 00 40        ..@   ldd 0040
DE73 F3 00 3E        ..>   addd 003e
DE76 37              7     pshb 
DE77 36              6     psha 
DE78 FC 00 44        ..D   ldd 0044
DE7B B3 0B B7        ...   subd 0bb7
DE7E F3 00 44        ..D   addd 0044
DE81 30              0     tsx 
DE82 A3 00           ..    subd 00,x
DE84 31              1     ins 
DE85 31              1     ins 
DE86 22 13           ".    bhi 13
DE88 FC 0B B7        ...   ldd 0bb7
DE8B 27 0E           '.    beq 0e
DE8D FC 00 44        ..D   ldd 0044
DE90 B3 0B B7        ...   subd 0bb7
DE93 F3 00 44        ..D   addd 0044
DE96 FD 00 44        ..D   stad 0044
DE99 20 09            .    bra 09
DE9B CE 00 1B        ...   ldx #001b
DE9E 1D 00           ..    bclr add,x 00,x
DEA0 01              .     nop 
DEA1 7F 00 4F        ..O   clr 004f
goto70:
DEA4 39              9     rts 

dump117:
DEA5 F6 00 2B        ..+   ldab 002b
DEA8 C1 00           ..    cmpb #00
DEAA 26 0D           &.    bne 0d
DEAC CE 00 12        ...   ldx #0012
DEAF 3C              <     pshx 
DEB0 CC AE 7E        ..~   ldd #ae7e
DEB3 BD C0 2E        ...   jsr c02e		;jump71
DEB6 38              8     pulx 
DEB7 20 2D            -    bra 2d
DEB9 C1 01           ..    cmpb #01
DEBB 26 0D           &.    bne 0d
DEBD CE 00 0F        ...   ldx #000f
DEC0 3C              <     pshx 
DEC1 CC AE 6D        ..m   ldd #ae6d
DEC4 BD C0 2E        ...   jsr c02e		;jump71
DEC7 38              8     pulx 
DEC8 20 1C            .    bra 1c
DECA C1 02           ..    cmpb #02
DECC 26 0D           &.    bne 0d
DECE CE 00 10        ...   ldx #0010
DED1 3C              <     pshx 
DED2 CC AE 5B        ..[   ldd #ae5b
DED5 BD C0 2E        ...   jsr c02e		;jump71
DED8 38              8     pulx 
DED9 20 0B            .    bra 0b
DEDB CE 00 15        ...   ldx #0015
DEDE 3C              <     pshx 
DEDF CC AE 44        ..D   ldd #ae44
DEE2 BD C0 2E        ...   jsr c02e		;jump71
DEE5 38              8     pulx 
DEE6 39              9     rts 

jump116:
DEE7 F6 0B C0        ...   ldab 0bc0
DEEA C4 01           ..    andb #01
DEEC 26 07           &.    bne 07
DEEE F6 0B C0        ...   ldab 0bc0
DEF1 C4 04           ..    andb #04
DEF3 27 15           '.    beq 15
DEF5 C6 FF           ..    ldab #ff
DEF7 F7 0B BC        ...   stb 0bbc
DEFA F6 00 15        ...   ldab 0015
DEFD 2D 03           -.    blt 03
DEFF BD C6 7D        ..}   jsr c67d		;jump124
DF02 BD D6 6B        ..k   jsr d66b		;jump107
DF05 BD DC A3        ...   jsr dca3		;jump29
DF08 20 25            %    bra 25
DF0A F6 0B C0        ...   ldab 0bc0
DF0D C4 02           ..    andb #02
DF0F 27 1E           '.    beq 1e
DF11 F6 00 2F        ../   ldab 002f
DF14 26 19           &.    bne 19
DF16 F6 00 1B        ...   ldab 001b
DF19 C4 02           ..    andb #02
DF1B 26 12           &.    bne 12
DF1D CC 00 FF        ...   ldd #00ff
DF20 BD C7 E7        ...   jsr c7e7		;jump37
DF23 CE 00 1B        ...   ldx #001b
DF26 1C 00           ..    bset add,x 00,x
DF28 02              .     idiv 
DF29 CE 0B C1        ...   ldx #0bc1
DF2C 1C 00           ..    bset add,x 00,x
DF2E 04              .     lsrd 
DF2F F6 00 21        ..!   ldab 0021
DF32 C4 20           .     andb #20
DF34 27 07           '.    beq 07
DF36 BD D6 52        ..R   jsr d652		;jump5
DF39 BD DE A5        ...   jsr dea5		;jump117
DF3C 39              9     rts 

DF3D F6 00 4F        ..O   ldab 004f
DF40 26 06           &.    bne 06
DF42 CE 00 1B        ...   ldx #001b
DF45 1D 00           ..    bclr add,x 00,x
DF47 01              .     nop 
DF48 F6 00 25        ..%   ldab 0025
DF4B 26 20           &     bne 20
DF4D F6 00 1B        ...   ldab 001b
DF50 C4 40           .@    andb #40
DF52 27 19           '.    beq 19
DF54 BD D6 52        ..R   jsr d652		;jump5
DF57 FC 00 40        ..@   ldd 0040
DF5A 26 05           &.    bne 05
DF5C FC 00 3E        ..>   ldd 003e
DF5F 27 03           '.    beq 03
DF61 BD DB 44        ..D   jsr db44		;jump104
DF64 BD D6 6B        ..k   jsr d66b		;jump107
DF67 CE 00 1B        ...   ldx #001b
DF6A 1D 00           ..    bclr add,x 00,x
DF6C 40              @     nega 
DF6D F6 00 2C        ..,   ldab 002c
DF70 27 03           '.    beq 03
DF72 7E E0 66        ~.f   jmp e066		;goto71
DF75 F6 00 34        ..4   ldab 0034
DF78 26 0B           &.    bne 0b
DF7A BD BE 69        ..i   jsr be69		;jump3
DF7D C6 32           .2    ldab #32
DF7F F7 00 34        ..4   stb 0034
DF82 7E E0 63        ~.c   jmp e063		;goto72
DF85 FC 00 40        ..@   ldd 0040
DF88 26 05           &.    bne 05
DF8A FC 00 3E        ..>   ldd 003e
DF8D 27 16           '.    beq 16
DF8F CE 00 01        ...   ldx #0001
DF92 3C              <     pshx 
DF93 FC 00 40        ..@   ldd 0040
DF96 F3 00 3E        ..>   addd 003e
DF99 BD BE 9E        ...   jsr be9e		;jump50
DF9C 38              8     pulx 
DF9D C6 05           ..    ldab #05
DF9F F7 00 2C        ..,   stb 002c
DFA2 7E E0 63        ~.c   jmp e063		;goto72
DFA5 F6 0C D9        ...   ldab 0cd9
DFA8 C1 04           ..    cmpb #04
DFAA 24 07           $.    bcc 07
DFAC F6 0B C0        ...   ldab 0bc0
DFAF C4 01           ..    andb #01
DFB1 26 07           &.    bne 07
DFB3 F6 00 21        ..!   ldab 0021
DFB6 C4 40           .@    andb #40
DFB8 27 43           'C    beq 43
DFBA F6 00 2B        ..+   ldab 002b
DFBD C1 00           ..    cmpb #00
DFBF 26 0D           &.    bne 0d
DFC1 CE 00 19        ...   ldx #0019
DFC4 3C              <     pshx 
DFC5 CC AD E7        ...   ldd #ade7
DFC8 BD C0 2E        ...   jsr c02e		;jump71
DFCB 38              8     pulx 
DFCC 20 2D            -    bra 2d
DFCE C1 01           ..    cmpb #01
DFD0 26 0D           &.    bne 0d
DFD2 CE 00 12        ...   ldx #0012
DFD5 3C              <     pshx 
DFD6 CC AD D3        ...   ldd #add3
DFD9 BD C0 2E        ...   jsr c02e		;jump71
DFDC 38              8     pulx 
DFDD 20 1C            .    bra 1c
DFDF C1 02           ..    cmpb #02
DFE1 26 0D           &.    bne 0d
DFE3 CE 00 19        ...   ldx #0019
DFE6 3C              <     pshx 
DFE7 CC AD B8        ...   ldd #adb8
DFEA BD C0 2E        ...   jsr c02e		;jump71
DFED 38              8     pulx 
DFEE 20 0B            .    bra 0b
DFF0 CE 00 11        ...   ldx #0011
DFF3 3C              <     pshx 
DFF4 CC AD A5        ...   ldd #ada5
DFF7 BD C0 2E        ...   jsr c02e		;jump71
DFFA 38              8     pulx 
DFFB 20 66            f    bra 66
DFFD F6 0B C0        ...   ldab 0bc0
E000 C4 08           ..    andb #08
E002 27 52           'R    beq 52
E004 F6 0B C1        ...   ldab 0bc1
E007 C4 08           ..    andb #08
E009 26 08           &.    bne 08
E00B FC 0B EF        ...   ldd 0bef
E00E 83 00 64        ..d   subd #0064
E011 24 43           $C    bcc 43
E013 F6 00 2B        ..+   ldab 002b
E016 C1 00           ..    cmpb #00
E018 26 0D           &.    bne 0d
E01A CE 00 12        ...   ldx #0012
E01D 3C              <     pshx 
E01E CC AE 30        ..0   ldd #ae30
E021 BD C0 2E        ...   jsr c02e		;jump71
E024 38              8     pulx 
E025 20 2D            -    bra 2d
E027 C1 01           ..    cmpb #01
E029 26 0D           &.    bne 0d
E02B CE 00 0B        ...   ldx #000b
E02E 3C              <     pshx 
E02F CC AE 23        ..#   ldd #ae23
E032 BD C0 2E        ...   jsr c02e		;jump71
E035 38              8     pulx 
E036 20 1C            .    bra 1c
E038 C1 02           ..    cmpb #02
E03A 26 0D           &.    bne 0d
E03C CE 00 12        ...   ldx #0012
E03F 3C              <     pshx 
E040 CC AE 0F        ...   ldd #ae0f
E043 BD C0 2E        ...   jsr c02e		;jump71
E046 38              8     pulx 
E047 20 0B            .    bra 0b
E049 CE 00 0B        ...   ldx #000b
E04C 3C              <     pshx 
E04D CC AE 02        ...   ldd #ae02
E050 BD C0 2E        ...   jsr c02e		;jump71
E053 38              8     pulx 
E054 20 0D            .    bra 0d
E056 F6 0B BF        ...   ldab 0bbf
E059 4F              O     clra 
E05A 37              7     pshb 
E05B 36              6     psha 
E05C CC 09 93        ...   ldd #0993
E05F BD C0 2E        ...   jsr c02e		;jump71
E062 38              8     pulx 
goto72:
E063 7E E5 6A        ~.j   jmp e56a		;goto73
goto71:
E066 F6 00 06        ...   ldab 0006
E069 26 03           &.    bne 03
E06B 7E E5 6A        ~.j   jmp e56a		;goto73
E06E CE 00 1D        ...   ldx #001d
E071 1C 00           ..    bset add,x 00,x
E073 40              @     nega 
E074 BD E7 C7        ...   jsr e7c7		;jump114
E077 F6 00 06        ...   ldab 0006
E07A C1 59           .Y    cmpb #59
E07C 26 06           &.    bne 06
E07E BD DE 09        ...   jsr de09		;jump119
E081 7E E5 5E        ~.^   jmp e55e		;goto74
E084 F6 00 06        ...   ldab 0006
E087 BD D4 68        ..h   jsr d468		;jump51
E08A 83 00 00        ...   subd #0000
E08D 27 18           '.    beq 18
E08F BD D5 61        ..a   jsr d561		;jump42
E092 83 00 00        ...   subd #0000
E095 26 10           &.    bne 10
E097 BD D3 46        ..F   jsr d346		;jump43
E09A 83 00 00        ...   subd #0000
E09D 26 08           &.    bne 08
E09F BD D7 83        ...   jsr d783		;jump44
E0A2 83 00 00        ...   subd #0000
E0A5 27 09           '.    beq 09
E0A7 BD E5 D1        ...   jsr e5d1		;jump52
E0AA BD D8 E9        ...   jsr d8e9		;jump92
E0AD 7E E5 5E        ~.^   jmp e55e		;goto74
E0B0 F6 00 06        ...   ldab 0006
E0B3 4F              O     clra 
E0B4 05              .     asld 
E0B5 C3 08 00        ...   addd #0800
E0B8 8F              .     xgdx 
E0B9 EC 00           ..    ldd 00,x
E0BB C4 FF           ..    andb #ff
E0BD 84 3F           .?    anda #3f
E0BF FD 00 44        ..D   stad 0044
E0C2 F6 00 06        ...   ldab 0006
E0C5 4F              O     clra 
E0C6 05              .     asld 
E0C7 C3 08 C8        ...   addd #08c8
E0CA 8F              .     xgdx 
E0CB EC 00           ..    ldd 00,x
E0CD C4 7F           ..    andb #7f
E0CF 4F              O     clra 
E0D0 FD 00 4D        ..M   stad 004d
E0D3 F6 0B C0        ...   ldab 0bc0
E0D6 2C 05           ,.    bge 05
E0D8 5F              _     clrb 
E0D9 4F              O     clra 
E0DA FD 00 44        ..D   stad 0044
E0DD F6 0B C0        ...   ldab 0bc0
E0E0 C4 02           ..    andb #02
E0E2 26 03           &.    bne 03
E0E4 BD DE 15        ...   jsr de15		;dump118
E0E7 F6 00 11        ...   ldab 0011
E0EA C4 20           .     andb #20
E0EC 26 1A           &.    bne 1a
E0EE F6 0B C0        ...   ldab 0bc0
E0F1 C4 02           ..    andb #02
E0F3 26 13           &.    bne 13
E0F5 FC 00 40        ..@   ldd 0040
E0F8 F3 00 3E        ..>   addd 003e
E0FB B3 00 44        ..D   subd 0044
E0FE 25 08           %.    bcs 08
E100 F6 0B C0        ...   ldab 0bc0
E103 2D 03           -.    blt 03
E105 BD DA 64        ..d   jsr da64		;jump122
E108 FC 00 40        ..@   ldd 0040
E10B F3 00 3E        ..>   addd 003e
E10E B3 00 44        ..D   subd 0044
E111 24 50           $P    bcc 50
E113 F6 0B C0        ...   ldab 0bc0
E116 C4 10           ..    andb #10
E118 26 49           &I    bne 49
E11A F6 00 2B        ..+   ldab 002b
E11D C1 00           ..    cmpb #00
E11F 26 08           &.    bne 08
E121 CC AC C9        ...   ldd #acc9
E124 BD C0 BA        ...   jsr c0ba		;display:
E127 20 1E            .    bra 1e
E129 C1 01           ..    cmpb #01
E12B 26 08           &.    bne 08
E12D CC AC BE        ...   ldd #acbe
E130 BD C0 BA        ...   jsr c0ba		;display:
E133 20 12            .    bra 12
E135 C1 02           ..    cmpb #02
E137 26 08           &.    bne 08
E139 CC AC B3        ...   ldd #acb3
E13C BD C0 BA        ...   jsr c0ba		;display:
E13F 20 06            .    bra 06
E141 CC AC A8        ...   ldd #aca8
E144 BD C0 BA        ...   jsr c0ba		;display:
E147 BD D8 D3        ...   jsr d8d3		;jump84
E14A CE 00 00        ...   ldx #0000
E14D 3C              <     pshx 
E14E FC 00 44        ..D   ldd 0044
E151 BD BE 9E        ...   jsr be9e		;jump50
E154 38              8     pulx 
E155 BD E5 D1        ...   jsr e5d1		;jump52
E158 BD D8 D3        ...   jsr d8d3		;jump84
E15B C6 0C           ..    ldab #0c
E15D F7 00 2C        ..,   stb 002c
E160 7E E5 5E        ~.^   jmp e55e		;goto74
E163 F6 0B C0        ...   ldab 0bc0
E166 C4 01           ..    andb #01
E168 26 11           &.    bne 11
E16A F6 0B C0        ...   ldab 0bc0
E16D C4 04           ..    andb #04
E16F 26 0A           &.    bne 0a
E171 F6 0B C0        ...   ldab 0bc0
E174 C4 0F           ..    andb #0f
E176 27 03           '.    beq 03
E178 7E E3 35        ~.5   jmp e335		;goto75
E17B FC 00 40        ..@   ldd 0040
E17E F3 00 3E        ..>   addd 003e
E181 B3 00 44        ..D   subd 0044
E184 FD 10 16        ...   stad 1016
E187 F6 00 15        ...   ldab 0015
E18A C4 01           ..    andb #01
E18C 27 08           '.    beq 08
E18E FC 10 16        ...   ldd 1016
E191 83 00 64        ..d   subd #0064
E194 25 3C           %<    bcs 3c
E196 F6 00 15        ...   ldab 0015
E199 C4 02           ..    andb #02
E19B 27 08           '.    beq 08
E19D FC 10 16        ...   ldd 1016
E1A0 83 00 C8        ...   subd #00c8
E1A3 25 2D           %-    bcs 2d
E1A5 F6 00 15        ...   ldab 0015
E1A8 C4 04           ..    andb #04
E1AA 27 08           '.    beq 08
E1AC FC 10 16        ...   ldd 1016
E1AF 83 01 F4        ...   subd #01f4
E1B2 25 1E           %.    bcs 1e
E1B4 F6 00 15        ...   ldab 0015
E1B7 C4 08           ..    andb #08
E1B9 27 08           '.    beq 08
E1BB FC 10 16        ...   ldd 1016
E1BE 83 03 E8        ...   subd #03e8
E1C1 25 0F           %.    bcs 0f
E1C3 F6 00 15        ...   ldab 0015
E1C6 C4 10           ..    andb #10
E1C8 27 13           '.    beq 13
E1CA FC 10 16        ...   ldd 1016
E1CD 83 07 D0        ...   subd #07d0
E1D0 24 0B           $.    bcc 0b
E1D2 BD C0 E0        ...   jsr c0e0		;jump94
E1D5 83 00 00        ...   subd #0000
E1D8 26 01           &.    bne 01
E1DA 39              9     rts 

E1DB 20 03            .    bra 03
E1DD BD C5 D3        ...   jsr c5d3		;jump6
E1E0 BD D6 52        ..R   jsr d652		;jump5
E1E3 FC 00 40        ..@   ldd 0040
E1E6 B3 00 44        ..D   subd 0044
E1E9 F3 00 3E        ..>   addd 003e
E1EC FD 00 40        ..@   stad 0040
E1EF 5F              _     clrb 
E1F0 4F              O     clra 
E1F1 FD 00 3E        ..>   stad 003e
E1F4 CE 00 1B        ...   ldx #001b
E1F7 1C 00           ..    bset add,x 00,x
E1F9 80 BD           ..    suba #bd
E1FB D2 13 83        ...   sbcb 13
E1FE 00              .     test 
E1FF 00              .     test 
E200 26 12           &.    bne 12
E202 BD E5 D1        ...   jsr e5d1		;jump52
E205 BD D8 E9        ...   jsr d8e9		;jump92
E208 FC 00 40        ..@   ldd 0040
E20B F3 00 44        ..D   addd 0044
E20E FD 00 40        ..@   stad 0040
E211 7E E3 29        ~.)   jmp e329		;goto92
E214 BD DB CB        ...   jsr dbcb		;jump121
E217 F6 00 1B        ...   ldab 001b
E21A C4 20           .     andb #20
E21C 27 45           'E    beq 45
E21E FC 00 44        ..D   ldd 0044
E221 37              7     pshb 
E222 36              6     psha 
E223 4F              O     clra 
E224 5F              _     clrb 
E225 37              7     pshb 
E226 36              6     psha 
E227 CC 0D FB        ...   ldd #0dfb
E22A BD FB DF        ...   jsr fbdf		;jump46
E22D 31              1     ins 
E22E 31              1     ins 
E22F FE 0D FD        ...   ldx 0dfd
E232 3C              <     pshx 
E233 FC 0D FB        ...   ldd 0dfb
E236 37              7     pshb 
E237 36              6     psha 
E238 CE E1 00        ...   ldx #e100
E23B 3C              <     pshx 
E23C CC 05 F5        ...   ldd #05f5
E23F 37              7     pshb 
E240 36              6     psha 
E241 30              0     tsx 
E242 EC 06           ..    ldd 06,x
E244 A3 02           ..    subd 02,x
E246 EC 04           ..    ldd 04,x
E248 E2 01           ..    sbcb 01,x
E24A A2 00           ..    sbca 00,x
E24C 38              8     pulx 
E24D 38              8     pulx 
E24E 38              8     pulx 
E24F 38              8     pulx 
E250 25 11           %.    bcs 11
E252 CE 1F 00        ...   ldx #1f00
E255 3C              <     pshx 
E256 CC FA 0A        ...   ldd #fa0a
E259 37              7     pshb 
E25A 36              6     psha 
E25B CC 0D FB        ...   ldd #0dfb
E25E BD FB DF        ...   jsr fbdf		;jump46
E261 31              1     ins 
E262 31              1     ins 
E263 F6 0B C0        ...   ldab 0bc0
E266 2C 78           ,x    bge 78
E268 F6 00 06        ...   ldab 0006
E26B 4F              O     clra 
E26C 05              .     asld 
E26D C3 08 00        ...   addd #0800
E270 8F              .     xgdx 
E271 EC 00           ..    ldd 00,x
E273 C4 FF           ..    andb #ff
E275 26 02           &.    bne 02
E277 84 3F           .?    anda #3f
E279 27 65           'e    beq 65
E27B FC 0D 0D        ...   ldd 0d0d
E27E 83 FF FF        ...   subd #ffff
E281 26 05           &.    bne 05
E283 5F              _     clrb 
E284 4F              O     clra 
E285 FD 0D 0D        ...   stad 0d0d
E288 FE 0D 0D        ...   ldx 0d0d
E28B 08              .     inx 
E28C FF 0D 0D        ...   stx 0d0d
E28F F6 00 06        ...   ldab 0006
E292 4F              O     clra 
E293 05              .     asld 
E294 C3 08 00        ...   addd #0800
E297 8F              .     xgdx 
E298 EC 00           ..    ldd 00,x
E29A C4 FF           ..    andb #ff
E29C 84 3F           .?    anda #3f
E29E 37              7     pshb 
E29F 36              6     psha 
E2A0 4F              O     clra 
E2A1 5F              _     clrb 
E2A2 37              7     pshb 
E2A3 36              6     psha 
E2A4 CC 0D 1B        ...   ldd #0d1b
E2A7 BD FB DF        ...   jsr fbdf		;jump46
E2AA 31              1     ins 
E2AB 31              1     ins 
E2AC FE 0D 1D        ...   ldx 0d1d
E2AF 3C              <     pshx 
E2B0 FC 0D 1B        ...   ldd 0d1b
E2B3 37              7     pshb 
E2B4 36              6     psha 
E2B5 CE E1 00        ...   ldx #e100
E2B8 3C              <     pshx 
E2B9 CC 05 F5        ...   ldd #05f5
E2BC 37              7     pshb 
E2BD 36              6     psha 
E2BE 30              0     tsx 
E2BF EC 06           ..    ldd 06,x
E2C1 A3 02           ..    subd 02,x
E2C3 EC 04           ..    ldd 04,x
E2C5 E2 01           ..    sbcb 01,x
E2C7 A2 00           ..    sbca 00,x
E2C9 38              8     pulx 
E2CA 38              8     pulx 
E2CB 38              8     pulx 
E2CC 38              8     pulx 
E2CD 25 11           %.    bcs 11
E2CF CE 1F 00        ...   ldx #1f00
E2D2 3C              <     pshx 
E2D3 CC FA 0A        ...   ldd #fa0a
E2D6 37              7     pshb 
E2D7 36              6     psha 
E2D8 CC 0D 1B        ...   ldd #0d1b
E2DB BD FB DF        ...   jsr fbdf		;jump46
E2DE 31              1     ins 
E2DF 31              1     ins 
E2E0 F6 00 11        ...   ldab 0011
E2E3 C4 10           ..    andb #10
E2E5 26 14           &.    bne 14
E2E7 FC 00 40        ..@   ldd 0040
E2EA 27 0F           '.    beq 0f
E2EC FC 00 40        ..@   ldd 0040
E2EF B3 00 50        ..P   subd 0050
E2F2 24 15           $.    bcc 15
E2F4 F6 00 11        ...   ldab 0011
E2F7 C4 10           ..    andb #10
E2F9 26 0E           &.    bne 0e
E2FB BD DB 44        ..D   jsr db44		;jump104
E2FE CE 00 1B        ...   ldx #001b
E301 1D 00           ..    bclr add,x 00,x
E303 40              @     nega 
E304 7F 00 25        ..%   clr 0025
E307 20 1A            .    bra 1a
E309 7F 00 2C        ..,   clr 002c
E30C CC 01 F4        ...   ldd #01f4
E30F BD E5 71        ..q   jsr e571		;jump13
E312 BD E5 AA        ...   jsr e5aa		;jump12
E315 BD D8 E9        ...   jsr d8e9		;jump92
E318 CE 00 1B        ...   ldx #001b
E31B 1C 00           ..    bset add,x 00,x
E31D 40              @     nega 
E31E C6 0A           ..    ldab #0a
E320 F7 00 25        ..%   stb 0025
E323 BD D7 FF        ...   jsr d7ff		;jump80
E326 BD D8 40        ..@   jsr d840		;jump105
goto92:
E329 BD E8 9A        ...   jsr e89a		;jump68
E32C CE 0B C1        ...   ldx #0bc1
E32F 1D 00           ..    bclr add,x 00,x
E331 08              .     inx 
E332 7E E5 5E        ~.^   jmp e55e		;goto74
goto75:
E335 F6 0B C0        ...   ldab 0bc0
E338 C4 02           ..    andb #02
E33A 26 03           &.    bne 03
E33C 7E E5 5E        ~.^   jmp e55e		;goto74
E33F 5F              _     clrb 
E340 4F              O     clra 
E341 FD 00 76        ..v   stad 0076
E344 CE 00 17        ...   ldx #0017
E347 1D 00           ..    bclr add,x 00,x
E349 01              .     nop 
E34A CE 00 17        ...   ldx #0017
E34D 1D 00           ..    bclr add,x 00,x
E34F 02              .     idiv 
E350 FC 00 44        ..D   ldd 0044
E353 26 1C           &.    bne 1c
E355 F6 0B C0        ...   ldab 0bc0
E358 C4 10           ..    andb #10
E35A 26 08           &.    bne 08
E35C CE 00 21        ..!   ldx #0021
E35F 1C 00           ..    bset add,x 00,x
E361 80 20           .     suba #20
E363 0B              .     sev 
E364 CE 00 21        ..!   ldx #0021
E367 1D 00           ..    bclr add,x 00,x
E369 80 C6           ..    suba #c6
E36B 05              .     asld 
E36C F7 00 75        ..u   stb 0075
E36F 20 57            W    bra 57
E371 F6 0B C0        ...   ldab 0bc0
E374 C4 10           ..    andb #10
E376 27 50           'P    beq 50
E378 FC 00 40        ..@   ldd 0040
E37B 26 4B           &K    bne 4b
E37D F6 0B C0        ...   ldab 0bc0
E380 C4 20           .     andb #20
E382 27 3E           '>    beq 3e
E384 CE 00 17        ...   ldx #0017
E387 1C 00           ..    bset add,x 00,x
E389 01              .     nop 
E38A F6 00 2B        ..+   ldab 002b
E38D C1 00           ..    cmpb #00
E38F 26 08           &.    bne 08
E391 CC AC C9        ...   ldd #acc9
E394 BD C0 BA        ...   jsr c0ba		;display:
E397 20 1E            .    bra 1e
E399 C1 01           ..    cmpb #01
E39B 26 08           &.    bne 08
E39D CC AC BE        ...   ldd #acbe
E3A0 BD C0 BA        ...   jsr c0ba		;display:
E3A3 20 12            .    bra 12
E3A5 C1 02           ..    cmpb #02
E3A7 26 08           &.    bne 08
E3A9 CC AC B3        ...   ldd #acb3
E3AC BD C0 BA        ...   jsr c0ba		;display:
E3AF 20 06            .    bra 06
E3B1 CC AC A8        ...   ldd #aca8
E3B4 BD C0 BA        ...   jsr c0ba		;display:
E3B7 BD D8 D3        ...   jsr d8d3		;jump84
E3BA CE 00 17        ...   ldx #0017
E3BD 1C 00           ..    bset add,x 00,x
E3BF 02              .     idiv 
E3C0 20 06            .    bra 06
E3C2 CE 00 1F        ...   ldx #001f
E3C5 1C 00           ..    bset add,x 00,x
E3C7 08              .     inx 
E3C8 CE 00 1D        ...   ldx #001d
E3CB 1C 00           ..    bset add,x 00,x
E3CD 02              .     idiv 
E3CE CE 00 19        ...   ldx #0019
E3D1 1C 00           ..    bset add,x 00,x
E3D3 08              .     inx 
E3D4 C6 32           .2    ldab #32
E3D6 F7 00 31        ..1   stb 0031
E3D9 C6 19           ..    ldab #19
E3DB F7 00 36        ..6   stb 0036
goto77:
E3DE F6 00 1D        ...   ldab 001d
E3E1 C4 02           ..    andb #02
E3E3 26 03           &.    bne 03
E3E5 7E E4 6E        ~.n   jmp e46e		;goto76
E3E8 F6 00 75        ..u   ldab 0075
E3EB C1 04           ..    cmpb #04
E3ED 22 05           ".    bhi 05
E3EF F6 00 36        ..6   ldab 0036
E3F2 26 3C           &<    bne 3c
E3F4 7F 00 06        ...   clr 0006
E3F7 5F              _     clrb 
E3F8 4F              O     clra 
E3F9 FD 00 76        ..v   stad 0076
E3FC 7F 00 2E        ...   clr 002e
E3FF 7F 00 2C        ..,   clr 002c
E402 7F 00 75        ..u   clr 0075
E405 CE 00 17        ...   ldx #0017
E408 1D 00           ..    bclr add,x 00,x
E40A 01              .     nop 
E40B CE 00 19        ...   ldx #0019
E40E 1D 00           ..    bclr add,x 00,x
E410 08              .     inx 
E411 CE 00 1B        ...   ldx #001b
E414 1D 00           ..    bclr add,x 00,x
E416 80 CE           ..    suba #ce
E418 00              .     test 
E419 1D 1D           ..    bclr add,x 1d,x
E41B 00              .     test 
E41C 40              @     nega 
E41D CE 00 1D        ...   ldx #001d
E420 1D 00           ..    bclr add,x 00,x
E422 02              .     idiv 
E423 CE 00 1F        ...   ldx #001f
E426 1D 00           ..    bclr add,x 00,x
E428 08              .     inx 
E429 CE 00 21        ..!   ldx #0021
E42C 1D 00           ..    bclr add,x 00,x
E42E 80 39           .9    suba #39
E430 F6 0B C0        ...   ldab 0bc0
E433 C4 20           .     andb #20
E435 27 34           '4    beq 34
E437 F6 00 17        ...   ldab 0017
E43A C4 01           ..    andb #01
E43C 26 2D           &-    bne 2d
E43E F6 00 17        ...   ldab 0017
E441 C4 02           ..    andb #02
E443 27 26           '&    beq 26
E445 CE 00 00        ...   ldx #0000
E448 3C              <     pshx 
E449 FC 00 76        ..v   ldd 0076
E44C BD BE 9E        ...   jsr be9e		;jump50
E44F 38              8     pulx 
E450 FC 00 76        ..v   ldd 0076
E453 26 06           &.    bne 06
E455 CE 00 17        ...   ldx #0017
E458 1D 00           ..    bclr add,x 00,x
E45A 02              .     idiv 
E45B F6 00 17        ...   ldab 0017
E45E C4 02           ..    andb #02
E460 27 09           '.    beq 09
E462 BD E5 D1        ...   jsr e5d1		;jump52
E465 CE 00 17        ...   ldx #0017
E468 1D 00           ..    bclr add,x 00,x
E46A 02              .     idiv 
E46B 7E E3 DE        ~..   jmp e3de		;goto77
goto76:
E46E CE 00 21        ..!   ldx #0021
E471 1D 00           ..    bclr add,x 00,x
E473 80 CE           ..    suba #ce
E475 00              .     test 
E476 1F 1D 00 08     ....  brclr 1d,x 00 08
E47A BD D2 13        ...   jsr d213		;jump45
E47D 83 00 00        ...   subd #0000
E480 26 0F           &.    bne 0f
E482 CC 00 80        ...   ldd #0080
E485 BD C7 E7        ...   jsr c7e7		;jump37
E488 BD E5 D1        ...   jsr e5d1		;jump52
E48B BD D8 E9        ...   jsr d8e9		;jump92
E48E 7E E5 5E        ~.^   jmp e55e		;goto74
E491 F6 00 2B        ..+   ldab 002b
E494 C1 00           ..    cmpb #00
E496 26 08           &.    bne 08
E498 CC AD 16        ...   ldd #ad16
E49B BD C0 BA        ...   jsr c0ba		;display:
E49E 20 1E            .    bra 1e
E4A0 C1 01           ..    cmpb #01
E4A2 26 08           &.    bne 08
E4A4 CC AD 0B        ...   ldd #ad0b
E4A7 BD C0 BA        ...   jsr c0ba		;display:
E4AA 20 12            .    bra 12
E4AC C1 02           ..    cmpb #02
E4AE 26 08           &.    bne 08
E4B0 CC AD 00        ...   ldd #ad00
E4B3 BD C0 BA        ...   jsr c0ba		;display:
E4B6 20 06            .    bra 06
E4B8 CC AC F5        ...   ldd #acf5
E4BB BD C0 BA        ...   jsr c0ba		;display:
E4BE BD D8 D3        ...   jsr d8d3		;jump84
E4C1 CE 00 01        ...   ldx #0001
E4C4 3C              <     pshx 
E4C5 FC 00 40        ..@   ldd 0040
E4C8 BD BE 9E        ...   jsr be9e		;jump50
E4CB 38              8     pulx 
E4CC 5F              _     clrb 
E4CD 4F              O     clra 
E4CE BD C7 E7        ...   jsr c7e7		;jump37
E4D1 F6 0B C0        ...   ldab 0bc0
E4D4 2D 03           -.    blt 03
E4D6 7E E5 58        ~.X   jmp e558		;goto78
E4D9 F6 0B C0        ...   ldab 0bc0
E4DC C4 10           ..    andb #10
E4DE 26 78           &x    bne 78
E4E0 F6 00 06        ...   ldab 0006
E4E3 4F              O     clra 
E4E4 05              .     asld 
E4E5 C3 08 00        ...   addd #0800
E4E8 8F              .     xgdx 
E4E9 EC 00           ..    ldd 00,x
E4EB C4 FF           ..    andb #ff
E4ED 26 02           &.    bne 02
E4EF 84 3F           .?    anda #3f
E4F1 27 65           'e    beq 65
E4F3 FC 0D 0D        ...   ldd 0d0d
E4F6 83 FF FF        ...   subd #ffff
E4F9 26 05           &.    bne 05
E4FB 5F              _     clrb 
E4FC 4F              O     clra 
E4FD FD 0D 0D        ...   stad 0d0d
E500 FE 0D 0D        ...   ldx 0d0d
E503 08              .     inx 
E504 FF 0D 0D        ...   stx 0d0d
E507 F6 00 06        ...   ldab 0006
E50A 4F              O     clra 
E50B 05              .     asld 
E50C C3 08 00        ...   addd #0800
E50F 8F              .     xgdx 
E510 EC 00           ..    ldd 00,x
E512 C4 FF           ..    andb #ff
E514 84 3F           .?    anda #3f
E516 37              7     pshb 
E517 36              6     psha 
E518 4F              O     clra 
E519 5F              _     clrb 
E51A 37              7     pshb 
E51B 36              6     psha 
E51C CC 0D 1B        ...   ldd #0d1b
E51F BD FB DF        ...   jsr fbdf		;jump46
E522 31              1     ins 
E523 31              1     ins 
E524 FE 0D 1D        ...   ldx 0d1d
E527 3C              <     pshx 
E528 FC 0D 1B        ...   ldd 0d1b
E52B 37              7     pshb 
E52C 36              6     psha 
E52D CE E1 00        ...   ldx #e100
E530 3C              <     pshx 
E531 CC 05 F5        ...   ldd #05f5
E534 37              7     pshb 
E535 36              6     psha 
E536 30              0     tsx 
E537 EC 06           ..    ldd 06,x
E539 A3 02           ..    subd 02,x
E53B EC 04           ..    ldd 04,x
E53D E2 01           ..    sbcb 01,x
E53F A2 00           ..    sbca 00,x
E541 38              8     pulx 
E542 38              8     pulx 
E543 38              8     pulx 
E544 38              8     pulx 
E545 25 11           %.    bcs 11
E547 CE 1F 00        ...   ldx #1f00
E54A 3C              <     pshx 
E54B CC FA 0A        ...   ldd #fa0a
E54E 37              7     pshb 
E54F 36              6     psha 
E550 CC 0D 1B        ...   ldd #0d1b
E553 BD FB DF        ...   jsr fbdf		;jump46
E556 31              1     ins 
E557 31              1     ins 
goto78:
E558 BD D7 FF        ...   jsr d7ff		;jump80
E55B BD D8 40        ..@   jsr d840		;jump105
goto74:
E55E 7F 00 06        ...   clr 0006
E561 7F 00 2E        ...   clr 002e
E564 CE 00 1B        ...   ldx #001b
E567 1D 00           ..    bclr add,x 00,x
E569 80 CE           ..    suba #ce
;e56a ???? goto73
E56B 00              .     test 
E56C 1D 1D           ..    bclr add,x 1d,x
E56E 00              .     test 
E56F 40              @     nega 
E570 39              9     rts 

;!!! Is this a delay routine?  It gets called between displaying strings 
;on startup.  Looks like a delay.  e61b just stores 0a at 0031 and returns.
jump13:
E571 37              7     pshb 
E572 36              6     psha 
E573 34              4     des 
E574 30              0     tsx 
E575 EC 01           ..    ldd 01,x
E577 83 00 01        ...   subd #0001
E57A ED 01           ..    stad 01,x
E57C 2D 15           -.    blt 15
E57E C6 41           .A    ldab #41
E580 30              0     tsx 
E581 E7 00           ..    stb 00,x
E583 30              0     tsx 
E584 E6 00           ..    ldab 00,x
E586 6A 00           j.    dec 00,x
E588 C1 00           ..    cmpb #00
E58A 23 02           #.    bls 02
E58C 20 F5            .    bra f5
E58E BD E6 1B        ...   jsr e61b		;jump4
E591 20 E1            .    bra e1
E593 38              8     pulx 	;this must just be a standard 'pull 3
E594 31              1     ins 		;bytes and I don't care where they go'
					;to clean up the stack.
E595 39              9     rts 

jump115:
E596 34              4     des 
E597 C6 32           .2    ldab #32
E599 30              0     tsx 
E59A E7 00           ..    stb 00,x
E59C 30              0     tsx 
E59D E6 00           ..    ldab 00,x
E59F C1 00           ..    cmpb #00
E5A1 23 05           #.    bls e5a8
E5A3 30              0     tsx 
E5A4 6A 00           j.    dec 00,x
E5A6 20 F4            .    bra e59c
E5A8 31              1     ins 
E5A9 39              9     rts 

jump12:
E5AA F6 00 33        ..3   ldab 0033
E5AD 27 0C           '.    beq 0c
E5AF F6 00 29        ..)   ldab 0029
E5B2 27 05           '.    beq 05
E5B4 C6 02           ..    ldab #02
E5B6 F7 00 33        ..3   stb 0033
E5B9 20 15            .    bra 15
E5BB C6 03           ..    ldab #03
E5BD F7 00 32        ..2   stb 0032
E5C0 FC 00 3B        ..;   ldd 003b
E5C3 27 06           '.    beq 06
E5C5 CC 02 33        ..3   ldd #0233
E5C8 FD 00 3B        ..;   stad 003b
E5CB C6 02           ..    ldab #02
E5CD F7 00 33        ..3   stb 0033
E5D0 39              9     rts 

jump52:
E5D1 C6 0B           ..    ldab #0b
E5D3 F7 00 32        ..2   stb 0032
E5D6 39              9     rts 

jump69:
E5D7 37              7     pshb 
E5D8 36              6     psha 
E5D9 3C              <     pshx 
E5DA 3C              <     pshx 
E5DB 5F              _     clrb 
E5DC 4F              O     clra 
E5DD 30              0     tsx 
E5DE ED 00           ..    stad 00,x
E5E0 CC 00 64        ..d   ldd #0064
E5E3 ED 02           ..    stad 02,x
E5E5 30              0     tsx 
E5E6 EC 02           ..    ldd 02,x
E5E8 2F 21           /!    ble 21
E5EA 30              0     tsx 
E5EB EE 04           ..    ldx 04,x
E5ED E6 00           ..    ldab 00,x
E5EF 30              0     tsx 
E5F0 E4 09           ..    andb 09,x
E5F2 27 05           '.    beq 05
E5F4 38              8     pulx 
E5F5 08              .     inx 
E5F6 3C              <     pshx 
E5F7 20 08            .    bra 08
E5F9 30              0     tsx 
E5FA EC 00           ..    ldd 00,x
E5FC 83 00 01        ...   subd #0001
E5FF ED 00           ..    stad 00,x
E601 30              0     tsx 
E602 EC 02           ..    ldd 02,x
E604 83 00 01        ...   subd #0001
E607 ED 02           ..    stad 02,x
E609 20 DA            .    bra da
E60B 30              0     tsx 
E60C EC 00           ..    ldd 00,x
E60E 2F 04           /.    ble 04
E610 5F              _     clrb 
E611 4F              O     clra 
E612 20 03            .    bra 03
E614 CC 00 01        ...   ldd #0001
E617 38              8     pulx 
E618 38              8     pulx 
E619 38              8     pulx 
E61A 39              9     rts 

jump4:
E61B C6 0A           ..    ldab #0a
E61D F7 00 31        ..1   stb 0031
E620 39              9     rts 

jump56:
E621 38              8     pulx 
E622 37              7     pshb 
E623 36              6     psha 
E624 3C              <     pshx 
E625 BD BE 4B        ..K   jsr be4b		;jump1
E628 30              0     tsx 
E629 EC 04           ..    ldd 04,x
E62B 37              7     pshb 
E62C 36              6     psha 
E62D EC 02           ..    ldd 02,x
E62F 37              7     pshb 
E630 36              6     psha 
E631 CE 96 80        ...   ldx #9680
E634 3C              <     pshx 
E635 CC 00 98        ...   ldd #0098
E638 BD FC 4C        ..L   jsr fc4c		;jump83
E63B 31              1     ins 
E63C 33              3     pulb 
E63D CB 30           .0    addb #30
E63F F7 0B 97        ...   stb 0b97
E642 30              0     tsx 
E643 EC 04           ..    ldd 04,x
E645 37              7     pshb 
E646 36              6     psha 
E647 EC 02           ..    ldd 02,x
E649 37              7     pshb 
E64A 36              6     psha 
E64B CE 96 80        ...   ldx #9680
E64E 3C              <     pshx 
E64F CC 00 98        ...   ldd #0098
E652 BD FC 5C        ..\   jsr fc5c		;jump82
E655 37              7     pshb 
E656 36              6     psha 
E657 CE 42 40        .B@   ldx #4240
E65A 3C              <     pshx 
E65B CC 00 0F        ...   ldd #000f
E65E BD FC 4C        ..L   jsr fc4c		;jump83
E661 31              1     ins 
E662 33              3     pulb 
E663 CB 30           .0    addb #30
E665 F7 0B 96        ...   stb 0b96
E668 30              0     tsx 
E669 EC 04           ..    ldd 04,x
E66B 37              7     pshb 
E66C 36              6     psha 
E66D EC 02           ..    ldd 02,x
E66F 37              7     pshb 
E670 36              6     psha 
E671 CE 42 40        .B@   ldx #4240
E674 3C              <     pshx 
E675 CC 00 0F        ...   ldd #000f
E678 BD FC 5C        ..\   jsr fc5c		;jump82
E67B 37              7     pshb 
E67C 36              6     psha 
E67D CE 86 A0        ...   ldx #86a0
E680 3C              <     pshx 
E681 CC 00 01        ...   ldd #0001
E684 BD FC 4C        ..L   jsr fc4c		;jump83
E687 31              1     ins 
E688 33              3     pulb 
E689 CB 30           .0    addb #30
E68B F7 0B 95        ...   stb 0b95
E68E 30              0     tsx 
E68F EC 04           ..    ldd 04,x
E691 37              7     pshb 
E692 36              6     psha 
E693 EC 02           ..    ldd 02,x
E695 37              7     pshb 
E696 36              6     psha 
E697 CE 86 A0        ...   ldx #86a0
E69A 3C              <     pshx 
E69B CC 00 01        ...   ldd #0001
E69E BD FC 5C        ..\   jsr fc5c		;jump82
E6A1 37              7     pshb 
E6A2 36              6     psha 
E6A3 CE 27 10        .'.   ldx #2710
E6A6 3C              <     pshx 
E6A7 CC 00 00        ...   ldd #0000
E6AA BD FC 4C        ..L   jsr fc4c		;jump83
E6AD 31              1     ins 
E6AE 33              3     pulb 
E6AF CB 30           .0    addb #30
E6B1 F7 0B 94        ...   stb 0b94
E6B4 30              0     tsx 
E6B5 EC 04           ..    ldd 04,x
E6B7 37              7     pshb 
E6B8 36              6     psha 
E6B9 EC 02           ..    ldd 02,x
E6BB 37              7     pshb 
E6BC 36              6     psha 
E6BD CE 27 10        .'.   ldx #2710
E6C0 3C              <     pshx 
E6C1 CC 00 00        ...   ldd #0000
E6C4 BD FC 5C        ..\   jsr fc5c		;jump82
E6C7 37              7     pshb 
E6C8 36              6     psha 
E6C9 CE 03 E8        ...   ldx #03e8
E6CC 3C              <     pshx 
E6CD CC 00 00        ...   ldd #0000
E6D0 BD FC 4C        ..L   jsr fc4c		;jump83
E6D3 31              1     ins 
E6D4 33              3     pulb 
E6D5 CB 30           .0    addb #30
E6D7 F7 0B 93        ...   stb 0b93
E6DA 30              0     tsx 
E6DB EC 04           ..    ldd 04,x
E6DD 37              7     pshb 
E6DE 36              6     psha 
E6DF EC 02           ..    ldd 02,x
E6E1 37              7     pshb 
E6E2 36              6     psha 
E6E3 CE 03 E8        ...   ldx #03e8
E6E6 3C              <     pshx 
E6E7 CC 00 00        ...   ldd #0000
E6EA BD FC 5C        ..\   jsr fc5c		;jump82
E6ED 37              7     pshb 
E6EE 36              6     psha 
E6EF CE 00 64        ..d   ldx #0064
E6F2 3C              <     pshx 
E6F3 CC 00 00        ...   ldd #0000
E6F6 BD FC 4C        ..L   jsr fc4c		;jump83
E6F9 31              1     ins 
E6FA 33              3     pulb 
E6FB CB 30           .0    addb #30
E6FD F7 0B 92        ...   stb 0b92
E700 30              0     tsx 
E701 EC 04           ..    ldd 04,x
E703 37              7     pshb 
E704 36              6     psha 
E705 EC 02           ..    ldd 02,x
E707 37              7     pshb 
E708 36              6     psha 
E709 CE 00 64        ..d   ldx #0064
E70C 3C              <     pshx 
E70D CC 00 00        ...   ldd #0000
E710 BD FC 5C        ..\   jsr fc5c		;jump82
E713 37              7     pshb 
E714 36              6     psha 
E715 CE 00 0A        ...   ldx #000a
E718 3C              <     pshx 
E719 CC 00 00        ...   ldd #0000
E71C BD FC 4C        ..L   jsr fc4c		;jump83
E71F 31              1     ins 
E720 33              3     pulb 
E721 CB 30           .0    addb #30
E723 F7 0B 91        ...   stb 0b91
E726 30              0     tsx 
E727 EC 04           ..    ldd 04,x
E729 37              7     pshb 
E72A 36              6     psha 
E72B EC 02           ..    ldd 02,x
E72D 37              7     pshb 
E72E 36              6     psha 
E72F CE 00 0A        ...   ldx #000a
E732 3C              <     pshx 
E733 CC 00 00        ...   ldd #0000
E736 BD FC 5C        ..\   jsr fc5c		;jump82
E739 31              1     ins 
E73A 33              3     pulb 
E73B CB 30           .0    addb #30
E73D F7 0B 90        ...   stb 0b90
E740 38              8     pulx 
E741 31              1     ins 
E742 31              1     ins 
E743 6E 00           n.    jmp 00,x

jump55:
E745 37              7     pshb 
E746 36              6     psha 
E747 30              0     tsx 
E748 EC 00           ..    ldd 00,x
E74A CE 27 10        .'.   ldx #2710
E74D 02              .     idiv 
E74E 8F              .     xgdx 
E74F CB 30           .0    addb #30
E751 F7 00 57        ..W   stb 0057
E754 30              0     tsx 
E755 EC 00           ..    ldd 00,x
E757 CE 27 10        .'.   ldx #2710
E75A 02              .     idiv 
E75B CE 03 E8        ...   ldx #03e8
E75E 02              .     idiv 
E75F 8F              .     xgdx 
E760 CB 30           .0    addb #30
E762 F7 00 5A        ..Z   stb 005a
E765 30              0     tsx 
E766 EC 00           ..    ldd 00,x
E768 CE 03 E8        ...   ldx #03e8
E76B 02              .     idiv 
E76C CE 00 64        ..d   ldx #0064
E76F 02              .     idiv 
E770 8F              .     xgdx 
E771 CB 30           .0    addb #30
E773 F7 00 5D        ..]   stb 005d
E776 30              0     tsx 
E777 EC 00           ..    ldd 00,x
E779 CE 00 64        ..d   ldx #0064
E77C 02              .     idiv 
E77D CE 00 0A        ...   ldx #000a
E780 02              .     idiv 
E781 8F              .     xgdx 
E782 CB 30           .0    addb #30
E784 F7 00 5E        ..^   stb 005e
E787 30              0     tsx 
E788 EC 00           ..    ldd 00,x
E78A CE 00 0A        ...   ldx #000a
E78D 02              .     idiv 
E78E CB 30           .0    addb #30
E790 F7 00 5F        .._   stb 005f
E793 38              8     pulx 
E794 39              9     rts 

jump54:
E795 37              7     pshb 
E796 36              6     psha 
E797 30              0     tsx 
E798 E6 01           ..    ldab 01,x
E79A 4F              O     clra 
E79B CE 00 64        ..d   ldx #0064
E79E 02              .     idiv 
E79F 8F              .     xgdx 
E7A0 CB 30           .0    addb #30
E7A2 F7 00 5D        ..]   stb 005d
E7A5 30              0     tsx 
E7A6 E6 01           ..    ldab 01,x
E7A8 4F              O     clra 
E7A9 CE 00 64        ..d   ldx #0064
E7AC 02              .     idiv 
E7AD 4F              O     clra 
E7AE CE 00 0A        ...   ldx #000a
E7B1 02              .     idiv 
E7B2 8F              .     xgdx 
E7B3 CB 30           .0    addb #30
E7B5 F7 00 5E        ..^   stb 005e
E7B8 30              0     tsx 
E7B9 E6 01           ..    ldab 01,x
E7BB 4F              O     clra 
E7BC CE 00 0A        ...   ldx #000a
E7BF 02              .     idiv 
E7C0 CB 30           .0    addb #30
E7C2 F7 00 5F        .._   stb 005f
E7C5 38              8     pulx 
E7C6 39              9     rts 

jump114:
E7C7 F6 00 06        ...   ldab 0006
E7CA 4F              O     clra 
E7CB CE 00 05        ...   ldx #0005
E7CE 02              .     idiv 
E7CF 5D              ]     tstb 
E7D0 26 03           &.    bne 03
E7D2 7E E8 99        ~..   jmp e899		;goto79
E7D5 F6 00 06        ...   ldab 0006
E7D8 BD D7 A0        ...   jsr d7a0		;jump10
E7DB 83 00 00        ...   subd #0000
E7DE 27 03           '.    beq 03
E7E0 7E E8 99        ~..   jmp e899		;goto79
E7E3 F6 00 06        ...   ldab 0006
E7E6 4F              O     clra 
E7E7 05              .     asld 
E7E8 C3 08 00        ...   addd #0800
E7EB 8F              .     xgdx 
E7EC EC 00           ..    ldd 00,x
E7EE C4 FF           ..    andb #ff
E7F0 84 3F           .?    anda #3f
E7F2 FD 10 16        ...   stad 1016
E7F5 F6 00 06        ...   ldab 0006
E7F8 4F              O     clra 
E7F9 05              .     asld 
E7FA C3 0B F1        ...   addd #0bf1
E7FD 8F              .     xgdx 
E7FE EC 00           ..    ldd 00,x
E800 C4 FF           ..    andb #ff
E802 84 3F           .?    anda #3f
E804 FD 10 18        ...   stad 1018
E807 FC 10 16        ...   ldd 1016
E80A 83 00 00        ...   subd #0000
E80D 25 08           %.    bcs 08
E80F FC 10 16        ...   ldd 1016
E812 83 27 0F        .'.   subd #270f
E815 23 35           #5    bls 35
E817 F6 00 06        ...   ldab 0006
E81A BD D4 68        ..h   jsr d468		;jump51
E81D 83 00 00        ...   subd #0000
E820 27 2A           '*    beq 2a
E822 CC 01 00        ...   ldd #0100
E825 37              7     pshb 
E826 36              6     psha 
E827 F6 00 06        ...   ldab 0006
E82A 4F              O     clra 
E82B 05              .     asld 
E82C C3 08 C8        ...   addd #08c8
E82F 38              8     pulx 
E830 8F              .     xgdx 
E831 AA 00           ..    ora 00,x
E833 EA 01           ..    orb 01,x
E835 ED 00           ..    stad 00,x
E837 CC 40 00        .@.   ldd #4000
E83A 37              7     pshb 
E83B 36              6     psha 
E83C F6 00 06        ...   ldab 0006
E83F 4F              O     clra 
E840 05              .     asld 
E841 C3 08 00        ...   addd #0800
E844 38              8     pulx 
E845 8F              .     xgdx 
E846 AA 00           ..    ora 00,x
E848 EA 01           ..    orb 01,x
E84A ED 00           ..    stad 00,x
E84C CE 10 16        ...   ldx #1016
E84F 1D 00           ..    bclr add,x 00,x
E851 C0 FC           ..    subb #fc
E853 10              .     sba 
E854 18              .     illegal 
E855 43              C     coma 
E856 53              S     comb 
E857 FD 10 18        ...   stad 1018
E85A CE 10 18        ...   ldx #1018
E85D 1D 00           ..    bclr add,x 00,x
E85F C0 FC           ..    subb #fc
E861 10              .     sba 
E862 16              .     tab 
E863 37              7     pshb 
E864 36              6     psha 
E865 FC 10 18        ...   ldd 1018
E868 30              0     tsx 
E869 A3 00           ..    subd 00,x
E86B 31              1     ins 
E86C 31              1     ins 
E86D 27 2A           '*    beq 2a
E86F CC 01 00        ...   ldd #0100
E872 37              7     pshb 
E873 36              6     psha 
E874 F6 00 06        ...   ldab 0006
E877 4F              O     clra 
E878 05              .     asld 
E879 C3 08 C8        ...   addd #08c8
E87C 38              8     pulx 
E87D 8F              .     xgdx 
E87E AA 00           ..    ora 00,x
E880 EA 01           ..    orb 01,x
E882 ED 00           ..    stad 00,x
E884 CC 40 00        .@.   ldd #4000
E887 37              7     pshb 
E888 36              6     psha 
E889 F6 00 06        ...   ldab 0006
E88C 4F              O     clra 
E88D 05              .     asld 
E88E C3 08 00        ...   addd #0800
E891 38              8     pulx 
E892 8F              .     xgdx 
E893 AA 00           ..    ora 00,x
E895 EA 01           ..    orb 01,x
E897 ED 00           ..    stad 00,x
goto79:
E899 39              9     rts 

jump68:
E89A 5F              _     clrb 
E89B 4F              O     clra 
E89C FD 00 0F        ...   stad 000f
E89F CC 3F FF        .?.   ldd #3fff
E8A2 FD 00 50        ..P   stad 0050
E8A5 7F 00 06        ...   clr 0006
E8A8 F6 00 06        ...   ldab 0006
E8AB C1 64           .d    cmpb #64
E8AD 24 64           $d    bcc 64
E8AF F6 00 06        ...   ldab 0006
E8B2 4F              O     clra 
E8B3 CE 00 05        ...   ldx #0005
E8B6 02              .     idiv 
E8B7 5D              ]     tstb 
E8B8 27 54           'T    beq 54
E8BA F6 00 06        ...   ldab 0006
E8BD BD D7 A0        ...   jsr d7a0		;jump10
E8C0 83 00 00        ...   subd #0000
E8C3 26 49           &I    bne 49
E8C5 F6 00 06        ...   ldab 0006
E8C8 4F              O     clra 
E8C9 05              .     asld 
E8CA C3 08 00        ...   addd #0800
E8CD 8F              .     xgdx 
E8CE EC 00           ..    ldd 00,x
E8D0 C4 FF           ..    andb #ff
E8D2 84 3F           .?    anda #3f
E8D4 FD 10 16        ...   stad 1016
E8D7 FC 10 16        ...   ldd 1016
E8DA 83 27 10        .'.   subd #2710
E8DD 24 2F           $/    bcc 2f
E8DF F6 00 06        ...   ldab 0006
E8E2 BD D4 68        ..h   jsr d468		;jump51
E8E5 83 00 00        ...   subd #0000
E8E8 27 24           '$    beq 24
E8EA BD D5 61        ..a   jsr d561		;jump42
E8ED 83 00 00        ...   subd #0000
E8F0 26 1C           &.    bne 1c
E8F2 FC 10 16        ...   ldd 1016
E8F5 B3 00 0F        ...   subd 000f
E8F8 23 06           #.    bls 06
E8FA FC 10 16        ...   ldd 1016
E8FD FD 00 0F        ...   stad 000f
E900 FC 10 16        ...   ldd 1016
E903 B3 00 50        ..P   subd 0050
E906 24 06           $.    bcc 06
E908 FC 10 16        ...   ldd 1016
E90B FD 00 50        ..P   stad 0050
E90E 7C 00 06        |..   inc 0006
E911 20 95            .    bra 95
E913 FC 0B B7        ...   ldd 0bb7
E916 27 12           '.    beq 12
E918 FC 00 0F        ...   ldd 000f
E91B B3 0B B7        ...   subd 0bb7
E91E 23 0A           #.    bls 0a
E920 FC 00 0F        ...   ldd 000f
E923 05              .     asld 
E924 B3 0B B7        ...   subd 0bb7
E927 FD 00 0F        ...   stad 000f
E92A 39              9     rts 

jump103:
E92B 34              4     des 
E92C 30              0     tsx 
E92D 6F 00           o.    clr 00,x
E92F 30              0     tsx 
E930 E6 00           ..    ldab 00,x
E932 C1 64           .d    cmpb #64
E934 24 15           $.    bcc 15
E936 5F              _     clrb 
E937 4F              O     clra 
E938 37              7     pshb 
E939 36              6     psha 
E93A 30              0     tsx 
E93B E6 02           ..    ldab 02,x
E93D 4F              O     clra 
E93E 05              .     asld 
E93F C3 0D 1F        ...   addd #0d1f
E942 38              8     pulx 
E943 8F              .     xgdx 
E944 ED 00           ..    stad 00,x
E946 30              0     tsx 
E947 6C 00           l.    inc 00,x
E949 20 E4            .    bra e4
E94B 4F              O     clra 
E94C 5F              _     clrb 
E94D FD 0D F9        ...   stad 0df9
E950 FD 0D F7        ...   stad 0df7
E953 4F              O     clra 
E954 5F              _     clrb 
E955 FD 0D F5        ...   stad 0df5
E958 FD 0D F3        ...   stad 0df3
E95B 4F              O     clra 
E95C 5F              _     clrb 
E95D FD 0D F1        ...   stad 0df1
E960 FD 0D EF        ...   stad 0def
E963 4F              O     clra 
E964 5F              _     clrb 
E965 FD 0D FD        ...   stad 0dfd
E968 FD 0D FB        ...   stad 0dfb
E96B 4F              O     clra 
E96C 5F              _     clrb 
E96D FD 0D E9        ...   stad 0de9
E970 FD 0D E7        ...   stad 0de7
E973 5F              _     clrb 
E974 4F              O     clra 
E975 FD 0C DD        ...   stad 0cdd
E978 5F              _     clrb 
E979 4F              O     clra 
E97A FD 0C DF        ...   stad 0cdf
E97D 5F              _     clrb 
E97E 4F              O     clra 
E97F FD 0C E1        ...   stad 0ce1
E982 5F              _     clrb 
E983 4F              O     clra 
E984 FD 0C E3        ...   stad 0ce3
E987 5F              _     clrb 
E988 4F              O     clra 
E989 FD 0C E5        ...   stad 0ce5
E98C 4F              O     clra 
E98D 5F              _     clrb 
E98E FD 0C F5        ...   stad 0cf5
E991 FD 0C F3        ...   stad 0cf3
E994 4F              O     clra 
E995 5F              _     clrb 
E996 FD 0C F9        ...   stad 0cf9
E999 FD 0C F7        ...   stad 0cf7
E99C 4F              O     clra 
E99D 5F              _     clrb 
E99E FD 0C FD        ...   stad 0cfd
E9A1 FD 0C FB        ...   stad 0cfb
E9A4 4F              O     clra 
E9A5 5F              _     clrb 
E9A6 FD 0D 01        ...   stad 0d01
E9A9 FD 0C FF        ...   stad 0cff
E9AC 4F              O     clra 
E9AD 5F              _     clrb 
E9AE FD 0D 05        ...   stad 0d05
E9B1 FD 0D 03        ...   stad 0d03
E9B4 5F              _     clrb 
E9B5 4F              O     clra 
E9B6 FD 0D 07        ...   stad 0d07
E9B9 5F              _     clrb 
E9BA 4F              O     clra 
E9BB FD 0D 09        ...   stad 0d09
E9BE 5F              _     clrb 
E9BF 4F              O     clra 
E9C0 FD 0D 0B        ...   stad 0d0b
E9C3 5F              _     clrb 
E9C4 4F              O     clra 
E9C5 FD 0D 0D        ...   stad 0d0d
E9C8 4F              O     clra 
E9C9 5F              _     clrb 
E9CA FD 0D 11        ...   stad 0d11
E9CD FD 0D 0F        ...   stad 0d0f
E9D0 4F              O     clra 
E9D1 5F              _     clrb 
E9D2 FD 0D 15        ...   stad 0d15
E9D5 FD 0D 13        ...   stad 0d13
E9D8 4F              O     clra 
E9D9 5F              _     clrb 
E9DA FD 0D 19        ...   stad 0d19
E9DD FD 0D 17        ...   stad 0d17
E9E0 4F              O     clra 
E9E1 5F              _     clrb 
E9E2 FD 0D 1D        ...   stad 0d1d
E9E5 FD 0D 1B        ...   stad 0d1b
E9E8 31              1     ins 
E9E9 39              9     rts 

jump113:
E9EA 7F 00 06        ...   clr 0006
E9ED F6 00 06        ...   ldab 0006
E9F0 C1 64           .d    cmpb #64
E9F2 24 64           $d    bcc 64
E9F4 F6 00 06        ...   ldab 0006
E9F7 4F              O     clra 
E9F8 CE 00 05        ...   ldx #0005
E9FB 02              .     idiv 
E9FC 5D              ]     tstb 
E9FD 27 54           'T    beq 54
E9FF CC BF FF        ...   ldd #bfff
EA02 37              7     pshb 
EA03 36              6     psha 
EA04 F6 00 06        ...   ldab 0006
EA07 4F              O     clra 
EA08 05              .     asld 
EA09 C3 08 00        ...   addd #0800
EA0C 38              8     pulx 
EA0D 8F              .     xgdx 
EA0E A4 00           ..    anda 00,x
EA10 E4 01           ..    andb 01,x
EA12 ED 00           ..    stad 00,x
EA14 CC FE FF        ...   ldd #feff
EA17 37              7     pshb 
EA18 36              6     psha 
EA19 F6 00 06        ...   ldab 0006
EA1C 4F              O     clra 
EA1D 05              .     asld 
EA1E C3 08 C8        ...   addd #08c8
EA21 38              8     pulx 
EA22 8F              .     xgdx 
EA23 A4 00           ..    anda 00,x
EA25 E4 01           ..    andb 01,x
EA27 ED 00           ..    stad 00,x
EA29 CC FD FF        ...   ldd #fdff
EA2C 37              7     pshb 
EA2D 36              6     psha 
EA2E F6 00 06        ...   ldab 0006
EA31 4F              O     clra 
EA32 05              .     asld 
EA33 C3 08 C8        ...   addd #08c8
EA36 38              8     pulx 
EA37 8F              .     xgdx 
EA38 A4 00           ..    anda 00,x
EA3A E4 01           ..    andb 01,x
EA3C ED 00           ..    stad 00,x
EA3E CC FB FF        ...   ldd #fbff
EA41 37              7     pshb 
EA42 36              6     psha 
EA43 F6 00 06        ...   ldab 0006
EA46 4F              O     clra 
EA47 05              .     asld 
EA48 C3 08 C8        ...   addd #08c8
EA4B 38              8     pulx 
EA4C 8F              .     xgdx 
EA4D A4 00           ..    anda 00,x
EA4F E4 01           ..    andb 01,x
EA51 ED 00           ..    stad 00,x
EA53 7C 00 06        |..   inc 0006
EA56 20 95            .    bra 95
EA58 39              9     rts 

jump65:
EA59 34              4     des 
EA5A CE 00 04        ...   ldx #0004
EA5D 3C              <     pshx 
EA5E CC 10 0A        ...   ldd #100a
EA61 BD E5 D7        ...   jsr e5d7		;jump69
EA64 38              8     pulx 
EA65 83 00 00        ...   subd #0000
EA68 27 3F           '?    beq 3f
EA6A CE 00 01        ...   ldx #0001
EA6D 3C              <     pshx 
EA6E CC 18 00        ...   ldd #1800
EA71 BD E5 D7        ...   jsr e5d7		;jump69
EA74 38              8     pulx 
EA75 83 00 00        ...   subd #0000
EA78 27 2F           '/    beq 2f
EA7A CC 03 E8        ...   ldd #03e8
EA7D BD E5 71        ..q   jsr e571		;jump13
EA80 7F 0B A8        ...   clr 0ba8
EA83 F6 18 00        ...   ldab 1800
EA86 C4 01           ..    andb #01
EA88 26 1C           &.    bne 1c
EA8A BD BE 4B        ..K   jsr be4b		;jump1
EA8D 0F              .     sei 
EA8E BD BE 01        ...   jsr be01		;jump70
EA91 0E              .     cli 
EA92 CC 01 F4        ...   ldd #01f4
EA95 BD E5 71        ..q   jsr e571		;jump13
EA98 CC AF D1        ...   ldd #afd1
EA9B BD C0 BA        ...   jsr c0ba		;display:
EA9E CC 01 F4        ...   ldd #01f4
EAA1 BD E5 71        ..q   jsr e571		;jump13
EAA4 20 DD            .    bra dd
EAA6 BD BE 69        ..i   jsr be69		;jump3
EAA9 C6 01           ..    ldab #01
EAAB F7 00 14        ...   stb 0014
EAAE F6 00 14        ...   ldab 0014
EAB1 F7 38 00        .8.   stb 3800
EAB4 CC 00 1E        ...   ldd #001e
EAB7 BD E5 71        ..q   jsr e571		;jump13
EABA CE 00 01        ...   ldx #0001
EABD 3C              <     pshx 
EABE CC 10 00        ...   ldd #1000
EAC1 BD E5 D7        ...   jsr e5d7		;jump69
EAC4 38              8     pulx 
EAC5 83 00 00        ...   subd #0000
EAC8 27 10           '.    beq 10
EACA CE 00 02        ...   ldx #0002
EACD 3C              <     pshx 
EACE CC 10 00        ...   ldd #1000
EAD1 BD E5 D7        ...   jsr e5d7		;jump69
EAD4 38              8     pulx 
EAD5 83 00 00        ...   subd #0000
EAD8 27 03           '.    beq 03
EADA 7E ED F8        ~..   jmp edf8		;goto80
EADD C6 02           ..    ldab #02
EADF F7 00 14        ...   stb 0014
EAE2 F6 00 14        ...   ldab 0014
EAE5 F7 38 00        .8.   stb 3800
EAE8 CC 00 1E        ...   ldd #001e
EAEB BD E5 71        ..q   jsr e571		;jump13
EAEE CE 00 01        ...   ldx #0001
EAF1 3C              <     pshx 
EAF2 CC 10 00        ...   ldd #1000
EAF5 BD E5 D7        ...   jsr e5d7		;jump69
EAF8 38              8     pulx 
EAF9 83 00 00        ...   subd #0000
EAFC 26 10           &.    bne 10
EAFE CE 00 02        ...   ldx #0002
EB01 3C              <     pshx 
EB02 CC 10 00        ...   ldd #1000
EB05 BD E5 D7        ...   jsr e5d7		;jump69
EB08 38              8     pulx 
EB09 83 00 00        ...   subd #0000
EB0C 26 03           &.    bne 03
EB0E 7E ED F8        ~..   jmp edf8		;goto80
EB11 CC 5A 5A        .ZZ   ldd #5a5a
EB14 FD 10 16        ...   stad 1016
EB17 CC B1 39        ..9   ldd #b139
EB1A BD C0 BA        ...   jsr c0ba		;display:
EB1D CC 07 D0        ...   ldd #07d0
EB20 BD E5 71        ..q   jsr e571		;jump13
EB23 CC B1 44        ..D   ldd #b144
EB26 BD C0 BA        ...   jsr c0ba		;display:
EB29 CC 07 D0        ...   ldd #07d0
EB2C BD E5 71        ..q   jsr e571		;jump13
EB2F CC 00 14        ...   ldd #0014
EB32 FD 00 3B        ..;   stad 003b
EB35 FC 00 3B        ..;   ldd 003b
EB38 27 47           'G    beq 47
EB3A 30              0     tsx 
EB3B 6F 00           o.    clr 00,x
EB3D 30              0     tsx 
EB3E E6 00           ..    ldab 00,x
EB40 C1 0A           ..    cmpb #0a
EB42 24 3B           $;    bcc 3b
EB44 BD BE 4B        ..K   jsr be4b		;jump1
EB47 C6 2E           ..    ldab #2e
EB49 37              7     pshb 
EB4A 30              0     tsx 
EB4B E6 01           ..    ldab 01,x
EB4D 4F              O     clra 
EB4E C3 0B 90        ...   addd #0b90
EB51 8F              .     xgdx 
EB52 33              3     pulb 
EB53 E7 00           ..    stb 00,x
EB55 0F              .     sei 
EB56 BD BE 69        ..i   jsr be69		;jump3
EB59 CE 10 28        ..(   ldx #1028
EB5C 1C 00           ..    bset add,x 00,x
EB5E 40              @     nega 
EB5F CE 00 01        ...   ldx #0001
EB62 3C              <     pshx 
EB63 CC 00 20        ..    ldd #0020
EB66 BD BD B5        ...   jsr bdb5		;jump78	
EB69 38              8     pulx 
EB6A CE 10 28        ..(   ldx #1028
EB6D 1D 00           ..    bclr add,x 00,x
EB6F 40              @     nega 
EB70 BD BE 10        ...   jsr be10		;jump88
EB73 0E              .     cli 
EB74 CC 00 07        ...   ldd #0007
EB77 BD E5 71        ..q   jsr e571		;jump13
EB7A 30              0     tsx 
EB7B 6C 00           l.    inc 00,x
EB7D 20 BE            .    bra be
EB7F 20 B4            .    bra b4
EB81 BD BE 69        ..i   jsr be69		;jump3
EB84 CC 08 00        ...   ldd #0800
EB87 FD 00 73        ..s   stad 0073
EB8A FC 00 73        ..s   ldd 0073
EB8D 83 0F FF        ...   subd #0fff
EB90 22 10           ".    bhi 10
EB92 C6 AA           ..    ldab #aa
EB94 FE 00 73        ..s   ldx 0073
EB97 E7 00           ..    stb 00,x
EB99 FE 00 73        ..s   ldx 0073
EB9C 08              .     inx 
EB9D FF 00 73        ..s   stx 0073
EBA0 20 E8            .    bra e8
EBA2 CC 08 00        ...   ldd #0800
EBA5 FD 00 73        ..s   stad 0073
EBA8 FC 00 73        ..s   ldd 0073
EBAB 83 0F FF        ...   subd #0fff
EBAE 22 1A           ".    bhi 1a
EBB0 FE 00 73        ..s   ldx 0073
EBB3 E6 00           ..    ldab 00,x
EBB5 C1 AA           ..    cmpb #aa
EBB7 27 08           '.    beq 08
EBB9 CE 10 16        ...   ldx #1016
EBBC 1D 00           ..    bclr add,x 00,x
EBBE FF 20 09        . .   stx 2009
EBC1 FE 00 73        ..s   ldx 0073
EBC4 08              .     inx 
EBC5 FF 00 73        ..s   stx 0073
EBC8 20 DE            .    bra de
EBCA CC 08 00        ...   ldd #0800
EBCD FD 00 73        ..s   stad 0073
EBD0 FC 00 73        ..s   ldd 0073
EBD3 83 0F FF        ...   subd #0fff
EBD6 22 10           ".    bhi 10
EBD8 C6 55           .U    ldab #55
EBDA FE 00 73        ..s   ldx 0073
EBDD E7 00           ..    stb 00,x
EBDF FE 00 73        ..s   ldx 0073
EBE2 08              .     inx 
EBE3 FF 00 73        ..s   stx 0073
EBE6 20 E8            .    bra e8
EBE8 CC 08 00        ...   ldd #0800
EBEB FD 00 73        ..s   stad 0073
EBEE FC 00 73        ..s   ldd 0073
EBF1 83 0F FF        ...   subd #0fff
EBF4 22 1A           ".    bhi 1a
EBF6 FE 00 73        ..s   ldx 0073
EBF9 E6 00           ..    ldab 00,x
EBFB C1 55           .U    cmpb #55
EBFD 27 08           '.    beq 08
EBFF CE 10 16        ...   ldx #1016
EC02 1D 00           ..    bclr add,x 00,x
EC04 FF 20 09        . .   stx 2009
EC07 FE 00 73        ..s   ldx 0073
EC0A 08              .     inx 
EC0B FF 00 73        ..s   stx 0073
EC0E 20 DE            .    bra de
EC10 CC 08 00        ...   ldd #0800
EC13 FD 00 73        ..s   stad 0073
EC16 FC 00 73        ..s   ldd 0073
EC19 83 0F FF        ...   subd #0fff
EC1C 22 0F           ".    bhi 0f
EC1E 5F              _     clrb 
EC1F FE 00 73        ..s   ldx 0073
EC22 E7 00           ..    stb 00,x
EC24 FE 00 73        ..s   ldx 0073
EC27 08              .     inx 
EC28 FF 00 73        ..s   stx 0073
EC2B 20 E9            .    bra e9
EC2D 7F 00 06        ...   clr 0006
EC30 F6 00 06        ...   ldab 0006
EC33 C1 64           .d    cmpb #64
EC35 24 4D           $M    bcc 4d
EC37 F6 00 06        ...   ldab 0006
EC3A 4F              O     clra 
EC3B CE 00 05        ...   ldx #0005
EC3E 02              .     idiv 
EC3F 5D              ]     tstb 
EC40 27 3D           '=    beq 3d
EC42 CC 00 32        ..2   ldd #0032
EC45 37              7     pshb 
EC46 36              6     psha 
EC47 F6 00 06        ...   ldab 0006
EC4A 4F              O     clra 
EC4B 05              .     asld 
EC4C C3 08 00        ...   addd #0800
EC4F 38              8     pulx 
EC50 8F              .     xgdx 
EC51 ED 00           ..    stad 00,x
EC53 F6 00 06        ...   ldab 0006
EC56 4F              O     clra 
EC57 05              .     asld 
EC58 C3 08 00        ...   addd #0800
EC5B 8F              .     xgdx 
EC5C EC 00           ..    ldd 00,x
EC5E 43              C     coma 
EC5F 53              S     comb 
EC60 37              7     pshb 
EC61 36              6     psha 
EC62 F6 00 06        ...   ldab 0006
EC65 4F              O     clra 
EC66 05              .     asld 
EC67 C3 0B F1        ...   addd #0bf1
EC6A 38              8     pulx 
EC6B 8F              .     xgdx 
EC6C ED 00           ..    stad 00,x
EC6E CC 08 01        ...   ldd #0801
EC71 37              7     pshb 
EC72 36              6     psha 
EC73 F6 00 06        ...   ldab 0006
EC76 4F              O     clra 
EC77 05              .     asld 
EC78 C3 08 C8        ...   addd #08c8
EC7B 38              8     pulx 
EC7C 8F              .     xgdx 
EC7D ED 00           ..    stad 00,x
EC7F 7C 00 06        |..   inc 0006
EC82 20 AC            .    bra ac
EC84 C6 1D           ..    ldab #1d
EC86 F7 0B BF        ...   stb 0bbf
EC89 30              0     tsx 
EC8A 6F 00           o.    clr 00,x
EC8C F6 0B BF        ...   ldab 0bbf
EC8F 4F              O     clra 
EC90 C3 00 02        ...   addd #0002
EC93 37              7     pshb 
EC94 36              6     psha 
EC95 30              0     tsx 
EC96 E6 02           ..    ldab 02,x
EC98 4F              O     clra 
EC99 A3 00           ..    subd 00,x
EC9B 31              1     ins 
EC9C 31              1     ins 
EC9D 2C 1B           ,.    bge 1b
EC9F 30              0     tsx 
ECA0 E6 00           ..    ldab 00,x
ECA2 4F              O     clra 
ECA3 C3 B1 D0        ...   addd #b1d0
ECA6 8F              .     xgdx 
ECA7 E6 00           ..    ldab 00,x
ECA9 37              7     pshb 
ECAA 30              0     tsx 
ECAB E6 01           ..    ldab 01,x
ECAD 4F              O     clra 
ECAE C3 09 93        ...   addd #0993
ECB1 8F              .     xgdx 
ECB2 33              3     pulb 
ECB3 E7 00           ..    stb 00,x
ECB5 30              0     tsx 
ECB6 6C 00           l.    inc 00,x
ECB8 20 D2            .    bra d2
ECBA 7F 0B A8        ...   clr 0ba8
ECBD CC 00 32        ..2   ldd #0032
ECC0 FD 0B B5        ...   stad 0bb5
ECC3 5F              _     clrb 
ECC4 4F              O     clra 
ECC5 FD 0B B7        ...   stad 0bb7
ECC8 7F 00 06        ...   clr 0006
ECCB BD A6 4E        ..N   jsr a64e		;jump135
ECCE C6 09           ..    ldab #09
ECD0 F7 0B C0        ...   stb 0bc0
ECD3 7F 0E 6D        ..m   clr 0e6d
ECD6 C6 01           ..    ldab #01
ECD8 F7 0E 6E        ..n   stb 0e6e
ECDB 30              0     tsx 
ECDC 6F 00           o.    clr 00,x
ECDE 30              0     tsx 
ECDF E6 00           ..    ldab 00,x
ECE1 C1 0F           ..    cmpb #0f
ECE3 24 21           $!    bcc 21
ECE5 C6 30           .0    ldab #30
ECE7 37              7     pshb 
ECE8 30              0     tsx 
ECE9 E6 01           ..    ldab 01,x
ECEB 4F              O     clra 
ECEC C3 0C B9        ...   addd #0cb9
ECEF 8F              .     xgdx 
ECF0 33              3     pulb 
ECF1 E7 00           ..    stb 00,x
ECF3 C6 30           .0    ldab #30
ECF5 37              7     pshb 
ECF6 30              0     tsx 
ECF7 E6 01           ..    ldab 01,x
ECF9 4F              O     clra 
ECFA C3 0C C9        ...   addd #0cc9
ECFD 8F              .     xgdx 
ECFE 33              3     pulb 
ECFF E7 00           ..    stb 00,x
ED01 30              0     tsx 
ED02 6C 00           l.    inc 00,x
ED04 20 D8            .    bra d8
ED06 30              0     tsx 
ED07 6F 00           o.    clr 00,x
ED09 30              0     tsx 
ED0A E6 00           ..    ldab 00,x
ED0C C1 0B           ..    cmpb #0b
ED0E 24 12           $.    bcc 12
ED10 5F              _     clrb 
ED11 37              7     pshb 
ED12 30              0     tsx 
ED13 E6 01           ..    ldab 01,x
ED15 4F              O     clra 
ED16 C3 0B E4        ...   addd #0be4
ED19 8F              .     xgdx 
ED1A 33              3     pulb 
ED1B E7 00           ..    stb 00,x
ED1D 30              0     tsx 
ED1E 6C 00           l.    inc 00,x
ED20 20 E7            .    bra e7
ED22 4F              O     clra 
ED23 5F              _     clrb 
ED24 FD 0D F9        ...   stad 0df9
ED27 FD 0D F7        ...   stad 0df7
ED2A 4F              O     clra 
ED2B 5F              _     clrb 
ED2C FD 0D F5        ...   stad 0df5
ED2F FD 0D F3        ...   stad 0df3
ED32 7F 0C D9        ...   clr 0cd9
ED35 7F 0C DA        ...   clr 0cda
ED38 7F 0C DB        ...   clr 0cdb
ED3B 7F 0C DC        ...   clr 0cdc
ED3E 4F              O     clra 
ED3F 5F              _     clrb 
ED40 FD 0D F1        ...   stad 0df1
ED43 FD 0D EF        ...   stad 0def
ED46 4F              O     clra 
ED47 5F              _     clrb 
ED48 FD 0D FD        ...   stad 0dfd
ED4B FD 0D FB        ...   stad 0dfb
ED4E 4F              O     clra 
ED4F 5F              _     clrb 
ED50 FD 0D E9        ...   stad 0de9
ED53 FD 0D E7        ...   stad 0de7
ED56 4F              O     clra 
ED57 5F              _     clrb 
goto80:
ED58 FD 0D ED        ...   stad 0ded
ED5B FD 0D EB        ...   stad 0deb
ED5E 5F              _     clrb 
ED5F 4F              O     clra 
ED60 FD 0C DD        ...   stad 0cdd
ED63 5F              _     clrb 
ED64 4F              O     clra 
ED65 FD 0C DF        ...   stad 0cdf
ED68 5F              _     clrb 
ED69 4F              O     clra 
ED6A FD 0C E1        ...   stad 0ce1
ED6D 5F              _     clrb 
ED6E 4F              O     clra 
ED6F FD 0C E3        ...   stad 0ce3
ED72 5F              _     clrb 
ED73 4F              O     clra 
ED74 FD 0C E5        ...   stad 0ce5
ED77 4F              O     clra 
ED78 5F              _     clrb 
ED79 FD 0C F5        ...   stad 0cf5
ED7C FD 0C F3        ...   stad 0cf3
ED7F 4F              O     clra 
ED80 5F              _     clrb 
ED81 FD 0C F9        ...   stad 0cf9
ED84 FD 0C F7        ...   stad 0cf7
ED87 4F              O     clra 
ED88 5F              _     clrb 
ED89 FD 0C FD        ...   stad 0cfd
ED8C FD 0C FB        ...   stad 0cfb
ED8F 4F              O     clra 
ED90 5F              _     clrb 
ED91 FD 0D 01        ...   stad 0d01
ED94 FD 0C FF        ...   stad 0cff
ED97 4F              O     clra 
ED98 5F              _     clrb 
ED99 FD 0D 05        ...   stad 0d05
ED9C FD 0D 03        ...   stad 0d03
ED9F 5F              _     clrb 
EDA0 4F              O     clra 
EDA1 FD 0D 07        ...   stad 0d07
EDA4 5F              _     clrb 
EDA5 4F              O     clra 
EDA6 FD 0D 09        ...   stad 0d09
EDA9 5F              _     clrb 
EDAA 4F              O     clra 
EDAB FD 0D 0B        ...   stad 0d0b
EDAE 5F              _     clrb 
EDAF 4F              O     clra 
EDB0 FD 0D 0D        ...   stad 0d0d
EDB3 4F              O     clra 
EDB4 5F              _     clrb 
EDB5 FD 0D 11        ...   stad 0d11
EDB8 FD 0D 0F        ...   stad 0d0f
EDBB 4F              O     clra 
EDBC 5F              _     clrb 
EDBD FD 0D 15        ...   stad 0d15
EDC0 FD 0D 13        ...   stad 0d13
EDC3 4F              O     clra 
EDC4 5F              _     clrb 
EDC5 FD 0D 19        ...   stad 0d19
EDC8 FD 0D 17        ...   stad 0d17
EDCB 4F              O     clra 
EDCC 5F              _     clrb 
EDCD FD 0D 1D        ...   stad 0d1d
EDD0 FD 0D 1B        ...   stad 0d1b
EDD3 7F 0B AF        ...   clr 0baf
EDD6 7F 0B AE        ...   clr 0bae
EDD9 7F 0B AD        ...   clr 0bad
EDDC 7F 0B AC        ...   clr 0bac
EDDF 7F 0B AB        ...   clr 0bab
EDE2 7F 0B B4        ...   clr 0bb4
EDE5 7F 0B B3        ...   clr 0bb3
EDE8 7F 0B B2        ...   clr 0bb2
EDEB 7F 0B B1        ...   clr 0bb1
EDEE 7F 0B B0        ...   clr 0bb0
EDF1 C6 05           ..    ldab #05
EDF3 F7 08 00        ...   stb 0800
EDF6 8D 21           .!    bsr dest 21
EDF8 7F 00 14        ...   clr 0014
EDFB F6 00 14        ...   ldab 0014
EDFE F7 38 00        .8.   stb 3800
EE01 31              1     ins 
EE02 39              9     rts 

jump36:
EE03 37              7     pshb 
EE04 36              6     psha 
EE05 30              0     tsx 
EE06 EC 00           ..    ldd 00,x
EE08 BD C0 BA        ...   jsr c0ba		;display:
EE0B CE 10 16        ...   ldx #1016
EE0E 1D 01           ..    bclr add,x 01,x
EE10 FF CC 05        ...   stx cc05
EE13 DC BD E5        ...   ldd bd
EE16 71              q     illegal 
EE17 38              8     pulx 
EE18 39              9     rts 

EE19 BD E5 D1        ...   jsr e5d1		;jump52
goto81:
EE1C CE 10 16        ...   ldx #1016
EE1F 1D 01           ..    bclr add,x 01,x
EE21 FF CE 10        ...   stx ce10
EE24 16              .     tab 
EE25 1C 01           ..    bset add,x 01,x
EE27 5A              Z     decb 
EE28 F6 10 16        ...   ldab 1016
EE2B 26 0C           &.    bne 0c
EE2D CC B1 4F        ..O   ldd #b14f
EE30 BD C0 BA        ...   jsr c0ba		;display:
EE33 CC 05 DC        ...   ldd #05dc
EE36 BD E5 71        ..q   jsr e571		;jump13
EE39 C6 04           ..    ldab #04
EE3B F7 00 1C        ...   stb 001c
EE3E F6 00 1C        ...   ldab 001c
EE41 C1 07           ..    cmpb #07
EE43 24 57           $W    bcc 57
EE45 F6 00 1C        ...   ldab 001c
EE48 4F              O     clra 
EE49 C3 B1 23        ..#   addd #b123
EE4C 8F              .     xgdx 
EE4D E6 00           ..    ldab 00,x
EE4F 53              S     comb 
EE50 F4 00 1A        ...   andb 001a
EE53 F7 00 1A        ...   stb 001a
EE56 F6 00 1A        ...   ldab 001a
EE59 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EE5C CC 00 0A        ...   ldd #000a
EE5F BD E5 71        ..q   jsr e571		;jump13
EE62 F6 18 00        ...   ldab 1800
EE65 2C 07           ,.    bge 07
EE67 CC B1 5A        ..Z   ldd #b15a
EE6A 8D 97           ..    bsr dest 97
EE6C 20 2E            .    bra 2e
EE6E F6 00 1C        ...   ldab 001c
EE71 4F              O     clra 
EE72 C3 B1 23        ..#   addd #b123
EE75 8F              .     xgdx 
EE76 E6 00           ..    ldab 00,x
EE78 FA 00 1A        ...   orb 001a
EE7B F7 00 1A        ...   stb 001a
EE7E F6 00 1A        ...   ldab 001a
EE81 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EE84 CC 00 0A        ...   ldd #000a
EE87 BD E5 71        ..q   jsr e571		;jump13
EE8A F6 18 00        ...   ldab 1800
EE8D 2D 08           -.    blt 08
EE8F CC B1 5A        ..Z   ldd #b15a
EE92 BD EE 03        ...   jsr ee03		;jump36
EE95 20 05            .    bra 05
EE97 7C 00 1C        |..   inc 001c
EE9A 20 A2            .    bra a2
EE9C CE 10 00        ...   ldx #1000
EE9F 1C 00           ..    bset add,x 00,x
EEA1 08              .     inx 
EEA2 CE 10 08        ...   ldx #1008
EEA5 1C 00           ..    bset add,x 00,x
EEA7 02              .     idiv 
EEA8 CE 00 1A        ...   ldx #001a
EEAB 1C 00           ..    bset add,x 00,x
EEAD 40              @     nega 
EEAE F6 00 1A        ...   ldab 001a
EEB1 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EEB4 CC 00 0A        ...   ldd #000a
EEB7 BD E5 71        ..q   jsr e571		;jump13
EEBA F6 10 08        ...   ldab 1008
EEBD C4 01           ..    andb #01
EEBF 26 06           &.    bne 06
EEC1 CC B1 5A        ..Z   ldd #b15a
EEC4 BD EE 03        ...   jsr ee03		;jump36
EEC7 CE 00 1A        ...   ldx #001a
EECA 1D 00           ..    bclr add,x 00,x
EECC 40              @     nega 
EECD F6 00 1A        ...   ldab 001a
EED0 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EED3 CC 00 0A        ...   ldd #000a
EED6 BD E5 71        ..q   jsr e571		;jump13
EED9 F6 10 08        ...   ldab 1008
EEDC C4 01           ..    andb #01
EEDE 27 06           '.    beq 06
EEE0 CC B1 5A        ..Z   ldd #b15a
EEE3 BD EE 03        ...   jsr ee03		;jump36
EEE6 CE 00 1A        ...   ldx #001a
EEE9 1C 00           ..    bset add,x 00,x
EEEB 02              .     idiv 
EEEC F6 00 1A        ...   ldab 001a
EEEF F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EEF2 CC 00 0A        ...   ldd #000a
EEF5 BD E5 71        ..q   jsr e571		;jump13
EEF8 F6 10 00        ...   ldab 1000
EEFB C4 04           ..    andb #04
EEFD 26 06           &.    bne 06
EEFF CC B1 65        ..e   ldd #b165
EF02 BD EE 03        ...   jsr ee03		;jump36
EF05 CE 00 1A        ...   ldx #001a
EF08 1D 00           ..    bclr add,x 00,x
EF0A 02              .     idiv 
EF0B F6 00 1A        ...   ldab 001a
EF0E F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EF11 CC 00 0A        ...   ldd #000a
EF14 BD E5 71        ..q   jsr e571		;jump13
EF17 F6 10 00        ...   ldab 1000
EF1A C4 04           ..    andb #04
EF1C 27 06           '.    beq 06
EF1E CC B1 65        ..e   ldd #b165
EF21 BD EE 03        ...   jsr ee03		;jump36
EF24 CE 00 1A        ...   ldx #001a
EF27 1C 00           ..    bset add,x 00,x
EF29 01              .     nop 
EF2A F6 00 1A        ...   ldab 001a
EF2D F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EF30 CC 00 0A        ...   ldd #000a
EF33 BD E5 71        ..q   jsr e571		;jump13
EF36 F6 18 00        ...   ldab 1800
EF39 C4 20           .     andb #20
EF3B 26 06           &.    bne 06
EF3D CC B1 70        ..p   ldd #b170
EF40 BD EE 03        ...   jsr ee03		;jump36
EF43 CE 00 1A        ...   ldx #001a
EF46 1D 00           ..    bclr add,x 00,x
EF48 01              .     nop 
EF49 F6 00 1A        ...   ldab 001a
EF4C F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EF4F CC 00 0A        ...   ldd #000a
EF52 BD E5 71        ..q   jsr e571		;jump13
EF55 F6 18 00        ...   ldab 1800
EF58 C4 20           .     andb #20
EF5A 27 06           '.    beq 06
EF5C CC B1 70        ..p   ldd #b170
EF5F BD EE 03        ...   jsr ee03		;jump36
EF62 CE 00 1A        ...   ldx #001a
EF65 1C 00           ..    bset add,x 00,x
EF67 04              .     lsrd 
EF68 F6 00 1A        ...   ldab 001a
EF6B F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EF6E CC 00 0A        ...   ldd #000a
EF71 BD E5 71        ..q   jsr e571		;jump13
EF74 F6 10 0A        ...   ldab 100a
EF77 C4 04           ..    andb #04
EF79 26 06           &.    bne 06
EF7B CC B1 7B        ..{   ldd #b17b
EF7E BD EE 03        ...   jsr ee03		;jump36
EF81 CE 00 1A        ...   ldx #001a
EF84 1D 00           ..    bclr add,x 00,x
EF86 04              .     lsrd 
EF87 F6 00 1A        ...   ldab 001a
EF8A F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EF8D CC 00 0A        ...   ldd #000a
EF90 BD E5 71        ..q   jsr e571		;jump13
EF93 F6 10 0A        ...   ldab 100a
EF96 C4 04           ..    andb #04
EF98 27 06           '.    beq 06
EF9A CC B1 7B        ..{   ldd #b17b
EF9D BD EE 03        ...   jsr ee03		;jump36
EFA0 CE 10 00        ...   ldx #1000
EFA3 1C 00           ..    bset add,x 00,x
EFA5 08              .     inx 
EFA6 CE 00 1A        ...   ldx #001a
EFA9 1C 00           ..    bset add,x 00,x
EFAB 40              @     nega 
EFAC F6 00 1A        ...   ldab 001a
EFAF F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
EFB2 CE 10 08        ...   ldx #1008
EFB5 1C 00           ..    bset add,x 00,x
EFB7 02              .     idiv 
EFB8 CC 00 0A        ...   ldd #000a
EFBB BD E5 71        ..q   jsr e571		;jump13
EFBE F6 10 08        ...   ldab 1008
EFC1 C4 01           ..    andb #01
EFC3 26 06           &.    bne 06
EFC5 CC B1 86        ...   ldd #b186
EFC8 BD EE 03        ...   jsr ee03		;jump36
EFCB CE 10 08        ...   ldx #1008
EFCE 1D 00           ..    bclr add,x 00,x
EFD0 02              .     idiv 
EFD1 CC 00 0A        ...   ldd #000a
EFD4 BD E5 71        ..q   jsr e571		;jump13
EFD7 F6 10 08        ...   ldab 1008
EFDA C4 01           ..    andb #01
EFDC 27 06           '.    beq 06
EFDE CC B1 86        ...   ldd #b186
EFE1 BD EE 03        ...   jsr ee03		;jump36
EFE4 CE 10 00        ...   ldx #1000
EFE7 1D 00           ..    bclr add,x 00,x
EFE9 08              .     inx 
EFEA CE 10 08        ...   ldx #1008
EFED 1C 00           ..    bset add,x 00,x
EFEF 02              .     idiv 
EFF0 CC 00 0A        ...   ldd #000a
EFF3 BD E5 71        ..q   jsr e571		;jump13
EFF6 F6 10 08        ...   ldab 1008
EFF9 C4 01           ..    andb #01
EFFB 26 06           &.    bne 06
EFFD CC B1 91        ...   ldd #b191
F000 BD EE 03        ...   jsr ee03		;jump36
F003 CE 10 08        ...   ldx #1008
F006 1D 00           ..    bclr add,x 00,x
F008 02              .     idiv 
F009 CC 00 0A        ...   ldd #000a
F00C BD E5 71        ..q   jsr e571		;jump13
F00F F6 10 08        ...   ldab 1008
F012 C4 01           ..    andb #01
F014 27 06           '.    beq 06
F016 CC B1 91        ...   ldd #b191
F019 BD EE 03        ...   jsr ee03		;jump36
F01C CE 00 14        ...   ldx #0014
F01F 1C 00           ..    bset add,x 00,x
F021 20 F6            .    bra f6
F023 00              .     test 
F024 14              .     bset 
F025 F7 38 00        .8.   stb 3800
F028 CC 00 0A        ...   ldd #000a
F02B BD E5 71        ..q   jsr e571		;jump13
F02E F6 20 00        . .   ldab 2000
F031 C4 40           .@    andb #40
F033 26 06           &.    bne 06
F035 CC B1 9C        ...   ldd #b19c
F038 BD EE 03        ...   jsr ee03		;jump36
F03B CE 00 14        ...   ldx #0014
F03E 1D 00           ..    bclr add,x 00,x
F040 20 F6            .    bra f6
F042 00              .     test 
F043 14              .     bset 
F044 F7 38 00        .8.   stb 3800
F047 CC 00 0A        ...   ldd #000a
F04A BD E5 71        ..q   jsr e571		;jump13
F04D F6 20 00        . .   ldab 2000
F050 C4 40           .@    andb #40
F052 27 06           '.    beq 06
F054 CC B1 9C        ...   ldd #b19c
F057 BD EE 03        ...   jsr ee03		;jump36
F05A CE 00 14        ...   ldx #0014
F05D 1C 00           ..    bset add,x 00,x
F05F 40              @     nega 
F060 F6 00 14        ...   ldab 0014
F063 F7 38 00        .8.   stb 3800
F066 CC 00 0A        ...   ldd #000a
F069 BD E5 71        ..q   jsr e571		;jump13
F06C F6 20 00        . .   ldab 2000
F06F 2C 06           ,.    bge 06
F071 CC B1 9C        ...   ldd #b19c
F074 BD EE 03        ...   jsr ee03		;jump36
F077 CE 00 14        ...   ldx #0014
F07A 1D 00           ..    bclr add,x 00,x
F07C 40              @     nega 
F07D F6 00 14        ...   ldab 0014
F080 F7 38 00        .8.   stb 3800
F083 CC 00 0A        ...   ldd #000a
F086 BD E5 71        ..q   jsr e571		;jump13
F089 F6 20 00        . .   ldab 2000
F08C 2D 06           -.    blt 06
F08E CC B1 9C        ...   ldd #b19c
F091 BD EE 03        ...   jsr ee03		;jump36
F094 7F 00 61        ..a   clr 0061
F097 F6 00 61        ..a   ldab 0061
F09A C1 08           ..    cmpb #08
F09C 24 22           $"    bcc 22
F09E F6 00 61        ..a   ldab 0061
F0A1 4F              O     clra 
F0A2 C3 B1 C8        ...   addd #b1c8
F0A5 8F              .     xgdx 
F0A6 E6 00           ..    ldab 00,x
F0A8 F7 00 06        ...   stb 0006
F0AB BD D3 11        ...   jsr d311		;jump30
F0AE 83 00 00        ...   subd #0000
F0B1 27 08           '.    beq 08
F0B3 CC B1 A7        ...   ldd #b1a7
F0B6 BD EE 03        ...   jsr ee03		;jump36
F0B9 20 05            .    bra 05
F0BB 7C 00 61        |.a   inc 0061
F0BE 20 D7            .    bra d7
F0C0 7F 00 61        ..a   clr 0061
F0C3 F6 00 61        ..a   ldab 0061
F0C6 C1 08           ..    cmpb #08
F0C8 24 2B           $+    bcc 2b
F0CA F6 00 61        ..a   ldab 0061
F0CD 4F              O     clra 
F0CE C3 B1 C8        ...   addd #b1c8
F0D1 8F              .     xgdx 
F0D2 E6 00           ..    ldab 00,x
F0D4 F7 00 06        ...   stb 0006
F0D7 BD D1 3D        ..=   jsr d13d		;jump34
F0DA BD D5 70        ..p   jsr d570		;jump35
F0DD 83 00 00        ...   subd #0000
F0E0 26 0B           &.    bne 0b
F0E2 BD D1 05        ...   jsr d105		;jump32
F0E5 CC B1 B2        ...   ldd #b1b2
F0E8 BD EE 03        ...   jsr ee03		;jump36
F0EB 20 08            .    bra 08
F0ED BD D1 05        ...   jsr d105		;jump32
F0F0 7C 00 61        |.a   inc 0061
F0F3 20 CE            .    bra ce
F0F5 7F 00 61        ..a   clr 0061
F0F8 F6 00 61        ..a   ldab 0061
F0FB C1 05           ..    cmpb #05
F0FD 24 45           $E    bcc 45
F0FF F6 00 61        ..a   ldab 0061
F102 4F              O     clra 
F103 C3 B1 C8        ...   addd #b1c8
F106 8F              .     xgdx 
F107 E6 00           ..    ldab 00,x
F109 F7 00 06        ...   stb 0006
F10C BD D1 3D        ..=   jsr d13d		;jump34
F10F CC 00 0A        ...   ldd #000a
F112 BD E5 71        ..q   jsr e571		;jump13
F115 F6 18 00        ...   ldab 1800
F118 C4 40           .@    andb #40
F11A 26 0B           &.    bne 0b
F11C BD D1 05        ...   jsr d105		;jump32
F11F CC B1 B2        ...   ldd #b1b2
F122 BD EE 03        ...   jsr ee03		;jump36
F125 20 1D            .    bra 1d
F127 BD D1 05        ...   jsr d105		;jump32
F12A CC 00 0A        ...   ldd #000a
F12D BD E5 71        ..q   jsr e571		;jump13
F130 F6 18 00        ...   ldab 1800
F133 C4 40           .@    andb #40
F135 27 08           '.    beq 08
F137 CC B1 B2        ...   ldd #b1b2
F13A BD EE 03        ...   jsr ee03		;jump36
F13D 20 05            .    bra 05
F13F 7C 00 61        |.a   inc 0061
F142 20 B4            .    bra b4
F144 FC 10 16        ...   ldd 1016
F147 83 5A 5A        .ZZ   subd #5a5a
F14A 26 0C           &.    bne 0c
F14C CC B1 BD        ...   ldd #b1bd
F14F BD C0 BA        ...   jsr c0ba		;display:
F152 CC 05 DC        ...   ldd #05dc
F155 BD E5 71        ..q   jsr e571		;jump13
F158 7E EE 1C        ~..   jmp ee1c		;goto81

;send a byte to the printer?
;it calls c7e7, which does things with the serial port
jump53:
F15B 37              7     pshb 
F15C 36              6     psha 
F15D F6 00 19        ...   ldab 0019
F160 C4 40           .@    andb #40
F162 26 5B           &[    bne 5b
F164 C6 FF           ..    ldab #ff
F166 F7 00 39        ..9   stb 0039
F169 F6 20 00        . .   ldab 2000
F16C 2C 11           ,.    bge 11
F16E F6 20 00        . .   ldab 2000
F171 C4 40           .@    andb #40
F173 27 0A           '.    beq 0a
F175 F6 00 39        ..9   ldab 0039
F178 27 05           '.    beq 05
F17A BD E6 1B        ...   jsr e61b		;jump4
F17D 20 EA            .    bra ea
F17F F6 00 39        ..9   ldab 0039
F182 26 08           &.    bne 08
F184 CE 00 19        ...   ldx #0019
F187 1C 00           ..    bset add,x 00,x
F189 40              @     nega 
F18A 20 33            3    bra 33
F18C F6 20 00        . .   ldab 2000
F18F C4 40           .@    andb #40
F191 27 2C           ',    beq 2c
F193 F6 00 1D        ...   ldab 001d
F196 C4 01           ..    andb #01
F198 27 1E           '.    beq 1e
F19A 30              0     tsx 
F19B E6 01           ..    ldab 01,x
F19D C1 30           .0    cmpb #30
F19F 26 08           &.    bne 08
F1A1 CC 00 20        ..    ldd #0020
F1A4 BD C7 E7        ...   jsr c7e7		;jump37
F1A7 20 0D            .    bra 0d
F1A9 30              0     tsx 
F1AA E6 01           ..    ldab 01,x
F1AC 4F              O     clra 
F1AD BD C7 E7        ...   jsr c7e7		;jump37
F1B0 CE 00 1D        ...   ldx #001d
F1B3 1D 00           ..    bclr add,x 00,x
F1B5 01              .     nop 
F1B6 20 07            .    bra 07
F1B8 30              0     tsx 
F1B9 E6 01           ..    ldab 01,x
F1BB 4F              O     clra 
F1BC BD C7 E7        ...   jsr c7e7		;jump37
F1BF 38              8     pulx 
F1C0 39              9     rts 

jump59:
F1C1 37              7     pshb 
F1C2 36              6     psha 
F1C3 F6 20 00        . .   ldab 2000
F1C6 C4 40           .@    andb #40
F1C8 27 1C           '.    beq 1c
F1CA 7F 00 18        ...   clr 0018
F1CD F6 00 18        ...   ldab 0018
F1D0 30              0     tsx 
F1D1 E1 05           ..    cmpb 05,x
F1D3 24 11           $.    bcc 11
F1D5 30              0     tsx 
F1D6 EE 00           ..    ldx 00,x
F1D8 E6 00           ..    ldab 00,x
F1DA 4F              O     clra 
F1DB BD F1 5B        ..[   jsr f15b		;jump53
F1DE 38              8     pulx 
F1DF 08              .     inx 
F1E0 3C              <     pshx 
F1E1 7C 00 18        |..   inc 0018
F1E4 20 E7            .    bra e7
F1E6 38              8     pulx 
F1E7 39              9     rts 

;send a string to the printer?  D is a pointer to a string, and some of them
;look *way* too long for the display.  Or does it scroll them?

jump57:
F1E8 37              7     pshb 
F1E9 36              6     psha 
F1EA F6 20 00        . .   ldab 2000
F1ED C4 40           .@    andb #40
F1EF 27 1B           '.    beq 1b
F1F1 CC 00 20        ..    ldd #0020
F1F4 BD F1 5B        ..[   jsr f15b		;jump53
F1F7 30              0     tsx 
F1F8 EE 00           ..    ldx 00,x
F1FA E6 00           ..    ldab 00,x
F1FC 27 0E           '.    beq 0e
F1FE 30              0     tsx 
F1FF EE 00           ..    ldx 00,x
F201 E6 00           ..    ldab 00,x
F203 4F              O     clra 
F204 BD F1 5B        ..[   jsr f15b		;jump53
F207 38              8     pulx 
F208 08              .     inx 
F209 3C              <     pshx 
F20A 20 EB            .    bra eb
F20C 38              8     pulx 
F20D 39              9     rts 

jump62:
F20E 37              7     pshb 
F20F 36              6     psha 
F210 F6 20 00        . .   ldab 2000
F213 C4 40           .@    andb #40
F215 27 34           '4    beq 34
F217 CE 00 1D        ...   ldx #001d
F21A 1C 00           ..    bset add,x 00,x
F21C 01              .     nop 
F21D 30              0     tsx 
F21E E6 01           ..    ldab 01,x
F220 4F              O     clra 
F221 BD E7 95        ...   jsr e795		;jump54
F224 F6 00 5D        ..]   ldab 005d
F227 4F              O     clra 
F228 BD F1 5B        ..[   jsr f15b		;jump53
F22B F6 00 5E        ..^   ldab 005e
F22E 4F              O     clra 
F22F BD F1 5B        ..[   jsr f15b		;jump53
F232 CE 00 1D        ...   ldx #001d
F235 1D 00           ..    bclr add,x 00,x
F237 01              .     nop 
F238 F6 00 5F        .._   ldab 005f
F23B 4F              O     clra 
F23C BD F1 5B        ..[   jsr f15b		;jump53
F23F CC 00 20        ..    ldd #0020
F242 BD F1 5B        ..[   jsr f15b		;jump53
F245 CC 00 20        ..    ldd #0020
F248 BD F1 5B        ..[   jsr f15b		;jump53
F24B 38              8     pulx 
F24C 39              9     rts 

jump58:
F24D 37              7     pshb 
F24E 36              6     psha 
F24F F6 20 00        . .   ldab 2000
F252 C4 40           .@    andb #40
F254 27 41           'A    beq 41
F256 CE 00 1D        ...   ldx #001d
F259 1C 00           ..    bset add,x 00,x
F25B 01              .     nop 
F25C 30              0     tsx 
F25D EC 00           ..    ldd 00,x
F25F BD E7 45        ..E   jsr e745		;jump55
F262 F6 00 57        ..W   ldab 0057
F265 4F              O     clra 
F266 BD F1 5B        ..[   jsr f15b		;jump53
F269 F6 00 5A        ..Z   ldab 005a
F26C 4F              O     clra 
F26D BD F1 5B        ..[   jsr f15b		;jump53
F270 F6 00 5D        ..]   ldab 005d
F273 4F              O     clra 
F274 BD F1 5B        ..[   jsr f15b		;jump53
F277 F6 00 5E        ..^   ldab 005e
F27A 4F              O     clra 
F27B BD F1 5B        ..[   jsr f15b		;jump53
F27E CE 00 1D        ...   ldx #001d
F281 1D 00           ..    bclr add,x 00,x
F283 01              .     nop 
F284 F6 00 5F        .._   ldab 005f
F287 4F              O     clra 
F288 BD F1 5B        ..[   jsr f15b		;jump53
F28B CC 00 20        ..    ldd #0020
F28E BD F1 5B        ..[   jsr f15b		;jump53
F291 CC 00 20        ..    ldd #0020
F294 BD F1 5B        ..[   jsr f15b		;jump53
F297 38              8     pulx 
F298 39              9     rts 

jump61
jump61::
F299 37              7     pshb 
F29A 36              6     psha 
F29B F6 20 00        . .   ldab 2000
F29E C4 40           .@    andb #40
F2A0 27 7B           '{    beq 7b
F2A2 F6 00 17        ...   ldab 0017
F2A5 C4 08           ..    andb #08
F2A7 26 06           &.    bne 06
F2A9 CC 00 24        ..$   ldd #0024
F2AC BD F1 5B        ..[   jsr f15b		;jump53
F2AF CE 00 1D        ...   ldx #001d
F2B2 1C 00           ..    bset add,x 00,x
F2B4 01              .     nop 
F2B5 30              0     tsx 
F2B6 EC 00           ..    ldd 00,x
F2B8 BD E7 45        ..E   jsr e745		;jump55
F2BB F6 00 57        ..W   ldab 0057
F2BE 4F              O     clra 
F2BF BD F1 5B        ..[   jsr f15b		;jump53
F2C2 F6 00 5A        ..Z   ldab 005a
F2C5 4F              O     clra 
F2C6 BD F1 5B        ..[   jsr f15b		;jump53
F2C9 F6 00 13        ...   ldab 0013
F2CC C1 08           ..    cmpb #08
F2CE 26 0C           &.    bne 0c
F2D0 CC 00 2E        ...   ldd #002e
F2D3 BD F1 5B        ..[   jsr f15b		;jump53
F2D6 CE 00 1D        ...   ldx #001d
F2D9 1D 00           ..    bclr add,x 00,x
F2DB 01              .     nop 
F2DC F6 00 5D        ..]   ldab 005d
F2DF 4F              O     clra 
F2E0 BD F1 5B        ..[   jsr f15b		;jump53
F2E3 F6 00 13        ...   ldab 0013
F2E6 C1 04           ..    cmpb #04
F2E8 26 0C           &.    bne 0c
F2EA CC 00 2E        ...   ldd #002e
F2ED BD F1 5B        ..[   jsr f15b		;jump53
F2F0 CE 00 1D        ...   ldx #001d
F2F3 1D 00           ..    bclr add,x 00,x
F2F5 01              .     nop 
F2F6 F6 00 5E        ..^   ldab 005e
F2F9 4F              O     clra 
F2FA BD F1 5B        ..[   jsr f15b		;jump53
F2FD F6 00 13        ...   ldab 0013
F300 C1 02           ..    cmpb #02
F302 26 06           &.    bne 06
F304 CC 00 2E        ...   ldd #002e
F307 BD F1 5B        ..[   jsr f15b		;jump53
F30A CE 00 1D        ...   ldx #001d
F30D 1D 00           ..    bclr add,x 00,x
F30F 01              .     nop 
F310 F6 00 5F        .._   ldab 005f
F313 4F              O     clra 
F314 BD F1 5B        ..[   jsr f15b		;jump53
F317 CC 00 0A        ...   ldd #000a
F31A BD F1 5B        ..[   jsr f15b		;jump53
F31D 38              8     pulx 
F31E 39              9     rts 

jump60:
F31F 38              8     pulx 
F320 37              7     pshb 
F321 36              6     psha 
F322 3C              <     pshx 
F323 F6 20 00        . .   ldab 2000
F326 C4 40           .@    andb #40
F328 26 03           &.    bne 03
F32A 7E F3 BC        ~..   jmp f3bc		;goto82
F32D F6 00 17        ...   ldab 0017
F330 C4 08           ..    andb #08
F332 26 06           &.    bne 06
F334 CC 00 24        ..$   ldd #0024
F337 BD F1 5B        ..[   jsr f15b		;jump53
F33A CE 00 1D        ...   ldx #001d
F33D 1C 00           ..    bset add,x 00,x
F33F 01              .     nop 
F340 30              0     tsx 
F341 EC 04           ..    ldd 04,x
F343 37              7     pshb 
F344 36              6     psha 
F345 EC 02           ..    ldd 02,x
F347 BD E6 21        ..!   jsr e621		;jump56
F34A 38              8     pulx 
F34B C6 07           ..    ldab #07
F34D F7 00 18        ...   stb 0018
F350 F6 00 18        ...   ldab 0018
F353 C1 FF           ..    cmpb #ff
F355 24 5F           $_    bcc 5f
F357 F6 00 18        ...   ldab 0018
F35A 4F              O     clra 
F35B C3 0B 90        ...   addd #0b90
F35E 8F              .     xgdx 
F35F E6 00           ..    ldab 00,x
F361 4F              O     clra 
F362 BD F1 5B        ..[   jsr f15b		;jump53
F365 F6 00 18        ...   ldab 0018
F368 C1 03           ..    cmpb #03
F36A 26 15           &.    bne 15
F36C F6 00 13        ...   ldab 0013
F36F C1 08           ..    cmpb #08
F371 26 0C           &.    bne 0c
F373 CC 00 2E        ...   ldd #002e
F376 BD F1 5B        ..[   jsr f15b		;jump53
F379 CE 00 1D        ...   ldx #001d
F37C 1D 00           ..    bclr add,x 00,x
F37E 01              .     nop 
F37F 20 30            0    bra 30
F381 C1 02           ..    cmpb #02
F383 26 15           &.    bne 15
F385 F6 00 13        ...   ldab 0013
F388 C1 04           ..    cmpb #04
F38A 26 0C           &.    bne 0c
F38C CC 00 2E        ...   ldd #002e
F38F BD F1 5B        ..[   jsr f15b		;jump53
F392 CE 00 1D        ...   ldx #001d
F395 1D 00           ..    bclr add,x 00,x
F397 01              .     nop 
F398 20 17            .    bra 17
F39A C1 01           ..    cmpb #01
F39C 26 13           &.    bne 13
F39E F6 00 13        ...   ldab 0013
F3A1 C1 02           ..    cmpb #02
F3A3 26 06           &.    bne 06
F3A5 CC 00 2E        ...   ldd #002e
F3A8 BD F1 5B        ..[   jsr f15b		;jump53
F3AB CE 00 1D        ...   ldx #001d
F3AE 1D 00           ..    bclr add,x 00,x
F3B0 01              .     nop 
F3B1 7A 00 18        z..   dec 0018
F3B4 20 9A            .    bra 9a
F3B6 CC 00 0A        ...   ldd #000a
F3B9 BD F1 5B        ..[   jsr f15b		;jump53
goto82:
F3BC 38              8     pulx 
F3BD 31              1     ins 
F3BE 31              1     ins 
F3BF 6E 00           n.    jmp 00,x

jump63:
F3C1 37              7     pshb 
F3C2 36              6     psha 
F3C3 F6 20 00        . .   ldab 2000
F3C6 C4 40           .@    andb #40
F3C8 27 49           'I    beq 49
F3CA CC B4 71        ..q   ldd #b471	"    CODE "
F3CD BD F1 E8        ...   jsr f1e8		;jump57
F3D0 30              0     tsx 
F3D1 E6 01           ..    ldab 01,x
F3D3 4F              O     clra 
F3D4 CE 00 0A        ...   ldx #000a
F3D7 02              .     idiv 
F3D8 8F              .     xgdx 
F3D9 CB 30           .0    addb #30
F3DB F7 00 5E        ..^   stb 005e
F3DE 30              0     tsx 
F3DF E6 01           ..    ldab 01,x
F3E1 4F              O     clra 
F3E2 CE 00 0A        ...   ldx #000a
F3E5 02              .     idiv 
F3E6 CB 30           .0    addb #30
F3E8 F7 00 5F        .._   stb 005f	;5e and 5f now param in ascii
F3EB F6 00 5E        ..^   ldab 005e
F3EE 4F              O     clra 
F3EF BD F1 5B        ..[   jsr f15b		;jump53
F3F2 F6 00 5F        .._   ldab 005f
F3F5 4F              O     clra 
F3F6 BD F1 5B        ..[   jsr f15b		;jump53
F3F9 CC B4 7B        ..{   ldd #b47b	"VENDS - "
F3FC BD F1 E8        ...   jsr f1e8		;jump57
F3FF 30              0     tsx 
F400 E6 01           ..    ldab 01,x
F402 4F              O     clra 
F403 05              .     asld 
F404 C3 0D 1F        ...   addd #0d1f
F407 8F              .     xgdx 
F408 EC 00           ..    ldd 00,x
F40A BD F2 4D        ..M   jsr f24d		;jump58
F40D CC 00 0A        ...   ldd #000a
F410 BD F1 5B        ..[   jsr f15b		;jump53
F413 38              8     pulx 
F414 39              9     rts 

jump112:
F415 F6 0B C0        ...   ldab 0bc0
F418 C4 02           ..    andb #02
F41A 27 15           '.    beq 15
F41C CE 00 21        ..!   ldx #0021
F41F 1C 00           ..    bset add,x 00,x
F421 80 F6           ..    suba #f6
F423 00              .     test 
F424 1D C4           ..    bclr add,x c4,x
F426 02              .     idiv 
F427 27 02           '.    beq 02
F429 20 F7            .    bra f7
F42B CE 00 21        ..!   ldx #0021
F42E 1D 00           ..    bclr add,x 00,x
F430 80 C6           ..    suba #c6
F432 10              .     sba 
F433 F7 10 2C        ..,   stb 102c
F436 C6 2E           ..    ldab #2e
F438 F7 10 2D        ..-   stb 102d
F43B C6 05           ..    ldab #05
F43D F7 10 2B        ..+   stb 102b
F440 CC 03 E8        ...   ldd #03e8
F443 BD E5 71        ..q   jsr e571		;jump13
F446 CE 10 00        ...   ldx #1000
F449 1D 00           ..    bclr add,x 00,x
F44B 08              .     inx 
F44C CE 00 19        ...   ldx #0019
F44F 1D 00           ..    bclr add,x 00,x
F451 40              @     nega 
F452 CE 00 1D        ...   ldx #001d
F455 1D 00           ..    bclr add,x 00,x
F457 01              .     nop 
F458 CC 00 18        ...   ldd #0018
F45B BD F1 5B        ..[   jsr f15b		;jump53
F45E CC B2 1B        ...   ldd #b21b	"         ROWE  INTERNATIONAL"
F461 BD F1 E8        ...   jsr f1e8		;jump57
F464 CC 00 0A        ...   ldd #000a
F467 BD F1 5B        ..[   jsr f15b		;jump53
F46A CC B2 38        ..8   ldd #b238
F46D BD F1 E8        ...   jsr f1e8		;jump57
F470 CC 00 0A        ...   ldd #000a
F473 BD F1 5B        ..[   jsr f15b		;jump53
F476 CC B2 56        ..V   ldd #b256
F479 BD F1 E8        ...   jsr f1e8		;jump57
F47C CC 00 0A        ...   ldd #000a
F47F BD F1 5B        ..[   jsr f15b		;jump53
F482 CC 00 0A        ...   ldd #000a
F485 BD F1 5B        ..[   jsr f15b		;jump53
F488 CC B2 75        ..u   ldd #b275
F48B BD F1 E8        ...   jsr f1e8		;jump57
F48E CC 00 0A        ...   ldd #000a
F491 BD F1 5B        ..[   jsr f15b		;jump53
F494 CC B2 38        ..8   ldd #b238
F497 BD F1 E8        ...   jsr f1e8		;jump57
F49A CC 00 0A        ...   ldd #000a
F49D BD F1 5B        ..[   jsr f15b		;jump53
F4A0 CC 00 0A        ...   ldd #000a
F4A3 BD F1 5B        ..[   jsr f15b		;jump53
F4A6 CC B2 90        ...   ldd #b290
F4A9 BD F1 E8        ...   jsr f1e8		;jump57
F4AC CE 00 0A        ...   ldx #000a
F4AF 3C              <     pshx 
F4B0 CC 0C BC        ...   ldd #0cbc
F4B3 BD F1 C1        ...   jsr f1c1		;jump59
F4B6 38              8     pulx 
F4B7 CC 00 0A        ...   ldd #000a
F4BA BD F1 5B        ..[   jsr f15b		;jump53
F4BD CC B2 A2        ...   ldd #b2a2
F4C0 BD F1 E8        ...   jsr f1e8		;jump57
F4C3 CE 00 0A        ...   ldx #000a
F4C6 3C              <     pshx 
F4C7 CC 0C CC        ...   ldd #0ccc
F4CA BD F1 C1        ...   jsr f1c1		;jump59
F4CD 38              8     pulx 
F4CE CC 00 0A        ...   ldd #000a
F4D1 BD F1 5B        ..[   jsr f15b		;jump53
F4D4 CC B2 B4        ...   ldd #b2b4
F4D7 BD F1 E8        ...   jsr f1e8		;jump57
F4DA FC 0C E7        ...   ldd 0ce7
F4DD BD F2 4D        ..M   jsr f24d		;jump58
F4E0 CC 00 0A        ...   ldd #000a
F4E3 BD F1 5B        ..[   jsr f15b		;jump53
F4E6 CC 00 0A        ...   ldd #000a
F4E9 BD F1 5B        ..[   jsr f15b		;jump53
F4EC CC B2 C6        ...   ldd #b2c6
F4EF BD F1 E8        ...   jsr f1e8		;jump57
F4F2 FE 0D E9        ...   ldx 0de9
F4F5 3C              <     pshx 
F4F6 FC 0D E7        ...   ldd 0de7
F4F9 BD F3 1F        ...   jsr f31f		;jump60
F4FC 38              8     pulx 
F4FD CC B2 D8        ...   ldd #b2d8
F500 BD F1 E8        ...   jsr f1e8		;jump57
F503 FE 0D ED        ...   ldx 0ded
F506 3C              <     pshx 
F507 FC 0D EB        ...   ldd 0deb
F50A BD F3 1F        ...   jsr f31f		;jump60
F50D 38              8     pulx 
F50E CC B2 EA        ...   ldd #b2ea
F511 BD F1 E8        ...   jsr f1e8		;jump57
F514 FE 0D F1        ...   ldx 0df1
F517 3C              <     pshx 
F518 FC 0D EF        ...   ldd 0def
F51B BD F3 1F        ...   jsr f31f		;jump60
F51E 38              8     pulx 
F51F CC B2 FC        ...   ldd #b2fc
F522 BD F1 E8        ...   jsr f1e8		;jump57
F525 FE 0D F9        ...   ldx 0df9
F528 3C              <     pshx 
F529 FC 0D F7        ...   ldd 0df7
F52C BD F3 1F        ...   jsr f31f		;jump60
F52F 38              8     pulx 
F530 CC B3 0E        ...   ldd #b30e
F533 BD F1 E8        ...   jsr f1e8		;jump57
F536 FE 0D FD        ...   ldx 0dfd
F539 3C              <     pshx 
F53A FC 0D FB        ...   ldd 0dfb
F53D BD F3 1F        ...   jsr f31f		;jump60
F540 38              8     pulx 
F541 CC 00 0A        ...   ldd #000a
F544 BD F1 5B        ..[   jsr f15b		;jump53
F547 CC B3 20        ..    ldd #b320
F54A BD F1 E8        ...   jsr f1e8		;jump57
F54D CC 00 0A        ...   ldd #000a
F550 BD F1 5B        ..[   jsr f15b		;jump53
F553 CC B3 31        ..1   ldd #b331
F556 BD F1 E8        ...   jsr f1e8		;jump57
F559 FE 0D F5        ...   ldx 0df5
F55C 3C              <     pshx 
F55D FC 0D F3        ...   ldd 0df3
F560 BD F3 1F        ...   jsr f31f		;jump60
F563 38              8     pulx 
F564 CC B3 4A        ..J   ldd #b34a
F567 BD F1 E8        ...   jsr f1e8		;jump57
F56A FC 0C DD        ...   ldd 0cdd
F56D BD F2 4D        ..M   jsr f24d		;jump58
F570 FE 0C F5        ...   ldx 0cf5
F573 3C              <     pshx 
F574 FC 0C F3        ...   ldd 0cf3
F577 BD F3 1F        ...   jsr f31f		;jump60
F57A 38              8     pulx 
F57B CC B3 5C        ..\   ldd #b35c
F57E BD F1 E8        ...   jsr f1e8		;jump57
F581 FC 0C DF        ...   ldd 0cdf
F584 BD F2 4D        ..M   jsr f24d		;jump58
F587 FE 0C F9        ...   ldx 0cf9
F58A 3C              <     pshx 
F58B FC 0C F7        ...   ldd 0cf7
F58E BD F3 1F        ...   jsr f31f		;jump60
F591 38              8     pulx 
F592 CC B3 6E        ..n   ldd #b36e
F595 BD F1 E8        ...   jsr f1e8		;jump57
F598 FC 0C E1        ...   ldd 0ce1
F59B BD F2 4D        ..M   jsr f24d		;jump58
F59E FE 0C FD        ...   ldx 0cfd
F5A1 3C              <     pshx 
F5A2 FC 0C FB        ...   ldd 0cfb
F5A5 BD F3 1F        ...   jsr f31f		;jump60
F5A8 38              8     pulx 
F5A9 CC B3 80        ...   ldd #b380
F5AC BD F1 E8        ...   jsr f1e8		;jump57
F5AF FC 0C E3        ...   ldd 0ce3
F5B2 BD F2 4D        ..M   jsr f24d		;jump58
F5B5 FE 0D 01        ...   ldx 0d01
F5B8 3C              <     pshx 
F5B9 FC 0C FF        ...   ldd 0cff
F5BC BD F3 1F        ...   jsr f31f		;jump60
F5BF 38              8     pulx 
F5C0 CC B3 92        ...   ldd #b392
F5C3 BD F1 E8        ...   jsr f1e8		;jump57
F5C6 FC 0C E5        ...   ldd 0ce5
F5C9 BD F2 4D        ..M   jsr f24d		;jump58
F5CC FE 0D 05        ...   ldx 0d05
F5CF 3C              <     pshx 
F5D0 FC 0D 03        ...   ldd 0d03
F5D3 BD F3 1F        ...   jsr f31f		;jump60
F5D6 38              8     pulx 
F5D7 CC 00 0A        ...   ldd #000a
F5DA BD F1 5B        ..[   jsr f15b		;jump53
F5DD CC B3 A4        ...   ldd #b3a4
F5E0 BD F1 E8        ...   jsr f1e8		;jump57
F5E3 CC 00 0A        ...   ldd #000a
F5E6 BD F1 5B        ..[   jsr f15b		;jump53
F5E9 CC B3 B3        ...   ldd #b3b3
F5EC BD F1 E8        ...   jsr f1e8		;jump57
F5EF FC 0C F1        ...   ldd 0cf1
F5F2 BD F2 99        ...   jsr f299		;jump61
F5F5 CC B3 CA        ...   ldd #b3ca
F5F8 BD F1 E8        ...   jsr f1e8		;jump57
F5FB F6 0C DC        ...   ldab 0cdc
F5FE 4F              O     clra 
F5FF BD F2 0E        ...   jsr f20e		;jump62
F602 FC 0C E9        ...   ldd 0ce9
F605 BD F2 99        ...   jsr f299		;jump61
F608 CC B3 DC        ...   ldd #b3dc
F60B BD F1 E8        ...   jsr f1e8		;jump57
F60E F6 0C DB        ...   ldab 0cdb
F611 4F              O     clra 
F612 BD F2 0E        ...   jsr f20e		;jump62
F615 FC 0C EB        ...   ldd 0ceb
F618 BD F2 99        ...   jsr f299		;jump61
F61B CC B3 EE        ...   ldd #b3ee
F61E BD F1 E8        ...   jsr f1e8		;jump57
F621 F6 0C DA        ...   ldab 0cda
F624 4F              O     clra 
F625 BD F2 0E        ...   jsr f20e		;jump62
F628 FC 0C ED        ...   ldd 0ced
F62B BD F2 99        ...   jsr f299		;jump61
F62E CC B4 00        ...   ldd #b400
F631 BD F1 E8        ...   jsr f1e8		;jump57
F634 F6 0C D9        ...   ldab 0cd9
F637 4F              O     clra 
F638 BD F2 0E        ...   jsr f20e		;jump62
F63B FC 0C EF        ...   ldd 0cef
F63E BD F2 99        ...   jsr f299		;jump61
F641 CC 00 0A        ...   ldd #000a
F644 BD F1 5B        ..[   jsr f15b		;jump53
F647 CC B4 12        ...   ldd #b412
F64A BD F1 E8        ...   jsr f1e8		;jump57
F64D FC 0D 07        ...   ldd 0d07
F650 BD F2 4D        ..M   jsr f24d		;jump58
F653 FE 0D 11        ...   ldx 0d11
F656 3C              <     pshx 
F657 FC 0D 0F        ...   ldd 0d0f
F65A BD F3 1F        ...   jsr f31f		;jump60
F65D 38              8     pulx 
F65E CC B4 24        ..$   ldd #b424
F661 BD F1 E8        ...   jsr f1e8		;jump57
F664 FC 0D 09        ...   ldd 0d09
F667 BD F2 4D        ..M   jsr f24d		;jump58
F66A FE 0D 15        ...   ldx 0d15
F66D 3C              <     pshx 
F66E FC 0D 13        ...   ldd 0d13
F671 BD F3 1F        ...   jsr f31f		;jump60
F674 38              8     pulx 
F675 CC B4 36        ..6   ldd #b436
F678 BD F1 E8        ...   jsr f1e8		;jump57
F67B FC 0D 0B        ...   ldd 0d0b
F67E BD F2 4D        ..M   jsr f24d		;jump58
F681 FE 0D 19        ...   ldx 0d19
F684 3C              <     pshx 
F685 FC 0D 17        ...   ldd 0d17
F688 BD F3 1F        ...   jsr f31f		;jump60
F68B 38              8     pulx 
F68C CC B4 48        ..H   ldd #b448
F68F BD F1 E8        ...   jsr f1e8		;jump57
F692 FC 0D 0D        ...   ldd 0d0d
F695 BD F2 4D        ..M   jsr f24d		;jump58
F698 FE 0D 1D        ...   ldx 0d1d
F69B 3C              <     pshx 
F69C FC 0D 1B        ...   ldd 0d1b
F69F BD F3 1F        ...   jsr f31f		;jump60
F6A2 38              8     pulx 
F6A3 CC 00 0A        ...   ldd #000a
F6A6 BD F1 5B        ..[   jsr f15b		;jump53
F6A9 CC B4 5A        ..Z   ldd #b45a
F6AC BD F1 E8        ...   jsr f1e8		;jump57
F6AF CC 00 0A        ...   ldd #000a
F6B2 BD F1 5B        ..[   jsr f15b		;jump53
F6B5 C6 01           ..    ldab #01
F6B7 F7 00 54        ..T   stb 0054
F6BA F6 00 54        ..T   ldab 0054
F6BD C1 63           .c    cmpb #63
F6BF 22 1B           ".    bhi 1b
F6C1 F6 00 54        ..T   ldab 0054
F6C4 4F              O     clra 
F6C5 05              .     asld 
F6C6 C3 08 C8        ...   addd #08c8
F6C9 8F              .     xgdx 
F6CA EC 00           ..    ldd 00,x
F6CC C4 80           ..    andb #80
F6CE 27 07           '.    beq 07
F6D0 F6 00 54        ..T   ldab 0054
F6D3 4F              O     clra 
F6D4 BD F3 C1        ...   jsr f3c1		;jump63
F6D7 7C 00 54        |.T   inc 0054
F6DA 20 DE            .    bra de
F6DC CC 00 0A        ...   ldd #000a
F6DF BD F1 5B        ..[   jsr f15b		;jump53
F6E2 F6 00 19        ...   ldab 0019
F6E5 C4 40           .@    andb #40
F6E7 26 07           &.    bne 07
F6E9 FE 0C E7        ...   ldx 0ce7
F6EC 08              .     inx 
F6ED FF 0C E7        ...   stx 0ce7
F6F0 CE 00 1D        ...   ldx #001d
F6F3 1D 00           ..    bclr add,x 00,x
F6F5 01              .     nop 
F6F6 CC 03 E8        ...   ldd #03e8
F6F9 BD E5 71        ..q   jsr e571		;jump13
F6FC C6 03           ..    ldab #03
F6FE F7 10 2B        ..+   stb 102b
F701 CE 10 00        ...   ldx #1000
F704 1C 00           ..    bset add,x 00,x
F706 08              .     inx 
F707 F6 0B C0        ...   ldab 0bc0
F70A C4 02           ..    andb #02
F70C 27 06           '.    beq 06
F70E CC 00 80        ...   ldd #0080
F711 BD C7 E7        ...   jsr c7e7		;jump37
F714 7F 00 38        ..8   clr 0038
F717 39              9     rts 
F718 3C              <     pshx 
F719 F6 0B C0        ...   ldab 0bc0
F71C C4 08           ..    andb #08
F71E 27 03           '.    beq 03
F720 BD C3 25        ..%   jsr c325		;jump64
F723 7C 00 0C        |..   inc 000c
F726 F6 00 0C        ...   ldab 000c
F729 C1 02           ..    cmpb #02
F72B 26 13           &.    bne 13
F72D 7F 00 0C        ...   clr 000c
F730 F6 00 07        ...   ldab 0007
F733 27 03           '.    beq 03
F735 7A 00 07        z..   dec 0007
F738 F6 00 00        ...   ldab 0000
F73B 27 03           '.    beq 03
F73D 7A 00 00        z..   dec 0000
F740 7C 00 0B        |..   inc 000b
F743 F6 00 0B        ...   ldab 000b
F746 C1 08           ..    cmpb #08
F748 26 37           &7    bne 37
F74A 7F 00 0B        ...   clr 000b
F74D FC 00 3B        ..;   ldd 003b
F750 27 07           '.    beq 07
F752 FE 00 3B        ..;   ldx 003b
F755 09              .     dex 
F756 FF 00 3B        ..;   stx 003b
F759 CC 00 2C        ..,   ldd #002c
F75C 30              0     tsx 
F75D ED 00           ..    stad 00,x
F75F 30              0     tsx 
F760 EC 00           ..    ldd 00,x
F762 37              7     pshb 
F763 36              6     psha 
F764 CC 00 3A        ..:   ldd #003a
F767 30              0     tsx 
F768 A3 00           ..    subd 00,x
F76A 31              1     ins 
F76B 31              1     ins 
F76C 25 13           %.    bcs 13
F76E 30              0     tsx 
F76F EE 00           ..    ldx 00,x
F771 E6 00           ..    ldab 00,x
F773 27 07           '.    beq 07
F775 30              0     tsx 
F776 EE 00           ..    ldx 00,x
F778 6A 00           j.    dec 00,x
F77A E6 00           ..    ldab 00,x
F77C 38              8     pulx 
F77D 08              .     inx 
F77E 3C              <     pshx 
F77F 20 DE            .    bra de
F781 F6 00 32        ..2   ldab 0032
F784 27 07           '.    beq 07
F786 F6 00 32        ..2   ldab 0032
F789 C4 02           ..    andb #02
F78B 26 08           &.    bne 08
F78D CE 10 00        ...   ldx #1000
F790 1D 00           ..    bclr add,x 00,x
F792 10              .     sba 
F793 20 0D            .    bra 0d
F795 F6 00 32        ..2   ldab 0032
F798 C4 01           ..    andb #01
F79A 26 06           &.    bne 06
F79C CE 10 00        ...   ldx #1000
F79F 1C 00           ..    bset add,x 00,x
F7A1 10              .     sba 
F7A2 7C 00 4C        |.L   inc 004c
F7A5 F6 00 4C        ..L   ldab 004c
F7A8 C1 4B           .K    cmpb #4b
F7AA 26 13           &.    bne 13
F7AC 7F 00 4C        ..L   clr 004c
F7AF F6 00 4F        ..O   ldab 004f
F7B2 27 03           '.    beq 03
F7B4 7A 00 4F        z.O   dec 004f
F7B7 F6 00 25        ..%   ldab 0025
F7BA 27 03           '.    beq 03
F7BC 7A 00 25        z.%   dec 0025
F7BF F6 00 31        ..1   ldab 0031
F7C2 26 03           &.    bne 03
F7C4 BD FE 69        ..i   jsr fe69		;inhibit interrupts
F7C7 F6 00 7F        ...   ldab 007f
F7CA 27 03           '.    beq 03
F7CC BD FE 69        ..i   jsr fe69		;inhibit interrupts
F7CF C6 40           .@    ldab #40
F7D1 F7 10 25        ..%   stb 1025
F7D4 38              8     pulx 
F7D5 3B              ;     rti 

main:

F7D6 7F 00 14        ...   clr 0014	;0 -> 0014
F7D9 5F              _     clrb 	;0 -> B
F7DA F7 38 00        .8.   stb 3800	;B -> 3800
F7DD C6 7F           ..    ldab #7f	;7F -> B
F7DF F7 00 1A        ...   stb 001a	;B -> 001a
F7E2 C6 7F           ..    ldab #7f	;7F -> B
F7E4 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
F7E7 C6 07           ..    ldab #07	;7 -> B
F7E9 F7 10 3C        ..<   stb 103c
F7EC C6 81           ..    ldab #81	;81 -> B
F7EE F7 10 26        ..&   stb 1026
F7F1 5F              _     clrb 	;0 -> B
F7F2 F7 10 30        ..0   stb 1030
F7F5 C6 C8           ..    ldab #c8	;c8 -> B
F7F7 F7 10 00        ...   stb 1000
F7FA C6 3E           .>    ldab #3e	;3e -> B
F7FC F7 10 09        ...   stb 1009
F7FF C6 12           ..    ldab #12	;12 -> B
F801 F7 10 28        ..(   stb 1028

F804 BD BE 69        ..i   jsr be69		;jump3
F807 BD EA 59        ..Y   jsr ea59		;jump65
F80A CC 00 64        ..d   ldd #0064
F80D BD E5 71        ..q   jsr e571		;jump13
F810 CC B1 EF        ...   ldd #b1ef	" ROWE INT "
F813 BD C0 BA        ...   jsr c0ba		;display:
F816 CC 01 F4        ...   ldd #01f4
F819 BD E5 71        ..q   jsr e571		;jump13
F81C CC B1 FA        ...   ldd #b1fa	"COPYRIGHT "
F81F BD C0 BA        ...   jsr c0ba		;display:
F822 CC 01 F4        ...   ldd #01f4
F825 BD E5 71        ..q   jsr e571		;jump13
F828 CC B2 05        ...   ldd #b205	"   1993   "
F82B BD C0 BA        ...   jsr c0ba		;display:
F82E CC 01 F4        ...   ldd #01f4
F831 BD E5 71        ..q   jsr e571		;jump13
F834 CC B2 10        ...   ldd #b210	"5900 VER 5"
F837 BD C0 BA        ...   jsr c0ba		;display:
F83A CC 00 0A        ...   ldd #000a
F83D BD E5 71        ..q   jsr e571		;jump13
F840 7F 00 7F        ...   clr 007f
F843 7F 00 21        ..!   clr 0021
F846 7F 00 1F        ...   clr 001f
F849 7F 00 1D        ...   clr 001d
F84C 7F 00 1B        ...   clr 001b
F84F 7F 00 19        ...   clr 0019
F852 F6 0B C0        ...   ldab 0bc0
F855 C4 01           ..    andb #01
F857 27 08           '.    beq 08
F859 CE 0B C1        ...   ldx #0bc1
F85C 1C 00           ..    bset add,x 00,x
F85E 01              .     nop 
F85F 20 06            .    bra 06
F861 CE 0B C1        ...   ldx #0bc1
F864 1D 00           ..    bclr add,x 00,x
F866 01              .     nop 
F867 F6 0B C0        ...   ldab 0bc0
F86A C4 04           ..    andb #04
F86C 27 08           '.    beq 08
F86E CE 0B C1        ...   ldx #0bc1
F871 1C 00           ..    bset add,x 00,x
F873 02              .     idiv 
F874 20 06            .    bra 06
F876 CE 0B C1        ...   ldx #0bc1
F879 1D 00           ..    bclr add,x 00,x
F87B 02              .     idiv 
F87C CE 0B C1        ...   ldx #0bc1
F87F 1D 00           ..    bclr add,x 00,x
F881 04              .     lsrd 
F882 F6 0B C0        ...   ldab 0bc0
F885 C4 02           ..    andb #02
F887 27 2D           '-    beq 2d
F889 C6 3C           .<    ldab #3c
F88B F7 00 31        ..1   stb 0031
F88E 0E              .     cli 
F88F CC 00 FF        ...   ldd #00ff
F892 BD C7 E7        ...   jsr c7e7		;jump37
F895 5F              _     clrb 
F896 4F              O     clra 
F897 FD 00 40        ..@   stad 0040
F89A C6 10           ..    ldab #10
F89C F7 10 2C        ..,   stb 102c
F89F C6 2E           ..    ldab #2e
F8A1 F7 10 2D        ..-   stb 102d
F8A4 C6 03           ..    ldab #03
F8A6 F7 10 2B        ..+   stb 102b
F8A9 C6 46           .F    ldab #46
F8AB F7 00 2F        ../   stb 002f
F8AE CC 07 D0        ...   ldd #07d0
F8B1 BD E5 71        ..q   jsr e571		;jump13
F8B4 20 2B            +    bra 2b
F8B6 BD D0 94        ...   jsr d094		;jump66
F8B9 C6 27           .'    ldab #27
F8BB F7 10 2D        ..-   stb 102d
F8BE C6 07           ..    ldab #07
F8C0 F7 10 2B        ..+   stb 102b
F8C3 CE 00 1A        ...   ldx #001a
F8C6 1C 00           ..    bset add,x 00,x
F8C8 04              .     lsrd 
F8C9 F6 00 1A        ...   ldab 001a
F8CC F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
F8CF CC 07 D0        ...   ldd #07d0
F8D2 BD E5 71        ..q   jsr e571		;jump13
F8D5 CE 00 1A        ...   ldx #001a
F8D8 1D 00           ..    bclr add,x 00,x
F8DA 04              .     lsrd 
F8DB F6 00 1A        ...   ldab 001a
F8DE F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
F8E1 7F 0E 6A        ..j   clr 0e6a
F8E4 BD D4 79        ..y   jsr d479		;jump67
F8E7 BD E8 9A        ...   jsr e89a		;jump68
F8EA 7F 00 06        ...   clr 0006
F8ED CE 00 1D        ...   ldx #001d
F8F0 1C 00           ..    bset add,x 00,x
F8F2 10              .     sba 
F8F3 CC 05 DC        ...   ldd #05dc
F8F6 BD E5 71        ..q   jsr e571		;jump13
F8F9 C6 1E           ..    ldab #1e
F8FB F7 00 35        ..5   stb 0035
F8FE 0E              .     cli 
goto85:
F8FF BD E6 1B        ...   jsr e61b		;jump4
F902 BD D1 05        ...   jsr d105		;jump32
F905 F6 10 0A        ...   ldab 100a
F908 C4 08           ..    andb #08
F90A 27 46           'F    beq 46
F90C F6 10 0A        ...   ldab 100a
F90F C4 08           ..    andb #08
F911 27 11           '.    beq 11
F913 CE 00 1A        ...   ldx #001a
F916 1C 00           ..    bset add,x 00,x
F918 04              .     lsrd 
F919 F6 00 1A        ...   ldab 001a
F91C F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
F91F BD E6 1B        ...   jsr e61b		;jump4
F922 20 E8            .    bra e8
F924 BD D0 94        ...   jsr d094		;jump66
F927 F6 0B C0        ...   ldab 0bc0
F92A C4 01           ..    andb #01
F92C 27 06           '.    beq 06
F92E CE 0B C1        ...   ldx #0bc1
F931 1C 00           ..    bset add,x 00,x
F933 01              .     nop 
F934 F6 0B C0        ...   ldab 0bc0
F937 C4 04           ..    andb #04
F939 27 06           '.    beq 06
F93B CE 0B C1        ...   ldx #0bc1
F93E 1C 00           ..    bset add,x 00,x
F940 02              .     idiv 
F941 CE 00 1A        ...   ldx #001a
F944 1D 00           ..    bclr add,x 00,x
F946 04              .     lsrd 
F947 F6 00 1A        ...   ldab 001a
F94A F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
F94D C6 1E           ..    ldab #1e
F94F F7 00 35        ..5   stb 0035
F952 F6 0B C0        ...   ldab 0bc0
F955 C4 08           ..    andb #08
F957 27 16           '.    beq 16
F959 CE 00 02        ...   ldx #0002
F95C 3C              <     pshx 
F95D CC 10 00        ...   ldd #1000
F960 BD E5 D7        ...   jsr e5d7		;jump69
F963 38              8     pulx 
F964 83 00 00        ...   subd #0000
F967 27 06           '.    beq 06
F969 CE 0B C1        ...   ldx #0bc1
F96C 1C 00           ..    bset add,x 00,x
F96E 08              .     inx 
F96F F6 0B C1        ...   ldab 0bc1
F972 C4 07           ..    andb #07
F974 27 0D           '.    beq 0d
F976 F6 00 35        ..5   ldab 0035
F979 26 08           &.    bne 08
F97B CE 00 21        ..!   ldx #0021
F97E 1C 00           ..    bset add,x 00,x
F980 20 20                 bra 20
F982 06              .     tap 
F983 CE 00 21        ..!   ldx #0021
F986 1D 00           ..    bclr add,x 00,x
F988 20 BD            .    bra bd
F98A BB 63 BD        .c.   adda 63bd
F98D B8 00 F6        ...   eora 00f6
F990 0B              .     sev 
F991 C0 C4           ..    subb #c4
F993 01              .     nop 
F994 27 0B           '.    beq 0b
F996 FC 00 40        ..@   ldd 0040
F999 F3 00 3E        ..>   addd 003e
F99C 26 03           &.    bne 03
F99E BD D6 F6        ...   jsr d6f6		;jump106
F9A1 F6 00 12        ...   ldab 0012
F9A4 C4 20           .     andb #20
F9A6 27 08           '.    beq 08
F9A8 F6 00 35        ..5   ldab 0035
F9AB 26 03           &.    bne 03
F9AD 7E FA 79        ~.y   jmp fa79		;goto83
F9B0 CE 00 1F        ...   ldx #001f
F9B3 1D 00           ..    bclr add,x 00,x
F9B5 04              .     lsrd 
F9B6 CE 00 1D        ...   ldx #001d
F9B9 1C 00           ..    bset add,x 00,x
F9BB 10              .     sba 
F9BC F6 00 1F        ...   ldab 001f
F9BF C4 20           .     andb #20
F9C1 26 03           &.    bne 03
F9C3 7E FA 74        ~.t   jmp fa74		;goto84
F9C6 F6 0B C0        ...   ldab 0bc0
F9C9 C4 01           ..    andb #01
F9CB 27 08           '.    beq 08
F9CD CE 0B C1        ...   ldx #0bc1
F9D0 1C 00           ..    bset add,x 00,x
F9D2 01              .     nop 
F9D3 20 06            .    bra 06
F9D5 CE 0B C1        ...   ldx #0bc1
F9D8 1D 00           ..    bclr add,x 00,x
F9DA 01              .     nop 
F9DB F6 0B C0        ...   ldab 0bc0
F9DE C4 04           ..    andb #04
F9E0 27 08           '.    beq 08
F9E2 CE 0B C1        ...   ldx #0bc1
F9E5 1C 00           ..    bset add,x 00,x
F9E7 02              .     idiv 
F9E8 20 06            .    bra 06
F9EA CE 0B C1        ...   ldx #0bc1
F9ED 1D 00           ..    bclr add,x 00,x
F9EF 02              .     idiv 
F9F0 CE 0B C1        ...   ldx #0bc1
F9F3 1D 00           ..    bclr add,x 00,x
F9F5 04              .     lsrd 
F9F6 CE 00 1B        ...   ldx #001b
F9F9 1D 00           ..    bclr add,x 00,x
F9FB 02              .     idiv 
F9FC F6 0B C0        ...   ldab 0bc0
F9FF C4 02           ..    andb #02
FA01 27 27           ''    beq 27
FA03 C6 3C           .<    ldab #3c
FA05 F7 00 31        ..1   stb 0031
FA08 0E              .     cli 
FA09 CC 00 FF        ...   ldd #00ff
FA0C BD C7 E7        ...   jsr c7e7		;jump37
FA0F 5F              _     clrb 
FA10 4F              O     clra 
FA11 FD 00 40        ..@   stad 0040
FA14 C6 10           ..    ldab #10
FA16 F7 10 2C        ..,   stb 102c
FA19 C6 2E           ..    ldab #2e
FA1B F7 10 2D        ..-   stb 102d
FA1E C6 03           ..    ldab #03
FA20 F7 10 2B        ..+   stb 102b
FA23 C6 46           .F    ldab #46
FA25 F7 00 2F        ../   stb 002f
FA28 20 31            1    bra 31
FA2A CE 00 21        ..!   ldx #0021
FA2D 1D 00           ..    bclr add,x 00,x
FA2F 40              @     nega 
FA30 BD D0 94        ...   jsr d094		;jump66
FA33 C6 27           .'    ldab #27
FA35 F7 10 2D        ..-   stb 102d
FA38 C6 07           ..    ldab #07
FA3A F7 10 2B        ..+   stb 102b
FA3D CE 00 1A        ...   ldx #001a
FA40 1C 00           ..    bset add,x 00,x
FA42 04              .     lsrd 
FA43 F6 00 1A        ...   ldab 001a
FA46 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
FA49 CC 00 32        ..2   ldd #0032
FA4C BD E5 71        ..q   jsr e571		;jump13
FA4F CE 00 1A        ...   ldx #001a
FA52 1D 00           ..    bclr add,x 00,x
FA54 04              .     lsrd 
FA55 F6 00 1A        ...   ldab 001a
FA58 F7 30 00        .0.   stb 3000	;snack motor drivers serial data is bit 7
FA5B BD D4 79        ..y   jsr d479		;jump67
FA5E BD E8 9A        ...   jsr e89a		;jump68
FA61 BD BE 69        ..i   jsr be69		;jump3
FA64 7F 00 06        ...   clr 0006
FA67 CE 00 1F        ...   ldx #001f
FA6A 1D 00           ..    bclr add,x 00,x
FA6C 20 C6            .    bra c6
FA6E 1E F7 00 35     ...5  brset f7,x 00 35
FA72 20 03            .    bra 03
goto84:
FA74 BD DE E7        ...   jsr dee7		;jump116
FA77 20 66            f    bra 66
goto83:
FA79 F6 00 12        ...   ldab 0012
FA7C C4 20           .     andb #20
FA7E 27 5F           '_    beq 5f
FA80 F6 00 1F        ...   ldab 001f
FA83 C4 20           .     andb #20
FA85 26 03           &.    bne 03
FA87 BD E5 AA        ...   jsr e5aa		;jump12
FA8A CE 00 1F        ...   ldx #001f
FA8D 1C 00           ..    bset add,x 00,x
FA8F 20 F6            .    bra f6
FA91 00              .     test 
FA92 12              .     brset 
FA93 C4 01           ..    andb #01
FA95 26 33           &3    bne 33
FA97 F6 00 1D        ...   ldab 001d
FA9A C4 08           ..    andb #08
FA9C 27 2A           '*    beq 2a
FA9E F6 00 1F        ...   ldab 001f
FAA1 C4 04           ..    andb #04
FAA3 26 14           &.    bne 14
FAA5 CE 00 1F        ...   ldx #001f
FAA8 1C 00           ..    bset add,x 00,x
FAAA 04              .     lsrd 
FAAB CE 00 1F        ...   ldx #001f
FAAE 1C 00           ..    bset add,x 00,x
FAB0 80 CC           ..    suba #cc
FAB2 02              .     idiv 
FAB3 33              3     pulb 
FAB4 FD 00 3B        ..;   stad 003b
FAB7 20 09            .    bra 09
FAB9 BD E5 AA        ...   jsr e5aa		;jump12
FABC CE 00 1F        ...   ldx #001f
FABF 1D 00           ..    bclr add,x 00,x
FAC1 04              .     lsrd 
FAC2 CE 00 1D        ...   ldx #001d
FAC5 1D 00           ..    bclr add,x 00,x
FAC7 08              .     inx 
FAC8 20 06            .    bra 06
FACA CE 00 1D        ...   ldx #001d
FACD 1C 00           ..    bset add,x 00,x
FACF 08              .     inx 
FAD0 F6 00 1F        ...   ldab 001f
FAD3 C4 04           ..    andb #04
FAD5 26 05           &.    bne 05
FAD7 BD 81 00        ...   jsr 8100		;jump72
FADA 20 03            .    bra 03
FADC BD 84 A0        ...   jsr 84a0		;jump73
FADF 7E F8 FF        ~..   jmp f8ff		;goto85
FAE2 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FAE5 BD C0 BA        ...   jsr c0ba		;display:
FAE8 CC 07 D0        ...   ldd #07d0
FAEB BD E5 71        ..q   jsr e571		;jump13
FAEE 3B              ;     rti 
FAEF CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FAF2 BD C0 BA        ...   jsr c0ba		;display:
FAF5 CC 07 D0        ...   ldd #07d0
FAF8 BD E5 71        ..q   jsr e571		;jump13
FAFB 3B              ;     rti 
FAFC CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FAFF BD C0 BA        ...   jsr c0ba		;display:
FB02 CC 07 D0        ...   ldd #07d0
FB05 BD E5 71        ..q   jsr e571		;jump13
FB08 3B              ;     rti 
FB09 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB0C BD C0 BA        ...   jsr c0ba		;display:
FB0F CC 07 D0        ...   ldd #07d0
FB12 BD E5 71        ..q   jsr e571		;jump13
FB15 3B              ;     rti 
FB16 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB19 BD C0 BA        ...   jsr c0ba		;display:
FB1C CC 07 D0        ...   ldd #07d0
FB1F BD E5 71        ..q   jsr e571		;jump13
FB22 3B              ;     rti 
FB23 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB26 BD C0 BA        ...   jsr c0ba		;display:
FB29 CC 07 D0        ...   ldd #07d0
FB2C BD E5 71        ..q   jsr e571		;jump13
FB2F 3B              ;     rti 
FB30 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB33 BD C0 BA        ...   jsr c0ba		;display:
FB36 CC 07 D0        ...   ldd #07d0
FB39 BD E5 71        ..q   jsr e571		;jump13
FB3C 3B              ;     rti 
FB3D CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB40 BD C0 BA        ...   jsr c0ba		;display:
FB43 CC 07 D0        ...   ldd #07d0
FB46 BD E5 71        ..q   jsr e571		;jump13
FB49 3B              ;     rti 
FB4A CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB4D BD C0 BA        ...   jsr c0ba		;display:
FB50 CC 07 D0        ...   ldd #07d0
FB53 BD E5 71        ..q   jsr e571		;jump13
FB56 3B              ;     rti 
FB57 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB5A BD C0 BA        ...   jsr c0ba		;display:
FB5D CC 07 D0        ...   ldd #07d0
FB60 BD E5 71        ..q   jsr e571		;jump13
FB63 3B              ;     rti 
FB64 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB67 BD C0 BA        ...   jsr c0ba		;display:
FB6A CC 07 D0        ...   ldd #07d0
FB6D BD E5 71        ..q   jsr e571		;jump13
FB70 3B              ;     rti 
FB71 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB74 BD C0 BA        ...   jsr c0ba		;display:
FB77 CC 07 D0        ...   ldd #07d0
FB7A BD E5 71        ..q   jsr e571		;jump13
FB7D 3B              ;     rti 
FB7E CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB81 BD C0 BA        ...   jsr c0ba		;display:
FB84 CC 07 D0        ...   ldd #07d0
FB87 BD E5 71        ..q   jsr e571		;jump13
FB8A 3B              ;     rti 
FB8B CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB8E BD C0 BA        ...   jsr c0ba		;display:
FB91 CC 07 D0        ...   ldd #07d0
FB94 BD E5 71        ..q   jsr e571		;jump13
FB97 3B              ;     rti 
FB98 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FB9B BD C0 BA        ...   jsr c0ba		;display:
FB9E CC 07 D0        ...   ldd #07d0
FBA1 BD E5 71        ..q   jsr e571		;jump13
FBA4 3B              ;     rti 
FBA5 CC B1 2E        ...   ldd #b12e		;"**ERROR***"
FBA8 BD C0 BA        ...   jsr c0ba		;display:
FBAB CC 07 D0        ...   ldd #07d0
FBAE BD E5 71        ..q   jsr e571		;jump13
FBB1 3B              ;     rti 
FBB2 BD FE 69        ..i   jsr fe69		;inhibit interrupts
FBB5 3B              ;     rti 
FBB6 BD FE 69        ..i   jsr fe69		;inhibit interrupts
FBB9 3B              ;     rti 

jump38:
FBBA 3C              <     pshx 
FBBB 37              7     pshb 
FBBC 30              0     tsx 
FBBD E6 06           ..    ldab 06,x
FBBF 3D              =     mul 
FBC0 A6 00           ..    ldaa 00,x
FBC2 37              7     pshb 
FBC3 E6 05           ..    ldab 05,x
FBC5 3D              =     mul 
FBC6 32              2     pula 
FBC7 1B              .     aba 
FBC8 33              3     pulb 
FBC9 36              6     psha 
FBCA A6 06           ..    ldaa 06,x
FBCC 3D              =     mul 
FBCD 37              7     pshb 
FBCE AB 00           ..    adda 00,x
FBD0 36              6     psha 
FBD1 EC 03           ..    ldd 03,x
FBD3 ED 05           ..    stad 05,x
FBD5 38              8     pulx 
FBD6 8F              .     xgdx 
FBD7 83 00 00        ...   subd #0000
FBDA 31              1     ins 
FBDB 38              8     pulx 
FBDC 31              1     ins 
FBDD 31              1     ins 
FBDE 39              9     rts 

jump46:
FBDF BD FB E8        ...   jsr fbe8		;jump86
FBE2 BD FC 25        ..%   jsr fc25		;jump85
FBE5 7E FC 04        ~..   jmp fc04		;goto87

jump86:
FBE8 3C              <     pshx 
FBE9 8F              .     xgdx 
FBEA EC 02           ..    ldd 02,x
FBEC 37              7     pshb 
FBED 36              6     psha 
FBEE EC 00           ..    ldd 00,x
FBF0 37              7     pshb 
FBF1 36              6     psha 
FBF2 34              4     des 
FBF3 34              4     des 
FBF4 34              4     des 
FBF5 34              4     des 
FBF6 3C              <     pshx 
FBF7 30              0     tsx 
FBF8 EC 12           ..    ldd 12,x
FBFA ED 04           ..    stad 04,x
FBFC EC 0C           ..    ldd 0c,x
FBFE ED 02           ..    stad 02,x
FC00 EC 10           ..    ldd 10,x
FC02 38              8     pulx 
FC03 39              9     rts 

goto87:
FC04 ED 00           ..    stad 00,x
FC06 3C              <     pshx 
FC07 30              0     tsx 
FC08 EC 08           ..    ldd 08,x
FC0A ED 0A           ..    stad 0a,x
FC0C EC 02           ..    ldd 02,x
FC0E ED 0C           ..    stad 0c,x
FC10 38              8     pulx 
FC11 ED 02           ..    stad 02,x
FC13 EC 00           ..    ldd 00,x
FC15 26 06           &.    bne 06
FC17 6D 02           m.    tst 02,x
FC19 26 02           &.    bne 02
FC1B 6D 03           m.    tst 03,x
FC1D 31              1     ins 
FC1E 31              1     ins 
FC1F 38              8     pulx 
FC20 31              1     ins 
FC21 31              1     ins 
FC22 31              1     ins 
FC23 31              1     ins 
FC24 39              9     rts 

jump85:
FC25 3C              <     pshx 
FC26 30              0     tsx 
FC27 E3 06           ..    addd 06,x
FC29 37              7     pshb 
FC2A 36              6     psha 
FC2B EC 02           ..    ldd 02,x
FC2D ED 06           ..    stad 06,x
FC2F EC 04           ..    ldd 04,x
FC31 E3 08           ..    addd 08,x
FC33 ED 08           ..    stad 08,x
FC35 32              2     pula 
FC36 33              3     pulb 
FC37 C9 00           ..    adcb #00
FC39 89 00           ..    adca #00
FC3B 83 00 00        ...   subd #0000
FC3E 26 06           &.    bne 06
FC40 6D 08           m.    tst 08,x
FC42 26 02           &.    bne 02
FC44 6D 09           m.    tst 09,x
FC46 38              8     pulx 
FC47 31              1     ins 
FC48 31              1     ins 
FC49 31              1     ins 
FC4A 31              1     ins 
FC4B 39              9     rts 

jump83:
FC4C 3C              <     pshx 
FC4D 3C              <     pshx 
FC4E 37              7     pshb 
FC4F 36              6     psha 
FC50 3C              <     pshx 
FC51 3C              <     pshx 
FC52 3C              <     pshx 
FC53 18 3C           .<    pshy 
FC55 30              0     tsx 
FC56 4F              O     clra 
FC57 A7 04           ..    sta 04,x
FC59 7E FC 6D        ~.m   jmp fc6d		;goto88

jump82:
FC5C 3C              <     pshx 
FC5D 3C              <     pshx 
FC5E 37              7     pshb 
FC5F 36              6     psha 
FC60 3C              <     pshx 
FC61 3C              <     pshx 
FC62 3C              <     pshx 
FC63 18 3C           .<    pshy 
FC65 30              0     tsx 
FC66 86 01           ..    ldaa #01
FC68 A7 04           ..    sta 04,x
FC6A 7E FC 6D        ~.m   jmp fc6d		;goto88
goto88:
FC6D 1A EE 0E        ...   ldy 0e,x
FC70 EC 08           ..    ldd 08,x
FC72 ED 0E           ..    stad 0e,x
FC74 EC 14           ..    ldd 14,x
FC76 ED 0C           ..    stad 0c,x
FC78 EC 12           ..    ldd 12,x
FC7A ED 0A           ..    stad 0a,x
FC7C 1A EF 12        ...   sty 12,x
FC7F 4F              O     clra 
FC80 5F              _     clrb 
FC81 ED 08           ..    stad 08,x
FC83 ED 06           ..    stad 06,x
FC85 EC 0A           ..    ldd 0a,x
FC87 1A A3 0E        ...   cmpd 0e,x
FC8A 22 23           "#    bhi 23
FC8C 27 15           '.    beq 15
FC8E 1E 04 01 08     ....  brset 04,x 01 08
FC92 CC 00 00        ...   ldd #0000
FC95 ED 14           ..    stad 14,x
FC97 7E FE 04        ~..   jmp fe04		;goto89
FC9A EC 0C           ..    ldd 0c,x
FC9C ED 14           ..    stad 14,x
FC9E EC 0A           ..    ldd 0a,x
FCA0 7E FD E3        ~..   jmp fde3		;goto90
FCA3 83 00 00        ...   subd #0000
FCA6 27 12           '.    beq 12
FCA8 EC 0C           ..    ldd 0c,x
FCAA 1A A3 10        ...   cmpd 10,x
FCAD 25 DF           %.    bcs df
FCAF 6D 0E           m.    tst 0e,x
FCB1 26 70           &p    bne 70
FCB3 6D 0F           m.    tst 0f,x
FCB5 27 1A           '.    beq 1a
FCB7 7E FD A4        ~..   jmp fda4		;goto91
FCBA 3C              <     pshx 
FCBB 3C              <     pshx 
FCBC 18 38           .8    puly 
FCBE EC 0C           ..    ldd 0c,x
FCC0 EE 10           ..    ldx 10,x
FCC2 02              .     idiv 
FCC3 18 1E 04 01 01  ..... brset 04,y 01 01
FCC8 8F              .     xgdx 
FCC9 38              8     pulx 
FCCA ED 14           ..    stad 14,x
FCCC 4F              O     clra 
FCCD 5F              _     clrb 
FCCE 7E FD E3        ~..   jmp fde3		;goto90
FCD1 3C              <     pshx 
FCD2 3C              <     pshx 
FCD3 18 38           .8    puly 
FCD5 EC 0A           ..    ldd 0a,x
FCD7 EE 10           ..    ldx 10,x
FCD9 3C              <     pshx 
FCDA 02              .     idiv 
FCDB CD EF 0A        ...   stx 02x,y
FCDE 38              8     pulx 
FCDF 3C              <     pshx 
FCE0 03              .     fdiv 
FCE1 CD EF 0E        ...   stx 02x,y
FCE4 38              8     pulx 
FCE5 18 E3 0C        ...   addd 0c,y
FCE8 24 1B           $.    bcc 1b
FCEA 18 ED 0C        ...   stad 0c,y
FCED 4F              O     clra 
FCEE C6 01           ..    ldab #01
FCF0 3C              <     pshx 
FCF1 03              .     fdiv 
FCF2 8F              .     xgdx 
FCF3 18 E3 0E        ...   addd 0e,y
FCF6 24 03           $.    bcc 03
FCF8 18 6C 0A        .l.   inc 0a,y
FCFB 18 ED 0E        ...   stad 0e,y
FCFE 8F              .     xgdx 
FCFF 38              8     pulx 
FD00 18 E3 0C        ...   addd 0c,y
FD03 25 E5           %.    bcs e5
FD05 02              .     idiv 
FD06 8F              .     xgdx 
FD07 18 E3 0E        ...   addd 0e,y
FD0A 24 03           $.    bcc 03
FD0C 18 6C 0A        .l.   inc 0a,y
FD0F 18 1F 04 01 07  ..... brclr 04,y 01 07
FD14 8F              .     xgdx 
FD15 18 6F 0A        .o.   clr 0a,y
FD18 18 6F 0B        .o.   clr 0b,y
FD1B 38              8     pulx 
FD1C ED 14           ..    stad 14,x
FD1E EC 0A           ..    ldd 0a,x
FD20 7E FD E3        ~..   jmp fde3		;goto90
FD23 86 08           ..    ldaa #08
FD25 A7 05           ..    sta 05,x
FD27 69              i     rol 
FD28 0D              .     sec 
FD29 69              i     rol 
FD2A 0C              .     clc 
FD2B 69              i     rol 
FD2C 0B              .     sev 
FD2D 69              i     rol 
FD2E 0A              .     clv 
FD2F 69              i     rol 
FD30 09              .     dex 
FD31 EC 0B           ..    ldd 0b,x
FD33 A3 10           ..    subd 10,x
FD35 ED 14           ..    stad 14,x
FD37 EC 09           ..    ldd 09,x
FD39 E2 0F           ..    sbcb 0f,x
FD3B A2 0E           ..    sbca 0e,x
FD3D 25 06           %.    bcs 06
FD3F ED 09           ..    stad 09,x
FD41 EC 14           ..    ldd 14,x
FD43 ED 0B           ..    stad 0b,x
FD45 6A 05           j.    dec 05,x
FD47 26 DE           &.    bne de
FD49 69              i     rol 
FD4A 0D              .     sec 
FD4B 1F 04 01 09     ....  brclr 04,x 01 09
FD4F EC 0B           ..    ldd 0b,x
FD51 ED 14           ..    stad 14,x
FD53 EC 09           ..    ldd 09,x
FD55 7E FD E3        ~..   jmp fde3		;goto90
FD58 4F              O     clra 
FD59 E6 0D           ..    ldab 0d,x
FD5B 53              S     comb 
FD5C ED 14           ..    stad 14,x
FD5E 5F              _     clrb 
FD5F 7E FD E3        ~..   jmp fde3		;goto90
FD62 86 10           ..    ldaa #10
FD64 A7 05           ..    sta 05,x
FD66 69              i     rol 
FD67 0D              .     sec 
FD68 69              i     rol 
FD69 0C              .     clc 
FD6A 69              i     rol 
FD6B 0B              .     sev 
FD6C 69              i     rol 
FD6D 0A              .     clv 
FD6E 69              i     rol 
FD6F 09              .     dex 
FD70 69              i     rol 
FD71 08              .     inx 
FD72 EC 0A           ..    ldd 0a,x
FD74 A3 10           ..    subd 10,x
FD76 ED 14           ..    stad 14,x
FD78 EC 08           ..    ldd 08,x
FD7A E2 0F           ..    sbcb 0f,x
FD7C 82 00           ..    sbca #00
FD7E 25 06           %.    bcs 06
FD80 ED 08           ..    stad 08,x
FD82 EC 14           ..    ldd 14,x
FD84 ED 0A           ..    stad 0a,x
FD86 6A 05           j.    dec 05,x
FD88 26 DC           &.    bne dc
FD8A 69              i     rol 
FD8B 0D              .     sec 
FD8C 69              i     rol 
FD8D 0C              .     clc 
FD8E 1F 04 01 08     ....  brclr 04,x 01 08
FD92 EC 0A           ..    ldd 0a,x
FD94 ED 14           ..    stad 14,x
FD96 EC 08           ..    ldd 08,x
FD98 20 49            I    bra 49
FD9A EC 0C           ..    ldd 0c,x
FD9C 43              C     coma 
FD9D 53              S     comb 
FD9E ED 14           ..    stad 14,x
FDA0 4F              O     clra 
FDA1 5F              _     clrb 
FDA2 20 3F            ?    bra 3f
goto91:
FDA4 2B BC           +.    bmi bc
FDA6 86 10           ..    ldaa #10
FDA8 A7 05           ..    sta 05,x
FDAA 69              i     rol 
FDAB 0D              .     sec 
FDAC 69              i     rol 
FDAD 0C              .     clc 
FDAE 69              i     rol 
FDAF 0B              .     sev 
FDB0 69              i     rol 
FDB1 0A              .     clv 
FDB2 69              i     rol 
FDB3 07              .     tpa 
FDB4 EC 0A           ..    ldd 0a,x
FDB6 A3 10           ..    subd 10,x
FDB8 ED 14           ..    stad 14,x
FDBA A6 07           ..    ldaa 07,x
FDBC A2 0F           ..    sbca 0f,x
FDBE 25 06           %.    bcs 06
FDC0 A7 07           ..    sta 07,x
FDC2 EC 14           ..    ldd 14,x
FDC4 ED 0A           ..    stad 0a,x
FDC6 6A 05           j.    dec 05,x
FDC8 26 E0           &.    bne e0
FDCA 69              i     rol 
FDCB 0D              .     sec 
FDCC 69              i     rol 
FDCD 0C              .     clc 
FDCE 1F 04 01 09     ....  brclr 04,x 01 09
FDD2 EC 0A           ..    ldd 0a,x
FDD4 ED 14           ..    stad 14,x
FDD6 4F              O     clra 
FDD7 E6 07           ..    ldab 07,x
FDD9 20 08            .    bra 08
FDDB EC 0C           ..    ldd 0c,x
FDDD 43              C     coma 
FDDE 53              S     comb 
FDDF ED 14           ..    stad 14,x
FDE1 4F              O     clra 
FDE2 5F              _     clrb 
goto90:
FDE3 1F 04 80 12     ....  brclr 04,x 80 12
FDE7 43              C     coma 
FDE8 53              S     comb 
FDE9 63 14           c.    com 14,x
FDEB 63 15           c.    com 15,x
FDED 6C 15           l.    inc 15,x
FDEF 26 08           &.    bne 08
FDF1 6C 14           l.    inc 14,x
FDF3 26 04           &.    bne 04
FDF5 5C              \     incb 
FDF6 26 01           &.    bne 01
FDF8 4C              L     inca 
FDF9 83 00 00        ...   subd #0000
FDFC 26 06           &.    bne 06
FDFE 6D 14           m.    tst 14,x
FE00 26 02           &.    bne 02
FE02 6D 15           m.    tst 15,x
goto89:
FE04 18 38           .8    puly 
FE06 38              8     pulx 
FE07 31              1     ins 
FE08 31              1     ins 
FE09 31              1     ins 
FE0A 31              1     ins 
FE0B 31              1     ins 
FE0C 31              1     ins 
FE0D 31              1     ins 
FE0E 31              1     ins 
FE0F 31              1     ins 
FE10 31              1     ins 
FE11 31              1     ins 
FE12 31              1     ins 
FE13 31              1     ins 
FE14 31              1     ins 
FE15 39              9     rts 

jump2:
FE16 38              8     pulx 
FE17 A3 00           ..    subd 00,x
FE19 2D 05           -.    blt 05
FE1B 1A A3 02        ...   cmpd 02,x
FE1E 2F 03           /.    ble 03
FE20 CC FF FF        ...   ldd #ffff
FE23 3C              <     pshx 
FE24 05              .     asld 
FE25 30              0     tsx 
FE26 E3 00           ..    addd 00,x
FE28 C3 00 06        ...   addd #0006
FE2B 8F              .     xgdx 
FE2C EC 00           ..    ldd 00,x
FE2E 30              0     tsx 
FE2F ED 00           ..    stad 00,x
FE31 39              9     rts 

jump25:
FE32 38              8     pulx 
FE33 6D 04           m.    tst 04,x
FE35 26 04           &.    bne 04
FE37 6D 05           m.    tst 05,x
FE39 27 0A           '.    beq 0a
FE3B A3 02           ..    subd 02,x
FE3D 27 06           '.    beq 06
FE3F 08              .     inx 
FE40 08              .     inx 
FE41 08              .     inx 
FE42 08              .     inx 
FE43 20 EE            .    bra ee
FE45 EE 00           ..    ldx 00,x
FE47 6E 00           n.    jmp 00,x

	reset:

FE49 86 40           .@    ldaa #40	;40 -> A
FE4B B7 10 24        ..$   sta 1024	;A -> 1024
FE4E 86 A0           ..    ldaa #a0	;a0 -> A
FE50 B7 10 39        ..9   sta 1039	;A -> 1024
FE53 8E 00 EF        ...   lds #00ef
FE56 CE 00 00        ...   ldx #0000	;wipe 0000-007f  X=0000
FE59 8C 00 80        ...   cpx #0080	;loop: if X = 0080
FE5C 24 05           $.    bcc fe63	;        finished
FE5E 6F 00           o.    clr 00,x	;      else: load addr X with 0
FE60 08              .     inx 		;        increment X
FE61 20 F6            .    bra fe59	;repeat
FE63 BD F7 D6        ...   jsr f7d6	;goto main:
FE66 7E FE 6D        ~.m   jmp fe6d	; goto91

FE69 0F              .     sei 		;inhibit interrupts
FE6A 7E FE 49        ~.I   jmp fe49	;goto reset:

goto91:
FE6D 20 FE            .    bra fe6d	;loop forever
					;[SAF] - wait for interrupts?

FE6F FF FF FF        ...   stx ffff
FE72 FF FF FF        ...   stx ffff
FE75 FF FF FF        ...   stx ffff
FE78 FF FF FF        ...   stx ffff
FE7B FF FF FF        ...   stx ffff
FE7E FF FF FF        ...   stx ffff
FE81 FF FF FF        ...   stx ffff
FE84 FF FF FF        ...   stx ffff
FE87 FF FF FF        ...   stx ffff
FE8A FF FF FF        ...   stx ffff
FE8D FF FF FF        ...   stx ffff
FE90 FF FF FF        ...   stx ffff
FE93 FF FF FF        ...   stx ffff
FE96 FF FF FF        ...   stx ffff
FE99 FF FF FF        ...   stx ffff
FE9C FF FF FF        ...   stx ffff
FE9F FF FF FF        ...   stx ffff
FEA2 FF FF FF        ...   stx ffff
FEA5 FF FF FF        ...   stx ffff
FEA8 FF FF FF        ...   stx ffff
FEAB FF FF FF        ...   stx ffff
FEAE FF FF FF        ...   stx ffff
FEB1 FF FF FF        ...   stx ffff
FEB4 FF FF FF        ...   stx ffff
FEB7 FF FF FF        ...   stx ffff
FEBA FF FF FF        ...   stx ffff
FEBD FF FF FF        ...   stx ffff
FEC0 FF FF FF        ...   stx ffff
FEC3 FF FF FF        ...   stx ffff
FEC6 FF FF FF        ...   stx ffff
FEC9 FF FF FF        ...   stx ffff
FECC FF FF FF        ...   stx ffff
FECF FF FF FF        ...   stx ffff
FED2 FF FF FF        ...   stx ffff
FED5 FF FF FF        ...   stx ffff
FED8 FF FF FF        ...   stx ffff
FEDB FF FF FF        ...   stx ffff
FEDE FF FF FF        ...   stx ffff
FEE1 FF FF FF        ...   stx ffff
FEE4 FF FF FF        ...   stx ffff
FEE7 FF FF FF        ...   stx ffff
FEEA FF FF FF        ...   stx ffff
FEED FF FF FF        ...   stx ffff
FEF0 FF FF FF        ...   stx ffff
FEF3 FF FF FF        ...   stx ffff
FEF6 FF FF FF        ...   stx ffff
FEF9 FF FF FF        ...   stx ffff
FEFC FF FF FF        ...   stx ffff
FEFF FF FF FF        ...   stx ffff
FF02 FF FF FF        ...   stx ffff
FF05 FF FF FF        ...   stx ffff
FF08 FF FF FF        ...   stx ffff
FF0B FF FF FF        ...   stx ffff
FF0E FF FF FF        ...   stx ffff
FF11 FF FF FF        ...   stx ffff
FF14 FF FF FF        ...   stx ffff
FF17 FF FF FF        ...   stx ffff
FF1A FF FF FF        ...   stx ffff
FF1D FF FF FF        ...   stx ffff
FF20 FF FF FF        ...   stx ffff
FF23 FF FF FF        ...   stx ffff
FF26 FF FF FF        ...   stx ffff
FF29 FF FF FF        ...   stx ffff
FF2C FF FF FF        ...   stx ffff
FF2F FF FF FF        ...   stx ffff
FF32 FF FF FF        ...   stx ffff
FF35 FF FF FF        ...   stx ffff
FF38 FF FF FF        ...   stx ffff
FF3B FF FF FF        ...   stx ffff
FF3E FF FF FF        ...   stx ffff
FF41 FF FF FF        ...   stx ffff
FF44 FF FF FF        ...   stx ffff
FF47 FF FF FF        ...   stx ffff
FF4A FF FF FF        ...   stx ffff
FF4D FF FF FF        ...   stx ffff
FF50 FF FF FF        ...   stx ffff
FF53 FF FF FF        ...   stx ffff
FF56 FF FF FF        ...   stx ffff
FF59 FF FF FF        ...   stx ffff
FF5C FF FF FF        ...   stx ffff
FF5F FF FF FF        ...   stx ffff
FF62 FF FF FF        ...   stx ffff
FF65 FF FF FF        ...   stx ffff
FF68 FF FF FF        ...   stx ffff
FF6B FF FF FF        ...   stx ffff
FF6E FF FF FF        ...   stx ffff
FF71 FF FF FF        ...   stx ffff
FF74 FF FF FF        ...   stx ffff
FF77 FF FF FF        ...   stx ffff
FF7A FF FF FF        ...   stx ffff
FF7D FF FF FF        ...   stx ffff
FF80 FF FF FF        ...   stx ffff
FF83 FF FF FF        ...   stx ffff
FF86 FF FF FF        ...   stx ffff
FF89 FF FF FF        ...   stx ffff
FF8C FF FF FF        ...   stx ffff
FF8F FF FF FF        ...   stx ffff
FF92 FF FF FF        ...   stx ffff
FF95 FF FF FF        ...   stx ffff
FF98 FF FF FF        ...   stx ffff
FF9B FF FF FF        ...   stx ffff
FF9E FF FF FF        ...   stx ffff
FFA1 FF FF FF        ...   stx ffff
FFA4 FF FF FF        ...   stx ffff
FFA7 FF FF FF        ...   stx ffff
FFAA FF FF FF        ...   stx ffff
FFAD FF FF FF        ...   stx ffff
FFB0 FF FF FF        ...   stx ffff
FFB3 FF FF FF        ...   stx ffff
FFB6 FF FF FF        ...   stx ffff
FFB9 FF FF FF        ...   stx ffff
FFBC FF FF FF        ...   stx ffff
FFBF FF FF FF        ...   stx ffff
FFC2 FF FF FF        ...   stx ffff
FFC5 FF FF FF        ...   stx ffff
FFC8 FF FF FF        ...   stx ffff
FFCB FF FF FF        ...   stx ffff
FFCE FF FF FF        ...   stx ffff
FFD1 FF FF FF        ...   stx ffff
FFD4 FF FF 

FFD6	;vectors
	.word	c81c
	.word	fae2
	.word	faef
	.word	fafc
	.word	fb09
	.word	fb16
	.word	fb23
	.word	fb30
	.word	fb3d
	.word	fb4a
	.word	fb57
	.word	fb64
	.word	fb71
	.word	f718
	.word	fb7e
	.word	fb8b
	.word	fb98
	.word	fba5
	.word	fbb2
	.word	fbb6
	.word	fe49

