my %commands;

$commands{'bra'}=1;
$commands{'bcc'}=1;
$commands{'bcs'}=1;
$commands{'beq'}=1;

$commands{'bge'}=1;
$commands{'bgt'}=1;
$commands{'bhi'}=1;
$commands{'ble'}=1;

$commands{'bls'}=1;
$commands{'blt'}=1;
$commands{'bmi'}=1;
$commands{'bne'}=1;

$commands{'bhs'}=1;
$commands{'blo'}=1;
$commands{'bsr'}=1;

$commands{'bpl'}=1;
$commands{'brn'}=1;
$commands{'bvc'}=1;
$commands{'bvs'}=1;

sub convert {
	($onum) = @_;

	$num = hex $onum;

	if ($num & 128) {
		$num = $num - 256;
	}

	return $num;

}

#printf "%X", convert("E1");
#printf "\n";
#printf "%X", convert("71");
#printf "\n";


while(<>) {	
	chomp;
	$l = length;
	if ($l == 42) {
		print "$_\n";
		next;
	} elsif ($l != 33) {
		print "$_\n";
		next;
	} else {

	$addr = substr($_,0,4);
	$inst = substr($_,27,3);
	$val = substr($_,31,2);

	#print "$val\n";
	$daddr = (hex $addr) + 2 + convert($val);

	
	if (defined $commands{$inst}) {
		print "$_\t\t;";
		printf "%4X", $daddr;
		#print " $addr $inst $val\n";
		print "\n";
	} 
	}
}
