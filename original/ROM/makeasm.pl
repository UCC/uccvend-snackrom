#!/usr/local/bin/perl -w

print "\tORG\t\$8000\n";

my %transtable = ( '02' => 'IDIV' ,
			'03' => 'FDIV' ,
			'08' => 'INX' , 
			'09' => 'DEX',
			'0A' => 'CLV',
			'0D' => 'SEC',
			'0E' => 'CLI',
			'0F' => 'SEI',
			'19' => 'DAA',
			'1B' => 'ABA',
			'30' => 'TSX',
			'31' => 'INS',
			'32' => 'PULA',
			'33' => 'PULB',
			'34' => 'DES',
			'35' => 'TXS',
			'36' => 'PSHA',
			'37' => 'PSHB',
			'3C' => 'PSHX',
			'38' => 'PULX',
			'3A' => 'ABX',
			'3B' => 'RTI',
			'3E' => 'WAI',
			'43' => 'COMA',
			'46' => 'RORA',
			'49' => 'ROLA',
			'4A' => 'DECA',
			'4C' => 'INCA',
			'4F' => 'CLRA',
			'53' => 'COMB',
			'56' => 'RORB',
			'59' => 'ROLB',
			'5A' => 'DECB',
			'5F' => 'CLRB',
			'8F' => 'XGDX',
			'39' => 'RTS' );

		#	'10' => 'SBA',


my %twobytefirst = ( '18' => '1' );

my %twobytesecond = ( '18 38' => 'PULY',
			'18 3A' => 'ABY',
			'18 3C' => 'PSHY');

my %twobytesecondword = ( '18 FE' => 'LDY\t>%s',
			'18 FF' => 'STY\t>%s');

my %onewitharg = ('60' => "NEG\t\$%s,X",
			'63' => "COM\t\$%s,X",
			'64' => "LSR\t\$%s,X",
			'67' => "ASR\t\$%s,X",
			'68' => "ASL\t\$%s,X",
			'6A' => "DEC\t\$%s,X",
			'6C' => "INC\t\$%s,X",
			'6D' => "TST\t\$%s,X",
			'6E' => "JMP\t\$%s,X",
			'6F' => "CLR\t\$%s,X",
			'80' => "SUBA\t#\$%s",
			'81' => "CMPA\t#\$%s",
			'82' => "SBCA\t#\$%s",
			'84' => "ANDA\t#\$%s",
			'85' => "BITA\t#\$%s",
			'86' => "LDAA\t#\$%s",
			'88' => "EORA\t#\$%s",
			'8A' => "ORA\t#\$%s",
			'8B' => "ADDA\t#\$%s",
			'A0' => "SUBA\t\$%s,X",
			'A1' => "CMPA\t\$%s,X",
			'A2' => "SBCA\t\$%s,X",
			'A3' => "SUBD\t\$%s,X",
			'A4' => "ANDA\t\$%s,X",
			'A6' => "LDAA\t\$%s,X",
			'A7' => "STA\t\$%s,X",
			'A8' => "EORA\t\$%s,X",
			'A9' => "ADCA\t\$%s,X",
			'AA' => "ORAA\t\$%s,X",
			'AB' => "ADDA\t\$%s,X",
			'AC' => "CPX\t\$%s,X",
			'AD' => "JSR\t\$%s,X",
			'AE' => "LDS\t\$%s,X",
			'AF' => "STS\t\$%s,X",
			'C0' => "SUBB\t#\$%s",
			'C1' => "CMPB\t#\$%s",
			'C2' => "SBCB\t#\$%s",
			'C4' => "ANDB\t#\$%s",
			'C5' => "BITB\t#\$%s",
			'C6' => "LDAB\t#\$%s",
			'C8' => "EORB\t#\$%s",
			'C9' => "ADCB\t#\$%s",
			'CA' => "ORB\t#\$%s",
			'CB' => "ADDB\t#\$%s",
			'E0' => "SUBB\t\$%s,X",
			'E1' => "CMPB\t\$%s,X",
			'E2' => "SBCB\t\$%s,X",
			'E3' => "ADDD\t\$%s,X",
			'E4' => "ANDB\t\$%s,X",
			'E6' => "LDAB\t\$%s,X",
			'E7' => "STAB\t\$%s,X",
			'E8' => "EORB\t\$%s,X",
			'E9' => "ADCB\t\$%s,X",
			'EA' => "ORAB\t\$%s,X",
			'EB' => "ADDB\t\$%s,X",
			'EC' => "LDD\t\$%s,X",
			'ED' => "STD\t\$%s,X",
			'EE' => "LDX\t\$%s,X",
			'EF' => "STX\t\$%s,X" );


my %onewithword = (
		   '70' => "NEG\t>%s",
		   '73' => "COM\t>%s",
		   '74' => "LSR\t>%s",
		   '76' => "ROR\t>%s",
		   '77' => "ASR\t>%s",
		   '78' => "LSL\t>%s",
# LSL == ASL
#		   '78' => "ASL\t>%s",
		   '79' => "ROL\t>%s",
		   '7A' => "DEC\t>%s",
		   '7C' => "INC\t>%s",
		   '7D' => "TST\t>%s",
		   '7F' => "CLR\t>%s",
#		   '95' => "BITA\t<%s",
		   'B0' => "SUBA\t>%s",
		   'B1' => "CMPA\t>%s",
		   'B2' => "SBCA\t>%s",
		   'B3' => "SUBD\t>%s",
		   'B4' => "ANDA\t>%s",
		   'B5' => "BITA\t>%s",
		   'B6' => "LDAA\t>%s",
		   'B7' => "STAA\t>%s",
		   'B9' => "ADCA\t>%s",
		   'BA' => "ORAA\t>%s",
		   'BB' => "ADDA\t>%s",
		   'BC' => "CPX\t>%s",
		   'BD' => "JSR\t>%s",
		   'BE' => "LDS\t>%s",
		   'BF' => "STS\t>%s",
		   'D5' => "BITB\t>%s",
		   'F0' => "SUBB\t>%s",
		   'F1' => "CMPB\t>%s",
		   'F2' => "SBCB\t>%s",
		   'F3' => "ADDD\t>%s",
		   'F4' => "ANDB\t>%s",
		   'F5' => "BITB\t>%s",
		   'F6' => "LDAB\t>%s",
		   'F7' => "STAB\t>%s",
		   'FA' => "ORAB\t>%s",
		   'FB' => "ADDB\t>%s",
		   'F8' => "EORB\t>%s",
		   'F9' => "ADCB\t>%s",
		   'FC' => "LDD\t>%s",
		   'FD' => "STD\t>%s",
		   'FE' => "LDX\t>%s",
		   'FF' => "STX\t>%s" );

LINE: while (<>) {

# if blank line, print blank line
  if (/^\s*$/) { print "\n"; next LINE; }

# if comment line, pass straight through
  if (/^\s*;/) { print; next LINE; }

# if duplicate label - rename uniquely
  if (/^\s*((loop|jump82|goto91):)/) { print "${1}_$.\n"; next LINE; }

# if it's a label, print label
  if (/^\s*(\w*:)/) { print "$1\n"; next LINE; }

# if it's a vector table, print words
  if (/^\t\.word\t([a-f0-9]{4})$/) { print "\tFDB\t\$\U$1\n"; next LINE; }

# otherwise, catch all, print bytes
  if (/^([A-F0-9]{4}) ((([A-F0-9]{2}) )*[A-F0-9]{2})/) {
    instruction($_);
    next LINE;
  }

# if control Z, remove it
  if (/^\cZ$/) { next LINE; }

# FINAL print commented out
  print ";$_";
}

# Process one instruction
# Usage: instruction line
sub instruction {
    my ($line) = @_;

    $instruction = substr($line,5,2);

    if (defined $transtable{$instruction}) {
    print ";$line";
    	print "\t$transtable{$instruction}";
	$line =~ /$transtable{$instruction}(.*)$/i;
	print "$1\n";
	return;
    }
   
    if (defined $twobytefirst{$instruction}) {
    print ";$line";
    	$thing = substr($line,5,5);
    	if (defined $twobytesecond{$thing}) {
    		print "\t" . $twobytesecond{$thing};
		$line =~ /$twobytesecond{$thing}(.*)$/i;
		print "$1\n";
		return;
	}
    
	if (defined $twobytesecondword{$thing}) {
    		$athing = substr($line,11,2);
    		$athing2 = substr($line,14,2);
		if (!($athing2 =~ /\s+/)) {
    		 $curraddr = substr($line,0,4);
		 printf "FOO%s\tEQU\t\$%s%s\n", $curraddr,$athing,$athing2;
    		 printf "\t$twobytesecondword{$thing}", "FOO$curraddr";
		 $line =~ /$twobytesecondword{$thing} [0-9A-F][0-9A-F][0-9A-F][0-9A-F](.*)$/i;
		 print "\t;$1\n";
		return;
		}
    	}
    }
    
    if (defined $onewitharg{$instruction}) {
    print ";$line";
    	$thing = substr($line,8,2);
	
	printf "\t$onewitharg{$instruction}", $thing;
	$line =~ /$onewitharg{$instruction} [0-9A-F][0-9A-F](.*)$/i;
	print "\t;$1\n";
	return;
    }
   
    if (defined $onewithword{$instruction}) {
    print ";$line";
    	$thing = substr($line,8,2);
    	$thing2 = substr($line,11,2);
	if (!($thing2 =~ /\s+/)) {
    	$curraddr = substr($line,0,4);
	printf "FOO%s\tEQU\t\$%s%s\n", $curraddr,$thing,$thing2;
    	printf "\t$onewithword{$instruction}", "FOO$curraddr";
	$line =~ /$onewithword{$instruction} [0-9A-F][0-9A-F][0-9A-F][0-9A-F](.*)$/i;
	print "\t;$1\n";
	return;
	}
    }
   
    print ";$line";
    if ($line =~ /^([A-F0-9]{4}) ((([A-F0-9]{2}) )*[A-F0-9]{2})/) {
        @_=split(' ',$2);
        print "\tFCB\t\$".join(', $',@_)."\n";
    }
}
