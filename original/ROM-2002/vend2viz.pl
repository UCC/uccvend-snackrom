#!/usr/bin/perl
#
#
#
$file = shift;

#
$curlabel = "";

print <<END;
digraph "Referer Graph" {
  fontsize = 9;
  ratio = auto;
  rotate = 90;
  page="8.2677165,11.692913" ; /* A4 page size */
  margin="0,0" ; /* default margin is 0.5 inches */

END

open (F, $file);
while (<F>) {
	($address, $label, $rest) = split(/\t/,$_,3);
	chomp $rest;

	if ($label =~ /^(\w+):/) {
		$curlabel = $1;
		push @labellist, $curlabel;
		print " $curlabel [label=\"$curlabel\"];\n";
	}

#	if ($rest =~ /(L\w\w\w\w)/) {
#		print "label $1 spotted in $curlabel\n";
#	}

}

close (F);

%fish = {};

open (F, $file);
while (<F>) {
	($address, $label, $rest) = split(/\t/,$_,3);
	chomp $rest;

	if ($label =~ /^(\w+):/) {
		$curlabel = $1;
		push @labellist, $label;
#		print "spotted label $curlabel\n";
	}

	if ($rest =~ /(L\w\w\w\w)/) {
#		print "label $1 spotted in $curlabel\n";
		print "$curlabel -> $1 [];\n";
	}

}
close (F);

print "}\n";

