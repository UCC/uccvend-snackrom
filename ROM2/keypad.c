#include "chime.h"
#include "vend.h"
#include "keypad.h"
#include "display_basic.h"

volatile u8 last_key; /* the last key registered */
volatile bool new_key;

/* first 8 from the first row, then 3 from the second row */
/* keys are 1-9, 0, reset */
const u8 keymap0[8] = {KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8};
const u8 keymap1[3] = {KEY_9, KEY_0, KEY_RESET};

#define	NO_KEY	8

/* reads the keypad and returns the bit number that was turned on in the shift
 * register (from 0..7). If no bits were turned on, it returns 8 (aka NO_KEY)
 */
extern inline u8 keypad_read_row(const u8 row) {
	u8 i, num;

	if (row)
		bset((void*)&_io_ports[M6811_PORTD], PORTD_KEYPAD_ROW);
	else
		bclr((void*)&_io_ports[M6811_PORTD], PORTD_KEYPAD_ROW);

	spi_enable();

	bset_misc_output(A3800_KEYPAD_STROBE);
	bclr_misc_output(A3800_KEYPAD_STROBE);

	bclr_misc_output(A3800_DISPLAY_WRITE);  /* disable the display clock */

	_io_ports[M6811_SPDR] = 0x55; /* doesn't matter what we send. */
	while(!(_io_ports[M6811_SPSR]&M6811_SPIF)); /* wait for completion */

	/* SPDR read to clear SPIF flag is performed below: */
	i = _io_ports[M6811_SPDR];

	num = 0;
	while (((i & 0x80) == 0) && (num < 8)) {
		i = i << 1;
		num++;
	}

	spi_disable();

	return num;
}

/* sets last_key to 1..10 or 11 for reset */
void keypad_read() {
	/* FIXME: need to do debouncing of some sort? */
	u8 key;
	key = keypad_read_row(0);
	if (NO_KEY == key) {
		key = keypad_read_row(1);
		if (key <= 2)
			key = keymap1[key];
		else
			key = 0;
	} else
		key = keymap0[key];

	if (key != last_key) {
		last_key = key;
		if (key != 0) {
			new_key = 1;
			chime_start();
		}
	}
}

bool keypad_pressed() {
	if (!new_key) return 0;
	new_key = 0;
	return 1;
}

void keypad_init() {
	last_key = 0;
	new_key = 0;
}
