#include <stdio.h>
#define CRC16 0x1021		/* Generator polynomial (X^16 + X^12 + X^5 + 1) */
int main() {
	int val;
	printf("const u16 crctab[] = {\n\t");
	for (val = 0; val <= 255; val++) {
		int i;
		unsigned int crc;
		crc = val << 8;
		for (i = 0; i < 8; i++) {
			crc <<= 1;
			if (crc & 0x10000)
				crc ^= CRC16;
		}
		printf("0x%04x,", crc&0xffff);
		if ((val+1)%8 == 0) printf("\n\t");
    }
	printf("};\n");
	return 0;
}
