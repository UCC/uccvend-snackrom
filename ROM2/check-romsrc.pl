#!/usr/bin/perl -w

# looking for a line like:
# 00009800 g       .rodata        00000000 _rom_src_data

open(OD, "m68hc11-objdump -x rom2.elf|") or die "Could not open objdump of rom image!\n";
while (<OD>) {
	if (/^([0-9a-fA-F]+).*_rom_src_data$/) { $origin = $1 }
}
close OD;
if (!defined $origin) {
	print "WARNING!!! No lzip data could be found in the ROM image!\n";
	exit 1;
}
$origin = hex($origin);

open(PL, "src2asm.pl") or die "Could not open src2asm.pl\n";
while (<PL>) {
	if (/^\$origin = 0x([0-9a-fA-F]+);$/) { $pl_origin = $1 }
}
close PL;
if (!defined $pl_origin) {
	print "WARNING!!! Couldn't find origin in src2asm.pl!\n";
	exit 2;
}
$pl_origin = hex($pl_origin);
if ($pl_origin != $origin) {
	printf "WARNING!!! The origin of the lzip data is now 0x%04x. This needs to\n", $origin;
	printf "           be updated in src2asm.pl (which currently says 0x%04x).\n", $pl_origin;
	exit 3;
}
printf "Origin of lzip data is 0x%04x and correct.\n", $origin;
