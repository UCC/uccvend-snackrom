.sect .text
.globl _start

_start:
	;; set port a to output
	ldab #0xfc  ; 11111000
	stab 0x1001

	;; start chiming
	ldx #0x1000
	bset 00,x #0x10

	;; enable the RTI
	ldaa #0x40
	staa 0x1024

	;; enable the ADC, and configure IRQ' for edge-sensitive operation
	ldaa #0xa0
	staa 0x1039

	;; set RTI intervals, and PA3/7 DDRs
	ldaa #0x81
	staa 0x1026  ;; RTI interval becomes E/2^14 = 6.6ms (or 150 Hz)

	;; clear ADC register
	clra
	staa 0x1030

	;; set the stack pointer
	lds #_stack

	;; blank initialised variables - should match memory.x's page0
	ldx #0x0000
loop1:
	cpx #0x0100
	bcc out1
	clr 00,x
	inx
	bra loop1
out1:

	;; stop chiming
	ldx #0x1000
	bclr 00,x #0x10

	jsr main

infinity:
	bra infinity
