#!/usr/bin/perl -w

# keep the format of this next line the same to match regex in check-romsrc.pl
$origin = 0x9d80;
$hole_start = 0xb600;
$hole_size = 0x0200;

print <<EOT;
.sect .rodata
.global _rom_src_data
.global _rom_src_len

.align 7 ; for a 128-bit boundary
_rom_src_data:
EOT
my $size = 0;
my $a;
while (read STDIN,$a,1) {
	if ($origin+$size == $hole_start) {
		for($i = 0; $i < $hole_size; $i++) {
			print "\t.byte 0x00\n";
		}
		$size += $hole_size;
	}
	printf "\t.byte 0x%04x\n", ord($a);
	$size++;
}
print <<EOT;

	.align 2
_rom_src_len:
	.word $size
EOT
