#include "vend.h"
#include "chime.h"

volatile u8 chime_count;
volatile u8 unchime_count; /* silence counter */

void chime() {
	/* called from the RTI interrupt, sees if we need to turn the chime on or
	 * off (chime would be on for N RTI cycles)
	 */
	if (chime_count) {
		chime_on();
		--chime_count; /* interrupts masked here, so this won't result in badness */
	} else
		chime_off();
	if (unchime_count) --unchime_count;
}

