#!/usr/bin/perl -w

use POSIX qw(strftime);

$revision = `git describe --long`;
$datestring = strftime "%Y%m%d", localtime;

chomp $revision;


print <<EOT;
#ifndef _VERSION_H_
#define _VERSION_H_

/* ROM version */

#define DATEBUILT_STRING "$datestring"
#define VERSION_STRING "$revision"

#endif /* _VERSION_H_ */
EOT
