#ifndef _KEYPAD_H_
#define _KEYPAD_H_

#define KEY_1     1
#define KEY_2     2
#define KEY_3     3
#define KEY_4     4
#define KEY_5     5
#define KEY_6     6
#define KEY_7     7
#define KEY_8     8
#define KEY_9     9
#define KEY_0     10
#define KEY_RESET 11

extern volatile u8 last_key;

/* returns true if a key has been pressed since the last call */
bool keypad_pressed();
void keypad_read();
u8   keypad_getkey();
void keypad_init();

#endif /* _KEYPAD_H_ */
