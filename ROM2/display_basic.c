/*
 * display_basic.c - simple functions for writing to the screen of the vendie.
 *
 * Use set_msg(char[10]) to write a new message to the screen, replacing the
 * previous one.
 *
 */

#include "display_basic.h"
#include "vend.h"

/* private prototypes */
void display_send_byte(char c);
void display_reset();

void set_msg(char newmsg[11]) {
	int i;
	int prevchar = 0;

	//display_reset();
	spi_enable();
	for (i=0; i < 10; i++) {
		if (newmsg[i] == 0) break;
	}
	display_send_byte(0xaf); // reset ptr to start
	for (i--; i >= 0; i--) {
		if (prevchar && newmsg[i] == 0x2E) {
			display_send_byte(newmsg[i]&0x7f);
			continue;
		}

		// check if next character is a period
		if (newmsg[i] == 0x2E) {
			prevchar = newmsg[i];
			continue;
		}

		if (prevchar) {
			display_send_byte(newmsg[i]&0x7f);
			display_send_byte(prevchar&0x7f);
			prevchar = 0;
			continue;
		}

		if (newmsg[i] == 0) break;
		
		display_send_byte(newmsg[i]&0x7f);
	}
	
	if (prevchar) {
		display_send_byte(prevchar&0x7f);
	}
	spi_disable();
}

void display_send_byte(char c) {
	bset_misc_output(A3800_DISPLAY_WRITE);  /* enable the display clock */

	_io_ports[M6811_SPDR] = c;                  /* load SPI with byte */
	while(!(_io_ports[M6811_SPSR]&M6811_SPIF)); /* wait for completion */
	_io_ports[M6811_SPDR];                      /* SPDR read to clear SPIF flag */

	bclr_misc_output(A3800_DISPLAY_WRITE);  /* disable the display clock */
}

#define DISPLAY_DELAY  20 /* ms to delay between ops - could be tweaked */
void display_reset() {
	/* lower the reset line for a while */
	bclr((void*)&_io_ports[M6811_PORTA], PORTA_DISP_RESET);
	delay(DISPLAY_DELAY);
	bset((void*)&_io_ports[M6811_PORTA], PORTA_DISP_RESET);
	delay(DISPLAY_DELAY);

	spi_enable();

	display_send_byte(0xC0 | 10);    /* tell the controller there are 10 digits */
	//display_send_byte(0xE0);    /* set duty cycle to 0%                  */
	display_send_byte(0xFF);    /* set duty cycle to 100%                */

	spi_disable();
}

void display_init() {
	display_reset();
}

