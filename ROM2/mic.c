#include "mic.h"
#include "types.h"
#include "vend.h"
#include "xmodem.h"

void mic_set_secret(char newsecret[16]) {
	my_strncpy(mic_secret, newsecret, 16);
}

bool mic_verify(void* msg) {
	char *message, *p;
	message = (char*)msg;
	p = message;
	while (*p != '\0' && *p != '|') p++;
	if (*p == '\0') return 0;
	u16 crc1 = docrc((char*)&current_challenge, 2, 0, 0);
	crc1 = docrc(mic_secret, 16, 0, crc1);
	crc1 = docrc(message, p-message, 0, crc1);
	u16 crc2;
	p++;
	if (*(p) == '\0' ||
		*(p+1) == '\0' ||
		*(p+2) == '\0' ||
		*(p+3) == '\0' ||
		*(p+4) != '\0')
		return 0;
	crc2 = hex2u8(*p, *(p+1)) << 8;
	crc2 |= hex2u8(*(p+2), *(p+3));

	if (crc1 != crc2) return 0;

	current_challenge++;
	return 1;
}
