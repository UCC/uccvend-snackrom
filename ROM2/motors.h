#ifndef _MOTORS_H_
#define _MOTORS_H_

#include "vend.h"

/* error codes for dispense_motor */
#define MOTOR_SUCCESS   0
#define MOTOR_NOSLOT    1
#define MOTOR_HOME_FAIL 2
#define MOTOR_CURRENT_FAIL 3
#define MOTOR_VOLTAGE_FAIL 4

extern const u8 motor_lookup[80];

bool is_motor(u8 slot);
u8 dispense_motor(u8 slot);

#endif /* _MOTORS_H_ */
