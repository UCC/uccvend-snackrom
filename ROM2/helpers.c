#include "display_basic.h"
#include "vend.h"

void delay(u16 ms) {
	asm volatile ("ldx %0\n" :: "m" (ms) : "x");
	asm volatile (
		"delay_loop:\n"
		"	ldd #150\n"                   /* 3 */
		"delay_inner_loop:\n" /* 15 cycles each */
		"	cpd #0x0000\n"                /* 5 */
		"	beq delay_inner_loop_end\n"   /* 3 */
		"	subd #0x0001\n"               /* 4 */
		"	bra delay_inner_loop\n"       /* 3 */
		"delay_inner_loop_end:\n"
		"	dex\n"                        /* 3 */
		"	beq delay_out\n"              /* 3 */
		"	bra delay_loop\n"             /* 3 */
		"delay_out:\n" ::: "x", "d");
}

u8 my_strlen(char* s) {
	char *p = s;
	while (*p) p++;
	return p-s;
}

void my_strncpy(char* dst, char* src, u8 max_size) {
	u8 i;
	for (i = 0; src[i] && i < max_size; i++) dst[i] = src[i];
	if (src[i] == 0 && i < max_size) dst[i] = 0; /* null terminator */
}

bool my_strncmp(char* a, char* b, u8 len) {
	u8 i;
	for (i = 0; i < len; i++) {
		if (*a != *b) return 0;
		a++;
		b++;
	}
	return 1;
}

void my_memcpy(char* dst, char* src, u8 size) {
	u8 i = 0;
	for (i = 0; i < size; i++) dst[i] = src[i];
}

void my_memset(char* dst, u8 val, u16 count) {
	char* c;
	for (c = dst; c < dst+count; c++) *c = val;
}

u8 hexchar2u8(char b) {
	if (b >= '0' && b <= '9') return b-'0';
	if (b >= 'a' && b <= 'f') return b-'a'+0x0a;
	if (b >= 'A' && b <= 'F') return b-'A'+0x0a;
	return 0;
}

char nibble2hexchar(u8 b) {
	if (b <= 9) return b+'0';
	if (b >= 10 && b <= 15) return b+'A'-10;
	return 'X';
}

u8 hex2u8(char msb, char lsb) {
	return (hexchar2u8(msb) << 4) + hexchar2u8(lsb);
}

static char hexconv_buf[3];
char* u82hex(u8 a) {
	hexconv_buf[0] = nibble2hexchar((a&0xf0) >> 4);
	hexconv_buf[1] = nibble2hexchar(a&0x0f);
	hexconv_buf[2] = '\0';
	return hexconv_buf;
}

bool ishex(char b) {
	if (b >= '0' && b <= '9') return 1;
	if (b >= 'a' && b <= 'f') return 1;
	if (b >= 'A' && b <= 'F') return 1;
	return 0;
}

