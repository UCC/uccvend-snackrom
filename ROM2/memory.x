MEMORY
{ /* we squeeze both page0 and data into the internal 256 bytes of RAM */
  page0 (rwx) : ORIGIN = 0x0000, LENGTH = 0x0080
  data  (rw)  : ORIGIN = 0x0080, LENGTH = 0x0080
  text  (rx)  : ORIGIN = 0x8000, LENGTH = 0x8000
  eeprom(rwx) : ORIGIN = 0xb600, LENGTH = 0x0020
}

/* Setup the stack on the top of the data internal ram (not used).  */
PROVIDE (_stack = 0x007f);
