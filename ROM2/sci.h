#ifndef _SCI_H_
#define _SCI_H_

#include "vend.h"

#define BUFFER_LEN 20
#define CRLF "\r\n"

#define SERIAL_TIMEOUT -2

void sci_init();
void msg_clr();
void send_buffer(bool crlf);
void send_string(char* s);
void send_ack();
void send_nack();
int serial_readchar(u8 timeout);
void serial_write(const char *str, int len);
#define wait_for_tx_free() do { } while(0)

extern char sci_tx_buf[BUFFER_LEN];
extern volatile char sci_rx_buf[BUFFER_LEN];
extern volatile u8 sci_have_packet;
extern volatile bool sci_echo;
extern bool sci_doing_xmodem;

#endif /* _SCI_H_ */
