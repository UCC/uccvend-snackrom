#ifndef _CHIME_H_
#define _CHIME_H_

#include "types.h"
#include "vend.h"

#define CHIME_TIME 1 /* number of RTIs to have the chime on (6.6ms each) */

extern volatile u8 chime_count;
extern volatile u8 unchime_count;

/* outside world interface */
extern inline void chime_start() { chime_count = CHIME_TIME; }
extern inline void unchime_start() { unchime_count = CHIME_TIME; }
extern inline void chime_for(u8 time) { chime_count = time; }
extern inline void unchime_for(u8 time) { unchime_count = time; }

void chime(); /* RTI interrupt */

/* internal helpers, also could be called from outside world */
extern inline void chime_on() { bset((void*)&_io_ports[M6811_PORTA], PORTA_CHIME); }
extern inline void chime_off() { bclr((void*)&_io_ports[M6811_PORTA], PORTA_CHIME);}

#endif /* _CHIME_H_ */
