#ifndef _VEND_H_
#define _VEND_H_

#include "ports.h"
#include "types.h"
#include "asm.h"

/* addresses of these set at link time
 * to add more addresses, define them here with buffered_addr_h, in helpers.c 
 * with buffered_addr, and also in LDFLAGS in the Makefile
 */
#define buffered_addr(a) \
	extern volatile u8 _##a;          \
	volatile u8 a; \
	extern inline void set_##a(u8 b) { a = b; _##a = b; } \
	extern inline void bset_##a(const u8 m) { bset((void*)&a, m); _##a = a; } \
	extern inline void bclr_##a(const u8 m) { bclr((void*)&a, m); _##a = a; }

buffered_addr(changer_output);
buffered_addr(misc_output);

extern volatile u8 _switch_input;
#define switch_input _switch_input
extern volatile u8 _misc_input;
#define misc_input _misc_input
extern volatile u8 _home_sensors;
#define home_sensors _home_sensors

#define is_standalone() (misc_input & 0x01) /* DIP sw 1 */
#define must_verify()   (misc_input & 0x02) /* DIP sw 2 */
#define cant_poke()     (misc_input & 0x04) /* DIP sw 3 */

extern u16 _stack;
extern char _nvram[]; /* 2048 bytes of nvram! */
// uncomment for testing.
//char _nvram[20];
/* NVRAM map:
 *
 * START LEN
 * 0x800 0x02  mic.h - current challenge
 * 0x802 0x10  mic.h - current secret password
 * 0x812
 *
 */

/******* from helpers.c *******/
void delay(u16 ms);
void my_strncpy(char* dst, char* src, u8 max_size); /* for null-term strings */
bool my_strncmp(char* a, char* b, u8 len);
void my_memcpy(char* dst, char* src, u8 size);
void my_memset(char* dst, u8 val, u16 count);
char* u82hex(u8 a);
u8 hex2u8(char msb, char lsb);
char nibble2hexchar(u8 b);
u8 hexchar2u8(char b);
bool ishex(char b);

/******** Some meaningful bits ******/
#define PORTA_CHIME         0x10 /* chime is on when set */
#define PORTA_MOTOR_CLOCK   0x20
#define PORTA_MOTOR_COL_DISABLE 0x40
#define PORTA_DISP_RESET    0x80 /* active low */

#define PORTD_KEYPAD_ROW    0x20 /* clear for row 0, set for row 1 */

#define PORTE_MOTOR_NOT_OVERCURRENT 0x01
#define PORTE_MOTOR_OVERVOLTAGE 0x02

/* Address 1800 bits */
#define A1800_DOOR_OPEN     0x20

/* Address 3000 bits */
#define A3000_MOTOR_ROW_DISABLE 0x80

/* Address 3800 bits */
#define A3800_KEYPAD_STROBE 0x04
#define A3800_DISPLAY_WRITE  0x08
#define A3800_MOTOR_DATA        0x10
#define A3800_MOTOR_COL8_ENABLE 0x20
#define A3800_MOTOR_COL9_ENABLE 0x40

/******* from main.c *******/
int __attribute__((noreturn)) main (void);
void __attribute__((interrupt)) rti (void);
void __attribute__((interrupt)) sci_interrupt_serial (void);
void __attribute__((interrupt)) sci_interrupt_coinmech (void);
void __attribute__((interrupt)) uart_interrupt (void);

/* other one liners */
extern inline bool door_open() { return switch_input & A1800_DOOR_OPEN; }
extern inline void spi_enable()  { bset((void*)&_io_ports[M6811_SPCR], M6811_SPE); }
extern inline void spi_disable() { bclr((void*)&_io_ports[M6811_SPCR], M6811_SPE); }

#endif /* _VEND_H_ */
