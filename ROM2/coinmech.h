#ifndef _COINMECH_H_
#define _COINMECH_H_

#include "vend.h"

extern volatile u16 coin_value;
extern volatile bool have_change;

void coin_eat();
void coin_cost(u16 cost); /* specify the cost of an item. */
void coinmech_init();

#endif /* _COINMECH_H_ */
