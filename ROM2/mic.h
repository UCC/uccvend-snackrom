#ifndef _MIC_H_
#define _MIC_H_

#include "types.h"

#define current_challenge (*((u16*)(_nvram+0)))
#define mic_secret (((char*)_nvram)+2)

#define mic_challenge current_challenge
void mic_set_secret(char newsecret[16]);
bool mic_verify(void* message);

#endif /* _MIC_H_ */
