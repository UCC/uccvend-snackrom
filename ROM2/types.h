#ifndef _TYPES_H_
#define _TYPES_H_

typedef unsigned char    u8;
typedef signed char      s8;
typedef unsigned short   u16;
typedef signed short     s16;
typedef unsigned long    u32;
typedef signed long      s32;
typedef u8               bool;
typedef u16              addr_t;

#endif /* _TYPES_H_ */
