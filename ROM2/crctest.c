#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef unsigned short u16;
typedef unsigned char bool;
#include "crctab.h"

unsigned short
docrc (unsigned char *p, int len, bool pad, unsigned short crcstart)
{
  int len2 = len;
  unsigned short crc = crcstart;

  while (len-- > 0)
    crc = (crc << 8) ^ crctab[(crc >> 8) ^ *p++];
  if (pad && len2 < 128) {
    len = 128-len;
    while (len-- > 0)
      crc = (crc << 8) ^ crctab[(crc >> 8) ^ 0x1a];
  }

  return crc;
}

int main(int argc, char** argv) {
	unsigned short crc;
	unsigned short msgid;
	char *password, *message;
	if (argc != 4) {
		fprintf(stderr, "Usage: %s <id> <password> <message>\n", argv[0]);
		return 1;
	}
	msgid = strtol(argv[1], NULL, 16);
	password = argv[2];
	message = argv[3];
	crc = docrc(((char*)&msgid)+1, 1, 0, 0);
	crc = docrc(((char*)&msgid), 1, 0, crc);
	crc = docrc(password, strlen(password), 0, crc);
	crc = docrc(message, strlen(message), 0, crc);
	printf("%s|%04x\n", message, crc);
	return 0;
}
